Meteor.startup ->
  if Meteor.isClient
    if Config.defaultLanguage
      T9n.setLanguage Config.defaultLanguage
      TAPi18n.setLanguage Config.defaultLanguage
    else
      T9n.setLanguage('en')
      TAPi18n.setLanguage('en')