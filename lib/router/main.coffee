Router.map ->
  @route "dashboard",
    path: "/"
    waitOn: ->
      [
        subs.subscribe 'posts'
        subs.subscribe 'questions'
        subs.subscribe 'comments'
        subs.subscribe 'attachments'
      ]
    data: ->
      posts: Posts.find({},{sort: {createdAt: -1}}).fetch()
      questions: Questions.find({},{sort: {createdAt: -1}}).fetch()