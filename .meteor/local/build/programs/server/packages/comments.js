(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/comments/lib/both/collections.coffee.js                  //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var CommentsSchema;                                                  // 1
                                                                     //
this.Comments = new Meteor.Collection('comments');                   // 1
                                                                     //
CommentsSchema = new SimpleSchema({                                  // 1
  doc: {                                                             // 4
    type: String,                                                    // 5
    regEx: SimpleSchema.RegEx.Id                                     // 5
  },                                                                 //
  owner: {                                                           // 4
    type: String,                                                    // 9
    regEx: SimpleSchema.RegEx.Id,                                    // 9
    autoform: {                                                      // 9
      options: function() {                                          // 12
        return _.map(Meteor.users.find().fetch(), function(user) {   //
          return {                                                   //
            label: user.emails[0].address,                           // 14
            value: user._id                                          // 14
          };                                                         //
        });                                                          //
      }                                                              //
    }                                                                //
  },                                                                 //
  createdAt: {                                                       // 4
    type: Date,                                                      // 18
    autoValue: function() {                                          // 18
      if (this.isInsert) {                                           // 20
        return new Date();                                           //
      }                                                              //
    }                                                                //
  },                                                                 //
  content: {                                                         // 4
    type: String,                                                    // 24
    label: 'Comment'                                                 // 24
  }                                                                  //
});                                                                  //
                                                                     //
Comments.attachSchema(CommentsSchema);                               // 1
                                                                     //
///////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/comments/lib/server/allow.coffee.js                      //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Comments.allow({                                                     // 1
  insert: function(userId, doc) {                                    // 2
    return doc.owner === userId;                                     //
  }                                                                  //
});                                                                  //
                                                                     //
///////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/comments/lib/server/publish.coffee.js                    //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.publish('comments', function() {                              // 1
  return Comments.find();                                            //
});                                                                  // 1
                                                                     //
Meteor.publish('commentsByDoc', function(_id) {                      // 1
  return Comments.find({                                             //
    doc: _id                                                         // 5
  });                                                                //
});                                                                  // 4
                                                                     //
Meteor.publish('commentsByUser', function(_id) {                     // 1
  return Favorites.find({                                            //
    owner: _id                                                       // 8
  });                                                                //
});                                                                  // 7
                                                                     //
///////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.comments = {};

})();

//# sourceMappingURL=comments.js.map
