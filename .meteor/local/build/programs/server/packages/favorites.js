(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/favorites/lib/both/collections.coffee.js                 //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var FavoritesSchemas;                                                // 1
                                                                     //
this.Favorites = new Meteor.Collection('favorites');                 // 1
                                                                     //
FavoritesSchemas = new SimpleSchema({                                // 1
  doc: {                                                             // 4
    type: String,                                                    // 5
    regEx: SimpleSchema.RegEx.Id                                     // 5
  },                                                                 //
  owner: {                                                           // 4
    type: String,                                                    // 9
    autoValue: function() {                                          // 9
      if (this.isInsert) {                                           // 11
        return Meteor.userId();                                      //
      }                                                              //
    }                                                                //
  },                                                                 //
  createdAt: {                                                       // 4
    type: Date,                                                      // 15
    autoValue: function() {                                          // 15
      if (this.isInsert) {                                           // 17
        return new Date();                                           //
      }                                                              //
    }                                                                //
  }                                                                  //
});                                                                  //
                                                                     //
Favorites.attachSchema(FavoritesSchemas);                            // 1
                                                                     //
///////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/favorites/lib/server/allow.coffee.js                     //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Favorites.allow({                                                    // 1
  insert: function(userId, doc) {                                    // 2
    return doc.owner === userId;                                     //
  },                                                                 //
  remove: function(userId, doc) {                                    // 2
    return doc.owner === userId;                                     //
  }                                                                  //
});                                                                  //
                                                                     //
///////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/favorites/lib/server/publish.coffee.js                   //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.publish('myFavorites', function() {                           // 1
  return Favorites.find({                                            //
    owner: this.userId                                               // 2
  });                                                                //
});                                                                  // 1
                                                                     //
///////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.favorites = {};

})();

//# sourceMappingURL=favorites.js.map
