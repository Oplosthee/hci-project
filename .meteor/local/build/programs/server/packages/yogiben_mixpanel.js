(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;

(function(){

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/yogiben_mixpanel/server/mixpanel.js                                         //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
Meteor.startup(function() {
    if (Meteor.settings && Meteor.settings.public && Meteor.settings.public.mixpanel) {
        var mp = Meteor.settings.public.mixpanel;
        global.Mixpanel = Npm.require('mixpanel');
        global.mixpanel = Mixpanel.init(mp.token);
    }
})
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['yogiben:mixpanel'] = {};

})();
