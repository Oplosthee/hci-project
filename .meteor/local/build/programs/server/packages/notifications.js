(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Tracker = Package.tracker.Tracker;
var Deps = Package.tracker.Deps;
var Router = Package['iron:router'].Router;
var RouteController = Package['iron:router'].RouteController;
var moment = Package['mrt:moment'].moment;
var Iron = Package['iron:core'].Iron;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/notifications/lib/both/collections.coffee.js                                //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var NotificationsSchema;                                                                // 1
                                                                                        //
this.Notifications = new Meteor.Collection('notifications');                            // 1
                                                                                        //
Notifications["new"] = function(doc) {                                                  // 1
  if (typeof doc.owner === 'undefined') {                                               // 4
    doc.owner = Meteor.userId();                                                        // 5
  }                                                                                     //
  return Notifications.insert(doc);                                                     //
};                                                                                      // 3
                                                                                        //
Notifications.readAll = function() {                                                    // 1
  return Meteor.call('readAllNotifications');                                           //
};                                                                                      // 9
                                                                                        //
Notifications.read = function(_id) {                                                    // 1
  return Notifications.update(_id, {                                                    //
    $set: {                                                                             // 13
      read: true                                                                        // 13
    }                                                                                   //
  });                                                                                   //
};                                                                                      // 12
                                                                                        //
NotificationsSchema = new SimpleSchema({                                                // 1
  owner: {                                                                              // 16
    type: String,                                                                       // 17
    regEx: SimpleSchema.RegEx.Id                                                        // 17
  },                                                                                    //
  link: {                                                                               // 16
    type: String,                                                                       // 21
    optional: true                                                                      // 21
  },                                                                                    //
  title: {                                                                              // 16
    type: String                                                                        // 25
  },                                                                                    //
  read: {                                                                               // 16
    type: Boolean,                                                                      // 28
    defaultValue: false                                                                 // 28
  },                                                                                    //
  date: {                                                                               // 16
    type: Date,                                                                         // 32
    autoValue: function() {                                                             // 32
      if (this.isInsert) {                                                              // 34
        return new Date();                                                              //
      }                                                                                 //
    }                                                                                   //
  },                                                                                    //
  icon: {                                                                               // 16
    type: String,                                                                       // 38
    defaultValue: 'circle-o'                                                            // 38
  },                                                                                    //
  "class": {                                                                            // 16
    type: String,                                                                       // 42
    defaultValue: 'default'                                                             // 42
  }                                                                                     //
});                                                                                     //
                                                                                        //
Notifications.attachSchema(NotificationsSchema);                                        // 1
                                                                                        //
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/notifications/lib/both/router.coffee.js                                     //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Router.map(function() {                                                                 // 1
  this.route('notifications', {                                                         // 2
    path: '/notifications',                                                             // 3
    waitOn: function() {                                                                // 3
      return [Meteor.subscribe('notifications')];                                       //
    }                                                                                   //
  });                                                                                   //
  return this.route('messages', {                                                       //
    path: '/messages/:_id',                                                             // 9
    layout: 'notifications'                                                             // 9
  });                                                                                   //
});                                                                                     // 1
                                                                                        //
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/notifications/lib/server/allow.coffee.js                                    //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Notifications.allow({                                                                   // 1
  insert: function(userId, doc) {                                                       // 2
    return doc.owner === userId;                                                        //
  },                                                                                    //
  update: function(userId, doc, fieldNames, modifier) {                                 // 2
    return doc.owner === userId && fieldNames.length === 1 && fieldNames[0] === 'read';
  },                                                                                    //
  remove: function(userId, doc) {                                                       // 2
    return doc.owner === userId;                                                        //
  }                                                                                     //
});                                                                                     //
                                                                                        //
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/notifications/lib/server/publish.coffee.js                                  //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.publish('notifications', function() {                                            // 1
  return Notifications.find({                                                           //
    owner: this.userId                                                                  // 2
  });                                                                                   //
});                                                                                     // 1
                                                                                        //
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/notifications/lib/server/methods.coffee.js                                  //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.methods({                                                                        // 1
  readAllNotifications: function() {                                                    // 2
    return Notifications.update({                                                       //
      read: false                                                                       // 3
    }, {                                                                                //
      $set: {                                                                           // 3
        read: true                                                                      // 3
      }                                                                                 //
    }, {                                                                                //
      multi: true                                                                       // 3
    });                                                                                 //
  }                                                                                     //
});                                                                                     //
                                                                                        //
//////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.notifications = {};

})();

//# sourceMappingURL=notifications.js.map
