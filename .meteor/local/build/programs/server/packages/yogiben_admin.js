(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Router = Package['iron:router'].Router;
var RouteController = Package['iron:router'].RouteController;
var _ = Package.underscore._;
var ReactiveVar = Package['reactive-var'].ReactiveVar;
var check = Package.check.check;
var Match = Package.check.Match;
var Roles = Package['alanning:roles'].Roles;
var Helpers = Package['raix:handlebar-helpers'].Helpers;
var moment = Package['momentjs:moment'].moment;
var Tabular = Package['aldeed:tabular'].Tabular;
var ActiveRoute = Package['zimme:active-route'].ActiveRoute;
var Email = Package.email.Email;
var EmailInternals = Package.email.EmailInternals;
var Iron = Package['iron:core'].Iron;
var Collection2 = Package['aldeed:collection2-core'].Collection2;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;
var MongoInternals = Package.mongo.MongoInternals;
var Mongo = Package.mongo.Mongo;

/* Package-scope variables */
var __coffeescriptShare, AdminDashboard;

(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/both/AdminDashboard.coffee.js                                                            //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
                                                                                                                       // 1
                                                                                                                       //
AdminDashboard = {                                                                                                     // 1
  schemas: {},                                                                                                         // 2
  sidebarItems: [],                                                                                                    // 2
  collectionItems: [],                                                                                                 // 2
  alertSuccess: function(message) {                                                                                    // 2
    return Session.set('adminSuccess', message);                                                                       //
  },                                                                                                                   //
  alertFailure: function(message) {                                                                                    // 2
    return Session.set('adminError', message);                                                                         //
  },                                                                                                                   //
  checkAdmin: function() {                                                                                             // 2
    if (!Roles.userIsInRole(Meteor.userId(), ['admin'])) {                                                             // 11
      Meteor.call('adminCheckAdmin');                                                                                  // 12
      if (typeof (typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.nonAdminRedirectRoute : void 0) === "string") {
        Router.go(AdminConfig.nonAdminRedirectRoute);                                                                  // 14
      }                                                                                                                //
    }                                                                                                                  //
    if (typeof this.next === 'function') {                                                                             // 15
      return this.next();                                                                                              //
    }                                                                                                                  //
  },                                                                                                                   //
  adminRoutes: ['adminDashboard', 'adminDashboardUsersNew', 'adminDashboardUsersEdit', 'adminDashboardView', 'adminDashboardNew', 'adminDashboardEdit'],
  collectionLabel: function(collection) {                                                                              // 2
    if (collection === 'Users') {                                                                                      // 19
      return 'Users';                                                                                                  //
    } else if ((collection != null) && typeof AdminConfig.collections[collection].label === 'string') {                //
      return AdminConfig.collections[collection].label;                                                                //
    } else {                                                                                                           //
      return Session.get('admin_collection_name');                                                                     //
    }                                                                                                                  //
  },                                                                                                                   //
  addSidebarItem: function(title, url, options) {                                                                      // 2
    var item;                                                                                                          // 26
    item = {                                                                                                           // 26
      title: title                                                                                                     // 26
    };                                                                                                                 //
    if (_.isObject(url) && typeof options === 'undefined') {                                                           // 27
      item.options = url;                                                                                              // 28
    } else {                                                                                                           //
      item.url = url;                                                                                                  // 30
      item.options = options;                                                                                          // 30
    }                                                                                                                  //
    return this.sidebarItems.push(item);                                                                               //
  },                                                                                                                   //
  extendSidebarItem: function(title, urls) {                                                                           // 2
    var existing;                                                                                                      // 36
    if (_.isObject(urls)) {                                                                                            // 36
      urls = [urls];                                                                                                   // 36
    }                                                                                                                  //
    existing = _.find(this.sidebarItems, function(item) {                                                              // 36
      return item.title === title;                                                                                     //
    });                                                                                                                //
    if (existing) {                                                                                                    // 39
      return existing.options.urls = _.union(existing.options.urls, urls);                                             //
    }                                                                                                                  //
  },                                                                                                                   //
  addCollectionItem: function(fn) {                                                                                    // 2
    return this.collectionItems.push(fn);                                                                              //
  },                                                                                                                   //
  path: function(s) {                                                                                                  // 2
    var path;                                                                                                          // 46
    path = '/admin';                                                                                                   // 46
    if (typeof s === 'string' && s.length > 0) {                                                                       // 47
      path += (s[0] === '/' ? '' : '/') + s;                                                                           // 48
    }                                                                                                                  //
    return path;                                                                                                       //
  }                                                                                                                    //
};                                                                                                                     //
                                                                                                                       //
AdminDashboard.schemas.newUser = new SimpleSchema({                                                                    // 1
  email: {                                                                                                             // 53
    type: String,                                                                                                      // 54
    label: "Email address"                                                                                             // 54
  },                                                                                                                   //
  chooseOwnPassword: {                                                                                                 // 53
    type: Boolean,                                                                                                     // 57
    label: 'Let this user choose their own password with an email',                                                    // 57
    defaultValue: true                                                                                                 // 57
  },                                                                                                                   //
  password: {                                                                                                          // 53
    type: String,                                                                                                      // 61
    label: 'Password',                                                                                                 // 61
    optional: true                                                                                                     // 61
  },                                                                                                                   //
  sendPassword: {                                                                                                      // 53
    type: Boolean,                                                                                                     // 65
    label: 'Send this user their password by email',                                                                   // 65
    optional: true                                                                                                     // 65
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
AdminDashboard.schemas.sendResetPasswordEmail = new SimpleSchema({                                                     // 1
  _id: {                                                                                                               // 70
    type: String                                                                                                       // 71
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
AdminDashboard.schemas.changePassword = new SimpleSchema({                                                             // 1
  _id: {                                                                                                               // 74
    type: String                                                                                                       // 75
  },                                                                                                                   //
  password: {                                                                                                          // 74
    type: String                                                                                                       // 77
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/both/router.coffee.js                                                                    //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.AdminController = RouteController.extend({                                                                        // 1
  layoutTemplate: 'AdminLayout',                                                                                       // 2
  waitOn: function() {                                                                                                 // 2
    return [Meteor.subscribe('adminUsers'), Meteor.subscribe('adminUser'), Meteor.subscribe('adminCollectionsCount')];
  },                                                                                                                   //
  onBeforeAction: function() {                                                                                         // 2
    Session.set('adminSuccess', null);                                                                                 // 10
    Session.set('adminError', null);                                                                                   // 10
    Session.set('admin_title', '');                                                                                    // 10
    Session.set('admin_subtitle', '');                                                                                 // 10
    Session.set('admin_collection_page', null);                                                                        // 10
    Session.set('admin_collection_name', null);                                                                        // 10
    Session.set('admin_id', null);                                                                                     // 10
    Session.set('admin_doc', null);                                                                                    // 10
    if (!Roles.userIsInRole(Meteor.userId(), ['admin'])) {                                                             // 20
      Meteor.call('adminCheckAdmin');                                                                                  // 21
      if (typeof (typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.nonAdminRedirectRoute : void 0) === 'string') {
        Router.go(AdminConfig.nonAdminRedirectRoute);                                                                  // 23
      }                                                                                                                //
    }                                                                                                                  //
    return this.next();                                                                                                //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Router.route("adminDashboard", {                                                                                       // 1
  path: "/admin",                                                                                                      // 29
  template: "AdminDashboard",                                                                                          // 29
  controller: "AdminController",                                                                                       // 29
  action: function() {                                                                                                 // 29
    return this.render();                                                                                              //
  },                                                                                                                   //
  onAfterAction: function() {                                                                                          // 29
    Session.set('admin_title', 'Dashboard');                                                                           // 35
    Session.set('admin_collection_name', '');                                                                          // 35
    return Session.set('admin_collection_page', '');                                                                   //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Router.route("adminDashboardUsersView", {                                                                              // 1
  path: "/admin/Users",                                                                                                // 40
  template: "AdminDashboardView",                                                                                      // 40
  controller: "AdminController",                                                                                       // 40
  action: function() {                                                                                                 // 40
    return this.render();                                                                                              //
  },                                                                                                                   //
  data: function() {                                                                                                   // 40
    return {                                                                                                           //
      admin_table: AdminTables.Users                                                                                   // 46
    };                                                                                                                 //
  },                                                                                                                   //
  onAfterAction: function() {                                                                                          // 40
    Session.set('admin_title', 'Users');                                                                               // 48
    Session.set('admin_subtitle', 'View');                                                                             // 48
    return Session.set('admin_collection_name', 'Users');                                                              //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Router.route("adminDashboardUsersNew", {                                                                               // 1
  path: "/admin/Users/new",                                                                                            // 53
  template: "AdminDashboardUsersNew",                                                                                  // 53
  controller: 'AdminController',                                                                                       // 53
  action: function() {                                                                                                 // 53
    return this.render();                                                                                              //
  },                                                                                                                   //
  onAfterAction: function() {                                                                                          // 53
    Session.set('admin_title', 'Users');                                                                               // 59
    Session.set('admin_subtitle', 'Create new user');                                                                  // 59
    Session.set('admin_collection_page', 'New');                                                                       // 59
    return Session.set('admin_collection_name', 'Users');                                                              //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Router.route("adminDashboardUsersEdit", {                                                                              // 1
  path: "/admin/Users/:_id/edit",                                                                                      // 65
  template: "AdminDashboardUsersEdit",                                                                                 // 65
  controller: "AdminController",                                                                                       // 65
  data: function() {                                                                                                   // 65
    return {                                                                                                           //
      user: Meteor.users.find(this.params._id).fetch(),                                                                // 69
      roles: Roles.getRolesForUser(this.params._id),                                                                   // 69
      otherRoles: _.difference(_.map(Meteor.roles.find().fetch(), function(role) {                                     // 69
        return role.name;                                                                                              //
      }), Roles.getRolesForUser(this.params._id))                                                                      //
    };                                                                                                                 //
  },                                                                                                                   //
  action: function() {                                                                                                 // 65
    return this.render();                                                                                              //
  },                                                                                                                   //
  onAfterAction: function() {                                                                                          // 65
    Session.set('admin_title', 'Users');                                                                               // 75
    Session.set('admin_subtitle', 'Edit user ' + this.params._id);                                                     // 75
    Session.set('admin_collection_page', 'edit');                                                                      // 75
    Session.set('admin_collection_name', 'Users');                                                                     // 75
    Session.set('admin_id', this.params._id);                                                                          // 75
    return Session.set('admin_doc', Meteor.users.findOne({                                                             //
      _id: this.params._id                                                                                             // 80
    }));                                                                                                               //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/both/utils.coffee.js                                                                     //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.adminCollectionObject = function(collection) {                                                                    // 1
  if (typeof AdminConfig.collections[collection] !== 'undefined' && typeof AdminConfig.collections[collection].collectionObject !== 'undefined') {
    return AdminConfig.collections[collection].collectionObject;                                                       //
  } else {                                                                                                             //
    return lookup(collection);                                                                                         //
  }                                                                                                                    //
};                                                                                                                     // 1
                                                                                                                       //
this.adminCallback = function(name, args, callback) {                                                                  // 1
  var ref1, ref2, stop;                                                                                                // 8
  stop = false;                                                                                                        // 8
  if (typeof (typeof AdminConfig !== "undefined" && AdminConfig !== null ? (ref1 = AdminConfig.callbacks) != null ? ref1[name] : void 0 : void 0) === 'function') {
    stop = (ref2 = AdminConfig.callbacks)[name].apply(ref2, args) === false;                                           // 10
  }                                                                                                                    //
  if (typeof callback === 'function') {                                                                                // 11
    if (!stop) {                                                                                                       // 12
      return callback.apply(null, args);                                                                               //
    }                                                                                                                  //
  }                                                                                                                    //
};                                                                                                                     // 7
                                                                                                                       //
this.lookup = function(obj, root, required) {                                                                          // 1
  var arr, ref;                                                                                                        // 15
  if (required == null) {                                                                                              //
    required = true;                                                                                                   //
  }                                                                                                                    //
  if (typeof root === 'undefined') {                                                                                   // 15
    root = Meteor.isServer ? global : window;                                                                          // 16
  }                                                                                                                    //
  if (typeof obj === 'string') {                                                                                       // 17
    ref = root;                                                                                                        // 18
    arr = obj.split('.');                                                                                              // 18
    while (arr.length && (ref = ref[arr.shift()])) {                                                                   // 20
      continue;                                                                                                        // 20
    }                                                                                                                  //
    if (!ref && required) {                                                                                            // 21
      throw new Error(obj + ' is not in the ' + root.toString());                                                      // 22
    } else {                                                                                                           //
      return ref;                                                                                                      // 24
    }                                                                                                                  //
  }                                                                                                                    //
  return obj;                                                                                                          // 25
};                                                                                                                     // 14
                                                                                                                       //
this.parseID = function(id) {                                                                                          // 1
  if (typeof id === 'string') {                                                                                        // 28
    if (id.indexOf("ObjectID") > -1) {                                                                                 // 29
      return new Mongo.ObjectID(id.slice(id.indexOf('"') + 1, id.lastIndexOf('"')));                                   // 30
    } else {                                                                                                           //
      return id;                                                                                                       // 32
    }                                                                                                                  //
  } else {                                                                                                             //
    return id;                                                                                                         // 34
  }                                                                                                                    //
};                                                                                                                     // 27
                                                                                                                       //
this.parseIDs = function(ids) {                                                                                        // 1
  return _.map(ids, function(id) {                                                                                     // 37
    return parseID(id);                                                                                                //
  });                                                                                                                  //
};                                                                                                                     // 36
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/both/startup.coffee.js                                                                   //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var adminCreateRouteEdit, adminCreateRouteEditOptions, adminCreateRouteNew, adminCreateRouteNewOptions, adminCreateRouteView, adminCreateRouteViewOptions, adminCreateRoutes, adminCreateTables, adminDelButton, adminEditButton, adminEditDelButtons, adminPublishTables, adminTablePubName, adminTablesDom, defaultColumns;
                                                                                                                       //
this.AdminTables = {};                                                                                                 // 1
                                                                                                                       //
adminTablesDom = '<"box"<"box-header"<"box-toolbar"<"pull-left"<lf>><"pull-right"p>>><"box-body"t>>';                  // 1
                                                                                                                       //
adminEditButton = {                                                                                                    // 1
  data: '_id',                                                                                                         // 5
  title: 'Edit',                                                                                                       // 5
  createdCell: function(node, cellData, rowData) {                                                                     // 5
    return $(node).html(Blaze.toHTMLWithData(Template.adminEditBtn, {                                                  //
      _id: cellData                                                                                                    // 9
    }));                                                                                                               //
  },                                                                                                                   //
  width: '40px',                                                                                                       // 5
  orderable: false                                                                                                     // 5
};                                                                                                                     //
                                                                                                                       //
adminDelButton = {                                                                                                     // 1
  data: '_id',                                                                                                         // 13
  title: 'Delete',                                                                                                     // 13
  createdCell: function(node, cellData, rowData) {                                                                     // 13
    return $(node).html(Blaze.toHTMLWithData(Template.adminDeleteBtn, {                                                //
      _id: cellData                                                                                                    // 17
    }));                                                                                                               //
  },                                                                                                                   //
  width: '40px',                                                                                                       // 13
  orderable: false                                                                                                     // 13
};                                                                                                                     //
                                                                                                                       //
adminEditDelButtons = [adminEditButton, adminDelButton];                                                               // 1
                                                                                                                       //
defaultColumns = function() {                                                                                          // 1
  return [                                                                                                             //
    {                                                                                                                  //
      data: '_id',                                                                                                     // 28
      title: 'ID'                                                                                                      // 28
    }                                                                                                                  //
  ];                                                                                                                   //
};                                                                                                                     // 27
                                                                                                                       //
adminTablePubName = function(collection) {                                                                             // 1
  return "admin_tabular_" + collection;                                                                                //
};                                                                                                                     // 32
                                                                                                                       //
adminCreateTables = function(collections) {                                                                            // 1
  return _.each(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : void 0, function(collection, name) {
    var columns;                                                                                                       // 37
    _.defaults(collection, {                                                                                           // 37
      showEditColumn: true,                                                                                            // 37
      showDelColumn: true,                                                                                             // 37
      showInSideBar: true                                                                                              // 37
    });                                                                                                                //
    columns = _.map(collection.tableColumns, function(column) {                                                        // 37
      var createdCell;                                                                                                 // 44
      if (column.template) {                                                                                           // 44
        createdCell = function(node, cellData, rowData) {                                                              // 45
          $(node).html('');                                                                                            // 46
          return Blaze.renderWithData(Template[column.template], {                                                     //
            value: cellData,                                                                                           // 47
            doc: rowData                                                                                               // 47
          }, node);                                                                                                    //
        };                                                                                                             //
      }                                                                                                                //
      return {                                                                                                         //
        data: column.name,                                                                                             // 49
        title: column.label,                                                                                           // 49
        createdCell: createdCell                                                                                       // 49
      };                                                                                                               //
    });                                                                                                                //
    if (columns.length === 0) {                                                                                        // 53
      columns = defaultColumns();                                                                                      // 54
    }                                                                                                                  //
    if (collection.showEditColumn) {                                                                                   // 56
      columns.push(adminEditButton);                                                                                   // 57
    }                                                                                                                  //
    if (collection.showDelColumn) {                                                                                    // 58
      columns.push(adminDelButton);                                                                                    // 59
    }                                                                                                                  //
    return AdminTables[name] = new Tabular.Table({                                                                     //
      name: name,                                                                                                      // 62
      collection: adminCollectionObject(name),                                                                         // 62
      pub: collection.children && adminTablePubName(name),                                                             // 62
      sub: collection.sub,                                                                                             // 62
      columns: columns,                                                                                                // 62
      extraFields: collection.extraFields,                                                                             // 62
      dom: adminTablesDom                                                                                              // 62
    });                                                                                                                //
  });                                                                                                                  //
};                                                                                                                     // 35
                                                                                                                       //
adminCreateRoutes = function(collections) {                                                                            // 1
  _.each(collections, adminCreateRouteView);                                                                           // 71
  _.each(collections, adminCreateRouteNew);                                                                            // 71
  return _.each(collections, adminCreateRouteEdit);                                                                    //
};                                                                                                                     // 70
                                                                                                                       //
adminCreateRouteView = function(collection, collectionName) {                                                          // 1
  return Router.route("adminDashboard" + collectionName + "View", adminCreateRouteViewOptions(collection, collectionName));
};                                                                                                                     // 75
                                                                                                                       //
adminCreateRouteViewOptions = function(collection, collectionName) {                                                   // 1
  var options, ref;                                                                                                    // 80
  options = {                                                                                                          // 80
    path: "/admin/" + collectionName,                                                                                  // 81
    template: "AdminDashboardViewWrapper",                                                                             // 81
    controller: "AdminController",                                                                                     // 81
    data: function() {                                                                                                 // 81
      return {                                                                                                         //
        admin_table: AdminTables[collectionName]                                                                       // 85
      };                                                                                                               //
    },                                                                                                                 //
    action: function() {                                                                                               // 81
      return this.render();                                                                                            //
    },                                                                                                                 //
    onAfterAction: function() {                                                                                        // 81
      var ref, ref1;                                                                                                   // 89
      Session.set('admin_title', collectionName);                                                                      // 89
      Session.set('admin_subtitle', 'View');                                                                           // 89
      Session.set('admin_collection_name', collectionName);                                                            // 89
      return (ref = collection.routes) != null ? (ref1 = ref.view) != null ? ref1.onAfterAction : void 0 : void 0;     //
    }                                                                                                                  //
  };                                                                                                                   //
  return _.defaults(options, (ref = collection.routes) != null ? ref.view : void 0);                                   //
};                                                                                                                     // 79
                                                                                                                       //
adminCreateRouteNew = function(collection, collectionName) {                                                           // 1
  return Router.route("adminDashboard" + collectionName + "New", adminCreateRouteNewOptions(collection, collectionName));
};                                                                                                                     // 95
                                                                                                                       //
adminCreateRouteNewOptions = function(collection, collectionName) {                                                    // 1
  var options, ref;                                                                                                    // 100
  options = {                                                                                                          // 100
    path: "/admin/" + collectionName + "/new",                                                                         // 101
    template: "AdminDashboardNew",                                                                                     // 101
    controller: "AdminController",                                                                                     // 101
    action: function() {                                                                                               // 101
      return this.render();                                                                                            //
    },                                                                                                                 //
    onAfterAction: function() {                                                                                        // 101
      var ref, ref1;                                                                                                   // 107
      Session.set('admin_title', AdminDashboard.collectionLabel(collectionName));                                      // 107
      Session.set('admin_subtitle', 'Create new');                                                                     // 107
      Session.set('admin_collection_page', 'new');                                                                     // 107
      Session.set('admin_collection_name', collectionName);                                                            // 107
      return (ref = collection.routes) != null ? (ref1 = ref["new"]) != null ? ref1.onAfterAction : void 0 : void 0;   //
    },                                                                                                                 //
    data: function() {                                                                                                 // 101
      return {                                                                                                         //
        admin_collection: adminCollectionObject(collectionName)                                                        // 113
      };                                                                                                               //
    }                                                                                                                  //
  };                                                                                                                   //
  return _.defaults(options, (ref = collection.routes) != null ? ref["new"] : void 0);                                 //
};                                                                                                                     // 99
                                                                                                                       //
adminCreateRouteEdit = function(collection, collectionName) {                                                          // 1
  return Router.route("adminDashboard" + collectionName + "Edit", adminCreateRouteEditOptions(collection, collectionName));
};                                                                                                                     // 116
                                                                                                                       //
adminCreateRouteEditOptions = function(collection, collectionName) {                                                   // 1
  var options, ref;                                                                                                    // 121
  options = {                                                                                                          // 121
    path: "/admin/" + collectionName + "/:_id/edit",                                                                   // 122
    template: "AdminDashboardEdit",                                                                                    // 122
    controller: "AdminController",                                                                                     // 122
    waitOn: function() {                                                                                               // 122
      var ref, ref1;                                                                                                   // 126
      Meteor.subscribe('adminCollectionDoc', collectionName, parseID(this.params._id));                                // 126
      return (ref = collection.routes) != null ? (ref1 = ref.edit) != null ? ref1.waitOn : void 0 : void 0;            //
    },                                                                                                                 //
    action: function() {                                                                                               // 122
      return this.render();                                                                                            //
    },                                                                                                                 //
    onAfterAction: function() {                                                                                        // 122
      var ref, ref1;                                                                                                   // 131
      Session.set('admin_title', AdminDashboard.collectionLabel(collectionName));                                      // 131
      Session.set('admin_subtitle', 'Edit ' + this.params._id);                                                        // 131
      Session.set('admin_collection_page', 'edit');                                                                    // 131
      Session.set('admin_collection_name', collectionName);                                                            // 131
      Session.set('admin_id', parseID(this.params._id));                                                               // 131
      Session.set('admin_doc', adminCollectionObject(collectionName).findOne({                                         // 131
        _id: parseID(this.params._id)                                                                                  // 136
      }));                                                                                                             //
      return (ref = collection.routes) != null ? (ref1 = ref.edit) != null ? ref1.onAfterAction : void 0 : void 0;     //
    },                                                                                                                 //
    data: function() {                                                                                                 // 122
      return {                                                                                                         //
        admin_collection: adminCollectionObject(collectionName)                                                        // 139
      };                                                                                                               //
    }                                                                                                                  //
  };                                                                                                                   //
  return _.defaults(options, (ref = collection.routes) != null ? ref.edit : void 0);                                   //
};                                                                                                                     // 120
                                                                                                                       //
adminPublishTables = function(collections) {                                                                           // 1
  return _.each(collections, function(collection, name) {                                                              //
    if (!collection.children) {                                                                                        // 144
      return void 0;                                                                                                   // 144
    }                                                                                                                  //
    return Meteor.publishComposite(adminTablePubName(name), function(tableName, ids, fields) {                         //
      var extraFields;                                                                                                 // 146
      check(tableName, String);                                                                                        // 146
      check(ids, Array);                                                                                               // 146
      check(fields, Match.Optional(Object));                                                                           // 146
      extraFields = _.reduce(collection.extraFields, function(fields, name) {                                          // 146
        fields[name] = 1;                                                                                              // 151
        return fields;                                                                                                 //
      }, {});                                                                                                          //
      _.extend(fields, extraFields);                                                                                   // 146
      this.unblock();                                                                                                  // 146
      return {                                                                                                         //
        find: function() {                                                                                             // 158
          this.unblock();                                                                                              // 159
          return adminCollectionObject(name).find({                                                                    //
            _id: {                                                                                                     // 160
              $in: ids                                                                                                 // 160
            }                                                                                                          //
          }, {                                                                                                         //
            fields: fields                                                                                             // 160
          });                                                                                                          //
        },                                                                                                             //
        children: collection.children                                                                                  // 158
      };                                                                                                               //
    });                                                                                                                //
  });                                                                                                                  //
};                                                                                                                     // 142
                                                                                                                       //
Meteor.startup(function() {                                                                                            // 1
  adminCreateTables(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : void 0);    // 164
  adminCreateRoutes(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : void 0);    // 164
  if (Meteor.isServer) {                                                                                               // 166
    adminPublishTables(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : void 0);
  }                                                                                                                    //
  if (AdminTables.Users) {                                                                                             // 168
    return void 0;                                                                                                     // 168
  }                                                                                                                    //
  return AdminTables.Users = new Tabular.Table({                                                                       //
    changeSelector: function(selector, userId) {                                                                       // 172
      var $or;                                                                                                         // 173
      $or = selector['$or'];                                                                                           // 173
      $or && (selector['$or'] = _.map($or, function(exp) {                                                             // 173
        var ref;                                                                                                       // 175
        if (((ref = exp.emails) != null ? ref['$regex'] : void 0) != null) {                                           // 175
          return {                                                                                                     //
            emails: {                                                                                                  // 176
              $elemMatch: {                                                                                            // 176
                address: exp.emails                                                                                    // 176
              }                                                                                                        //
            }                                                                                                          //
          };                                                                                                           //
        } else {                                                                                                       //
          return exp;                                                                                                  //
        }                                                                                                              //
      }));                                                                                                             //
      return selector;                                                                                                 //
    },                                                                                                                 //
    name: 'Users',                                                                                                     // 172
    collection: Meteor.users,                                                                                          // 172
    columns: _.union([                                                                                                 // 172
      {                                                                                                                //
        data: '_id',                                                                                                   // 184
        title: 'Admin',                                                                                                // 184
        createdCell: function(node, cellData, rowData) {                                                               // 184
          return $(node).html(Blaze.toHTMLWithData(Template.adminUsersIsAdmin, {                                       //
            _id: cellData                                                                                              // 189
          }));                                                                                                         //
        },                                                                                                             //
        width: '40px'                                                                                                  // 184
      }, {                                                                                                             //
        data: 'emails',                                                                                                // 192
        title: 'Email',                                                                                                // 192
        render: function(value) {                                                                                      // 192
          return value[0].address;                                                                                     //
        },                                                                                                             //
        searchable: true                                                                                               // 192
      }, {                                                                                                             //
        data: 'emails',                                                                                                // 199
        title: 'Mail',                                                                                                 // 199
        createdCell: function(node, cellData, rowData) {                                                               // 199
          return $(node).html(Blaze.toHTMLWithData(Template.adminUsersMailBtn, {                                       //
            emails: cellData                                                                                           // 204
          }));                                                                                                         //
        },                                                                                                             //
        width: '40px'                                                                                                  // 199
      }, {                                                                                                             //
        data: 'createdAt',                                                                                             // 207
        title: 'Joined'                                                                                                // 207
      }                                                                                                                //
    ], adminEditDelButtons),                                                                                           //
    dom: adminTablesDom                                                                                                // 172
  });                                                                                                                  //
});                                                                                                                    // 163
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/both/collections.coffee.js                                                               //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.AdminCollectionsCount = new Mongo.Collection('adminCollectionsCount');                                            // 1
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/server/publish.coffee.js                                                                 //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.publishComposite('adminCollectionDoc', function(collection, id) {                                               // 1
  var ref, ref1;                                                                                                       // 2
  check(collection, String);                                                                                           // 2
  check(id, Match.OneOf(String, Mongo.ObjectID));                                                                      // 2
  if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                    // 4
    return {                                                                                                           //
      find: function() {                                                                                               // 5
        return adminCollectionObject(collection).find(id);                                                             //
      },                                                                                                               //
      children: (typeof AdminConfig !== "undefined" && AdminConfig !== null ? (ref = AdminConfig.collections) != null ? (ref1 = ref[collection]) != null ? ref1.children : void 0 : void 0 : void 0) || []
    };                                                                                                                 //
  } else {                                                                                                             //
    return this.ready();                                                                                               //
  }                                                                                                                    //
});                                                                                                                    // 1
                                                                                                                       //
Meteor.publish('adminUsers', function() {                                                                              // 1
  if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                    // 12
    return Meteor.users.find();                                                                                        //
  } else {                                                                                                             //
    return this.ready();                                                                                               //
  }                                                                                                                    //
});                                                                                                                    // 11
                                                                                                                       //
Meteor.publish('adminUser', function() {                                                                               // 1
  return Meteor.users.find(this.userId);                                                                               //
});                                                                                                                    // 17
                                                                                                                       //
Meteor.publish('adminCollectionsCount', function() {                                                                   // 1
  var handles, self;                                                                                                   // 21
  handles = [];                                                                                                        // 21
  self = this;                                                                                                         // 21
  _.each(AdminTables, function(table, name) {                                                                          // 21
    var count, id, ready;                                                                                              // 25
    id = new Mongo.ObjectID;                                                                                           // 25
    count = 0;                                                                                                         // 25
    ready = false;                                                                                                     // 25
    handles.push(table.collection.find().observeChanges({                                                              // 25
      added: function() {                                                                                              // 30
        count += 1;                                                                                                    // 31
        return ready && self.changed('adminCollectionsCount', id, {                                                    //
          count: count                                                                                                 // 32
        });                                                                                                            //
      },                                                                                                               //
      removed: function() {                                                                                            // 30
        count -= 1;                                                                                                    // 34
        return ready && self.changed('adminCollectionsCount', id, {                                                    //
          count: count                                                                                                 // 35
        });                                                                                                            //
      }                                                                                                                //
    }));                                                                                                               //
    ready = true;                                                                                                      // 25
    return self.added('adminCollectionsCount', id, {                                                                   //
      collection: name,                                                                                                // 38
      count: count                                                                                                     // 38
    });                                                                                                                //
  });                                                                                                                  //
  self.onStop(function() {                                                                                             // 21
    return _.each(handles, function(handle) {                                                                          //
      return handle.stop();                                                                                            //
    });                                                                                                                //
  });                                                                                                                  //
  return self.ready();                                                                                                 //
});                                                                                                                    // 20
                                                                                                                       //
Meteor.publish(null, function() {                                                                                      // 1
  return Meteor.roles.find({});                                                                                        //
});                                                                                                                    // 44
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/server/methods.coffee.js                                                                 //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.methods({                                                                                                       // 1
  adminInsertDoc: function(doc, collection) {                                                                          // 2
    var result;                                                                                                        // 3
    check(arguments, [Match.Any]);                                                                                     // 3
    if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                  // 4
      this.unblock();                                                                                                  // 5
      result = adminCollectionObject(collection).insert(doc);                                                          // 5
      return result;                                                                                                   // 8
    }                                                                                                                  //
  },                                                                                                                   //
  adminUpdateDoc: function(modifier, collection, _id) {                                                                // 2
    var result;                                                                                                        // 11
    check(arguments, [Match.Any]);                                                                                     // 11
    if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                  // 12
      this.unblock();                                                                                                  // 13
      result = adminCollectionObject(collection).update({                                                              // 13
        _id: _id                                                                                                       // 14
      }, modifier);                                                                                                    //
      return result;                                                                                                   // 15
    }                                                                                                                  //
  },                                                                                                                   //
  adminRemoveDoc: function(collection, _id) {                                                                          // 2
    check(arguments, [Match.Any]);                                                                                     // 18
    if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                  // 19
      if (collection === 'Users') {                                                                                    // 20
        return Meteor.users.remove({                                                                                   //
          _id: _id                                                                                                     // 21
        });                                                                                                            //
      } else {                                                                                                         //
        return adminCollectionObject(collection).remove({                                                              //
          _id: _id                                                                                                     // 24
        });                                                                                                            //
      }                                                                                                                //
    }                                                                                                                  //
  },                                                                                                                   //
  adminNewUser: function(doc) {                                                                                        // 2
    var emails;                                                                                                        // 28
    check(arguments, [Match.Any]);                                                                                     // 28
    if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                  // 29
      emails = doc.email.split(',');                                                                                   // 30
      return _.each(emails, function(email) {                                                                          //
        var _id, user;                                                                                                 // 32
        user = {};                                                                                                     // 32
        user.email = email;                                                                                            // 32
        if (!doc.chooseOwnPassword) {                                                                                  // 34
          user.password = doc.password;                                                                                // 35
        }                                                                                                              //
        _id = Accounts.createUser(user);                                                                               // 32
        if (doc.sendPassword && (AdminConfig.fromEmail != null)) {                                                     // 39
          Email.send({                                                                                                 // 40
            to: user.email,                                                                                            // 41
            from: AdminConfig.fromEmail,                                                                               // 41
            subject: 'Your account has been created',                                                                  // 41
            html: 'You\'ve just had an account created for ' + Meteor.absoluteUrl() + ' with password ' + doc.password
          });                                                                                                          //
        }                                                                                                              //
        if (!doc.sendPassword) {                                                                                       // 46
          return Accounts.sendEnrollmentEmail(_id);                                                                    //
        }                                                                                                              //
      });                                                                                                              //
    }                                                                                                                  //
  },                                                                                                                   //
  adminUpdateUser: function(modifier, _id) {                                                                           // 2
    var result;                                                                                                        // 50
    check(arguments, [Match.Any]);                                                                                     // 50
    if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                  // 51
      this.unblock();                                                                                                  // 52
      result = Meteor.users.update({                                                                                   // 52
        _id: _id                                                                                                       // 53
      }, modifier);                                                                                                    //
      return result;                                                                                                   // 54
    }                                                                                                                  //
  },                                                                                                                   //
  adminSendResetPasswordEmail: function(doc) {                                                                         // 2
    check(arguments, [Match.Any]);                                                                                     // 57
    if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                  // 58
      console.log('Changing password for user ' + doc._id);                                                            // 59
      return Accounts.sendResetPasswordEmail(doc._id);                                                                 //
    }                                                                                                                  //
  },                                                                                                                   //
  adminChangePassword: function(doc) {                                                                                 // 2
    check(arguments, [Match.Any]);                                                                                     // 63
    if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                  // 64
      console.log('Changing password for user ' + doc._id);                                                            // 65
      Accounts.setPassword(doc._id, doc.password);                                                                     // 65
      return {                                                                                                         //
        label: 'Email user their new password'                                                                         // 67
      };                                                                                                               //
    }                                                                                                                  //
  },                                                                                                                   //
  adminCheckAdmin: function() {                                                                                        // 2
    var adminEmails, email, user;                                                                                      // 70
    check(arguments, [Match.Any]);                                                                                     // 70
    user = Meteor.users.findOne({                                                                                      // 70
      _id: this.userId                                                                                                 // 71
    });                                                                                                                //
    if (this.userId && !Roles.userIsInRole(this.userId, ['admin']) && (user.emails.length > 0)) {                      // 72
      email = user.emails[0].address;                                                                                  // 73
      if (typeof Meteor.settings.adminEmails !== 'undefined') {                                                        // 74
        adminEmails = Meteor.settings.adminEmails;                                                                     // 75
        if (adminEmails.indexOf(email) > -1) {                                                                         // 76
          console.log('Adding admin user: ' + email);                                                                  // 77
          return Roles.addUsersToRoles(this.userId, ['admin'], Roles.GLOBAL_GROUP);                                    //
        }                                                                                                              //
      } else if (typeof AdminConfig !== 'undefined' && typeof AdminConfig.adminEmails === 'object') {                  //
        adminEmails = AdminConfig.adminEmails;                                                                         // 80
        if (adminEmails.indexOf(email) > -1) {                                                                         // 81
          console.log('Adding admin user: ' + email);                                                                  // 82
          return Roles.addUsersToRoles(this.userId, ['admin'], Roles.GLOBAL_GROUP);                                    //
        }                                                                                                              //
      } else if (this.userId === Meteor.users.findOne({}, {                                                            //
        sort: {                                                                                                        // 84
          createdAt: 1                                                                                                 // 84
        }                                                                                                              //
      })._id) {                                                                                                        //
        console.log('Making first user admin: ' + email);                                                              // 85
        return Roles.addUsersToRoles(this.userId, ['admin']);                                                          //
      }                                                                                                                //
    }                                                                                                                  //
  },                                                                                                                   //
  adminAddUserToRole: function(_id, role) {                                                                            // 2
    check(arguments, [Match.Any]);                                                                                     // 89
    if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                  // 90
      return Roles.addUsersToRoles(_id, role, Roles.GLOBAL_GROUP);                                                     //
    }                                                                                                                  //
  },                                                                                                                   //
  adminRemoveUserToRole: function(_id, role) {                                                                         // 2
    check(arguments, [Match.Any]);                                                                                     // 94
    if (Roles.userIsInRole(this.userId, ['admin'])) {                                                                  // 95
      return Roles.removeUsersFromRoles(_id, role, Roles.GLOBAL_GROUP);                                                //
    }                                                                                                                  //
  },                                                                                                                   //
  adminSetCollectionSort: function(collection, _sort) {                                                                // 2
    check(arguments, [Match.Any]);                                                                                     // 99
    return global.AdminPages[collection].set({                                                                         //
      sort: _sort                                                                                                      // 101
    });                                                                                                                //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
(function (pkg, symbols) {
  for (var s in symbols)
    (s in pkg) || (pkg[s] = symbols[s]);
})(Package['yogiben:admin'] = {}, {
  AdminDashboard: AdminDashboard
});

})();

//# sourceMappingURL=yogiben_admin.js.map
