(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/router/accounts.coffee.js                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Router.map(function() {                                                // 1
  this.route("profile", {                                              // 2
    path: "/profile"                                                   // 3
  });                                                                  //
  this.route("account", {                                              // 2
    path: "/account"                                                   // 6
  });                                                                  //
  this.route("setUserName", {                                          // 2
    path: "/setUserName",                                              // 9
    onBeforeAction: function() {                                       // 9
      if (!Config.username || (Meteor.userId() && Meteor.user().username)) {
        this.redirect('/dashboard');                                   // 12
      }                                                                //
      return this.next();                                              //
    }                                                                  //
  });                                                                  //
  return this.route('signOut', {                                       //
    path: '/sign-out',                                                 // 16
    onBeforeAction: function() {                                       // 16
      Meteor.logout(function() {});                                    // 18
      this.redirect('/');                                              // 18
      return this.next();                                              //
    }                                                                  //
  });                                                                  //
});                                                                    // 1
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=accounts.coffee.js.map
