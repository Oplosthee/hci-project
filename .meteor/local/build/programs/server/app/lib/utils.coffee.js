(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/utils.coffee.js                                                 //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.Utils = {                                                         // 1
  prettyDate: function(date) {                                         // 2
    if (date) {                                                        // 3
      if (Config.dateFormat) {                                         // 4
        return moment(date).format(Config.dateFormat);                 //
      } else {                                                         //
        return moment(date).format('D/M/YYYY');                        //
      }                                                                //
    }                                                                  //
  },                                                                   //
  timeSince: function(date) {                                          // 2
    var interval, seconds;                                             // 10
    if (date) {                                                        // 10
      seconds = Math.floor((new Date() - date) / 1000);                // 11
      interval = Math.floor(seconds / 31536000);                       // 11
      if (interval > 1) {                                              // 13
        return interval + "years ago";                                 // 13
      }                                                                //
      interval = Math.floor(seconds / 2592000);                        // 11
      if (interval > 1) {                                              // 15
        return interval + " months ago";                               // 15
      }                                                                //
      interval = Math.floor(seconds / 86400);                          // 11
      if (interval > 1) {                                              // 17
        return interval + " days ago";                                 // 17
      }                                                                //
      interval = Math.floor(seconds / 3600);                           // 11
      if (interval > 1) {                                              // 19
        return interval + " hours ago";                                // 19
      }                                                                //
      interval = Math.floor(seconds / 60);                             // 11
      if (interval > 1) {                                              // 21
        return interval + " minutes";                                  // 21
      }                                                                //
      return "just now";                                               //
    }                                                                  //
  },                                                                   //
  isMobile: function() {                                               // 2
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  },                                                                   //
  loginRequired: function() {                                          // 2
    return Router.go('/sign-in');                                      //
  },                                                                   //
  countryList: ["Select Country", "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts & Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"]
};                                                                     //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=utils.coffee.js.map
