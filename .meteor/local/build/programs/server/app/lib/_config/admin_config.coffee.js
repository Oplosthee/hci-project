(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/admin_config.coffee.js                                  //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.AdminConfig = {                                                   // 1
  name: Config.name,                                                   // 2
  collections: {                                                       // 2
    Groups: {                                                          // 4
      color: 'green',                                                  // 5
      icon: 'users',                                                   // 5
      tableColumns: [                                                  // 5
        {                                                              //
          label: 'Naam',                                               // 8
          name: 'name'                                                 // 8
        }                                                              //
      ]                                                                //
    },                                                                 //
    Questions: {                                                       // 4
      color: 'red',                                                    // 11
      icon: 'pencil',                                                  // 11
      tableColumns: [                                                  // 11
        {                                                              //
          label: 'Vraag',                                              // 14
          name: 'question'                                             // 14
        }, {                                                           //
          label: 'Antwoord',                                           // 15
          name: 'answer'                                               // 15
        }                                                              //
      ]                                                                //
    }                                                                  //
                                                                       // 17
    /*                                                                 // 17
    		Posts:                                                           //
    			color: 'red'                                                    //
    			icon: 'pencil'                                                  //
    			extraFields: ['owner']                                          //
    			tableColumns: [                                                 //
    				{ label: 'Title', name: 'title' }                              //
    				{ label: 'User', name: 'author()', template: 'adminUserCell' }
    			]                                                               //
    		Comments:                                                        //
    			color: 'green'                                                  //
    			icon: 'comments'                                                //
    			extraFields: ['doc', 'owner']                                   //
    			tableColumns: [                                                 //
    				{ label: 'Content', name: 'content' }                          //
    				{ label: 'Post', name: 'docTitle()', template: 'adminPostCell' }
    				{ label: 'User', name: 'author()', template: 'adminUserCell' }
    			]                                                               //
    			children: [                                                     //
    				{                                                              //
    					find: (comment) ->                                            //
    						Posts.find comment.doc, limit: 1                             //
    				}                                                              //
    				{                                                              //
    					find: (comment) ->                                            //
    						Meteor.users.find comment.owner, limit: 1                    //
    				}                                                              //
    			]                                                               //
     */                                                                //
  },                                                                   //
  dashboard: {                                                         // 2
    homeUrl: '/'                                                       // 47
  },                                                                   //
  autoForm: {                                                          // 2
    omitFields: ['createdAt', 'updatedAt']                             // 49
  }                                                                    //
};                                                                     //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=admin_config.coffee.js.map
