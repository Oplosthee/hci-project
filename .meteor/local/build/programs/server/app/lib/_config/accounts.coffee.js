(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/accounts.coffee.js                                      //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
AccountsTemplates.configure({                                          // 3
  confirmPassword: false,                                              // 6
  enablePasswordChange: true,                                          // 6
  forbidClientAccountCreation: true,                                   // 6
  overrideLoginErrors: true,                                           // 6
  sendVerificationEmail: false,                                        // 6
  lowercaseUsername: false,                                            // 6
  showAddRemoveServices: true,                                         // 6
  showForgotPasswordLink: true,                                        // 6
  showLabels: true,                                                    // 6
  showPlaceholders: true,                                              // 6
  showResendVerificationEmailLink: false,                              // 6
  continuousValidation: false,                                         // 6
  negativeFeedback: false,                                             // 6
  negativeValidation: true,                                            // 6
  positiveValidation: false,                                           // 6
  positiveFeedback: true,                                              // 6
  showValidating: true,                                                // 6
  homeRoutePath: Config.dashboardRoute || null,                        // 6
  onLogoutHook: function() {                                           // 6
    return console.log('logout');                                      //
  },                                                                   //
  onSubmitHook: function() {                                           // 6
    return console.log('submitting form');                             //
  }                                                                    //
});                                                                    //
                                                                       //
AccountsTemplates.configureRoute('signIn');                            // 3
                                                                       //
AccountsTemplates.configureRoute('forgotPwd');                         // 3
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=accounts.coffee.js.map
