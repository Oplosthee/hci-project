(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/i18n.coffee.js                                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.startup(function() {                                            // 1
  if (Meteor.isClient) {                                               // 2
    if (Config.defaultLanguage) {                                      // 3
      T9n.setLanguage(Config.defaultLanguage);                         // 4
      return TAPi18n.setLanguage(Config.defaultLanguage);              //
    } else {                                                           //
      T9n.setLanguage('en');                                           // 7
      return TAPi18n.setLanguage('en');                                //
    }                                                                  //
  }                                                                    //
});                                                                    // 1
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=i18n.coffee.js.map
