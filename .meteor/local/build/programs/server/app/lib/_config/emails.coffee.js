(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/emails.coffee.js                                        //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var options;                                                           // 1
                                                                       //
if (Meteor.isServer) {                                                 // 1
  options = {                                                          // 2
    siteName: Config.name                                              // 3
  };                                                                   //
  if (Config.socialMedia) {                                            // 5
    _.each(Config.socialMedia, function(v, k) {                        // 6
      return options[k] = v.url;                                       //
    });                                                                //
  }                                                                    //
  if (Config.legal) {                                                  // 9
    options.companyAddress = Config.legal.address;                     // 10
    options.companyName = Config.legal.name;                           // 10
    options.companyUrl = Config.legal.url;                             // 10
  }                                                                    //
  PrettyEmail.options = options;                                       // 2
}                                                                      //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=emails.coffee.js.map
