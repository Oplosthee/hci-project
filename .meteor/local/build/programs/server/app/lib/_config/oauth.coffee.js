(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/oauth.coffee.js                                         //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
if (Meteor.isServer) {                                                 // 1
  Meteor.startup(function() {                                          // 2
    if (Meteor.settings && Meteor.settings.serviceConfigurations) {    // 5
      return _.each(Meteor.settings.serviceConfigurations, function(config, service) {
        return ServiceConfiguration.configurations.upsert({            //
          service: service                                             // 7
        }, {                                                           //
          $set: config                                                 // 7
        });                                                            //
      });                                                              //
    }                                                                  //
  });                                                                  //
}                                                                      //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=oauth.coffee.js.map
