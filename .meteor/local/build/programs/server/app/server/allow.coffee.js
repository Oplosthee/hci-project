(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// server/allow.coffee.js                                              //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
ProfilePictures.allow({                                                // 1
  insert: function(userId, doc) {                                      // 2
    return true;                                                       //
  },                                                                   //
  update: function(userId, doc, fieldNames, modifier) {                // 2
    return true;                                                       //
  },                                                                   //
  download: function(userId) {                                         // 2
    return true;                                                       //
  }                                                                    //
});                                                                    //
                                                                       //
Posts.allow({                                                          // 1
  insert: function(userId, doc) {                                      // 10
    return userId === doc.owner;                                       //
  },                                                                   //
  update: function(userId, doc, fields, modifier) {                    // 10
    return userId === doc.owner;                                       //
  },                                                                   //
  remove: function(userId, doc) {                                      // 10
    return userId === doc.owner;                                       //
  }                                                                    //
});                                                                    //
                                                                       //
Questions.allow({                                                      // 1
  insert: function(userId, doc) {                                      // 18
    return userId === doc.owner;                                       //
  },                                                                   //
  update: function(userId, doc, fields, modifier) {                    // 18
    return userId === doc.owner;                                       //
  },                                                                   //
  remove: function(userId, doc) {                                      // 18
    return userId === doc.owner;                                       //
  }                                                                    //
});                                                                    //
                                                                       //
Attachments.allow({                                                    // 1
  insert: function(userId, doc) {                                      // 26
    return true;                                                       //
  },                                                                   //
  update: function(userId, doc, fieldNames, modifier) {                // 26
    return true;                                                       //
  },                                                                   //
  download: function(userId) {                                         // 26
    return true;                                                       //
  }                                                                    //
});                                                                    //
                                                                       //
Meteor.users.allow({                                                   // 1
  update: function(userId, doc, fieldNames, modifier) {                // 34
    if (userId === doc._id && !doc.username && fieldNames.length === 1 && fieldNames[0] === 'username') {
      return true;                                                     //
    } else {                                                           //
      return false;                                                    //
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=allow.coffee.js.map
