(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// server/methods/accounts.coffee.js                                   //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.methods({                                                       // 1
  deleteAccount: function(userId) {                                    // 2
    if (this.userId === userId) {                                      // 3
      return Meteor.users.remove({                                     //
        _id: this.userId                                               // 4
      });                                                              //
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=accounts.coffee.js.map
