(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// server/publish/collections.coffee.js                                //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.publish('posts', function() {                                   // 4
  return Posts.find();                                                 //
});                                                                    // 4
                                                                       //
Meteor.publish('questions', function() {                               // 4
  return Questions.find();                                             //
});                                                                    // 7
                                                                       //
Meteor.publish('attachments', function() {                             // 4
  return Attachments.find();                                           //
});                                                                    // 10
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=collections.coffee.js.map
