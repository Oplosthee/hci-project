(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// server/publish/user.coffee.js                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.publishComposite('user', function() {                           // 1
  return {                                                             //
    find: function() {                                                 // 2
      return Meteor.users.find({                                       //
        _id: this.userId                                               // 3
      });                                                              //
    },                                                                 //
    children: [                                                        // 2
      {                                                                //
        find: function(user) {                                         // 5
          var _id, ref;                                                // 6
          _id = ((ref = user.profile) != null ? ref.picture : void 0) || null;
          return ProfilePictures.find({                                //
            _id: _id                                                   // 7
          });                                                          //
        }                                                              //
      }                                                                //
    ]                                                                  //
  };                                                                   //
});                                                                    // 1
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=user.coffee.js.map
