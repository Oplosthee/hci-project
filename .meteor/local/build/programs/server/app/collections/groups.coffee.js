(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// collections/groups.coffee.js                                        //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.Groups = new Meteor.Collection('groups');                         // 1
                                                                       //
Schemas.Groups = new SimpleSchema({                                    // 1
  name: {                                                              // 4
    type: String,                                                      // 5
    max: 60                                                            // 5
  }                                                                    //
});                                                                    //
                                                                       //
Groups.attachSchema(Schemas.Groups);                                   // 1
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=groups.coffee.js.map
