(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// collections/_schemas.coffee.js                                      //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.Schemas = {};                                                     // 1
                                                                       //
Schemas.updatePassword = new SimpleSchema({                            // 1
  old: {                                                               // 5
    type: String,                                                      // 6
    label: "Huidig wachtwoord",                                        // 6
    max: 50                                                            // 6
  },                                                                   //
  "new": {                                                             // 5
    type: String,                                                      // 11
    min: 6,                                                            // 11
    max: 20,                                                           // 11
    label: "Nieuw wachtwoord"                                          // 11
  },                                                                   //
  confirm: {                                                           // 5
    type: String,                                                      // 17
    min: 6,                                                            // 17
    max: 20,                                                           // 17
    label: "Bevestig nieuw wachtwoord"                                 // 17
  }                                                                    //
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=_schemas.coffee.js.map
