(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;

/* Package-scope variables */
var HTTP, _methodHTTP;

(function(){

////////////////////////////////////////////////////////////////////////////
//                                                                        //
// packages/cfs_http-methods/packages/cfs_http-methods.js                 //
//                                                                        //
////////////////////////////////////////////////////////////////////////////
                                                                          //
(function () {                                                            // 1
                                                                          // 2
///////////////////////////////////////////////////////////////////////   // 3
//                                                                   //   // 4
// packages/cfs:http-methods/http.methods.client.api.js              //   // 5
//                                                                   //   // 6
///////////////////////////////////////////////////////////////////////   // 7
                                                                     //   // 8
HTTP = Package.http && Package.http.HTTP || {};                      // 1
                                                                     // 2
// Client-side simulation is not yet implemented                     // 3
HTTP.methods = function() {                                          // 4
  throw new Error('HTTP.methods not implemented on client-side');    // 5
};                                                                   // 6
                                                                     // 7
///////////////////////////////////////////////////////////////////////   // 16
                                                                          // 17
}).call(this);                                                            // 18
                                                                          // 19
////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
(function (pkg, symbols) {
  for (var s in symbols)
    (s in pkg) || (pkg[s] = symbols[s]);
})(Package['cfs:http-methods'] = {}, {
  HTTP: HTTP,
  _methodHTTP: _methodHTTP
});

})();
