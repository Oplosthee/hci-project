(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Tracker = Package.tracker.Tracker;
var Deps = Package.tracker.Deps;
var Router = Package['iron:router'].Router;
var RouteController = Package['iron:router'].RouteController;
var Momentum = Package['percolate:momentum'].Momentum;
var Iron = Package['iron:core'].Iron;

(function(){

///////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                           //
// packages/percolate_momentum-iron-router/packages/percolate_momentum-iron-router.js        //
//                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////
                                                                                             //
(function () {                                                                               // 1
                                                                                             // 2
/////////////////////////////////////////////////////////////////////////////////////////    // 3
//                                                                                     //    // 4
// packages/percolate:momentum-iron-router/momentum-iron-router.js                     //    // 5
//                                                                                     //    // 6
/////////////////////////////////////////////////////////////////////////////////////////    // 7
                                                                                       //    // 8
var Transitioner = {                                                                   // 1  // 9
  renders: [],                                                                         // 2  // 10
  start: function() {                                                                  // 3  // 11
    var self = this;                                                                   // 4  // 12
                                                                                       // 5  // 13
    // a single autorun that keeps a list of rendered pages                            // 6  // 14
    Deps.autorun(function() {                                                          // 7  // 15
      if (! Router.current())                                                          // 8  // 16
        return;                                                                        // 9  // 17
                                                                                       // 10
      var render = {                                                                   // 11
        path: Router.current().path,                                                   // 12
        template: Router._layout.region('main').template(),                            // 13
        initiator: Router.current().options.initiator                                  // 14
      };                                                                               // 15
                                                                                       // 16
      self.renders.unshift(render);                                                    // 17
    });                                                                                // 18
  },                                                                                   // 19
                                                                                       // 20
}                                                                                      // 21
Transitioner.start();                                                                  // 22
                                                                                       // 23
Momentum.registerPlugin('iron-router', function(options) {                             // 24
  check(options.options, Match.Optional(Function));                                    // 25
                                                                                       // 26
  var getPluginOptions = function(node) {                                              // 27
    var type = options.options &&                                                      // 28
      options.options(Transitioner.renders[1], Transitioner.renders[0], node);         // 29
                                                                                       // 30
    if (_.isUndefined(type))                                                           // 31
      type = 'none'; // XXX: what should this be?                                      // 32
                                                                                       // 33
    return type;                                                                       // 34
  }                                                                                    // 35
                                                                                       // 36
  var getPlugin = function(node) {                                                     // 37
    var pluginOptions = getPluginOptions(node)                                         // 38
                                                                                       // 39
    if (_.isString(pluginOptions))                                                     // 40
      pluginOptions = {with: pluginOptions};                                           // 41
                                                                                       // 42
    var plugin = Momentum.plugins[pluginOptions.with];                                 // 43
    if (! plugin)                                                                      // 44
      return console.error("Can't find momentum plugin '" + pluginOptions.with + "'"); // 45
                                                                                       // 46
    var pluginOptions = _.extend(_.omit(options, 'options'), pluginOptions);           // 47
    return plugin(_.omit(pluginOptions, 'with'));                                      // 48
  }                                                                                    // 49
                                                                                       // 50
  return {                                                                             // 51
    insertElement: function(node, next, done) {                                        // 52
      getPlugin(node).insertElement(node, next, done);                                 // 53
    },                                                                                 // 54
    moveElement: function(node, next, done) {                                          // 55
      getPlugin(node).moveElement(node, next, done);                                   // 56
    },                                                                                 // 57
    removeElement: function(node, done) {                                              // 58
      getPlugin(node).removeElement(node, done);                                       // 59
    },                                                                                 // 60
    // force: true                                                                     // 61
  }                                                                                    // 62
});                                                                                    // 63
/////////////////////////////////////////////////////////////////////////////////////////    // 72
                                                                                             // 73
}).call(this);                                                                               // 74
                                                                                             // 75
///////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['percolate:momentum-iron-router'] = {};

})();
