//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var check = Package.check.check;
var Match = Package.check.Match;
var _ = Package.underscore._;
var ReactiveVar = Package['reactive-var'].ReactiveVar;
var Template = Package.templating.Template;
var AutoForm = Package['aldeed:autoform'].AutoForm;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var Spacebars = Package.spacebars.Spacebars;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;
var FS = Package['cfs:base-package'].FS;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                            //
// packages/yogiben_autoform-file/lib/client/template.autoform-file.js                                        //
//                                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                              //
                                                                                                              // 1
Template.__checkName("afFileUpload");                                                                         // 2
Template["afFileUpload"] = new Template("Template.afFileUpload", (function() {                                // 3
  var view = this;                                                                                            // 4
  return HTML.DIV("\n    ", Blaze.If(function() {                                                             // 5
    return Spacebars.call(view.lookup("file"));                                                               // 6
  }, function() {                                                                                             // 7
    return [ "\n      ", Blaze.If(function() {                                                                // 8
      return Spacebars.call(Spacebars.dot(view.lookup("file"), "isUploaded"));                                // 9
    }, function() {                                                                                           // 10
      return [ "\n        ", Blaze._TemplateWith(function() {                                                 // 11
        return {                                                                                              // 12
          template: Spacebars.call(view.lookup("previewTemplate")),                                           // 13
          data: Spacebars.call(view.lookup("previewTemplateData"))                                            // 14
        };                                                                                                    // 15
      }, function() {                                                                                         // 16
        return Spacebars.include(function() {                                                                 // 17
          return Spacebars.call(Template.__dynamic);                                                          // 18
        });                                                                                                   // 19
      }), "\n        ", Blaze._TemplateWith(function() {                                                      // 20
        return {                                                                                              // 21
          template: Spacebars.call(view.lookup("removeFileBtnTemplate")),                                     // 22
          data: Spacebars.call(view.lookup("removeLabel"))                                                    // 23
        };                                                                                                    // 24
      }, function() {                                                                                         // 25
        return Spacebars.include(function() {                                                                 // 26
          return Spacebars.call(Template.__dynamic);                                                          // 27
        });                                                                                                   // 28
      }), "\n      " ];                                                                                       // 29
    }, function() {                                                                                           // 30
      return [ "\n        ", Blaze._TemplateWith(function() {                                                 // 31
        return {                                                                                              // 32
          template: Spacebars.call(view.lookup("uploadProgressTemplate")),                                    // 33
          data: Spacebars.call(view.lookup("file"))                                                           // 34
        };                                                                                                    // 35
      }, function() {                                                                                         // 36
        return Spacebars.include(function() {                                                                 // 37
          return Spacebars.call(Template.__dynamic);                                                          // 38
        });                                                                                                   // 39
      }), "\n      " ];                                                                                       // 40
    }), "\n    " ];                                                                                           // 41
  }, function() {                                                                                             // 42
    return [ "\n      ", Blaze._TemplateWith(function() {                                                     // 43
      return {                                                                                                // 44
        template: Spacebars.call(view.lookup("selectFileBtnTemplate")),                                       // 45
        data: Spacebars.call(view.lookup("selectFileBtnData"))                                                // 46
      };                                                                                                      // 47
    }, function() {                                                                                           // 48
      return Spacebars.include(function() {                                                                   // 49
        return Spacebars.call(Template.__dynamic);                                                            // 50
      });                                                                                                     // 51
    }), "\n    " ];                                                                                           // 52
  }), "\n    ", HTML.INPUT({                                                                                  // 53
    type: "hidden",                                                                                           // 54
    "class": "js-value",                                                                                      // 55
    value: function() {                                                                                       // 56
      return Spacebars.mustache(view.lookup("value"));                                                        // 57
    },                                                                                                        // 58
    "data-schema-key": function() {                                                                           // 59
      return Spacebars.mustache(view.lookup("schemaKey"));                                                    // 60
    }                                                                                                         // 61
  }), "\n  ");                                                                                                // 62
}));                                                                                                          // 63
                                                                                                              // 64
Template.__checkName("afFileUploadThumbIcon");                                                                // 65
Template["afFileUploadThumbIcon"] = new Template("Template.afFileUploadThumbIcon", (function() {              // 66
  var view = this;                                                                                            // 67
  return HTML.A({                                                                                             // 68
    "class": "btn btn-default",                                                                               // 69
    target: "_blank",                                                                                         // 70
    href: function() {                                                                                        // 71
      return Spacebars.mustache(view.lookup("url"));                                                          // 72
    }                                                                                                         // 73
  }, "\n		", HTML.I({                                                                                         // 74
    "class": function() {                                                                                     // 75
      return [ "fa fa-", Spacebars.mustache(view.lookup("icon")) ];                                           // 76
    }                                                                                                         // 77
  }), " ", Blaze.View("lookup:file.original.name", function() {                                               // 78
    return Spacebars.mustache(Spacebars.dot(view.lookup("file"), "original", "name"));                        // 79
  }), "\n	");                                                                                                 // 80
}));                                                                                                          // 81
                                                                                                              // 82
Template.__checkName("afFileUploadThumbImg");                                                                 // 83
Template["afFileUploadThumbImg"] = new Template("Template.afFileUploadThumbImg", (function() {                // 84
  var view = this;                                                                                            // 85
  return HTML.IMG({                                                                                           // 86
    src: function() {                                                                                         // 87
      return Spacebars.mustache(view.lookup("url"));                                                          // 88
    },                                                                                                        // 89
    alt: "",                                                                                                  // 90
    "class": "img-fileUpload-thumbnail img-thumbnail img-rounded"                                             // 91
  });                                                                                                         // 92
}));                                                                                                          // 93
                                                                                                              // 94
Template.__checkName("afFileRemoveFileBtnTemplate");                                                          // 95
Template["afFileRemoveFileBtnTemplate"] = new Template("Template.afFileRemoveFileBtnTemplate", (function() {  // 96
  var view = this;                                                                                            // 97
  return HTML.DIV("\n    ", HTML.A({                                                                          // 98
    href: "#",                                                                                                // 99
    "class": "js-af-remove-file"                                                                              // 100
  }, HTML.Raw('<i class="fa fa-times"></i>'), " ", Blaze.View("lookup:.", function() {                        // 101
    return Spacebars.mustache(view.lookup("."));                                                              // 102
  })), "\n  ");                                                                                               // 103
}));                                                                                                          // 104
                                                                                                              // 105
Template.__checkName("afFileSelectFileBtnTemplate");                                                          // 106
Template["afFileSelectFileBtnTemplate"] = new Template("Template.afFileSelectFileBtnTemplate", (function() {  // 107
  var view = this;                                                                                            // 108
  return HTML.BUTTON({                                                                                        // 109
    type: "button",                                                                                           // 110
    "class": "btn btn-default af-select-file js-af-select-file"                                               // 111
  }, "\n    ", Blaze.View("lookup:label", function() {                                                        // 112
    return Spacebars.mustache(view.lookup("label"));                                                          // 113
  }), "\n    ", HTML.INPUT({                                                                                  // 114
    type: "file",                                                                                             // 115
    "class": "js-file",                                                                                       // 116
    accept: function() {                                                                                      // 117
      return Spacebars.mustache(view.lookup("accept"));                                                       // 118
    }                                                                                                         // 119
  }), "\n  ");                                                                                                // 120
}));                                                                                                          // 121
                                                                                                              // 122
Template.__checkName("afFileUploadProgress");                                                                 // 123
Template["afFileUploadProgress"] = new Template("Template.afFileUploadProgress", (function() {                // 124
  var view = this;                                                                                            // 125
  return Spacebars.include(function() {                                                                       // 126
    return Spacebars.call(Spacebars.dot(view.lookup("FS"), "UploadProgressBar"));                             // 127
  });                                                                                                         // 128
}));                                                                                                          // 129
                                                                                                              // 130
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                            //
// packages/yogiben_autoform-file/lib/client/autoform-file.coffee.js                                          //
//                                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                              //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var getCollection, getDocument;                                                                               // 1
                                                                                                              //
AutoForm.addInputType('fileUpload', {                                                                         // 1
  template: 'afFileUpload',                                                                                   // 2
  valueOut: function() {                                                                                      // 2
    return this.val();                                                                                        //
  }                                                                                                           //
});                                                                                                           //
                                                                                                              //
getCollection = function(context) {                                                                           // 1
  if (typeof context.atts.collection === 'string') {                                                          // 7
    return FS._collections[context.atts.collection] || window[context.atts.collection];                       //
  }                                                                                                           //
};                                                                                                            // 6
                                                                                                              //
getDocument = function(context) {                                                                             // 1
  var collection, id, ref, ref1;                                                                              // 11
  collection = getCollection(context);                                                                        // 11
  id = (ref = Template.instance()) != null ? (ref1 = ref.value) != null ? typeof ref1.get === "function" ? ref1.get() : void 0 : void 0 : void 0;
  return collection != null ? collection.findOne(id) : void 0;                                                //
};                                                                                                            // 10
                                                                                                              //
Template.afFileUpload.onCreated(function() {                                                                  // 1
  var self;                                                                                                   // 16
  self = this;                                                                                                // 16
  this.value = new ReactiveVar(this.data.value);                                                              // 16
  this._stopInterceptValue = false;                                                                           // 16
  this._interceptValue = (function(_this) {                                                                   // 16
    return function(ctx) {                                                                                    //
      var ref, t;                                                                                             // 21
      if (!_this._stopInterceptValue) {                                                                       // 21
        t = Template.instance();                                                                              // 22
        if (t.value.get() !== false && t.value.get() !== ctx.value && ((ref = ctx.value) != null ? ref.length : void 0) > 0) {
          t.value.set(ctx.value);                                                                             // 24
          return _this._stopInterceptValue = true;                                                            //
        }                                                                                                     //
      }                                                                                                       //
    };                                                                                                        //
  })(this);                                                                                                   //
  this._insert = function(file) {                                                                             // 16
    var collection, ref;                                                                                      // 28
    collection = getCollection(self.data);                                                                    // 28
    if (Meteor.userId) {                                                                                      // 30
      file.owner = Meteor.userId();                                                                           // 31
    }                                                                                                         //
    if (typeof ((ref = self.data.atts) != null ? ref.onBeforeInsert : void 0) === 'function') {               // 33
      file = (self.data.atts.onBeforeInsert(file)) || file;                                                   // 34
    }                                                                                                         //
    return collection.insert(file, function(err, fileObj) {                                                   //
      var ref1;                                                                                               // 37
      if (typeof ((ref1 = self.data.atts) != null ? ref1.onAfterInsert : void 0) === 'function') {            // 37
        self.data.atts.onAfterInsert(err, fileObj);                                                           // 38
      }                                                                                                       //
      fileObj.update({                                                                                        // 37
        $set: {                                                                                               // 40
          metadata: {                                                                                         // 40
            owner: Meteor.userId()                                                                            // 40
          }                                                                                                   //
        }                                                                                                     //
      });                                                                                                     //
      if (err) {                                                                                              // 42
        return console.log(err);                                                                              // 42
      }                                                                                                       //
      return self.value.set(fileObj._id);                                                                     //
    });                                                                                                       //
  };                                                                                                          //
  return this.autorun(function() {                                                                            //
    var _id;                                                                                                  // 46
    _id = self.value.get();                                                                                   // 46
    return _id && Meteor.subscribe('autoformFileDoc', self.data.atts.collection, _id);                        //
  });                                                                                                         //
});                                                                                                           // 15
                                                                                                              //
Template.afFileUpload.onRendered(function() {                                                                 // 1
  var self;                                                                                                   // 50
  self = this;                                                                                                // 50
  return $(self.firstNode).closest('form').on('reset', function() {                                           //
    return self.value.set(false);                                                                             //
  });                                                                                                         //
});                                                                                                           // 49
                                                                                                              //
Template.afFileUpload.helpers({                                                                               // 1
  label: function() {                                                                                         // 55
    return this.atts.label || 'Choose file';                                                                  //
  },                                                                                                          //
  removeLabel: function() {                                                                                   // 55
    return this.atts.removeLabel || 'Remove';                                                                 //
  },                                                                                                          //
  value: function() {                                                                                         // 55
    var doc;                                                                                                  // 60
    doc = getDocument(this);                                                                                  // 60
    return (doc != null ? doc.isUploaded() : void 0) && doc._id;                                              //
  },                                                                                                          //
  schemaKey: function() {                                                                                     // 55
    return this.atts['data-schema-key'];                                                                      //
  },                                                                                                          //
  previewTemplate: function() {                                                                               // 55
    var ref, ref1;                                                                                            // 65
    return ((ref = this.atts) != null ? ref.previewTemplate : void 0) || (((ref1 = getDocument(this)) != null ? ref1.isImage() : void 0) ? 'afFileUploadThumbImg' : 'afFileUploadThumbIcon');
  },                                                                                                          //
  previewTemplateData: function() {                                                                           // 55
    return {                                                                                                  //
      file: getDocument(this),                                                                                // 67
      atts: this.atts                                                                                         // 67
    };                                                                                                        //
  },                                                                                                          //
  file: function() {                                                                                          // 55
    Template.instance()._interceptValue(this);                                                                // 70
    return getDocument(this);                                                                                 //
  },                                                                                                          //
  removeFileBtnTemplate: function() {                                                                         // 55
    var ref;                                                                                                  // 73
    return ((ref = this.atts) != null ? ref.removeFileBtnTemplate : void 0) || 'afFileRemoveFileBtnTemplate';
  },                                                                                                          //
  selectFileBtnTemplate: function() {                                                                         // 55
    var ref;                                                                                                  // 75
    return ((ref = this.atts) != null ? ref.selectFileBtnTemplate : void 0) || 'afFileSelectFileBtnTemplate';
  },                                                                                                          //
  selectFileBtnData: function() {                                                                             // 55
    return {                                                                                                  //
      label: this.atts.label || 'Choose file',                                                                // 77
      accepts: this.atts.accepts                                                                              // 77
    };                                                                                                        //
  },                                                                                                          //
  uploadProgressTemplate: function() {                                                                        // 55
    var ref;                                                                                                  // 80
    return ((ref = this.atts) != null ? ref.uploadProgressTemplate : void 0) || 'afFileUploadProgress';       //
  }                                                                                                           //
});                                                                                                           //
                                                                                                              //
Template.afFileUpload.events({                                                                                // 1
  "dragover .js-af-select-file": function(e) {                                                                // 83
    e.stopPropagation();                                                                                      // 84
    return e.preventDefault();                                                                                //
  },                                                                                                          //
  "dragenter .js-af-select-file": function(e) {                                                               // 83
    e.stopPropagation();                                                                                      // 88
    return e.preventDefault();                                                                                //
  },                                                                                                          //
  "drop .js-af-select-file": function(e, t) {                                                                 // 83
    e.stopPropagation();                                                                                      // 92
    e.preventDefault();                                                                                       // 92
    return t._insert(new FS.File(e.originalEvent.dataTransfer.files[0]));                                     //
  },                                                                                                          //
  'click .js-af-remove-file': function(e, t) {                                                                // 83
    e.preventDefault();                                                                                       // 97
    return t.value.set(false);                                                                                //
  },                                                                                                          //
  'fileuploadchange .js-file': function(e, t, data) {                                                         // 83
    return t._insert(new FS.File(data.files[0]));                                                             //
  }                                                                                                           //
});                                                                                                           //
                                                                                                              //
Template.afFileUploadThumbImg.helpers({                                                                       // 1
  url: function() {                                                                                           // 104
    return this.file.url({                                                                                    //
      store: this.atts.store                                                                                  // 105
    });                                                                                                       //
  }                                                                                                           //
});                                                                                                           //
                                                                                                              //
Template.afFileUploadThumbIcon.helpers({                                                                      // 1
  url: function() {                                                                                           // 108
    return this.file.url({                                                                                    //
      store: this.atts.store                                                                                  // 109
    });                                                                                                       //
  },                                                                                                          //
  icon: function() {                                                                                          // 108
    switch (this.file.extension()) {                                                                          // 111
      case 'pdf':                                                                                             // 111
        return 'file-pdf-o';                                                                                  //
      case 'doc':                                                                                             // 111
      case 'docx':                                                                                            // 111
        return 'file-word-o';                                                                                 //
      case 'ppt':                                                                                             // 111
      case 'avi':                                                                                             // 111
      case 'mov':                                                                                             // 111
      case 'mp4':                                                                                             // 111
        return 'file-powerpoint-o';                                                                           //
      default:                                                                                                // 111
        return 'file-o';                                                                                      //
    }                                                                                                         // 111
  }                                                                                                           //
});                                                                                                           //
                                                                                                              //
Template.afFileSelectFileBtnTemplate.onRendered(function() {                                                  // 1
  return this.$('.js-file').fileupload();                                                                     //
});                                                                                                           // 121
                                                                                                              //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['yogiben:autoform-file'] = {};

})();
