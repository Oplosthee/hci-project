(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Template = Package.templating.Template;
var check = Package.check.check;
var Match = Package.check.Match;
var $ = Package.jquery.$;
var jQuery = Package.jquery.jQuery;
var _ = Package.underscore._;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var Spacebars = Package.spacebars.Spacebars;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var Momentum;

(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                               //
// packages/percolate_momentum/packages/percolate_momentum.js                                                    //
//                                                                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                 //
(function () {                                                                                                   // 1
                                                                                                                 // 2
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 3
//                                                                                                        //     // 4
// packages/percolate:momentum/template.momentum.js                                                       //     // 5
//                                                                                                        //     // 6
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 7
                                                                                                          //     // 8
                                                                                                          // 1   // 9
Template.__checkName("momentum");                                                                         // 2   // 10
Template["momentum"] = new Template("Template.momentum", (function() {                                    // 3   // 11
  var view = this;                                                                                        // 4   // 12
  return HTML.DIV({                                                                                       // 5   // 13
    "data-momentum": ""                                                                                   // 6   // 14
  }, "\n    ", Blaze._InOuterTemplateScope(view, function() {                                             // 7   // 15
    return Spacebars.include(function() {                                                                 // 8   // 16
      return Spacebars.call(view.templateContentBlock);                                                   // 9   // 17
    });                                                                                                   // 10  // 18
  }), "\n  ");                                                                                            // 11  // 19
}));                                                                                                      // 12  // 20
                                                                                                          // 13  // 21
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 22
                                                                                                                 // 23
}).call(this);                                                                                                   // 24
                                                                                                                 // 25
                                                                                                                 // 26
                                                                                                                 // 27
                                                                                                                 // 28
                                                                                                                 // 29
                                                                                                                 // 30
(function () {                                                                                                   // 31
                                                                                                                 // 32
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 33
//                                                                                                        //     // 34
// packages/percolate:momentum/momentum.js                                                                //     // 35
//                                                                                                        //     // 36
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 37
                                                                                                          //     // 38
Template.momentum.rendered = function() {                                                                 // 1   // 39
  if (! this.data || ! this.data.plugin)                                                                  // 2   // 40
    return console.error("Missing 'plugin' argument to momentum");                                        // 3   // 41
                                                                                                          // 4   // 42
  var plugin = Momentum.plugins[this.data.plugin];                                                        // 5   // 43
                                                                                                          // 6   // 44
  if (! plugin)                                                                                           // 7   // 45
    return console.error("Can't find momentum plugin '" + this.data.plugin + "'");                        // 8   // 46
                                                                                                          // 9   // 47
  var hooks = plugin(_.omit(this.data, 'plugin'));                                                        // 10  // 48
                                                                                                          // 11  // 49
  // default is to remove, *then* add                                                                     // 12  // 50
  if (! hooks.moveElement)                                                                                // 13  // 51
    hooks.moveElement = function(node, next, done) {                                                      // 14  // 52
      hooks.removeElement(node, function() {                                                              // 15  // 53
        hooks.insertElement(node, next, done);                                                            // 16  // 54
      });                                                                                                 // 17  // 55
    }                                                                                                     // 18  // 56
                                                                                                          // 19  // 57
  check(hooks, Match.Where(function (x) {                                                                 // 20  // 58
    return _.isFunction(x.insertElement) && _.isFunction(x.moveElement) && _.isFunction(x.removeElement); // 21  // 59
  }));                                                                                                    // 22  // 60
                                                                                                          // 23  // 61
  // Pass in the _identity function for the done callback as by default                                   // 24  // 62
  // momentum doesn't care about when transitions are done.                                               // 25  // 63
  this.lastNode._uihooks = {                                                                              // 26  // 64
    insertElement: function(node, next) {                                                                 // 27  // 65
      hooks.insertElement(node, next, _.identity);                                                        // 28  // 66
    },                                                                                                    // 29  // 67
    moveElement: function(node, next) {                                                                   // 30  // 68
      hooks.moveElement(node, next, _.identity);                                                          // 31  // 69
    },                                                                                                    // 32  // 70
    removeElement: function(node, done) {                                                                 // 33  // 71
      hooks.removeElement(node, _.identity);                                                              // 34  // 72
    }                                                                                                     // 35  // 73
  };                                                                                                      // 36  // 74
}                                                                                                         // 37  // 75
                                                                                                          // 38  // 76
Momentum = {                                                                                              // 39  // 77
  plugins: {},                                                                                            // 40  // 78
  registerPlugin: function(name, plugin) {                                                                // 41  // 79
    check(name, String);                                                                                  // 42  // 80
    check(plugin, Function)                                                                               // 43  // 81
    this.plugins[name] = plugin;                                                                          // 44  // 82
  }                                                                                                       // 45  // 83
}                                                                                                         // 46  // 84
                                                                                                          // 47  // 85
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 86
                                                                                                                 // 87
}).call(this);                                                                                                   // 88
                                                                                                                 // 89
                                                                                                                 // 90
                                                                                                                 // 91
                                                                                                                 // 92
                                                                                                                 // 93
                                                                                                                 // 94
(function () {                                                                                                   // 95
                                                                                                                 // 96
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 97
//                                                                                                        //     // 98
// packages/percolate:momentum/plugins/none.js                                                            //     // 99
//                                                                                                        //     // 100
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 101
                                                                                                          //     // 102
Momentum.registerPlugin('none', function(options) {                                                       // 1   // 103
  return {                                                                                                // 2   // 104
    insertElement: function(node, next, done) {                                                           // 3   // 105
      next.parentNode.insertBefore(node, next);                                                           // 4   // 106
      done();                                                                                             // 5   // 107
    },                                                                                                    // 6   // 108
    moveElement: function(node, next, done) {                                                             // 7   // 109
      next.parentNode.insertBefore(node, next);                                                           // 8   // 110
      done();                                                                                             // 9   // 111
    },                                                                                                    // 10  // 112
    removeElement: function(node, done) {                                                                 // 11  // 113
      node.parentNode.removeChild(node);                                                                  // 12  // 114
      done();                                                                                             // 13  // 115
    }                                                                                                     // 14  // 116
  }                                                                                                       // 15  // 117
});                                                                                                       // 16  // 118
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 119
                                                                                                                 // 120
}).call(this);                                                                                                   // 121
                                                                                                                 // 122
                                                                                                                 // 123
                                                                                                                 // 124
                                                                                                                 // 125
                                                                                                                 // 126
                                                                                                                 // 127
(function () {                                                                                                   // 128
                                                                                                                 // 129
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 130
//                                                                                                        //     // 131
// packages/percolate:momentum/plugins/css.js                                                             //     // 132
//                                                                                                        //     // 133
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 134
                                                                                                          //     // 135
// defaults, should be overrideable                                                                       // 1   // 136
var OFFSCREEN_CLASS = 'off-screen';                                                                       // 2   // 137
var IN_CLASS = 'in';                                                                                      // 3   // 138
var OUT_CLASS = 'out';                                                                                    // 4   // 139
                                                                                                          // 5   // 140
var EVENTS = 'webkitTransitionEnd oTransitionEnd transitionEnd '                                          // 6   // 141
  + 'msTransitionEnd transitionend';                                                                      // 7   // 142
                                                                                                          // 8   // 143
Momentum.registerPlugin('css', function(options) {                                                        // 9   // 144
  options = _.extend({                                                                                    // 10  // 145
    // extra: a function that returns an extra class to be added                                          // 11  // 146
    // timeout: a "maximum" time that the transition can take                                             // 12  // 147
  }, options);                                                                                            // 13  // 148
                                                                                                          // 14  // 149
  if (_.isString(options.extra)) {                                                                        // 15  // 150
    var extra = options.extra;                                                                            // 16  // 151
    options.extra = function() { return extra; }                                                          // 17  // 152
  }                                                                                                       // 18  // 153
  check(options.extra, Match.Optional(Function));                                                         // 19  // 154
                                                                                                          // 20  // 155
  return {                                                                                                // 21  // 156
    insertElement: function(node, next, done) {                                                           // 22  // 157
      var klass = IN_CLASS;                                                                               // 23  // 158
      if (options.extra)                                                                                  // 24  // 159
        klass += ' ' + options.extra();                                                                   // 25  // 160
                                                                                                          // 26  // 161
      $(node)                                                                                             // 27  // 162
        .addClass(OFFSCREEN_CLASS)                                                                        // 28  // 163
        .addClass(klass)                                                                                  // 29  // 164
        .insertBefore(next);                                                                              // 30  // 165
                                                                                                          // 31  // 166
      Deps.afterFlush(function() {                                                                        // 32  // 167
        // call width to force the browser to draw before we do anything                                  // 33  // 168
        $(node).width()                                                                                   // 34  // 169
                                                                                                          // 35  // 170
        var finish = _.once(function() {                                                                  // 36  // 171
          $(node).removeClass(klass);                                                                     // 37  // 172
          done();                                                                                         // 38  // 173
        });                                                                                               // 39  // 174
                                                                                                          // 40  // 175
        $(node).removeClass(OFFSCREEN_CLASS);                                                             // 41  // 176
                                                                                                          // 42  // 177
        // XXX: We have to bind the event listener to the PARENT of the node,                             // 43  // 178
        //  because Blaze runs jQ.cleanNode before "removing" the node.                                   // 44  // 179
        //                                                                                                // 45  // 180
        // What this means is, that if we are mid-way through our "add"                                   // 46  // 181
        // transition, and then the node is "removed", then even though                                   // 47  // 182
        // we may not actually remove the node from the DOM, this                                         // 48  // 183
        // event will never fire, meaning classes and junk will be left on                                // 49  // 184
        // the node. This seems a pretty reasonable workaround.                                           // 50  // 185
        //                                                                                                // 51  // 186
        // If the parent itself is "removed".. well then we'll have a problem.                            // 52  // 187
        $(node).parent()                                                                                  // 53  // 188
          .on(EVENTS, function(e) {                                                                       // 54  // 189
            if (e.target === node) {                                                                      // 55  // 190
              $(this).off(e);                                                                             // 56  // 191
              finish()                                                                                    // 57  // 192
            }                                                                                             // 58  // 193
          });                                                                                             // 59  // 194
                                                                                                          // 60  // 195
        if (options.timeout)                                                                              // 61  // 196
          Meteor.setTimeout(finish, options.timeout);                                                     // 62  // 197
      });                                                                                                 // 63  // 198
    },                                                                                                    // 64  // 199
      // we could do better I guess?                                                                      // 65  // 200
    moveElement: function(node, next, done) {                                                             // 66  // 201
      var self = this;                                                                                    // 67  // 202
      self.removeElement(node, function() {                                                               // 68  // 203
        self.insertElement(node, next, done);                                                             // 69  // 204
      });                                                                                                 // 70  // 205
    },                                                                                                    // 71  // 206
    removeElement: function(node, done) {                                                                 // 72  // 207
      var klass = OUT_CLASS;                                                                              // 73  // 208
      if (options.extra)                                                                                  // 74  // 209
        klass += ' ' + options.extra();                                                                   // 75  // 210
                                                                                                          // 76  // 211
      // add the out class and redraw                                                                     // 77  // 212
      $(node)                                                                                             // 78  // 213
        .addClass(klass)                                                                                  // 79  // 214
        .width();                                                                                         // 80  // 215
                                                                                                          // 81  // 216
      var finish = _.once(function() {                                                                    // 82  // 217
        $(node).remove();                                                                                 // 83  // 218
        done();                                                                                           // 84  // 219
      });                                                                                                 // 85  // 220
                                                                                                          // 86  // 221
      // now make it transition off                                                                       // 87  // 222
      $(node)                                                                                             // 88  // 223
        .addClass(OFFSCREEN_CLASS)                                                                        // 89  // 224
        .on(EVENTS, function(e) {                                                                         // 90  // 225
          if (e.target === node) {                                                                        // 91  // 226
            $(this).off(e);                                                                               // 92  // 227
            finish();                                                                                     // 93  // 228
          }                                                                                               // 94  // 229
        });                                                                                               // 95  // 230
                                                                                                          // 96  // 231
      if (options.timeout)                                                                                // 97  // 232
        Meteor.setTimeout(finish, options.timeout);                                                       // 98  // 233
    }                                                                                                     // 99  // 234
  }                                                                                                       // 100
});                                                                                                       // 101
                                                                                                          // 102
                                                                                                          // 103
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 239
                                                                                                                 // 240
}).call(this);                                                                                                   // 241
                                                                                                                 // 242
                                                                                                                 // 243
                                                                                                                 // 244
                                                                                                                 // 245
                                                                                                                 // 246
                                                                                                                 // 247
(function () {                                                                                                   // 248
                                                                                                                 // 249
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 250
//                                                                                                        //     // 251
// packages/percolate:momentum/plugins/velocity.js                                                        //     // 252
//                                                                                                        //     // 253
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 254
                                                                                                          //     // 255
// WIP: Currently, this plugin is just a testing-ground                                                   // 1   // 256
                                                                                                          // 2   // 257
Momentum.registerPlugin('velocity', function(options) {                                                   // 3   // 258
  return {                                                                                                // 4   // 259
    insertElement: function(node, next) {                                                                 // 5   // 260
      $(node)                                                                                             // 6   // 261
        .insertBefore(next)                                                                               // 7   // 262
        // .velocity("fadeIn", { duration: 500 });                                                        // 8   // 263
        .velocity("transition.slideLeftIn", { duration: 500 });                                           // 9   // 264
    },                                                                                                    // 10  // 265
    moveElement: function(node, next) {                                                                   // 11  // 266
      this.removeElement(node);                                                                           // 12  // 267
      this.insertElement(node, next);                                                                     // 13  // 268
    },                                                                                                    // 14  // 269
    removeElement: function(node) {                                                                       // 15  // 270
      $(node)                                                                                             // 16  // 271
        .velocity("transition.slideLeftOut", {                                                            // 17  // 272
        // .velocity("fadeOut", {                                                                         // 18  // 273
          duration: 500,                                                                                  // 19  // 274
          complete: function() {                                                                          // 20  // 275
            $(node).remove()                                                                              // 21  // 276
          }                                                                                               // 22  // 277
        });                                                                                               // 23  // 278
    }                                                                                                     // 24  // 279
  }                                                                                                       // 25  // 280
});                                                                                                       // 26  // 281
                                                                                                          // 27  // 282
                                                                                                          // 28  // 283
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 284
                                                                                                                 // 285
}).call(this);                                                                                                   // 286
                                                                                                                 // 287
                                                                                                                 // 288
                                                                                                                 // 289
                                                                                                                 // 290
                                                                                                                 // 291
                                                                                                                 // 292
(function () {                                                                                                   // 293
                                                                                                                 // 294
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 295
//                                                                                                        //     // 296
// packages/percolate:momentum/plugins/growl.js                                                           //     // 297
//                                                                                                        //     // 298
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 299
                                                                                                          //     // 300
Momentum.registerPlugin('growl', function(options) {                                                      // 1   // 301
  options = _.extend({}, options, {                                                                       // 2   // 302
    duration: 500,                                                                                        // 3   // 303
    easing: [ 250, 15 ], //spring physics                                                                 // 4   // 304
    // easing: [ 0.17, 0.67, 0.83, 0.67 ] //bezier curve                                                  // 5   // 305
  });                                                                                                     // 6   // 306
                                                                                                          // 7   // 307
  return {                                                                                                // 8   // 308
    insertElement: function(node, next) {                                                                 // 9   // 309
      var $node = $(node);                                                                                // 10  // 310
                                                                                                          // 11  // 311
      $node                                                                                               // 12  // 312
        .insertBefore(next)                                                                               // 13  // 313
        .velocity({                                                                                       // 14  // 314
          translateZ: 0,                                                                                  // 15  // 315
          scale: [1, 0]                                                                                   // 16  // 316
        }, {                                                                                              // 17  // 317
          easing: options.easing,                                                                         // 18  // 318
          duration: options.duration,                                                                     // 19  // 319
          queue: false                                                                                    // 20  // 320
        })                                                                                                // 21  // 321
        .velocity("fadeIn", {                                                                             // 22  // 322
          duration: options.duration,                                                                     // 23  // 323
          queue: false                                                                                    // 24  // 324
        });                                                                                               // 25  // 325
                                                                                                          // 26  // 326
      $(next).nextAll()                                                                                   // 27  // 327
        .velocity({                                                                                       // 28  // 328
          translateZ: 0,                                                                                  // 29  // 329
          translateY: [-1 * $node.outerHeight()]                                                          // 30  // 330
        }, {                                                                                              // 31  // 331
          duration: 0,                                                                                    // 32  // 332
          queue: false                                                                                    // 33  // 333
        })                                                                                                // 34  // 334
        .velocity({                                                                                       // 35  // 335
          translateY: [0]                                                                                 // 36  // 336
        }, {                                                                                              // 37  // 337
          duration: options.duration - 100, //speed it up a little                                        // 38  // 338
          queue: false                                                                                    // 39  // 339
        });                                                                                               // 40  // 340
    },                                                                                                    // 41  // 341
    moveElement: function(node, next) {                                                                   // 42  // 342
      this.removeElement(node);                                                                           // 43  // 343
      this.insertElement(node, next);                                                                     // 44  // 344
    },                                                                                                    // 45  // 345
    removeElement: function(node) {                                                                       // 46  // 346
      var $node = $(node);                                                                                // 47  // 347
                                                                                                          // 48  // 348
      $node                                                                                               // 49  // 349
        .velocity("fadeOut", {                                                                            // 50  // 350
          duration: options.duration,                                                                     // 51  // 351
          complete: function() {                                                                          // 52  // 352
            $node.remove();                                                                               // 53  // 353
          }                                                                                               // 54  // 354
        });                                                                                               // 55  // 355
    }                                                                                                     // 56  // 356
  }                                                                                                       // 57  // 357
});                                                                                                       // 58  // 358
                                                                                                          // 59  // 359
                                                                                                          // 60  // 360
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 361
                                                                                                                 // 362
}).call(this);                                                                                                   // 363
                                                                                                                 // 364
                                                                                                                 // 365
                                                                                                                 // 366
                                                                                                                 // 367
                                                                                                                 // 368
                                                                                                                 // 369
(function () {                                                                                                   // 370
                                                                                                                 // 371
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 372
//                                                                                                        //     // 373
// packages/percolate:momentum/plugins/side-to-side.js                                                    //     // 374
//                                                                                                        //     // 375
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 376
                                                                                                          //     // 377
// XXX: make this a plugin itself?                                                                        // 1   // 378
var sideToSide = function(fromX, toX) {                                                                   // 2   // 379
  return function(options) {                                                                              // 3   // 380
    options = _.extend({                                                                                  // 4   // 381
      duration: 500,                                                                                      // 5   // 382
      easing: 'ease-in-out'                                                                               // 6   // 383
    }, options);                                                                                          // 7   // 384
                                                                                                          // 8   // 385
    return {                                                                                              // 9   // 386
      insertElement: function(node, next, done) {                                                         // 10  // 387
        var $node = $(node);                                                                              // 11  // 388
                                                                                                          // 12  // 389
        $node                                                                                             // 13  // 390
          .css('transform', 'translateX(' + fromX + ')')                                                  // 14  // 391
          .insertBefore(next)                                                                             // 15  // 392
          .velocity({                                                                                     // 16  // 393
            translateX: [0, fromX]                                                                        // 17  // 394
          }, {                                                                                            // 18  // 395
            easing: options.easing,                                                                       // 19  // 396
            duration: options.duration,                                                                   // 20  // 397
            queue: false,                                                                                 // 21  // 398
            complete: function() {                                                                        // 22  // 399
              $node.css('transform', '');                                                                 // 23  // 400
              done();                                                                                     // 24  // 401
            }                                                                                             // 25  // 402
          });                                                                                             // 26  // 403
      },                                                                                                  // 27  // 404
      removeElement: function(node, done) {                                                               // 28  // 405
        var $node = $(node);                                                                              // 29  // 406
                                                                                                          // 30  // 407
        $node                                                                                             // 31  // 408
          .velocity({                                                                                     // 32  // 409
            translateX: [toX]                                                                             // 33  // 410
          }, {                                                                                            // 34  // 411
            duration: options.duration,                                                                   // 35  // 412
            easing: options.easing,                                                                       // 36  // 413
            complete: function() {                                                                        // 37  // 414
              $node.remove();                                                                             // 38  // 415
              done();                                                                                     // 39  // 416
            }                                                                                             // 40  // 417
          });                                                                                             // 41  // 418
      }                                                                                                   // 42  // 419
    }                                                                                                     // 43  // 420
  }                                                                                                       // 44  // 421
}                                                                                                         // 45  // 422
                                                                                                          // 46  // 423
Momentum.registerPlugin('right-to-left', sideToSide('100%', '-100%'));                                    // 47  // 424
Momentum.registerPlugin('left-to-right', sideToSide('-100%', '100%'));                                    // 48  // 425
                                                                                                          // 49  // 426
                                                                                                          // 50  // 427
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 428
                                                                                                                 // 429
}).call(this);                                                                                                   // 430
                                                                                                                 // 431
                                                                                                                 // 432
                                                                                                                 // 433
                                                                                                                 // 434
                                                                                                                 // 435
                                                                                                                 // 436
(function () {                                                                                                   // 437
                                                                                                                 // 438
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 439
//                                                                                                        //     // 440
// packages/percolate:momentum/plugins/slide-height.js                                                    //     // 441
//                                                                                                        //     // 442
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 443
                                                                                                          //     // 444
Momentum.registerPlugin('slide-height', function(options) {                                               // 1   // 445
  options = _.extend({                                                                                    // 2   // 446
    duration: 500,                                                                                        // 3   // 447
    easing: 'ease-in-out'                                                                                 // 4   // 448
  }, options);                                                                                            // 5   // 449
                                                                                                          // 6   // 450
  return {                                                                                                // 7   // 451
    insertElement: function(node, next, done) {                                                           // 8   // 452
      var $node = $(node);                                                                                // 9   // 453
                                                                                                          // 10  // 454
      $node                                                                                               // 11  // 455
        .insertBefore(next)                                                                               // 12  // 456
        .css('height', $node.height()) // set explicit height                                             // 13  // 457
        .velocity('slideDown', {                                                                          // 14  // 458
          easing: options.easing,                                                                         // 15  // 459
          duration: options.duration,                                                                     // 16  // 460
          queue: false,                                                                                   // 17  // 461
          complete: function() {                                                                          // 18  // 462
            $node.css('height', ''); // remove explicit height                                            // 19  // 463
            done();                                                                                       // 20  // 464
          }                                                                                               // 21  // 465
        });                                                                                               // 22  // 466
    },                                                                                                    // 23  // 467
    removeElement: function(node, done) {                                                                 // 24  // 468
      var $node = $(node);                                                                                // 25  // 469
                                                                                                          // 26  // 470
      $node.velocity('slideUp', {                                                                         // 27  // 471
        easing: options.easing,                                                                           // 28  // 472
        duration: options.duration,                                                                       // 29  // 473
        complete: function() {                                                                            // 30  // 474
          $node.remove();                                                                                 // 31  // 475
          done();                                                                                         // 32  // 476
        }                                                                                                 // 33  // 477
      });                                                                                                 // 34  // 478
    }                                                                                                     // 35  // 479
  }                                                                                                       // 36  // 480
});                                                                                                       // 37  // 481
                                                                                                          // 38  // 482
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 483
                                                                                                                 // 484
}).call(this);                                                                                                   // 485
                                                                                                                 // 486
                                                                                                                 // 487
                                                                                                                 // 488
                                                                                                                 // 489
                                                                                                                 // 490
                                                                                                                 // 491
(function () {                                                                                                   // 492
                                                                                                                 // 493
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 494
//                                                                                                        //     // 495
// packages/percolate:momentum/plugins/fade.js                                                            //     // 496
//                                                                                                        //     // 497
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 498
                                                                                                          //     // 499
Momentum.registerPlugin('fade', function(options) {                                                       // 1   // 500
  return {                                                                                                // 2   // 501
    insertElement: function(node, next) {                                                                 // 3   // 502
      $(node)                                                                                             // 4   // 503
        .hide()                                                                                           // 5   // 504
        .insertBefore(next)                                                                               // 6   // 505
        .velocity('fadeIn');                                                                              // 7   // 506
    },                                                                                                    // 8   // 507
    removeElement: function(node) {                                                                       // 9   // 508
      $(node).velocity('fadeOut', function() {                                                            // 10  // 509
        $(this).remove();                                                                                 // 11  // 510
      });                                                                                                 // 12  // 511
    }                                                                                                     // 13  // 512
  }                                                                                                       // 14  // 513
});                                                                                                       // 15  // 514
////////////////////////////////////////////////////////////////////////////////////////////////////////////     // 515
                                                                                                                 // 516
}).call(this);                                                                                                   // 517
                                                                                                                 // 518
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
(function (pkg, symbols) {
  for (var s in symbols)
    (s in pkg) || (pkg[s] = symbols[s]);
})(Package['percolate:momentum'] = {}, {
  Momentum: Momentum
});

})();
