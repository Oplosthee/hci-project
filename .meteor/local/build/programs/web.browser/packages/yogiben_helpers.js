(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Template = Package.templating.Template;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var Spacebars = Package.spacebars.Spacebars;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

/////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                         //
// packages/yogiben_helpers/packages/yogiben_helpers.js                                    //
//                                                                                         //
/////////////////////////////////////////////////////////////////////////////////////////////
                                                                                           //
(function () {                                                                             // 1
                                                                                           // 2
//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// packages/yogiben:helpers/lib/client/helpers.coffee.js                                //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.registerHelper('log', function(value) {                                           // 10
  console.log(value);                                                                      // 11
  return value;                                                                            // 12
});                                                                                        // 13
                                                                                           // 14
Template.registerHelper('Users', function() {                                              // 15
  return Meteor.users;                                                                     // 16
});                                                                                        // 17
                                                                                           // 18
Template.registerHelper('User', function() {                                               // 19
  return Meteor.user();                                                                    // 20
});                                                                                        // 21
                                                                                           // 22
Template.registerHelper('getUser', function(_id) {                                         // 23
  if (Meteor.users) {                                                                      // 24
    return Meteor.users.findOne(_id);                                                      // 25
  }                                                                                        // 26
});                                                                                        // 27
                                                                                           // 28
Template.registerHelper('getDoc', function(_id, collection) {                              // 29
  if (window[collection].findOne(_id)) {                                                   // 30
    return window[collection].findOne(_id);                                                // 31
  }                                                                                        // 32
});                                                                                        // 33
                                                                                           // 34
Template.registerHelper('parseLinks', function(inputText) {                                // 35
  var emailAddressPattern, pseudoUrlPattern, urlPattern;                                   // 36
  urlPattern = /\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/g;    // 37
  pseudoUrlPattern = /(^|[^\/])(www\.[\S]+(\b|$))/g;                                       // 38
  emailAddressPattern = /\w+@[a-zA-Z_]+?(?:\.[a-zA-Z]{2,6})+/g;                            // 39
  return inputText.replace(urlPattern, "<a target='_blank' href=\"$&\">$&</a>").replace(pseudoUrlPattern, "$1<a target='_blank' href=\"http://$2\">$2</a>").replace(emailAddressPattern, "<a target='_blank' href=\"mailto:$&\">$&</a>");
});                                                                                        // 41
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                           // 43
}).call(this);                                                                             // 44
                                                                                           // 45
/////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['yogiben:helpers'] = {};

})();
