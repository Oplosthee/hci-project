//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Template = Package.templating.Template;
var ReactiveVar = Package['reactive-var'].ReactiveVar;
var AutoForm = Package['aldeed:autoform'].AutoForm;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var Spacebars = Package.spacebars.Spacebars;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                     //
// packages/yogiben_autoform-map/lib/client/template.autoform-map.js                                   //
//                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                       //
                                                                                                       // 1
Template.__checkName("afMap");                                                                         // 2
Template["afMap"] = new Template("Template.afMap", (function() {                                       // 3
  var view = this;                                                                                     // 4
  return HTML.DIV({                                                                                    // 5
    "data-schema-key": function() {                                                                    // 6
      return Spacebars.mustache(view.lookup("schemaKey"));                                             // 7
    },                                                                                                 // 8
    reverse: function() {                                                                              // 9
      return Spacebars.mustache(Spacebars.dot(view.lookup("atts"), "reverse"));                        // 10
    }                                                                                                  // 11
  }, HTML.Raw('\n		<input type="text" class="js-lat" style="display: none">\n		<input type="text" class="js-lng" style="display: none">\n		<input type="text" class="controls af-map-search-box js-search af-map-search-box-hidden">\n		'), Blaze.If(function() {
    return Spacebars.call(Spacebars.dot(view.lookup("atts"), "geolocation"));                          // 13
  }, function() {                                                                                      // 14
    return [ "\n		", HTML.A({                                                                          // 15
      style: "margin-bottom:5px;",                                                                     // 16
      "class": "btn btn-default js-locate"                                                             // 17
    }, "\n			", HTML.I({                                                                               // 18
      "class": function() {                                                                            // 19
        return [ "fa ", Blaze.If(function() {                                                          // 20
          return Spacebars.call(view.lookup("loading"));                                               // 21
        }, function() {                                                                                // 22
          return "fa-spin fa-circle-o-notch";                                                          // 23
        }, function() {                                                                                // 24
          return "fa-map-marker";                                                                      // 25
        }) ];                                                                                          // 26
      }                                                                                                // 27
    }), " My location\n		"), "\n		" ];                                                                 // 28
  }), "\n		", HTML.DIV({                                                                               // 29
    "class": "js-map",                                                                                 // 30
    style: function() {                                                                                // 31
      return [ "width:", Spacebars.mustache(view.lookup("width")), "; height:", Spacebars.mustache(view.lookup("height")), ";" ];
    }                                                                                                  // 33
  }), "\n	");                                                                                          // 34
}));                                                                                                   // 35
                                                                                                       // 36
/////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                     //
// packages/yogiben_autoform-map/lib/client/autoform-map.coffee.js                                     //
//                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var KEY_ENTER, defaults, initTemplateAndGoogleMaps;                                                    // 1
                                                                                                       //
KEY_ENTER = 13;                                                                                        // 1
                                                                                                       //
defaults = {                                                                                           // 1
  mapType: 'roadmap',                                                                                  // 4
  defaultLat: 1,                                                                                       // 4
  defaultLng: 1,                                                                                       // 4
  geolocation: false,                                                                                  // 4
  searchBox: false,                                                                                    // 4
  autolocate: false,                                                                                   // 4
  zoom: 1                                                                                              // 4
};                                                                                                     //
                                                                                                       //
AutoForm.addInputType('map', {                                                                         // 1
  template: 'afMap',                                                                                   // 13
  valueOut: function() {                                                                               // 13
    var lat, lng, node;                                                                                // 15
    node = $(this.context);                                                                            // 15
    lat = node.find('.js-lat').val();                                                                  // 15
    lng = node.find('.js-lng').val();                                                                  // 15
    if (lat.length > 0 && lng.length > 0) {                                                            // 20
      return {                                                                                         //
        lat: lat,                                                                                      // 21
        lng: lng                                                                                       // 21
      };                                                                                               //
    }                                                                                                  //
  },                                                                                                   //
  contextAdjust: function(ctx) {                                                                       // 13
    ctx.loading = new ReactiveVar(false);                                                              // 24
    return ctx;                                                                                        //
  },                                                                                                   //
  valueConverters: {                                                                                   // 13
    string: function(value) {                                                                          // 27
      if (this.attr('reverse')) {                                                                      // 28
        return value.lng + "," + value.lat;                                                            //
      } else {                                                                                         //
        return value.lat + "," + value.lng;                                                            //
      }                                                                                                //
    },                                                                                                 //
    numberArray: function(value) {                                                                     // 27
      return [value.lng, value.lat];                                                                   //
    }                                                                                                  //
  }                                                                                                    //
});                                                                                                    //
                                                                                                       //
Template.afMap.created = function() {                                                                  // 1
  this.mapReady = new ReactiveVar(false);                                                              // 36
  GoogleMaps.load({                                                                                    // 36
    libraries: 'places'                                                                                // 37
  });                                                                                                  //
  this._stopInterceptValue = false;                                                                    // 36
  return this._interceptValue = function(ctx) {                                                        //
    var location, t;                                                                                   // 41
    t = Template.instance();                                                                           // 41
    if (t.mapReady.get() && ctx.value && !t._stopInterceptValue) {                                     // 42
      location = typeof ctx.value === 'string' ? ctx.value.split(',') : ctx.value.hasOwnProperty('lat') ? [ctx.value.lat, ctx.value.lng] : [ctx.value[1], ctx.value[0]];
      location = new google.maps.LatLng(parseFloat(location[0]), parseFloat(location[1]));             // 43
      t.setMarker(t.map, location, t.options.zoom);                                                    // 43
      t.map.setCenter(location);                                                                       // 43
      return t._stopInterceptValue = true;                                                             //
    }                                                                                                  //
  };                                                                                                   //
};                                                                                                     // 35
                                                                                                       //
initTemplateAndGoogleMaps = function() {                                                               // 1
  var input, mapOptions, searchBox;                                                                    // 50
  this.options = _.extend({}, defaults, this.data.atts);                                               // 50
  this.data.marker = void 0;                                                                           // 50
  this.setMarker = (function(_this) {                                                                  // 50
    return function(map, location, zoom) {                                                             //
      if (zoom == null) {                                                                              //
        zoom = 0;                                                                                      //
      }                                                                                                //
      _this.$('.js-lat').val(location.lat());                                                          // 54
      _this.$('.js-lng').val(location.lng());                                                          // 54
      if (_this.data.marker) {                                                                         // 57
        _this.data.marker.setMap(null);                                                                // 57
      }                                                                                                //
      _this.data.marker = new google.maps.Marker({                                                     // 54
        position: location,                                                                            // 59
        map: map                                                                                       // 59
      });                                                                                              //
      if (zoom > 0) {                                                                                  // 62
        return _this.map.setZoom(zoom);                                                                //
      }                                                                                                //
    };                                                                                                 //
  })(this);                                                                                            //
  mapOptions = {                                                                                       // 50
    zoom: 0,                                                                                           // 66
    mapTypeId: google.maps.MapTypeId[this.options.mapType],                                            // 66
    streetViewControl: false                                                                           // 66
  };                                                                                                   //
  if (this.data.atts.googleMap) {                                                                      // 70
    _.extend(mapOptions, this.data.atts.googleMap);                                                    // 71
  }                                                                                                    //
  this.map = new google.maps.Map(this.find('.js-map'), mapOptions);                                    // 50
  this.map.setCenter(new google.maps.LatLng(this.options.defaultLat, this.options.defaultLng));        // 50
  this.map.setZoom(this.options.zoom);                                                                 // 50
  if (this.data.atts.searchBox) {                                                                      // 78
    input = this.find('.js-search');                                                                   // 79
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);                               // 79
    searchBox = new google.maps.places.SearchBox(input);                                               // 79
    google.maps.event.addListener(searchBox, 'places_changed', (function(_this) {                      // 79
      return function() {                                                                              //
        var location;                                                                                  // 85
        location = searchBox.getPlaces()[0].geometry.location;                                         // 85
        _this.setMarker(_this.map, location);                                                          // 85
        return _this.map.setCenter(location);                                                          //
      };                                                                                               //
    })(this));                                                                                         //
    $(input).removeClass('af-map-search-box-hidden');                                                  // 79
  }                                                                                                    //
  if (this.data.atts.autolocate && navigator.geolocation && !this.data.value) {                        // 91
    navigator.geolocation.getCurrentPosition((function(_this) {                                        // 92
      return function(position) {                                                                      //
        var location;                                                                                  // 93
        location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);        // 93
        _this.setMarker(_this.map, location, _this.options.zoom);                                      // 93
        return _this.map.setCenter(location);                                                          //
      };                                                                                               //
    })(this));                                                                                         //
  }                                                                                                    //
  if (typeof this.data.atts.rendered === 'function') {                                                 // 97
    this.data.atts.rendered(this.map);                                                                 // 98
  }                                                                                                    //
  google.maps.event.addListener(this.map, 'click', (function(_this) {                                  // 50
    return function(e) {                                                                               //
      return _this.setMarker(_this.map, e.latLng);                                                     //
    };                                                                                                 //
  })(this));                                                                                           //
  this.$('.js-map').closest('form').on('reset', (function(_this) {                                     // 50
    return function() {                                                                                //
      var ref;                                                                                         // 104
      _this.data.marker && _this.data.marker.setMap(null);                                             // 104
      _this.map.setCenter(new google.maps.LatLng(_this.options.defaultLat, _this.options.defaultLng));
      return _this.map.setZoom(((ref = _this.options) != null ? ref.zoom : void 0) || 0);              //
    };                                                                                                 //
  })(this));                                                                                           //
  return this.mapReady.set(true);                                                                      //
};                                                                                                     // 49
                                                                                                       //
Template.afMap.rendered = function() {                                                                 // 1
  return this.autorun((function(_this) {                                                               //
    return function() {                                                                                //
      return GoogleMaps.loaded() && initTemplateAndGoogleMaps.apply(_this);                            //
    };                                                                                                 //
  })(this));                                                                                           //
};                                                                                                     // 110
                                                                                                       //
Template.afMap.helpers({                                                                               // 1
  schemaKey: function() {                                                                              // 115
    Template.instance()._interceptValue(this);                                                         // 116
    return this.atts['data-schema-key'];                                                               //
  },                                                                                                   //
  width: function() {                                                                                  // 115
    if (typeof this.atts.width === 'string') {                                                         // 119
      return this.atts.width;                                                                          //
    } else if (typeof this.atts.width === 'number') {                                                  //
      return this.atts.width + 'px';                                                                   //
    } else {                                                                                           //
      return '100%';                                                                                   //
    }                                                                                                  //
  },                                                                                                   //
  height: function() {                                                                                 // 115
    if (typeof this.atts.height === 'string') {                                                        // 126
      return this.atts.height;                                                                         //
    } else if (typeof this.atts.height === 'number') {                                                 //
      return this.atts.height + 'px';                                                                  //
    } else {                                                                                           //
      return '200px';                                                                                  //
    }                                                                                                  //
  },                                                                                                   //
  loading: function() {                                                                                // 115
    return this.loading.get();                                                                         //
  }                                                                                                    //
});                                                                                                    //
                                                                                                       //
Template.afMap.events({                                                                                // 1
  'click .js-locate': function(e, t) {                                                                 // 136
    e.preventDefault();                                                                                // 137
    if (!navigator.geolocation) {                                                                      // 139
      return false;                                                                                    // 139
    }                                                                                                  //
    this.loading.set(true);                                                                            // 137
    return navigator.geolocation.getCurrentPosition((function(_this) {                                 //
      return function(position) {                                                                      //
        var location;                                                                                  // 143
        location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);        // 143
        _this.setMarker(_this.map, location, _this.options.zoom);                                      // 143
        _this.map.setCenter(location);                                                                 // 143
        return _this.loading.set(false);                                                               //
      };                                                                                               //
    })(this));                                                                                         //
  },                                                                                                   //
  'keydown .js-search': function(e) {                                                                  // 136
    if (e.keyCode === KEY_ENTER) {                                                                     // 149
      return e.preventDefault();                                                                       //
    }                                                                                                  //
  }                                                                                                    //
});                                                                                                    //
                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['yogiben:autoform-map'] = {};

})();
