//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Template = Package.templating.Template;
var AutoForm = Package['aldeed:autoform'].AutoForm;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var Spacebars = Package.spacebars.Spacebars;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                         //
// packages/favorites/lib/both/collections.coffee.js                                                       //
//                                                                                                         //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                           //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var FavoritesSchemas;                                                                                      // 1
                                                                                                           //
this.Favorites = new Meteor.Collection('favorites');                                                       // 1
                                                                                                           //
FavoritesSchemas = new SimpleSchema({                                                                      // 1
  doc: {                                                                                                   // 4
    type: String,                                                                                          // 5
    regEx: SimpleSchema.RegEx.Id                                                                           // 5
  },                                                                                                       //
  owner: {                                                                                                 // 4
    type: String,                                                                                          // 9
    autoValue: function() {                                                                                // 9
      if (this.isInsert) {                                                                                 // 11
        return Meteor.userId();                                                                            //
      }                                                                                                    //
    }                                                                                                      //
  },                                                                                                       //
  createdAt: {                                                                                             // 4
    type: Date,                                                                                            // 15
    autoValue: function() {                                                                                // 15
      if (this.isInsert) {                                                                                 // 17
        return new Date();                                                                                 //
      }                                                                                                    //
    }                                                                                                      //
  }                                                                                                        //
});                                                                                                        //
                                                                                                           //
Favorites.attachSchema(FavoritesSchemas);                                                                  // 1
                                                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                         //
// packages/favorites/lib/client/template.templates.js                                                     //
//                                                                                                         //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                           //
                                                                                                           // 1
Template.__checkName("favoriteButton");                                                                    // 2
Template["favoriteButton"] = new Template("Template.favoriteButton", (function() {                         // 3
  var view = this;                                                                                         // 4
  return Blaze.If(function() {                                                                             // 5
    return Spacebars.dataMustache(view.lookup("isFavorite"), view.lookup("_id"));                          // 6
  }, function() {                                                                                          // 7
    return [ "\n	", Blaze._TemplateWith(function() {                                                       // 8
      return {                                                                                             // 9
        _id: Spacebars.call(view.lookup("_id"))                                                            // 10
      };                                                                                                   // 11
    }, function() {                                                                                        // 12
      return Spacebars.include(view.lookupTemplate("favoriteButtonFavorited"));                            // 13
    }), "\n	" ];                                                                                           // 14
  }, function() {                                                                                          // 15
    return [ "\n	", Spacebars.include(view.lookupTemplate("favoriteButtonNotFavorited")), "\n	" ];         // 16
  });                                                                                                      // 17
}));                                                                                                       // 18
                                                                                                           // 19
Template.__checkName("favoriteButtonFavorited");                                                           // 20
Template["favoriteButtonFavorited"] = new Template("Template.favoriteButtonFavorited", (function() {       // 21
  var view = this;                                                                                         // 22
  return HTML.A({                                                                                          // 23
    "class": "btn btn-primary btn-sm js-favorite-button",                                                  // 24
    doc: function() {                                                                                      // 25
      return Spacebars.mustache(view.lookup("_id"));                                                       // 26
    }                                                                                                      // 27
  }, HTML.Raw('<i class="fa fa-star"></i> Favorite'));                                                     // 28
}));                                                                                                       // 29
                                                                                                           // 30
Template.__checkName("favoriteButtonNotFavorited");                                                        // 31
Template["favoriteButtonNotFavorited"] = new Template("Template.favoriteButtonNotFavorited", (function() {
  var view = this;                                                                                         // 33
  return HTML.A({                                                                                          // 34
    "class": "btn btn-default btn-sm js-favorite-button",                                                  // 35
    doc: function() {                                                                                      // 36
      return Spacebars.mustache(view.lookup("_id"));                                                       // 37
    }                                                                                                      // 38
  }, HTML.Raw('<i class="fa fa-star-o"></i> Favorite'));                                                   // 39
}));                                                                                                       // 40
                                                                                                           // 41
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                         //
// packages/favorites/lib/client/templates.coffee.js                                                       //
//                                                                                                         //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                           //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.favoriteButton.helpers({                                                                          // 1
  isFavorite: function(_id) {                                                                              // 2
    return Favorites.findOne({                                                                             //
      doc: _id,                                                                                            // 4
      owner: Meteor.userId()                                                                               // 4
    });                                                                                                    //
  }                                                                                                        //
});                                                                                                        //
                                                                                                           //
Template.favoriteButtonNotFavorited.events({                                                               // 1
  'click .js-favorite-button': function(e, t) {                                                            // 8
    return Favorites.insert({                                                                              //
      doc: $(e.currentTarget).attr('doc'),                                                                 // 10
      owner: Meteor.userId()                                                                               // 10
    });                                                                                                    //
  }                                                                                                        //
});                                                                                                        //
                                                                                                           //
Template.favoriteButtonFavorited.events({                                                                  // 1
  'click .js-favorite-button': function(e, t) {                                                            // 14
    var favorite;                                                                                          // 15
    favorite = Favorites.findOne({                                                                         // 15
      owner: Meteor.userId(),                                                                              // 16
      doc: $(e.currentTarget).attr('doc')                                                                  // 16
    });                                                                                                    //
    return Favorites.remove({                                                                              //
      _id: favorite._id                                                                                    // 19
    });                                                                                                    //
  }                                                                                                        //
});                                                                                                        //
                                                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                         //
// packages/favorites/lib/client/helpers.coffee.js                                                         //
//                                                                                                         //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                           //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.registerHelper('favoritesByDoc', function(_id) {                                                  // 1
  return Favorites.find({                                                                                  //
    doc: _id                                                                                               // 2
  });                                                                                                      //
});                                                                                                        // 1
                                                                                                           //
Template.registerHelper('myFavorites', function() {                                                        // 1
  return Favorites.find({                                                                                  //
    owner: Meteor.userId()                                                                                 // 5
  });                                                                                                      //
});                                                                                                        // 4
                                                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.favorites = {};

})();
