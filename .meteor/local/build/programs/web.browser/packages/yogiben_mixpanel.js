(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;

(function(){

////////////////////////////////////////////////////////////////////////////////////////
//                                                                                    //
// packages/yogiben_mixpanel/client/mixpanel-loader.js                                //
//                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////
                                                                                      //
(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

////////////////////////////////////////////////////////////////////////////////////////
//                                                                                    //
// packages/yogiben_mixpanel/client/mixpanel.js                                       //
//                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////
                                                                                      //
Meteor.startup(function() {                                                           // 1
  if (Meteor.settings && Meteor.settings.public && Meteor.settings.public.mixpanel) {
                                                                                      // 3
    var mp = Meteor.settings.public.mixpanel;                                         // 4
                                                                                      // 5
    // Allows you to send events                                                      // 6
    if (mp.token) {                                                                   // 7
      mixpanel.init(mp.token)                                                         // 8
    }                                                                                 // 9
  } else {                                                                            // 10
    // mixpanel.track will cause an error as it is undefined unless                   // 11
    // the mixpanel.init is called with the right key                                 // 12
    mixpanel.track = function() {                                                     // 13
      return;                                                                         // 14
    }                                                                                 // 15
    mixpanel.identify = function() {                                                  // 16
      return;                                                                         // 17
    }                                                                                 // 18
    mixpanel.people = {                                                               // 19
      set: function(){                                                                // 20
        return;                                                                       // 21
      }                                                                               // 22
    }                                                                                 // 23
  }                                                                                   // 24
})                                                                                    // 25
                                                                                      // 26
////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['yogiben:mixpanel'] = {};

})();
