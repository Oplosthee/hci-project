//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Template = Package.templating.Template;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var Spacebars = Package.spacebars.Spacebars;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                       //
// packages/user-thumbs/lib/client/template.templates.js                                                 //
//                                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                         //
                                                                                                         // 1
Template.__checkName("profileThumb");                                                                    // 2
Template["profileThumb"] = new Template("Template.profileThumb", (function() {                           // 3
  var view = this;                                                                                       // 4
  return HTML.DIV({                                                                                      // 5
    "class": "profile-thumb"                                                                             // 6
  }, "\n		", Blaze.If(function() {                                                                       // 7
    return Spacebars.dataMustache(view.lookup("profileThumbSrc"), view.lookup("_id"));                   // 8
  }, function() {                                                                                        // 9
    return [ "\n			", HTML.DIV({                                                                         // 10
      "class": "profile-thumb-img"                                                                       // 11
    }, "\n				", HTML.IMG({                                                                              // 12
      "class": "media-object img-thumbnail img-circle",                                                  // 13
      src: function() {                                                                                  // 14
        return Spacebars.mustache(view.lookup("profileThumbSrc"), view.lookup("_id"));                   // 15
      },                                                                                                 // 16
      alt: "Image"                                                                                       // 17
    }), "\n			"), "\n		" ];                                                                              // 18
  }, function() {                                                                                        // 19
    return [ "\n			", Spacebars.With(function() {                                                        // 20
      return Spacebars.dataMustache(view.lookup("profileThumbInitial"), view.lookup("_id"));             // 21
    }, function() {                                                                                      // 22
      return [ "\n				", HTML.DIV({                                                                      // 23
        "class": "profile-thumb-initial"                                                                 // 24
      }, "\n					", HTML.DIV({                                                                           // 25
        style: function() {                                                                              // 26
          return [ "color:", Spacebars.mustache(view.lookup("color")), ";background-color:", Spacebars.mustache(view.lookup("backgroundColor")) ];
        }                                                                                                // 28
      }, Blaze.View("lookup:html", function() {                                                          // 29
        return Spacebars.makeRaw(Spacebars.mustache(view.lookup("html")));                               // 30
      })), "\n				"), "\n			" ];                                                                         // 31
    }), "\n		" ];                                                                                        // 32
  }), "\n	");                                                                                            // 33
}));                                                                                                     // 34
                                                                                                         // 35
///////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                       //
// packages/user-thumbs/lib/client/templates.coffee.js                                                   //
//                                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                         //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var getUserColor, getUserInitial;                                                                        // 1
                                                                                                         //
getUserInitial = function(user) {                                                                        // 1
  if (!user) {                                                                                           // 2
    return '<i class="fa fa-user"></i>';                                                                 //
  } else {                                                                                               //
    if (user.username) {                                                                                 // 5
      return user.username.charAt(0).toUpperCase();                                                      //
    } else if (user.profile && user.profile.firstName) {                                                 //
      return user.profile.firstName.charAt(0).toUpperCase();                                             //
    } else if (user.emails[0].address) {                                                                 //
      return user.emails[0].address.charAt(0).toUpperCase();                                             //
    } else {                                                                                             //
      return '<i class="fa fa-user"></i>';                                                               //
    }                                                                                                    //
  }                                                                                                      //
};                                                                                                       // 1
                                                                                                         //
getUserColor = function(_id) {                                                                           // 1
  var index;                                                                                             // 15
  if (_id) {                                                                                             // 15
    index = _id.charCodeAt(0) - 48;                                                                      // 16
    return UserHelpers.colorPalette[index];                                                              //
  }                                                                                                      //
};                                                                                                       // 14
                                                                                                         //
Template.profileThumb.helpers({                                                                          // 1
  profileThumbInitial: function(_id) {                                                                   // 20
    var html, user;                                                                                      // 21
    if (typeof Meteor.users !== 'undefined') {                                                           // 21
      user = Meteor.users.findOne({                                                                      // 22
        _id: _id                                                                                         // 22
      });                                                                                                //
      html = getUserInitial(user);                                                                       // 22
      return {                                                                                           //
        html: html,                                                                                      // 24
        color: 'white',                                                                                  // 24
        backgroundColor: getUserColor(_id)                                                               // 24
      };                                                                                                 //
    }                                                                                                    //
  }                                                                                                      //
});                                                                                                      //
                                                                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                       //
// packages/user-thumbs/lib/client/helpers.coffee.js                                                     //
//                                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                         //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.registerHelper('niceName', function(_id) {                                                      // 1
  var user;                                                                                              // 2
  if (_id) {                                                                                             // 2
    user = Meteor.users.findOne(_id);                                                                    // 3
  }                                                                                                      //
  if (user) {                                                                                            // 5
    if (user.username) {                                                                                 // 6
      return user.username;                                                                              //
    } else if (typeof user.profile !== 'undefined' && user.profile.firstName) {                          //
      return user.profile.firstName;                                                                     //
    } else if (user.emails[0].address) {                                                                 //
      return user.emails[0].address;                                                                     //
    } else {                                                                                             //
      return 'A user';                                                                                   //
    }                                                                                                    //
  }                                                                                                      //
});                                                                                                      // 1
                                                                                                         //
Template.registerHelper('profileThumbSrc', function(_id) {                                               // 1
  var picture, user;                                                                                     // 16
  if (typeof Meteor.users !== 'undefined') {                                                             // 16
    if (Meteor.users.findOne(_id)) {                                                                     // 17
      user = Meteor.users.findOne({                                                                      // 18
        _id: _id                                                                                         // 18
      });                                                                                                //
      if (typeof user.profile !== 'undefined' && typeof user.profile.picture !== 'undefined') {          // 19
        picture = user.profile.picture;                                                                  // 20
        if (picture.indexOf('/') > -1) {                                                                 // 22
          return picture;                                                                                //
        } else {                                                                                         //
          if (typeof ProfilePictures !== 'undefined' && ProfilePictures.findOne(user.profile.picture)) {
            picture = ProfilePictures.findOne(picture);                                                  // 26
            return picture.url({                                                                         //
              store: 'thumbs'                                                                            // 27
            });                                                                                          //
          }                                                                                              //
        }                                                                                                //
      }                                                                                                  //
    }                                                                                                    //
  }                                                                                                      //
});                                                                                                      // 15
                                                                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                       //
// packages/user-thumbs/lib/client/utils.coffee.js                                                       //
//                                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                         //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.UserHelpers = {};                                                                                   // 1
                                                                                                         //
UserHelpers.colorPalette = ['#661e1e', '#66261e', '#662d1e', '#66351e', '#663c1e', '#66441e', '#664b1e', '#66531e', '#665a1e', '#66621e', '#62661e', '#5a661e', '#53661e', '#4b661e', '#44661e', '#3c661e', '#35661e', '#2d661e', '#26661e', '#1e661e', '#1e6625', '#1e662d', '#1e6634', '#1e663c', '#1e6643', '#1e664b', '#1e6652', '#1e665a', '#1e6661', '#1e6266', '#1e5b66', '#1e5366', '#1e4c66', '#1e4466', '#1e3d66', '#1e3566', '#1e2e66', '#1e2666', '#1e1f66', '#251e66', '#2c1e66', '#341e66', '#3b1e66', '#431e66', '#4a1e66', '#521e66', '#591e66', '#611e66', '#661e63', '#661e5b', '#661e54', '#661e4c', '#661e45', '#661e3d', '#661e36', '#661e2e', '#661e27', '#661e1f', '#66251e', '#662c1e', '#66341e', '#663b1e', '#66431e', '#664a1e', '#66521e', '#66591e', '#66611e', '#63661e', '#5c661e', '#54661e', '#4d661e', '#45661e', '#3e661e', '#36661e'];
                                                                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['user-thumbs'] = {};

})();
