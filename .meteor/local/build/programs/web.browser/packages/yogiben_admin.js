//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Router = Package['iron:router'].Router;
var RouteController = Package['iron:router'].RouteController;
var _ = Package.underscore._;
var ReactiveVar = Package['reactive-var'].ReactiveVar;
var check = Package.check.check;
var Match = Package.check.Match;
var AutoForm = Package['aldeed:autoform'].AutoForm;
var Roles = Package['alanning:roles'].Roles;
var Helpers = Package['raix:handlebar-helpers'].Helpers;
var moment = Package['momentjs:moment'].moment;
var Tabular = Package['aldeed:tabular'].Tabular;
var ActiveRoute = Package['zimme:active-route'].ActiveRoute;
var Session = Package.session.Session;
var $ = Package.jquery.$;
var jQuery = Package.jquery.jQuery;
var Template = Package.templating.Template;
var Iron = Package['iron:core'].Iron;
var Collection2 = Package['aldeed:collection2-core'].Collection2;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var Spacebars = Package.spacebars.Spacebars;
var Mongo = Package.mongo.Mongo;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var __coffeescriptShare, AdminDashboard, dataTableOptions, t, pageY, currTop;

(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/both/AdminDashboard.coffee.js                                                            //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
                                                                                                                       // 1
                                                                                                                       //
AdminDashboard = {                                                                                                     // 1
  schemas: {},                                                                                                         // 2
  sidebarItems: [],                                                                                                    // 2
  collectionItems: [],                                                                                                 // 2
  alertSuccess: function(message) {                                                                                    // 2
    return Session.set('adminSuccess', message);                                                                       //
  },                                                                                                                   //
  alertFailure: function(message) {                                                                                    // 2
    return Session.set('adminError', message);                                                                         //
  },                                                                                                                   //
  checkAdmin: function() {                                                                                             // 2
    if (!Roles.userIsInRole(Meteor.userId(), ['admin'])) {                                                             // 11
      Meteor.call('adminCheckAdmin');                                                                                  // 12
      if (typeof (typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.nonAdminRedirectRoute : void 0) === "string") {
        Router.go(AdminConfig.nonAdminRedirectRoute);                                                                  // 14
      }                                                                                                                //
    }                                                                                                                  //
    if (typeof this.next === 'function') {                                                                             // 15
      return this.next();                                                                                              //
    }                                                                                                                  //
  },                                                                                                                   //
  adminRoutes: ['adminDashboard', 'adminDashboardUsersNew', 'adminDashboardUsersEdit', 'adminDashboardView', 'adminDashboardNew', 'adminDashboardEdit'],
  collectionLabel: function(collection) {                                                                              // 2
    if (collection === 'Users') {                                                                                      // 19
      return 'Users';                                                                                                  //
    } else if ((collection != null) && typeof AdminConfig.collections[collection].label === 'string') {                //
      return AdminConfig.collections[collection].label;                                                                //
    } else {                                                                                                           //
      return Session.get('admin_collection_name');                                                                     //
    }                                                                                                                  //
  },                                                                                                                   //
  addSidebarItem: function(title, url, options) {                                                                      // 2
    var item;                                                                                                          // 26
    item = {                                                                                                           // 26
      title: title                                                                                                     // 26
    };                                                                                                                 //
    if (_.isObject(url) && typeof options === 'undefined') {                                                           // 27
      item.options = url;                                                                                              // 28
    } else {                                                                                                           //
      item.url = url;                                                                                                  // 30
      item.options = options;                                                                                          // 30
    }                                                                                                                  //
    return this.sidebarItems.push(item);                                                                               //
  },                                                                                                                   //
  extendSidebarItem: function(title, urls) {                                                                           // 2
    var existing;                                                                                                      // 36
    if (_.isObject(urls)) {                                                                                            // 36
      urls = [urls];                                                                                                   // 36
    }                                                                                                                  //
    existing = _.find(this.sidebarItems, function(item) {                                                              // 36
      return item.title === title;                                                                                     //
    });                                                                                                                //
    if (existing) {                                                                                                    // 39
      return existing.options.urls = _.union(existing.options.urls, urls);                                             //
    }                                                                                                                  //
  },                                                                                                                   //
  addCollectionItem: function(fn) {                                                                                    // 2
    return this.collectionItems.push(fn);                                                                              //
  },                                                                                                                   //
  path: function(s) {                                                                                                  // 2
    var path;                                                                                                          // 46
    path = '/admin';                                                                                                   // 46
    if (typeof s === 'string' && s.length > 0) {                                                                       // 47
      path += (s[0] === '/' ? '' : '/') + s;                                                                           // 48
    }                                                                                                                  //
    return path;                                                                                                       //
  }                                                                                                                    //
};                                                                                                                     //
                                                                                                                       //
AdminDashboard.schemas.newUser = new SimpleSchema({                                                                    // 1
  email: {                                                                                                             // 53
    type: String,                                                                                                      // 54
    label: "Email address"                                                                                             // 54
  },                                                                                                                   //
  chooseOwnPassword: {                                                                                                 // 53
    type: Boolean,                                                                                                     // 57
    label: 'Let this user choose their own password with an email',                                                    // 57
    defaultValue: true                                                                                                 // 57
  },                                                                                                                   //
  password: {                                                                                                          // 53
    type: String,                                                                                                      // 61
    label: 'Password',                                                                                                 // 61
    optional: true                                                                                                     // 61
  },                                                                                                                   //
  sendPassword: {                                                                                                      // 53
    type: Boolean,                                                                                                     // 65
    label: 'Send this user their password by email',                                                                   // 65
    optional: true                                                                                                     // 65
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
AdminDashboard.schemas.sendResetPasswordEmail = new SimpleSchema({                                                     // 1
  _id: {                                                                                                               // 70
    type: String                                                                                                       // 71
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
AdminDashboard.schemas.changePassword = new SimpleSchema({                                                             // 1
  _id: {                                                                                                               // 74
    type: String                                                                                                       // 75
  },                                                                                                                   //
  password: {                                                                                                          // 74
    type: String                                                                                                       // 77
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/both/router.coffee.js                                                                    //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.AdminController = RouteController.extend({                                                                        // 1
  layoutTemplate: 'AdminLayout',                                                                                       // 2
  waitOn: function() {                                                                                                 // 2
    return [Meteor.subscribe('adminUsers'), Meteor.subscribe('adminUser'), Meteor.subscribe('adminCollectionsCount')];
  },                                                                                                                   //
  onBeforeAction: function() {                                                                                         // 2
    Session.set('adminSuccess', null);                                                                                 // 10
    Session.set('adminError', null);                                                                                   // 10
    Session.set('admin_title', '');                                                                                    // 10
    Session.set('admin_subtitle', '');                                                                                 // 10
    Session.set('admin_collection_page', null);                                                                        // 10
    Session.set('admin_collection_name', null);                                                                        // 10
    Session.set('admin_id', null);                                                                                     // 10
    Session.set('admin_doc', null);                                                                                    // 10
    if (!Roles.userIsInRole(Meteor.userId(), ['admin'])) {                                                             // 20
      Meteor.call('adminCheckAdmin');                                                                                  // 21
      if (typeof (typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.nonAdminRedirectRoute : void 0) === 'string') {
        Router.go(AdminConfig.nonAdminRedirectRoute);                                                                  // 23
      }                                                                                                                //
    }                                                                                                                  //
    return this.next();                                                                                                //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Router.route("adminDashboard", {                                                                                       // 1
  path: "/admin",                                                                                                      // 29
  template: "AdminDashboard",                                                                                          // 29
  controller: "AdminController",                                                                                       // 29
  action: function() {                                                                                                 // 29
    return this.render();                                                                                              //
  },                                                                                                                   //
  onAfterAction: function() {                                                                                          // 29
    Session.set('admin_title', 'Dashboard');                                                                           // 35
    Session.set('admin_collection_name', '');                                                                          // 35
    return Session.set('admin_collection_page', '');                                                                   //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Router.route("adminDashboardUsersView", {                                                                              // 1
  path: "/admin/Users",                                                                                                // 40
  template: "AdminDashboardView",                                                                                      // 40
  controller: "AdminController",                                                                                       // 40
  action: function() {                                                                                                 // 40
    return this.render();                                                                                              //
  },                                                                                                                   //
  data: function() {                                                                                                   // 40
    return {                                                                                                           //
      admin_table: AdminTables.Users                                                                                   // 46
    };                                                                                                                 //
  },                                                                                                                   //
  onAfterAction: function() {                                                                                          // 40
    Session.set('admin_title', 'Users');                                                                               // 48
    Session.set('admin_subtitle', 'View');                                                                             // 48
    return Session.set('admin_collection_name', 'Users');                                                              //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Router.route("adminDashboardUsersNew", {                                                                               // 1
  path: "/admin/Users/new",                                                                                            // 53
  template: "AdminDashboardUsersNew",                                                                                  // 53
  controller: 'AdminController',                                                                                       // 53
  action: function() {                                                                                                 // 53
    return this.render();                                                                                              //
  },                                                                                                                   //
  onAfterAction: function() {                                                                                          // 53
    Session.set('admin_title', 'Users');                                                                               // 59
    Session.set('admin_subtitle', 'Create new user');                                                                  // 59
    Session.set('admin_collection_page', 'New');                                                                       // 59
    return Session.set('admin_collection_name', 'Users');                                                              //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Router.route("adminDashboardUsersEdit", {                                                                              // 1
  path: "/admin/Users/:_id/edit",                                                                                      // 65
  template: "AdminDashboardUsersEdit",                                                                                 // 65
  controller: "AdminController",                                                                                       // 65
  data: function() {                                                                                                   // 65
    return {                                                                                                           //
      user: Meteor.users.find(this.params._id).fetch(),                                                                // 69
      roles: Roles.getRolesForUser(this.params._id),                                                                   // 69
      otherRoles: _.difference(_.map(Meteor.roles.find().fetch(), function(role) {                                     // 69
        return role.name;                                                                                              //
      }), Roles.getRolesForUser(this.params._id))                                                                      //
    };                                                                                                                 //
  },                                                                                                                   //
  action: function() {                                                                                                 // 65
    return this.render();                                                                                              //
  },                                                                                                                   //
  onAfterAction: function() {                                                                                          // 65
    Session.set('admin_title', 'Users');                                                                               // 75
    Session.set('admin_subtitle', 'Edit user ' + this.params._id);                                                     // 75
    Session.set('admin_collection_page', 'edit');                                                                      // 75
    Session.set('admin_collection_name', 'Users');                                                                     // 75
    Session.set('admin_id', this.params._id);                                                                          // 75
    return Session.set('admin_doc', Meteor.users.findOne({                                                             //
      _id: this.params._id                                                                                             // 80
    }));                                                                                                               //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/both/utils.coffee.js                                                                     //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.adminCollectionObject = function(collection) {                                                                    // 1
  if (typeof AdminConfig.collections[collection] !== 'undefined' && typeof AdminConfig.collections[collection].collectionObject !== 'undefined') {
    return AdminConfig.collections[collection].collectionObject;                                                       //
  } else {                                                                                                             //
    return lookup(collection);                                                                                         //
  }                                                                                                                    //
};                                                                                                                     // 1
                                                                                                                       //
this.adminCallback = function(name, args, callback) {                                                                  // 1
  var ref1, ref2, stop;                                                                                                // 8
  stop = false;                                                                                                        // 8
  if (typeof (typeof AdminConfig !== "undefined" && AdminConfig !== null ? (ref1 = AdminConfig.callbacks) != null ? ref1[name] : void 0 : void 0) === 'function') {
    stop = (ref2 = AdminConfig.callbacks)[name].apply(ref2, args) === false;                                           // 10
  }                                                                                                                    //
  if (typeof callback === 'function') {                                                                                // 11
    if (!stop) {                                                                                                       // 12
      return callback.apply(null, args);                                                                               //
    }                                                                                                                  //
  }                                                                                                                    //
};                                                                                                                     // 7
                                                                                                                       //
this.lookup = function(obj, root, required) {                                                                          // 1
  var arr, ref;                                                                                                        // 15
  if (required == null) {                                                                                              //
    required = true;                                                                                                   //
  }                                                                                                                    //
  if (typeof root === 'undefined') {                                                                                   // 15
    root = Meteor.isServer ? global : window;                                                                          // 16
  }                                                                                                                    //
  if (typeof obj === 'string') {                                                                                       // 17
    ref = root;                                                                                                        // 18
    arr = obj.split('.');                                                                                              // 18
    while (arr.length && (ref = ref[arr.shift()])) {                                                                   // 20
      continue;                                                                                                        // 20
    }                                                                                                                  //
    if (!ref && required) {                                                                                            // 21
      throw new Error(obj + ' is not in the ' + root.toString());                                                      // 22
    } else {                                                                                                           //
      return ref;                                                                                                      // 24
    }                                                                                                                  //
  }                                                                                                                    //
  return obj;                                                                                                          // 25
};                                                                                                                     // 14
                                                                                                                       //
this.parseID = function(id) {                                                                                          // 1
  if (typeof id === 'string') {                                                                                        // 28
    if (id.indexOf("ObjectID") > -1) {                                                                                 // 29
      return new Mongo.ObjectID(id.slice(id.indexOf('"') + 1, id.lastIndexOf('"')));                                   // 30
    } else {                                                                                                           //
      return id;                                                                                                       // 32
    }                                                                                                                  //
  } else {                                                                                                             //
    return id;                                                                                                         // 34
  }                                                                                                                    //
};                                                                                                                     // 27
                                                                                                                       //
this.parseIDs = function(ids) {                                                                                        // 1
  return _.map(ids, function(id) {                                                                                     // 37
    return parseID(id);                                                                                                //
  });                                                                                                                  //
};                                                                                                                     // 36
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/both/startup.coffee.js                                                                   //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var adminCreateRouteEdit, adminCreateRouteEditOptions, adminCreateRouteNew, adminCreateRouteNewOptions, adminCreateRouteView, adminCreateRouteViewOptions, adminCreateRoutes, adminCreateTables, adminDelButton, adminEditButton, adminEditDelButtons, adminPublishTables, adminTablePubName, adminTablesDom, defaultColumns;
                                                                                                                       //
this.AdminTables = {};                                                                                                 // 1
                                                                                                                       //
adminTablesDom = '<"box"<"box-header"<"box-toolbar"<"pull-left"<lf>><"pull-right"p>>><"box-body"t>>';                  // 1
                                                                                                                       //
adminEditButton = {                                                                                                    // 1
  data: '_id',                                                                                                         // 5
  title: 'Edit',                                                                                                       // 5
  createdCell: function(node, cellData, rowData) {                                                                     // 5
    return $(node).html(Blaze.toHTMLWithData(Template.adminEditBtn, {                                                  //
      _id: cellData                                                                                                    // 9
    }));                                                                                                               //
  },                                                                                                                   //
  width: '40px',                                                                                                       // 5
  orderable: false                                                                                                     // 5
};                                                                                                                     //
                                                                                                                       //
adminDelButton = {                                                                                                     // 1
  data: '_id',                                                                                                         // 13
  title: 'Delete',                                                                                                     // 13
  createdCell: function(node, cellData, rowData) {                                                                     // 13
    return $(node).html(Blaze.toHTMLWithData(Template.adminDeleteBtn, {                                                //
      _id: cellData                                                                                                    // 17
    }));                                                                                                               //
  },                                                                                                                   //
  width: '40px',                                                                                                       // 13
  orderable: false                                                                                                     // 13
};                                                                                                                     //
                                                                                                                       //
adminEditDelButtons = [adminEditButton, adminDelButton];                                                               // 1
                                                                                                                       //
defaultColumns = function() {                                                                                          // 1
  return [                                                                                                             //
    {                                                                                                                  //
      data: '_id',                                                                                                     // 28
      title: 'ID'                                                                                                      // 28
    }                                                                                                                  //
  ];                                                                                                                   //
};                                                                                                                     // 27
                                                                                                                       //
adminTablePubName = function(collection) {                                                                             // 1
  return "admin_tabular_" + collection;                                                                                //
};                                                                                                                     // 32
                                                                                                                       //
adminCreateTables = function(collections) {                                                                            // 1
  return _.each(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : void 0, function(collection, name) {
    var columns;                                                                                                       // 37
    _.defaults(collection, {                                                                                           // 37
      showEditColumn: true,                                                                                            // 37
      showDelColumn: true,                                                                                             // 37
      showInSideBar: true                                                                                              // 37
    });                                                                                                                //
    columns = _.map(collection.tableColumns, function(column) {                                                        // 37
      var createdCell;                                                                                                 // 44
      if (column.template) {                                                                                           // 44
        createdCell = function(node, cellData, rowData) {                                                              // 45
          $(node).html('');                                                                                            // 46
          return Blaze.renderWithData(Template[column.template], {                                                     //
            value: cellData,                                                                                           // 47
            doc: rowData                                                                                               // 47
          }, node);                                                                                                    //
        };                                                                                                             //
      }                                                                                                                //
      return {                                                                                                         //
        data: column.name,                                                                                             // 49
        title: column.label,                                                                                           // 49
        createdCell: createdCell                                                                                       // 49
      };                                                                                                               //
    });                                                                                                                //
    if (columns.length === 0) {                                                                                        // 53
      columns = defaultColumns();                                                                                      // 54
    }                                                                                                                  //
    if (collection.showEditColumn) {                                                                                   // 56
      columns.push(adminEditButton);                                                                                   // 57
    }                                                                                                                  //
    if (collection.showDelColumn) {                                                                                    // 58
      columns.push(adminDelButton);                                                                                    // 59
    }                                                                                                                  //
    return AdminTables[name] = new Tabular.Table({                                                                     //
      name: name,                                                                                                      // 62
      collection: adminCollectionObject(name),                                                                         // 62
      pub: collection.children && adminTablePubName(name),                                                             // 62
      sub: collection.sub,                                                                                             // 62
      columns: columns,                                                                                                // 62
      extraFields: collection.extraFields,                                                                             // 62
      dom: adminTablesDom                                                                                              // 62
    });                                                                                                                //
  });                                                                                                                  //
};                                                                                                                     // 35
                                                                                                                       //
adminCreateRoutes = function(collections) {                                                                            // 1
  _.each(collections, adminCreateRouteView);                                                                           // 71
  _.each(collections, adminCreateRouteNew);                                                                            // 71
  return _.each(collections, adminCreateRouteEdit);                                                                    //
};                                                                                                                     // 70
                                                                                                                       //
adminCreateRouteView = function(collection, collectionName) {                                                          // 1
  return Router.route("adminDashboard" + collectionName + "View", adminCreateRouteViewOptions(collection, collectionName));
};                                                                                                                     // 75
                                                                                                                       //
adminCreateRouteViewOptions = function(collection, collectionName) {                                                   // 1
  var options, ref;                                                                                                    // 80
  options = {                                                                                                          // 80
    path: "/admin/" + collectionName,                                                                                  // 81
    template: "AdminDashboardViewWrapper",                                                                             // 81
    controller: "AdminController",                                                                                     // 81
    data: function() {                                                                                                 // 81
      return {                                                                                                         //
        admin_table: AdminTables[collectionName]                                                                       // 85
      };                                                                                                               //
    },                                                                                                                 //
    action: function() {                                                                                               // 81
      return this.render();                                                                                            //
    },                                                                                                                 //
    onAfterAction: function() {                                                                                        // 81
      var ref, ref1;                                                                                                   // 89
      Session.set('admin_title', collectionName);                                                                      // 89
      Session.set('admin_subtitle', 'View');                                                                           // 89
      Session.set('admin_collection_name', collectionName);                                                            // 89
      return (ref = collection.routes) != null ? (ref1 = ref.view) != null ? ref1.onAfterAction : void 0 : void 0;     //
    }                                                                                                                  //
  };                                                                                                                   //
  return _.defaults(options, (ref = collection.routes) != null ? ref.view : void 0);                                   //
};                                                                                                                     // 79
                                                                                                                       //
adminCreateRouteNew = function(collection, collectionName) {                                                           // 1
  return Router.route("adminDashboard" + collectionName + "New", adminCreateRouteNewOptions(collection, collectionName));
};                                                                                                                     // 95
                                                                                                                       //
adminCreateRouteNewOptions = function(collection, collectionName) {                                                    // 1
  var options, ref;                                                                                                    // 100
  options = {                                                                                                          // 100
    path: "/admin/" + collectionName + "/new",                                                                         // 101
    template: "AdminDashboardNew",                                                                                     // 101
    controller: "AdminController",                                                                                     // 101
    action: function() {                                                                                               // 101
      return this.render();                                                                                            //
    },                                                                                                                 //
    onAfterAction: function() {                                                                                        // 101
      var ref, ref1;                                                                                                   // 107
      Session.set('admin_title', AdminDashboard.collectionLabel(collectionName));                                      // 107
      Session.set('admin_subtitle', 'Create new');                                                                     // 107
      Session.set('admin_collection_page', 'new');                                                                     // 107
      Session.set('admin_collection_name', collectionName);                                                            // 107
      return (ref = collection.routes) != null ? (ref1 = ref["new"]) != null ? ref1.onAfterAction : void 0 : void 0;   //
    },                                                                                                                 //
    data: function() {                                                                                                 // 101
      return {                                                                                                         //
        admin_collection: adminCollectionObject(collectionName)                                                        // 113
      };                                                                                                               //
    }                                                                                                                  //
  };                                                                                                                   //
  return _.defaults(options, (ref = collection.routes) != null ? ref["new"] : void 0);                                 //
};                                                                                                                     // 99
                                                                                                                       //
adminCreateRouteEdit = function(collection, collectionName) {                                                          // 1
  return Router.route("adminDashboard" + collectionName + "Edit", adminCreateRouteEditOptions(collection, collectionName));
};                                                                                                                     // 116
                                                                                                                       //
adminCreateRouteEditOptions = function(collection, collectionName) {                                                   // 1
  var options, ref;                                                                                                    // 121
  options = {                                                                                                          // 121
    path: "/admin/" + collectionName + "/:_id/edit",                                                                   // 122
    template: "AdminDashboardEdit",                                                                                    // 122
    controller: "AdminController",                                                                                     // 122
    waitOn: function() {                                                                                               // 122
      var ref, ref1;                                                                                                   // 126
      Meteor.subscribe('adminCollectionDoc', collectionName, parseID(this.params._id));                                // 126
      return (ref = collection.routes) != null ? (ref1 = ref.edit) != null ? ref1.waitOn : void 0 : void 0;            //
    },                                                                                                                 //
    action: function() {                                                                                               // 122
      return this.render();                                                                                            //
    },                                                                                                                 //
    onAfterAction: function() {                                                                                        // 122
      var ref, ref1;                                                                                                   // 131
      Session.set('admin_title', AdminDashboard.collectionLabel(collectionName));                                      // 131
      Session.set('admin_subtitle', 'Edit ' + this.params._id);                                                        // 131
      Session.set('admin_collection_page', 'edit');                                                                    // 131
      Session.set('admin_collection_name', collectionName);                                                            // 131
      Session.set('admin_id', parseID(this.params._id));                                                               // 131
      Session.set('admin_doc', adminCollectionObject(collectionName).findOne({                                         // 131
        _id: parseID(this.params._id)                                                                                  // 136
      }));                                                                                                             //
      return (ref = collection.routes) != null ? (ref1 = ref.edit) != null ? ref1.onAfterAction : void 0 : void 0;     //
    },                                                                                                                 //
    data: function() {                                                                                                 // 122
      return {                                                                                                         //
        admin_collection: adminCollectionObject(collectionName)                                                        // 139
      };                                                                                                               //
    }                                                                                                                  //
  };                                                                                                                   //
  return _.defaults(options, (ref = collection.routes) != null ? ref.edit : void 0);                                   //
};                                                                                                                     // 120
                                                                                                                       //
adminPublishTables = function(collections) {                                                                           // 1
  return _.each(collections, function(collection, name) {                                                              //
    if (!collection.children) {                                                                                        // 144
      return void 0;                                                                                                   // 144
    }                                                                                                                  //
    return Meteor.publishComposite(adminTablePubName(name), function(tableName, ids, fields) {                         //
      var extraFields;                                                                                                 // 146
      check(tableName, String);                                                                                        // 146
      check(ids, Array);                                                                                               // 146
      check(fields, Match.Optional(Object));                                                                           // 146
      extraFields = _.reduce(collection.extraFields, function(fields, name) {                                          // 146
        fields[name] = 1;                                                                                              // 151
        return fields;                                                                                                 //
      }, {});                                                                                                          //
      _.extend(fields, extraFields);                                                                                   // 146
      this.unblock();                                                                                                  // 146
      return {                                                                                                         //
        find: function() {                                                                                             // 158
          this.unblock();                                                                                              // 159
          return adminCollectionObject(name).find({                                                                    //
            _id: {                                                                                                     // 160
              $in: ids                                                                                                 // 160
            }                                                                                                          //
          }, {                                                                                                         //
            fields: fields                                                                                             // 160
          });                                                                                                          //
        },                                                                                                             //
        children: collection.children                                                                                  // 158
      };                                                                                                               //
    });                                                                                                                //
  });                                                                                                                  //
};                                                                                                                     // 142
                                                                                                                       //
Meteor.startup(function() {                                                                                            // 1
  adminCreateTables(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : void 0);    // 164
  adminCreateRoutes(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : void 0);    // 164
  if (Meteor.isServer) {                                                                                               // 166
    adminPublishTables(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : void 0);
  }                                                                                                                    //
  if (AdminTables.Users) {                                                                                             // 168
    return void 0;                                                                                                     // 168
  }                                                                                                                    //
  return AdminTables.Users = new Tabular.Table({                                                                       //
    changeSelector: function(selector, userId) {                                                                       // 172
      var $or;                                                                                                         // 173
      $or = selector['$or'];                                                                                           // 173
      $or && (selector['$or'] = _.map($or, function(exp) {                                                             // 173
        var ref;                                                                                                       // 175
        if (((ref = exp.emails) != null ? ref['$regex'] : void 0) != null) {                                           // 175
          return {                                                                                                     //
            emails: {                                                                                                  // 176
              $elemMatch: {                                                                                            // 176
                address: exp.emails                                                                                    // 176
              }                                                                                                        //
            }                                                                                                          //
          };                                                                                                           //
        } else {                                                                                                       //
          return exp;                                                                                                  //
        }                                                                                                              //
      }));                                                                                                             //
      return selector;                                                                                                 //
    },                                                                                                                 //
    name: 'Users',                                                                                                     // 172
    collection: Meteor.users,                                                                                          // 172
    columns: _.union([                                                                                                 // 172
      {                                                                                                                //
        data: '_id',                                                                                                   // 184
        title: 'Admin',                                                                                                // 184
        createdCell: function(node, cellData, rowData) {                                                               // 184
          return $(node).html(Blaze.toHTMLWithData(Template.adminUsersIsAdmin, {                                       //
            _id: cellData                                                                                              // 189
          }));                                                                                                         //
        },                                                                                                             //
        width: '40px'                                                                                                  // 184
      }, {                                                                                                             //
        data: 'emails',                                                                                                // 192
        title: 'Email',                                                                                                // 192
        render: function(value) {                                                                                      // 192
          return value[0].address;                                                                                     //
        },                                                                                                             //
        searchable: true                                                                                               // 192
      }, {                                                                                                             //
        data: 'emails',                                                                                                // 199
        title: 'Mail',                                                                                                 // 199
        createdCell: function(node, cellData, rowData) {                                                               // 199
          return $(node).html(Blaze.toHTMLWithData(Template.adminUsersMailBtn, {                                       //
            emails: cellData                                                                                           // 204
          }));                                                                                                         //
        },                                                                                                             //
        width: '40px'                                                                                                  // 199
      }, {                                                                                                             //
        data: 'createdAt',                                                                                             // 207
        title: 'Joined'                                                                                                // 207
      }                                                                                                                //
    ], adminEditDelButtons),                                                                                           //
    dom: adminTablesDom                                                                                                // 172
  });                                                                                                                  //
});                                                                                                                    // 163
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/both/collections.coffee.js                                                               //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.AdminCollectionsCount = new Mongo.Collection('adminCollectionsCount');                                            // 1
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/html/template.admin_templates.js                                                  //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("AdminDashboard");                                                                                // 2
Template["AdminDashboard"] = new Template("Template.AdminDashboard", (function() {                                     // 3
  var view = this;                                                                                                     // 4
  return Blaze.Each(function() {                                                                                       // 5
    return Spacebars.call(view.lookup("adminWidgets"));                                                                // 6
  }, function() {                                                                                                      // 7
    return [ "\n	", Blaze._TemplateWith(function() {                                                                   // 8
      return {                                                                                                         // 9
        template: Spacebars.call(view.lookup("template")),                                                             // 10
        data: Spacebars.call(view.lookup("data"))                                                                      // 11
      };                                                                                                               // 12
    }, function() {                                                                                                    // 13
      return Spacebars.include(function() {                                                                            // 14
        return Spacebars.call(Template.__dynamic);                                                                     // 15
      });                                                                                                              // 16
    }), "\n	" ];                                                                                                       // 17
  }, function() {                                                                                                      // 18
    return [ "\n	", Spacebars.include(view.lookupTemplate("adminDefaultWidgets")), "\n	" ];                            // 19
  });                                                                                                                  // 20
}));                                                                                                                   // 21
                                                                                                                       // 22
Template.__checkName("AdminDashboardNew");                                                                             // 23
Template["AdminDashboardNew"] = new Template("Template.AdminDashboardNew", (function() {                               // 24
  var view = this;                                                                                                     // 25
  return [ Spacebars.include(view.lookupTemplate("adminAlert")), "\n	", Blaze.If(function() {                          // 26
    return Spacebars.dataMustache(view.lookup("adminTemplate"), view.lookup("admin_collection_name"), "new");          // 27
  }, function() {                                                                                                      // 28
    return [ "\n		", Spacebars.With(function() {                                                                       // 29
      return Spacebars.dataMustache(view.lookup("adminTemplate"), view.lookup("admin_collection_name"), "new");        // 30
    }, function() {                                                                                                    // 31
      return [ "\n		", Blaze._TemplateWith(function() {                                                                // 32
        return {                                                                                                       // 33
          template: Spacebars.call(view.lookup("name")),                                                               // 34
          data: Spacebars.call(view.lookup("data"))                                                                    // 35
        };                                                                                                             // 36
      }, function() {                                                                                                  // 37
        return Spacebars.include(function() {                                                                          // 38
          return Spacebars.call(Template.__dynamic);                                                                   // 39
        });                                                                                                            // 40
      }), "\n		" ];                                                                                                    // 41
    }), "\n	" ];                                                                                                       // 42
  }, function() {                                                                                                      // 43
    return [ "\n		", HTML.DIV({                                                                                        // 44
      "class": "box box-default"                                                                                       // 45
    }, "\n			", HTML.DIV({                                                                                             // 46
      "class": "box-body"                                                                                              // 47
    }, "\n				", Blaze._TemplateWith(function() {                                                                      // 48
      return {                                                                                                         // 49
        id: Spacebars.call("admin_insert"),                                                                            // 50
        collection: Spacebars.call(view.lookup("admin_collection")),                                                   // 51
        fields: Spacebars.call(view.lookup("admin_fields")),                                                           // 52
        omitFields: Spacebars.call(view.lookup("admin_omit_fields")),                                                  // 53
        buttonContent: Spacebars.call("Create")                                                                        // 54
      };                                                                                                               // 55
    }, function() {                                                                                                    // 56
      return Spacebars.include(view.lookupTemplate("quickForm"));                                                      // 57
    }), "\n			"), "\n		"), "\n	" ];                                                                                    // 58
  }) ];                                                                                                                // 59
}));                                                                                                                   // 60
                                                                                                                       // 61
Template.__checkName("AdminDashboardEdit");                                                                            // 62
Template["AdminDashboardEdit"] = new Template("Template.AdminDashboardEdit", (function() {                             // 63
  var view = this;                                                                                                     // 64
  return [ Spacebars.include(view.lookupTemplate("adminAlert")), "\n	", Blaze.If(function() {                          // 65
    return Spacebars.dataMustache(view.lookup("adminTemplate"), view.lookup("admin_collection_name"), "edit");         // 66
  }, function() {                                                                                                      // 67
    return [ "\n		", Spacebars.With(function() {                                                                       // 68
      return Spacebars.dataMustache(view.lookup("adminTemplate"), view.lookup("admin_collection_name"), "edit");       // 69
    }, function() {                                                                                                    // 70
      return [ "\n		", Blaze._TemplateWith(function() {                                                                // 71
        return {                                                                                                       // 72
          template: Spacebars.call(view.lookup("name")),                                                               // 73
          data: Spacebars.call(view.lookup("data"))                                                                    // 74
        };                                                                                                             // 75
      }, function() {                                                                                                  // 76
        return Spacebars.include(function() {                                                                          // 77
          return Spacebars.call(Template.__dynamic);                                                                   // 78
        });                                                                                                            // 79
      }), "\n		" ];                                                                                                    // 80
    }), "\n	" ];                                                                                                       // 81
  }, function() {                                                                                                      // 82
    return [ "\n		", HTML.DIV({                                                                                        // 83
      "class": "box box-default"                                                                                       // 84
    }, "\n			", HTML.DIV({                                                                                             // 85
      "class": "box-body"                                                                                              // 86
    }, "\n				", Blaze.If(function() {                                                                                 // 87
      return Spacebars.call(view.lookup("admin_current_doc"));                                                         // 88
    }, function() {                                                                                                    // 89
      return [ "\n				", Blaze._TemplateWith(function() {                                                              // 90
        return {                                                                                                       // 91
          id: Spacebars.call("admin_update"),                                                                          // 92
          collection: Spacebars.call(view.lookup("admin_collection")),                                                 // 93
          doc: Spacebars.call(view.lookup("admin_current_doc")),                                                       // 94
          fields: Spacebars.call(view.lookup("admin_fields")),                                                         // 95
          omitFields: Spacebars.call(view.lookup("admin_omit_fields")),                                                // 96
          buttonContent: Spacebars.call("Update")                                                                      // 97
        };                                                                                                             // 98
      }, function() {                                                                                                  // 99
        return Spacebars.include(view.lookupTemplate("quickForm"));                                                    // 100
      }), "\n				" ];                                                                                                  // 101
    }), "\n			"), "\n		"), "\n	" ];                                                                                    // 102
  }) ];                                                                                                                // 103
}));                                                                                                                   // 104
                                                                                                                       // 105
Template.__checkName("AdminDashboardViewWrapper");                                                                     // 106
Template["AdminDashboardViewWrapper"] = new Template("Template.AdminDashboardViewWrapper", (function() {               // 107
  var view = this;                                                                                                     // 108
  return HTML.Raw("<div></div>\n	");                                                                                   // 109
}));                                                                                                                   // 110
                                                                                                                       // 111
Template.__checkName("AdminDashboardView");                                                                            // 112
Template["AdminDashboardView"] = new Template("Template.AdminDashboardView", (function() {                             // 113
  var view = this;                                                                                                     // 114
  return [ Spacebars.include(view.lookupTemplate("adminAlert")), "\n	", Blaze.If(function() {                          // 115
    return Spacebars.dataMustache(view.lookup("adminTemplate"), view.lookup("admin_collection_name"), "view");         // 116
  }, function() {                                                                                                      // 117
    return [ "\n		", Spacebars.With(function() {                                                                       // 118
      return Spacebars.dataMustache(view.lookup("adminTemplate"), view.lookup("admin_collection_name"), "view");       // 119
    }, function() {                                                                                                    // 120
      return [ "\n		", Blaze._TemplateWith(function() {                                                                // 121
        return {                                                                                                       // 122
          template: Spacebars.call(view.lookup("name")),                                                               // 123
          data: Spacebars.call(view.lookup("data"))                                                                    // 124
        };                                                                                                             // 125
      }, function() {                                                                                                  // 126
        return Spacebars.include(function() {                                                                          // 127
          return Spacebars.call(Template.__dynamic);                                                                   // 128
        });                                                                                                            // 129
      }), "\n		" ];                                                                                                    // 130
    }), "\n	" ];                                                                                                       // 131
  }, function() {                                                                                                      // 132
    return [ "\n		", Blaze.If(function() {                                                                             // 133
      return Spacebars.call(view.lookup("hasDocuments"));                                                              // 134
    }, function() {                                                                                                    // 135
      return [ "\n			", Blaze._TemplateWith(function() {                                                               // 136
        return {                                                                                                       // 137
          table: Spacebars.call(view.lookup("admin_table")),                                                           // 138
          "class": Spacebars.call("table dataTable")                                                                   // 139
        };                                                                                                             // 140
      }, function() {                                                                                                  // 141
        return Spacebars.include(view.lookupTemplate("tabular"));                                                      // 142
      }), "\n		" ];                                                                                                    // 143
    }, function() {                                                                                                    // 144
      return [ "\n			", HTML.DIV({                                                                                     // 145
        "class": "alert alert-info"                                                                                    // 146
      }, "\n				", HTML.P("There are no visible items in this collection."), "\n				", HTML.P(HTML.A({                 // 147
        href: function() {                                                                                             // 148
          return Spacebars.mustache(view.lookup("newPath"));                                                           // 149
        },                                                                                                             // 150
        "class": "btn btn-primary"                                                                                     // 151
      }, HTML.I({                                                                                                      // 152
        "class": "fa fa-plus"                                                                                          // 153
      }), " Add one")), "\n			"), "\n		" ];                                                                            // 154
    }), "\n	" ];                                                                                                       // 155
  }) ];                                                                                                                // 156
}));                                                                                                                   // 157
                                                                                                                       // 158
Template.__checkName("AdminDashboardUsersNew");                                                                        // 159
Template["AdminDashboardUsersNew"] = new Template("Template.AdminDashboardUsersNew", (function() {                     // 160
  var view = this;                                                                                                     // 161
  return HTML.DIV({                                                                                                    // 162
    "class": "box box-default"                                                                                         // 163
  }, "\n		", HTML.DIV({                                                                                                // 164
    "class": "box-body"                                                                                                // 165
  }, "\n			", Spacebars.include(view.lookupTemplate("adminAlert")), "\n			", Blaze._TemplateWith(function() {          // 166
    return {                                                                                                           // 167
      id: Spacebars.call("adminNewUser"),                                                                              // 168
      schema: Spacebars.call(Spacebars.dot(view.lookup("AdminSchemas"), "newUser")),                                   // 169
      type: Spacebars.call("method"),                                                                                  // 170
      meteormethod: Spacebars.call("adminNewUser")                                                                     // 171
    };                                                                                                                 // 172
  }, function() {                                                                                                      // 173
    return Spacebars.include(view.lookupTemplate("autoForm"), function() {                                             // 174
      return [ "\n\n			", Blaze._TemplateWith(function() {                                                             // 175
        return {                                                                                                       // 176
          name: Spacebars.call("email")                                                                                // 177
        };                                                                                                             // 178
      }, function() {                                                                                                  // 179
        return Spacebars.include(view.lookupTemplate("afQuickField"));                                                 // 180
      }), "\n			", Blaze._TemplateWith(function() {                                                                    // 181
        return {                                                                                                       // 182
          name: Spacebars.call("chooseOwnPassword")                                                                    // 183
        };                                                                                                             // 184
      }, function() {                                                                                                  // 185
        return Spacebars.include(view.lookupTemplate("afQuickField"));                                                 // 186
      }), "\n\n			", Blaze.If(function() {                                                                             // 187
        return Spacebars.dataMustache(view.lookup("afFieldValueIs"), Spacebars.kw({                                    // 188
          name: "chooseOwnPassword",                                                                                   // 189
          value: false                                                                                                 // 190
        }));                                                                                                           // 191
      }, function() {                                                                                                  // 192
        return [ "\n\n			", Blaze._TemplateWith(function() {                                                           // 193
          return {                                                                                                     // 194
            name: Spacebars.call("password")                                                                           // 195
          };                                                                                                           // 196
        }, function() {                                                                                                // 197
          return Spacebars.include(view.lookupTemplate("afQuickField"));                                               // 198
        }), "\n			", Blaze._TemplateWith(function() {                                                                  // 199
          return {                                                                                                     // 200
            name: Spacebars.call("sendPassword")                                                                       // 201
          };                                                                                                           // 202
        }, function() {                                                                                                // 203
          return Spacebars.include(view.lookupTemplate("afQuickField"));                                               // 204
        }), "\n\n			" ];                                                                                               // 205
      }), "\n\n			", HTML.BUTTON({                                                                                     // 206
        type: "submit",                                                                                                // 207
        "class": "btn btn-primary"                                                                                     // 208
      }, "Add User"), "\n\n			" ];                                                                                     // 209
    });                                                                                                                // 210
  }), "\n		"), "\n	");                                                                                                 // 211
}));                                                                                                                   // 212
                                                                                                                       // 213
Template.__checkName("AdminDashboardUsersEdit");                                                                       // 214
Template["AdminDashboardUsersEdit"] = new Template("Template.AdminDashboardUsersEdit", (function() {                   // 215
  var view = this;                                                                                                     // 216
  return HTML.DIV({                                                                                                    // 217
    "class": "box box-default"                                                                                         // 218
  }, "\n		", HTML.DIV({                                                                                                // 219
    "class": "box-body"                                                                                                // 220
  }, "\n			", Spacebars.include(view.lookupTemplate("adminAlert")), "\n			", Blaze.If(function() {                     // 221
    return Spacebars.call(view.lookup("adminGetUserSchema"));                                                          // 222
  }, function() {                                                                                                      // 223
    return [ "\n			", Blaze._TemplateWith(function() {                                                                 // 224
      return {                                                                                                         // 225
        id: Spacebars.call("adminUpdateUser"),                                                                         // 226
        buttonContent: Spacebars.call("Update"),                                                                       // 227
        buttonClasses: Spacebars.call("btn btn-primary btn-sm"),                                                       // 228
        collection: Spacebars.call(view.lookup("adminGetUsers")),                                                      // 229
        schema: Spacebars.call(view.lookup("adminGetUserSchema")),                                                     // 230
        doc: Spacebars.call(view.lookup("admin_current_doc")),                                                         // 231
        omitFields: Spacebars.call("roles,services")                                                                   // 232
      };                                                                                                               // 233
    }, function() {                                                                                                    // 234
      return Spacebars.include(view.lookupTemplate("quickForm"));                                                      // 235
    }), "\n			", HTML.HR(), "\n			" ];                                                                                 // 236
  }), "\n\n			", HTML.Raw("<h4>User Roles</h4>"), "\n			", Blaze.Each(function() {                                     // 237
    return Spacebars.call(view.lookup("roles"));                                                                       // 238
  }, function() {                                                                                                      // 239
    return [ "\n			", HTML.BUTTON({                                                                                    // 240
      "class": "btn btn-primary btn-xs btn-remove-role",                                                               // 241
      role: function() {                                                                                               // 242
        return Spacebars.mustache(view.lookup("."));                                                                   // 243
      },                                                                                                               // 244
      user: function() {                                                                                               // 245
        return Spacebars.mustache(view.lookup("admin_current_id"));                                                    // 246
      }                                                                                                                // 247
    }, Blaze.View("lookup:.", function() {                                                                             // 248
      return Spacebars.mustache(view.lookup("."));                                                                     // 249
    })), "\n			" ];                                                                                                    // 250
  }), "\n			", Blaze.Each(function() {                                                                                 // 251
    return Spacebars.call(view.lookup("otherRoles"));                                                                  // 252
  }, function() {                                                                                                      // 253
    return [ "\n			", HTML.BUTTON({                                                                                    // 254
      "class": "btn btn-default btn-xs btn-add-role",                                                                  // 255
      role: function() {                                                                                               // 256
        return Spacebars.mustache(view.lookup("."));                                                                   // 257
      },                                                                                                               // 258
      user: function() {                                                                                               // 259
        return Spacebars.mustache(view.lookup("admin_current_id"));                                                    // 260
      }                                                                                                                // 261
    }, Blaze.View("lookup:.", function() {                                                                             // 262
      return Spacebars.mustache(view.lookup("."));                                                                     // 263
    })), "\n			" ];                                                                                                    // 264
  }), "\n			", Blaze.If(function() {                                                                                   // 265
    return Spacebars.call(view.lookup("roles"));                                                                       // 266
  }, function() {                                                                                                      // 267
    return [ "\n			", HTML.P({                                                                                         // 268
      style: "margin-top:5px;"                                                                                         // 269
    }, "Click a role to toggle it."), "\n			" ];                                                                       // 270
  }, function() {                                                                                                      // 271
    return [ "\n			", HTML.P("User not in any roles. Click a role to add it to a user."), "\n			" ];                   // 272
  }), "\n\n			", HTML.Raw("<hr>"), "\n			", HTML.Raw("<h4>Reset Password</h4>"), "\n			", Blaze._TemplateWith(function() {
    return {                                                                                                           // 274
      id: Spacebars.call("adminSendResetPasswordEmail"),                                                               // 275
      schema: Spacebars.call(Spacebars.dot(view.lookup("AdminSchemas"), "sendResetPasswordEmail")),                    // 276
      type: Spacebars.call("method"),                                                                                  // 277
      meteormethod: Spacebars.call("adminSendResetPasswordEmail")                                                      // 278
    };                                                                                                                 // 279
  }, function() {                                                                                                      // 280
    return Spacebars.include(view.lookupTemplate("autoForm"), function() {                                             // 281
      return [ "\n			", HTML.DIV({                                                                                     // 282
        "class": "form-group hidden"                                                                                   // 283
      }, "\n				", HTML.LABEL({                                                                                        // 284
        "class": "control-label",                                                                                      // 285
        "for": "title"                                                                                                 // 286
      }, "ID"), "\n				", HTML.INPUT({                                                                                 // 287
        value: function() {                                                                                            // 288
          return Spacebars.mustache(view.lookup("admin_current_id"));                                                  // 289
        },                                                                                                             // 290
        type: "text",                                                                                                  // 291
        name: "_id",                                                                                                   // 292
        omitfields: "createdAtupdatedAt",                                                                              // 293
        required: "",                                                                                                  // 294
        "data-schema-key": "_id",                                                                                      // 295
        "class": "form-control",                                                                                       // 296
        autocomplete: "off"                                                                                            // 297
      }), "\n				", HTML.SPAN({                                                                                        // 298
        "class": "help-block"                                                                                          // 299
      }), "\n			"), "\n			", HTML.P("Send a reset password email to ", Blaze.View("lookup:admin_current_doc.emails.0.address", function() {
        return Spacebars.mustache(Spacebars.dot(view.lookup("admin_current_doc"), "emails", "0", "address"));          // 301
      })), "\n			", HTML.BUTTON({                                                                                      // 302
        type: "submit",                                                                                                // 303
        "class": "btn btn-primary btn-sm"                                                                              // 304
      }, "Send Email"), "\n			" ];                                                                                     // 305
    });                                                                                                                // 306
  }), "\n\n			", HTML.Raw("<hr>"), "\n			", HTML.Raw("<h4>Change Password</h4>"), "\n			", Blaze._TemplateWith(function() {
    return {                                                                                                           // 308
      id: Spacebars.call("adminChangePassword"),                                                                       // 309
      schema: Spacebars.call(Spacebars.dot(view.lookup("AdminSchemas"), "changePassword")),                            // 310
      type: Spacebars.call("method"),                                                                                  // 311
      meteormethod: Spacebars.call("adminChangePassword")                                                              // 312
    };                                                                                                                 // 313
  }, function() {                                                                                                      // 314
    return Spacebars.include(view.lookupTemplate("autoForm"), function() {                                             // 315
      return [ "\n			", HTML.DIV({                                                                                     // 316
        "class": "form-group hidden"                                                                                   // 317
      }, "\n				", HTML.LABEL({                                                                                        // 318
        "class": "control-label",                                                                                      // 319
        "for": "title"                                                                                                 // 320
      }, "ID"), "\n				", HTML.INPUT({                                                                                 // 321
        value: function() {                                                                                            // 322
          return Spacebars.mustache(view.lookup("admin_current_id"));                                                  // 323
        },                                                                                                             // 324
        type: "text",                                                                                                  // 325
        name: "_id",                                                                                                   // 326
        omitfields: "createdAtupdatedAt",                                                                              // 327
        required: "",                                                                                                  // 328
        "data-schema-key": "_id",                                                                                      // 329
        "class": "form-control",                                                                                       // 330
        autocomplete: "off"                                                                                            // 331
      }), "\n				", HTML.SPAN({                                                                                        // 332
        "class": "help-block"                                                                                          // 333
      }), "\n			"), "\n			", Blaze._TemplateWith(function() {                                                          // 334
        return {                                                                                                       // 335
          name: Spacebars.call("password")                                                                             // 336
        };                                                                                                             // 337
      }, function() {                                                                                                  // 338
        return Spacebars.include(view.lookupTemplate("afQuickField"));                                                 // 339
      }), "\n			", HTML.BUTTON({                                                                                       // 340
        type: "submit",                                                                                                // 341
        "class": "btn btn-primary btn-sm"                                                                              // 342
      }, "Change Password"), "\n			" ];                                                                                // 343
    });                                                                                                                // 344
  }), "\n		"), "\n	");                                                                                                 // 345
}));                                                                                                                   // 346
                                                                                                                       // 347
Template.__checkName("adminAlert");                                                                                    // 348
Template["adminAlert"] = new Template("Template.adminAlert", (function() {                                             // 349
  var view = this;                                                                                                     // 350
  return [ Blaze.If(function() {                                                                                       // 351
    return Spacebars.dataMustache(Spacebars.dot(view.lookup("$"), "Session", "get"), "adminSuccess");                  // 352
  }, function() {                                                                                                      // 353
    return [ "\n	", HTML.DIV({                                                                                         // 354
      "class": "alert alert-success admin-alert"                                                                       // 355
    }, "\n		", Blaze.View("lookup:$.Session.get", function() {                                                         // 356
      return Spacebars.mustache(Spacebars.dot(view.lookup("$"), "Session", "get"), "adminSuccess");                    // 357
    }), "\n	"), "\n	" ];                                                                                               // 358
  }), "\n\n	", Blaze.If(function() {                                                                                   // 359
    return Spacebars.dataMustache(Spacebars.dot(view.lookup("$"), "Session", "get"), "adminError");                    // 360
  }, function() {                                                                                                      // 361
    return [ "\n	", HTML.DIV({                                                                                         // 362
      "class": "alert alert-danger admin-alert"                                                                        // 363
    }, "\n		", Blaze.View("lookup:$.Session.get", function() {                                                         // 364
      return Spacebars.mustache(Spacebars.dot(view.lookup("$"), "Session", "get"), "adminError");                      // 365
    }), "\n	"), "\n	" ];                                                                                               // 366
  }) ];                                                                                                                // 367
}));                                                                                                                   // 368
                                                                                                                       // 369
Template.__checkName("adminUsersIsAdmin");                                                                             // 370
Template["adminUsersIsAdmin"] = new Template("Template.adminUsersIsAdmin", (function() {                               // 371
  var view = this;                                                                                                     // 372
  return Blaze.If(function() {                                                                                         // 373
    return Spacebars.dataMustache(view.lookup("adminIsUserInRole"), Spacebars.dot(view.lookup("."), "_id"), "admin");  // 374
  }, function() {                                                                                                      // 375
    return HTML.I({                                                                                                    // 376
      "class": "fa fa-check"                                                                                           // 377
    });                                                                                                                // 378
  });                                                                                                                  // 379
}));                                                                                                                   // 380
                                                                                                                       // 381
Template.__checkName("adminUsersMailBtn");                                                                             // 382
Template["adminUsersMailBtn"] = new Template("Template.adminUsersMailBtn", (function() {                               // 383
  var view = this;                                                                                                     // 384
  return HTML.A({                                                                                                      // 385
    href: function() {                                                                                                 // 386
      return [ "mailto:", Spacebars.mustache(view.lookup("adminUserEmail"), view.lookup(".")) ];                       // 387
    },                                                                                                                 // 388
    "class": "btn btn-default btn-xs"                                                                                  // 389
  }, HTML.Raw('<i class="fa fa-envelope"></i>'));                                                                      // 390
}));                                                                                                                   // 391
                                                                                                                       // 392
Template.__checkName("adminEditBtn");                                                                                  // 393
Template["adminEditBtn"] = new Template("Template.adminEditBtn", (function() {                                         // 394
  var view = this;                                                                                                     // 395
  return [ HTML.A({                                                                                                    // 396
    href: function() {                                                                                                 // 397
      return Spacebars.mustache(view.lookup("path"));                                                                  // 398
    },                                                                                                                 // 399
    "class": "hidden-xs btn btn-xs btn-primary"                                                                        // 400
  }, HTML.Raw('<i class="fa fa-pencil"></i>')), "\n	", HTML.A({                                                        // 401
    href: function() {                                                                                                 // 402
      return Spacebars.mustache(view.lookup("path"));                                                                  // 403
    },                                                                                                                 // 404
    "class": "visible-xs btn btn-sm btn-primary"                                                                       // 405
  }, HTML.Raw('<i class="fa fa-pencil"></i>'), " Edit") ];                                                             // 406
}));                                                                                                                   // 407
                                                                                                                       // 408
Template.__checkName("adminDeleteBtn");                                                                                // 409
Template["adminDeleteBtn"] = new Template("Template.adminDeleteBtn", (function() {                                     // 410
  var view = this;                                                                                                     // 411
  return [ HTML.A({                                                                                                    // 412
    "data-toggle": "modal",                                                                                            // 413
    doc: function() {                                                                                                  // 414
      return Spacebars.mustache(view.lookup("_id"));                                                                   // 415
    },                                                                                                                 // 416
    href: "#admin-delete-modal",                                                                                       // 417
    "class": "hidden-xs btn btn-xs btn-danger btn-delete"                                                              // 418
  }, HTML.I({                                                                                                          // 419
    "class": "fa fa-times",                                                                                            // 420
    doc: function() {                                                                                                  // 421
      return Spacebars.mustache(view.lookup("_id"));                                                                   // 422
    }                                                                                                                  // 423
  })), "\n	", HTML.A({                                                                                                 // 424
    "data-toggle": "modal",                                                                                            // 425
    doc: function() {                                                                                                  // 426
      return Spacebars.mustache(view.lookup("_id"));                                                                   // 427
    },                                                                                                                 // 428
    href: "#admin-delete-modal",                                                                                       // 429
    "class": "visible-xs btn btn-sm btn-danger btn-delete"                                                             // 430
  }, HTML.I({                                                                                                          // 431
    "class": "fa fa-times",                                                                                            // 432
    doc: function() {                                                                                                  // 433
      return Spacebars.mustache(view.lookup("_id"));                                                                   // 434
    }                                                                                                                  // 435
  }), " Delete") ];                                                                                                    // 436
}));                                                                                                                   // 437
                                                                                                                       // 438
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/html/template.admin_widgets.js                                                    //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("adminDefaultWidgets");                                                                           // 2
Template["adminDefaultWidgets"] = new Template("Template.adminDefaultWidgets", (function() {                           // 3
  var view = this;                                                                                                     // 4
  return Blaze.Each(function() {                                                                                       // 5
    return Spacebars.call(view.lookup("admin_collections"));                                                           // 6
  }, function() {                                                                                                      // 7
    return [ "\n		", Blaze.Unless(function() {                                                                         // 8
      return Spacebars.dataMustache(view.lookup("$eq"), view.lookup("showWidget"), false);                             // 9
    }, function() {                                                                                                    // 10
      return [ "\n			", Blaze._TemplateWith(function() {                                                               // 11
        return {                                                                                                       // 12
          collection: Spacebars.call(view.lookup("name"))                                                              // 13
        };                                                                                                             // 14
      }, function() {                                                                                                  // 15
        return Spacebars.include(view.lookupTemplate("adminCollectionWidget"));                                        // 16
      }), "\n		" ];                                                                                                    // 17
    }), "\n	" ];                                                                                                       // 18
  });                                                                                                                  // 19
}));                                                                                                                   // 20
                                                                                                                       // 21
Template.__checkName("adminCollectionWidget");                                                                         // 22
Template["adminCollectionWidget"] = new Template("Template.adminCollectionWidget", (function() {                       // 23
  var view = this;                                                                                                     // 24
  return HTML.DIV({                                                                                                    // 25
    "class": function() {                                                                                              // 26
      return Blaze.If(function() {                                                                                     // 27
        return Spacebars.call(view.lookup("class"));                                                                   // 28
      }, function() {                                                                                                  // 29
        return Blaze.View("lookup:class", function() {                                                                 // 30
          return Spacebars.mustache(view.lookup("class"));                                                             // 31
        });                                                                                                            // 32
      }, function() {                                                                                                  // 33
        return "col-lg-3 col-xs-6";                                                                                    // 34
      });                                                                                                              // 35
    }                                                                                                                  // 36
  }, "\n	", Spacebars.With(function() {                                                                                // 37
    return Spacebars.dataMustache(view.lookup("adminGetCollection"), view.lookup("collection"));                       // 38
  }, function() {                                                                                                      // 39
    return [ "\n		", HTML.A({                                                                                          // 40
      href: function() {                                                                                               // 41
        return [ "/admin/", Spacebars.mustache(Spacebars.dot(view.lookup("."), "name")) ];                             // 42
      }                                                                                                                // 43
    }, "\n			", HTML.DIV({                                                                                             // 44
      "class": function() {                                                                                            // 45
        return [ "small-box bg-", Spacebars.mustache(view.lookup("color")) ];                                          // 46
      }                                                                                                                // 47
    }, "\n				", HTML.DIV({                                                                                            // 48
      "class": "inner"                                                                                                 // 49
    }, "\n					", HTML.H3("\n						", Blaze.View("lookup:adminCollectionCount", function() {                           // 50
      return Spacebars.mustache(view.lookup("adminCollectionCount"), view.lookup("name"));                             // 51
    }), "\n					"), "\n					", HTML.P("\n						", Blaze.View("lookup:..label", function() {                            // 52
      return Spacebars.mustache(Spacebars.dot(view.lookup("."), "label"));                                             // 53
    }), "\n					"), "\n				"), "\n				", HTML.DIV({                                                                    // 54
      "class": "icon"                                                                                                  // 55
    }, "\n					", HTML.I({                                                                                             // 56
      "class": function() {                                                                                            // 57
        return [ "fa fa-", Spacebars.mustache(Spacebars.dot(view.lookup("."), "icon")) ];                              // 58
      }                                                                                                                // 59
    }), "\n				"), "\n				", HTML.A({                                                                                  // 60
      "class": "small-box-footer"                                                                                      // 61
    }, "\n					See all ", HTML.I({                                                                                     // 62
      "class": "fa fa-arrow-circle-right"                                                                              // 63
    }), "\n				"), "\n			"), "\n		"), "\n		" ];                                                                        // 64
  }), "\n	");                                                                                                          // 65
}));                                                                                                                   // 66
                                                                                                                       // 67
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/html/template.admin_layouts.js                                                    //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("AdminLayout");                                                                                   // 2
Template["AdminLayout"] = new Template("Template.AdminLayout", (function() {                                           // 3
  var view = this;                                                                                                     // 4
  return Blaze.If(function() {                                                                                         // 5
    return Spacebars.call(view.lookup("AdminConfig"));                                                                 // 6
  }, function() {                                                                                                      // 7
    return [ "\n  ", Blaze.If(function() {                                                                             // 8
      return Spacebars.dataMustache(view.lookup("isInRole"), "admin");                                                 // 9
    }, function() {                                                                                                    // 10
      return [ "\n  ", HTML.DIV({                                                                                      // 11
        "class": "admin-layout"                                                                                        // 12
      }, "\n    ", Blaze._TemplateWith(function() {                                                                    // 13
        return {                                                                                                       // 14
          skin: Spacebars.call(view.lookup("admin_skin"))                                                              // 15
        };                                                                                                             // 16
      }, function() {                                                                                                  // 17
        return Spacebars.include(view.lookupTemplate("AdminLTE"), function() {                                         // 18
          return [ "\n      ", Spacebars.include(view.lookupTemplate("AdminHeader")), "\n      ", Spacebars.include(view.lookupTemplate("AdminSidebar")), "\n      ", HTML.DIV({
            "class": "content-wrapper",                                                                                // 20
            style: function() {                                                                                        // 21
              return [ "min-height: ", Spacebars.mustache(view.lookup("minHeight")) ];                                 // 22
            }                                                                                                          // 23
          }, "\n        ", HTML.SECTION({                                                                              // 24
            "class": "content-header"                                                                                  // 25
          }, "\n          ", HTML.H1("\n            ", Blaze.View("lookup:$.Session.get", function() {                 // 26
            return Spacebars.mustache(Spacebars.dot(view.lookup("$"), "Session", "get"), "admin_title");               // 27
          }), "\n            ", HTML.SMALL(Blaze.View("lookup:$.Session.get", function() {                             // 28
            return Spacebars.mustache(Spacebars.dot(view.lookup("$"), "Session", "get"), "admin_subtitle");            // 29
          })), "\n          "), "\n          ", HTML.OL({                                                              // 30
            "class": "breadcrumb"                                                                                      // 31
          }, "\n            ", HTML.LI(HTML.A({                                                                        // 32
            href: "/admin/"                                                                                            // 33
          }, "Dashboard")), "\n            ", Blaze.If(function() {                                                    // 34
            return Spacebars.dataMustache(Spacebars.dot(view.lookup("$"), "Session", "get"), "admin_collection_name");
          }, function() {                                                                                              // 36
            return [ "\n              ", HTML.LI(HTML.A({                                                              // 37
              href: function() {                                                                                       // 38
                return [ "/admin/", Spacebars.mustache(Spacebars.dot(view.lookup("$"), "Session", "get"), "admin_collection_name"), "/" ];
              }                                                                                                        // 40
            }, "\n                ", Blaze.View("lookup:adminCollectionLabel", function() {                            // 41
              return Spacebars.mustache(view.lookup("adminCollectionLabel"), view.lookup("admin_collection_name"));    // 42
            }), "\n              ")), "\n            " ];                                                              // 43
          }), "\n\n            ", Blaze.If(function() {                                                                // 44
            return Spacebars.dataMustache(Spacebars.dot(view.lookup("$"), "Session", "equals"), "admin_collection_page", "new");
          }, function() {                                                                                              // 46
            return [ "\n              ", HTML.LI("New"), "\n            " ];                                           // 47
          }), "\n\n            ", Blaze.If(function() {                                                                // 48
            return Spacebars.dataMustache(Spacebars.dot(view.lookup("$"), "Session", "equals"), "admin_collection_page", "edit");
          }, function() {                                                                                              // 50
            return [ "\n              ", HTML.LI("Edit"), "\n            " ];                                          // 51
          }), "\n          "), "\n        "), "\n        ", HTML.SECTION({                                             // 52
            "class": "content"                                                                                         // 53
          }, "\n          ", Spacebars.include(view.lookupTemplate("yield")), "\n        "), "\n      "), "\n    " ];  // 54
        });                                                                                                            // 55
      }), "\n  "), "\n  ", Spacebars.include(view.lookupTemplate("AdminDeleteModal")), "\n  " ];                       // 56
    }, function() {                                                                                                    // 57
      return [ "\n  ", Spacebars.include(view.lookupTemplate("NotAdmin")), "\n  " ];                                   // 58
    }), "\n  " ];                                                                                                      // 59
  }, function() {                                                                                                      // 60
    return [ "\n  ", Spacebars.include(view.lookupTemplate("NoConfig")), "\n  " ];                                     // 61
  });                                                                                                                  // 62
}));                                                                                                                   // 63
                                                                                                                       // 64
Template.__checkName("AdminDeleteModal");                                                                              // 65
Template["AdminDeleteModal"] = new Template("Template.AdminDeleteModal", (function() {                                 // 66
  var view = this;                                                                                                     // 67
  return HTML.Raw('<div class="modal fade" id="admin-delete-modal">\n    <div class="modal-dialog">\n      <div class="modal-content">\n        <div class="modal-header">\n          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n          <h4 class="modal-title">Confirm delete</h4>\n        </div>\n        <div class="modal-body">\n          <p>Are you sure you want to delete this?</p>\n        </div>\n        <div class="modal-footer">\n          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n          <button type="button" id="confirm-delete" class="btn btn-danger">Delete</button>\n        </div>\n      </div><!-- /.modal-content -->\n    </div><!-- /.modal-dialog -->\n  </div><!-- /.modal -->');
}));                                                                                                                   // 69
                                                                                                                       // 70
Template.__checkName("NotAdmin");                                                                                      // 71
Template["NotAdmin"] = new Template("Template.NotAdmin", (function() {                                                 // 72
  var view = this;                                                                                                     // 73
  return HTML.Raw('<div class="container">\n    <div class="row">\n      <div class="col-md-4 col-md-offset-4">\n        <p class="alert alert-info" style="margin-top:100px;">\n          You need to be an admin to view this page\n        </p>\n      </div>\n    </div>\n  </div>');
}));                                                                                                                   // 75
                                                                                                                       // 76
Template.__checkName("NoConfig");                                                                                      // 77
Template["NoConfig"] = new Template("Template.NoConfig", (function() {                                                 // 78
  var view = this;                                                                                                     // 79
  return HTML.Raw("<p class=\"alert alert-info\">\n    You need to define an AdminConfig object to use the admin dashboard.\n    <br>\n      A basic config to manage the 'Posts' and 'Comments' collection would look like this:\n    <br>\n    <code>\n      AdminConfig = {\n        <br>\n        adminEmails: ['	ben@code2create.com'],\n        <br>\n        collections:\n        <br>\n        {\n          <br>\n          Posts: {},\n          <br>\n          Comments: {}\n          <br>\n        }\n        <br>\n      }\n    </code>\n  </p>");
}));                                                                                                                   // 81
                                                                                                                       // 82
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/html/template.admin_sidebar.js                                                    //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("AdminSidebar");                                                                                  // 2
Template["AdminSidebar"] = new Template("Template.AdminSidebar", (function() {                                         // 3
  var view = this;                                                                                                     // 4
  return HTML.ASIDE({                                                                                                  // 5
    "class": "main-sidebar"                                                                                            // 6
  }, "\n		", HTML.DIV({                                                                                                // 7
    "class": "sidebar"                                                                                                 // 8
  }, "\n			", HTML.UL({                                                                                                // 9
    "class": "sidebar-menu"                                                                                            // 10
  }, "\n				", HTML.LI({                                                                                               // 11
    "class": function() {                                                                                              // 12
      return Spacebars.mustache(view.lookup("isActiveRoute"), "adminDashboard");                                       // 13
    }                                                                                                                  // 14
  }, "\n					", HTML.A({                                                                                               // 15
    href: function() {                                                                                                 // 16
      return Spacebars.mustache(view.lookup("pathFor"), "adminDashboard");                                             // 17
    }                                                                                                                  // 18
  }, "\n						", HTML.Raw('<i class="fa fa-dashboard"></i>'), " ", HTML.Raw("<span>Dashboard</span>"), "\n					"), "\n				"), "\n				", Blaze.Each(function() {
    return Spacebars.call(view.lookup("admin_collections"));                                                           // 20
  }, function() {                                                                                                      // 21
    return [ "\n					", Blaze.Unless(function() {                                                                      // 22
      return Spacebars.dataMustache(view.lookup("$eq"), view.lookup("showInSideBar"), false);                          // 23
    }, function() {                                                                                                    // 24
      return [ "\n					", HTML.LI({                                                                                    // 25
        "class": "treeview"                                                                                            // 26
      }, "\n						", HTML.A({                                                                                          // 27
        href: "#"                                                                                                      // 28
      }, "\n							", HTML.I({                                                                                         // 29
        "class": function() {                                                                                          // 30
          return [ "fa fa-", Spacebars.mustache(Spacebars.dot(view.lookup("."), "icon")) ];                            // 31
        }                                                                                                              // 32
      }), "\n							", HTML.SPAN(Blaze.View("lookup:..label", function() {                                             // 33
        return Spacebars.mustache(Spacebars.dot(view.lookup("."), "label"));                                           // 34
      })), "\n							", HTML.I({                                                                                       // 35
        "class": "fa fa-angle-left pull-right"                                                                         // 36
      }), "\n						"), "\n						", HTML.UL({                                                                           // 37
        "class": "treeview-menu"                                                                                       // 38
      }, "\n							", HTML.LI({                                                                                        // 39
        "class": function() {                                                                                          // 40
          return Spacebars.mustache(view.lookup("isActivePath"), Spacebars.kw({                                        // 41
            path: view.lookup("newPath")                                                                               // 42
          }));                                                                                                         // 43
        }                                                                                                              // 44
      }, HTML.A({                                                                                                      // 45
        href: function() {                                                                                             // 46
          return Spacebars.mustache(view.lookup("newPath"));                                                           // 47
        }                                                                                                              // 48
      }, HTML.I({                                                                                                      // 49
        "class": "fa fa-angle-double-right"                                                                            // 50
      }), " New")), "\n							", HTML.LI({                                                                             // 51
        "class": function() {                                                                                          // 52
          return Spacebars.mustache(view.lookup("isActivePath"), Spacebars.kw({                                        // 53
            path: view.lookup("viewPath")                                                                              // 54
          }));                                                                                                         // 55
        }                                                                                                              // 56
      }, HTML.A({                                                                                                      // 57
        href: function() {                                                                                             // 58
          return Spacebars.mustache(view.lookup("viewPath"));                                                          // 59
        }                                                                                                              // 60
      }, HTML.I({                                                                                                      // 61
        "class": "fa fa-angle-double-right"                                                                            // 62
      }), " View All")), "\n							", Blaze.Each(function() {                                                          // 63
        return Spacebars.call(view.lookup("admin_collection_items"));                                                  // 64
      }, function() {                                                                                                  // 65
        return [ "\n							", HTML.LI({                                                                                // 66
          "class": function() {                                                                                        // 67
            return Spacebars.mustache(view.lookup("isActivePath"), Spacebars.kw({                                      // 68
              path: view.lookup("url")                                                                                 // 69
            }));                                                                                                       // 70
          }                                                                                                            // 71
        }, HTML.A({                                                                                                    // 72
          href: function() {                                                                                           // 73
            return Spacebars.mustache(view.lookup("url"));                                                             // 74
          }                                                                                                            // 75
        }, HTML.I({                                                                                                    // 76
          "class": "fa fa-angle-double-right"                                                                          // 77
        }), " ", Blaze.View("lookup:title", function() {                                                               // 78
          return Spacebars.mustache(view.lookup("title"));                                                             // 79
        }))), "\n							" ];                                                                                           // 80
      }), "\n						"), "\n					"), "\n					" ];                                                                        // 81
    }), "\n				" ];                                                                                                    // 82
  }), "\n				", Blaze.Each(function() {                                                                                // 83
    return Spacebars.call(view.lookup("admin_sidebar_items"));                                                         // 84
  }, function() {                                                                                                      // 85
    return [ "\n					", Blaze.If(function() {                                                                          // 86
      return Spacebars.call(Spacebars.dot(view.lookup("options"), "urls"));                                            // 87
    }, function() {                                                                                                    // 88
      return [ "\n						", Spacebars.include(view.lookupTemplate("adminSidebarItemTree")), "\n					" ];                // 89
    }, function() {                                                                                                    // 90
      return [ "\n						", Spacebars.include(view.lookupTemplate("adminSidebarItem")), "\n					" ];                    // 91
    }), "\n				" ];                                                                                                    // 92
  }), "\n			"), "\n		"), "\n	");                                                                                       // 93
}));                                                                                                                   // 94
                                                                                                                       // 95
Template.__checkName("adminSidebarItem");                                                                              // 96
Template["adminSidebarItem"] = new Template("Template.adminSidebarItem", (function() {                                 // 97
  var view = this;                                                                                                     // 98
  return HTML.LI({                                                                                                     // 99
    "class": function() {                                                                                              // 100
      return Spacebars.mustache(view.lookup("isActivePath"), Spacebars.kw({                                            // 101
        path: view.lookup("url")                                                                                       // 102
      }));                                                                                                             // 103
    }                                                                                                                  // 104
  }, "\n		", HTML.A({                                                                                                  // 105
    href: function() {                                                                                                 // 106
      return Spacebars.mustache(view.lookup("url"));                                                                   // 107
    }                                                                                                                  // 108
  }, "\n			", Blaze.If(function() {                                                                                    // 109
    return Spacebars.call(Spacebars.dot(view.lookup("options"), "icon"));                                              // 110
  }, function() {                                                                                                      // 111
    return [ "\n			", HTML.I({                                                                                         // 112
      "class": function() {                                                                                            // 113
        return [ "fa fa-", Spacebars.mustache(Spacebars.dot(view.lookup("options"), "icon")) ];                        // 114
      }                                                                                                                // 115
    }), "\n			" ];                                                                                                     // 116
  }), "\n			", HTML.SPAN(Blaze.View("lookup:title", function() {                                                       // 117
    return Spacebars.mustache(view.lookup("title"));                                                                   // 118
  })), "\n		"), "\n	");                                                                                                // 119
}));                                                                                                                   // 120
                                                                                                                       // 121
Template.__checkName("adminSidebarItemTree");                                                                          // 122
Template["adminSidebarItemTree"] = new Template("Template.adminSidebarItemTree", (function() {                         // 123
  var view = this;                                                                                                     // 124
  return HTML.LI({                                                                                                     // 125
    "class": "treeview"                                                                                                // 126
  }, "\n		", HTML.A({                                                                                                  // 127
    href: "#"                                                                                                          // 128
  }, "\n			", Blaze.If(function() {                                                                                    // 129
    return Spacebars.call(Spacebars.dot(view.lookup("options"), "icon"));                                              // 130
  }, function() {                                                                                                      // 131
    return [ "\n			", HTML.I({                                                                                         // 132
      "class": function() {                                                                                            // 133
        return [ "fa fa-", Spacebars.mustache(Spacebars.dot(view.lookup("options"), "icon")) ];                        // 134
      }                                                                                                                // 135
    }), "\n			" ];                                                                                                     // 136
  }), "\n			", HTML.SPAN(Blaze.View("lookup:title", function() {                                                       // 137
    return Spacebars.mustache(view.lookup("title"));                                                                   // 138
  })), "\n			", HTML.Raw('<i class="fa fa-angle-left pull-right"></i>'), "\n		"), "\n		", HTML.UL({                    // 139
    "class": "treeview-menu"                                                                                           // 140
  }, "\n			", Blaze.Each(function() {                                                                                  // 141
    return Spacebars.call(Spacebars.dot(view.lookup("options"), "urls"));                                              // 142
  }, function() {                                                                                                      // 143
    return [ "\n			", HTML.LI({                                                                                        // 144
      "class": function() {                                                                                            // 145
        return Spacebars.mustache(view.lookup("isActivePath"), Spacebars.kw({                                          // 146
          path: view.lookup("url")                                                                                     // 147
        }));                                                                                                           // 148
      }                                                                                                                // 149
    }, HTML.A({                                                                                                        // 150
      href: function() {                                                                                               // 151
        return Spacebars.mustache(view.lookup("url"));                                                                 // 152
      }                                                                                                                // 153
    }, HTML.I({                                                                                                        // 154
      "class": "fa fa-angle-double-right"                                                                              // 155
    }), Blaze.View("lookup:title", function() {                                                                        // 156
      return Spacebars.mustache(view.lookup("title"));                                                                 // 157
    }))), "\n			" ];                                                                                                   // 158
  }), "\n		"), "\n	");                                                                                                 // 159
}));                                                                                                                   // 160
                                                                                                                       // 161
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/html/template.admin_header.js                                                     //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("AdminHeader");                                                                                   // 2
Template["AdminHeader"] = new Template("Template.AdminHeader", (function() {                                           // 3
  var view = this;                                                                                                     // 4
  return HTML.HEADER({                                                                                                 // 5
    "class": "main-header"                                                                                             // 6
  }, "\n        ", HTML.A({                                                                                            // 7
    href: "/admin",                                                                                                    // 8
    "class": "logo"                                                                                                    // 9
  }, "\n            ", Blaze.If(function() {                                                                           // 10
    return Spacebars.call(Spacebars.dot(view.lookup("AdminConfig"), "name"));                                          // 11
  }, function() {                                                                                                      // 12
    return [ "\n            ", Blaze.View("lookup:AdminConfig.name", function() {                                      // 13
      return Spacebars.mustache(Spacebars.dot(view.lookup("AdminConfig"), "name"));                                    // 14
    }), "\n            " ];                                                                                            // 15
  }, function() {                                                                                                      // 16
    return "\n            Admin\n            ";                                                                        // 17
  }), "\n        "), "\n        ", HTML.NAV({                                                                          // 18
    "class": "navbar navbar-static-top",                                                                               // 19
    role: "navigation"                                                                                                 // 20
  }, "\n            ", HTML.Raw("<!-- Sidebar toggle button-->"), "\n            ", HTML.Raw('<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">\n                <span class="sr-only">Toggle navigation</span>\n                <span class="icon-bar"></span>\n                <span class="icon-bar"></span>\n                <span class="icon-bar"></span>\n            </a>'), "\n            ", HTML.DIV({
    "class": "navbar-custom-menu"                                                                                      // 22
  }, "\n                ", HTML.UL({                                                                                   // 23
    "class": "nav navbar-nav"                                                                                          // 24
  }, "\n                    ", HTML.LI("\n                        ", Blaze.If(function() {                             // 25
    return Spacebars.call(Spacebars.dot(view.lookup("AdminConfig"), "dashboard", "homeUrl"));                          // 26
  }, function() {                                                                                                      // 27
    return [ "\n                        ", HTML.A({                                                                    // 28
      href: function() {                                                                                               // 29
        return Spacebars.mustache(Spacebars.dot(view.lookup("AdminConfig"), "dashboard", "homeUrl"));                  // 30
      }                                                                                                                // 31
    }, "Home"), "\n                        " ];                                                                        // 32
  }, function() {                                                                                                      // 33
    return [ "\n                        ", HTML.A({                                                                    // 34
      href: "/"                                                                                                        // 35
    }, "Home"), "\n                        " ];                                                                        // 36
  }), "\n\n                    "), "\n                    ", HTML.LI({                                                 // 37
    "class": "dropdown"                                                                                                // 38
  }, "\n                        ", HTML.Raw('<a href="#" class="dropdown-toggle" data-toggle="dropdown">\n                            <span>Admin<i class="caret"></i></span>\n                        </a>'), "\n                        ", HTML.UL({
    "class": "dropdown-menu"                                                                                           // 40
  }, "\n                            ", HTML.LI({                                                                       // 41
    "class": ""                                                                                                        // 42
  }, "\n                            ", HTML.A({                                                                        // 43
    href: function() {                                                                                                 // 44
      return Spacebars.mustache(view.lookup("pathFor"), "adminDashboardUsersEdit", Spacebars.kw({                      // 45
        _id: Spacebars.dot(view.lookup("currentUser"), "_id")                                                          // 46
      }));                                                                                                             // 47
    }                                                                                                                  // 48
  }, "\n                                Your profile\n                                "), "\n                            "), "\n                            ", HTML.Raw('<li class="">\n                                <a href="#" class="btn-sign-out">Sign out\n                                </a>\n                            </li>'), "\n                        "), "\n                    "), "\n                "), "\n            "), "\n        "), "\n    ");
}));                                                                                                                   // 50
                                                                                                                       // 51
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/js/admin_layout.js                                                                //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
Template.AdminLayout.created = function () {                                                                           // 1
  var self = this;                                                                                                     // 2
                                                                                                                       // 3
  self.minHeight = new ReactiveVar(                                                                                    // 4
    $(window).height() - $('.main-header').height());                                                                  // 5
                                                                                                                       // 6
  $(window).resize(function () {                                                                                       // 7
    self.minHeight.set($(window).height() - $('.main-header').height());                                               // 8
  });                                                                                                                  // 9
                                                                                                                       // 10
  $('body').addClass('fixed');                                                                                         // 11
};                                                                                                                     // 12
                                                                                                                       // 13
Template.AdminLayout.destroyed = function () {                                                                         // 14
  $('body').removeClass('fixed');                                                                                      // 15
};                                                                                                                     // 16
                                                                                                                       // 17
Template.AdminLayout.helpers({                                                                                         // 18
  minHeight: function () {                                                                                             // 19
    return Template.instance().minHeight.get() + 'px'                                                                  // 20
  }                                                                                                                    // 21
});                                                                                                                    // 22
                                                                                                                       // 23
dataTableOptions = {                                                                                                   // 24
    "aaSorting": [],                                                                                                   // 25
    "bPaginate": true,                                                                                                 // 26
    "bLengthChange": false,                                                                                            // 27
    "bFilter": true,                                                                                                   // 28
    "bSort": true,                                                                                                     // 29
    "bInfo": true,                                                                                                     // 30
    "bAutoWidth": false                                                                                                // 31
};                                                                                                                     // 32
                                                                                                                       // 33
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/js/helpers.coffee.js                                                              //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var adminCollections;                                                                                                  // 1
                                                                                                                       //
Template.registerHelper('AdminTables', AdminTables);                                                                   // 1
                                                                                                                       //
adminCollections = function() {                                                                                        // 1
  var collections;                                                                                                     // 4
  collections = {};                                                                                                    // 4
  if (typeof AdminConfig !== 'undefined' && typeof AdminConfig.collections === 'object') {                             // 6
    collections = AdminConfig.collections;                                                                             // 7
  }                                                                                                                    //
  collections.Users = {                                                                                                // 4
    collectionObject: Meteor.users,                                                                                    // 10
    icon: 'user',                                                                                                      // 10
    label: 'Users'                                                                                                     // 10
  };                                                                                                                   //
  return _.map(collections, function(obj, key) {                                                                       //
    obj = _.extend(obj, {                                                                                              // 15
      name: key                                                                                                        // 15
    });                                                                                                                //
    obj = _.defaults(obj, {                                                                                            // 15
      label: key,                                                                                                      // 16
      icon: 'plus',                                                                                                    // 16
      color: 'blue'                                                                                                    // 16
    });                                                                                                                //
    return obj = _.extend(obj, {                                                                                       //
      viewPath: Router.path("adminDashboard" + key + "View"),                                                          // 18
      newPath: Router.path("adminDashboard" + key + "New")                                                             // 18
    });                                                                                                                //
  });                                                                                                                  //
};                                                                                                                     // 3
                                                                                                                       //
UI.registerHelper('AdminConfig', function() {                                                                          // 1
  if (typeof AdminConfig !== 'undefined') {                                                                            // 22
    return AdminConfig;                                                                                                //
  }                                                                                                                    //
});                                                                                                                    // 21
                                                                                                                       //
UI.registerHelper('admin_skin', function() {                                                                           // 1
  return (typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.skin : void 0) || 'blue';           //
});                                                                                                                    // 24
                                                                                                                       //
UI.registerHelper('admin_collections', adminCollections);                                                              // 1
                                                                                                                       //
UI.registerHelper('admin_collection_name', function() {                                                                // 1
  return Session.get('admin_collection_name');                                                                         //
});                                                                                                                    // 29
                                                                                                                       //
UI.registerHelper('admin_current_id', function() {                                                                     // 1
  return Session.get('admin_id');                                                                                      //
});                                                                                                                    // 32
                                                                                                                       //
UI.registerHelper('admin_current_doc', function() {                                                                    // 1
  return Session.get('admin_doc');                                                                                     //
});                                                                                                                    // 35
                                                                                                                       //
UI.registerHelper('admin_is_users_collection', function() {                                                            // 1
  return Session.get('admin_collection_name') === 'Users';                                                             //
});                                                                                                                    // 38
                                                                                                                       //
UI.registerHelper('admin_sidebar_items', function() {                                                                  // 1
  return AdminDashboard.sidebarItems;                                                                                  //
});                                                                                                                    // 41
                                                                                                                       //
UI.registerHelper('admin_collection_items', function() {                                                               // 1
  var items;                                                                                                           // 45
  items = [];                                                                                                          // 45
  _.each(AdminDashboard.collectionItems, (function(_this) {                                                            // 45
    return function(fn) {                                                                                              //
      var item;                                                                                                        // 47
      item = fn(_this.name, '/admin/' + _this.name);                                                                   // 47
      if ((item != null ? item.title : void 0) && (item != null ? item.url : void 0)) {                                // 48
        return items.push(item);                                                                                       //
      }                                                                                                                //
    };                                                                                                                 //
  })(this));                                                                                                           //
  return items;                                                                                                        //
});                                                                                                                    // 44
                                                                                                                       //
UI.registerHelper('admin_omit_fields', function() {                                                                    // 1
  var collection, global;                                                                                              // 53
  if (typeof AdminConfig.autoForm !== 'undefined' && typeof AdminConfig.autoForm.omitFields === 'object') {            // 53
    global = AdminConfig.autoForm.omitFields;                                                                          // 54
  }                                                                                                                    //
  if (!Session.equals('admin_collection_name', 'Users') && typeof AdminConfig !== 'undefined' && typeof AdminConfig.collections[Session.get('admin_collection_name')].omitFields === 'object') {
    collection = AdminConfig.collections[Session.get('admin_collection_name')].omitFields;                             // 56
  }                                                                                                                    //
  if (typeof global === 'object' && typeof collection === 'object') {                                                  // 57
    return _.union(global, collection);                                                                                //
  } else if (typeof global === 'object') {                                                                             //
    return global;                                                                                                     //
  } else if (typeof collection === 'object') {                                                                         //
    return collection;                                                                                                 //
  }                                                                                                                    //
});                                                                                                                    // 52
                                                                                                                       //
UI.registerHelper('AdminSchemas', function() {                                                                         // 1
  return AdminDashboard.schemas;                                                                                       //
});                                                                                                                    // 64
                                                                                                                       //
UI.registerHelper('adminGetSkin', function() {                                                                         // 1
  if (typeof AdminConfig.dashboard !== 'undefined' && typeof AdminConfig.dashboard.skin === 'string') {                // 68
    return AdminConfig.dashboard.skin;                                                                                 //
  } else {                                                                                                             //
    return 'blue';                                                                                                     //
  }                                                                                                                    //
});                                                                                                                    // 67
                                                                                                                       //
UI.registerHelper('adminIsUserInRole', function(_id, role) {                                                           // 1
  return Roles.userIsInRole(_id, role);                                                                                //
});                                                                                                                    // 73
                                                                                                                       //
UI.registerHelper('adminGetUsers', function() {                                                                        // 1
  return Meteor.users;                                                                                                 //
});                                                                                                                    // 76
                                                                                                                       //
UI.registerHelper('adminGetUserSchema', function() {                                                                   // 1
  var schema;                                                                                                          // 80
  if (_.has(AdminConfig, 'userSchema')) {                                                                              // 80
    schema = AdminConfig.userSchema;                                                                                   // 81
  } else if (typeof Meteor.users._c2 === 'object') {                                                                   //
    schema = Meteor.users.simpleSchema();                                                                              // 83
  }                                                                                                                    //
  return schema;                                                                                                       // 85
});                                                                                                                    // 79
                                                                                                                       //
UI.registerHelper('adminCollectionLabel', function(collection) {                                                       // 1
  if (collection != null) {                                                                                            // 88
    return AdminDashboard.collectionLabel(collection);                                                                 //
  }                                                                                                                    //
});                                                                                                                    // 87
                                                                                                                       //
UI.registerHelper('adminCollectionCount', function(collection) {                                                       // 1
  var ref;                                                                                                             // 91
  if (collection === 'Users') {                                                                                        // 91
    return Meteor.users.find().count();                                                                                //
  } else {                                                                                                             //
    return (ref = AdminCollectionsCount.findOne({                                                                      //
      collection: collection                                                                                           //
    })) != null ? ref.count : void 0;                                                                                  //
  }                                                                                                                    //
});                                                                                                                    // 90
                                                                                                                       //
UI.registerHelper('adminTemplate', function(collection, mode) {                                                        // 1
  var ref, ref1;                                                                                                       // 97
  if ((collection != null ? collection.toLowerCase() : void 0) !== 'users' && typeof (typeof AdminConfig !== "undefined" && AdminConfig !== null ? (ref = AdminConfig.collections) != null ? (ref1 = ref[collection]) != null ? ref1.templates : void 0 : void 0 : void 0) !== 'undefined') {
    return AdminConfig.collections[collection].templates[mode];                                                        //
  }                                                                                                                    //
});                                                                                                                    // 96
                                                                                                                       //
UI.registerHelper('adminGetCollection', function(collection) {                                                         // 1
  return _.find(adminCollections(), function(item) {                                                                   //
    return item.name === collection;                                                                                   //
  });                                                                                                                  //
});                                                                                                                    // 100
                                                                                                                       //
UI.registerHelper('adminWidgets', function() {                                                                         // 1
  if (typeof AdminConfig.dashboard !== 'undefined' && typeof AdminConfig.dashboard.widgets !== 'undefined') {          // 104
    return AdminConfig.dashboard.widgets;                                                                              //
  }                                                                                                                    //
});                                                                                                                    // 103
                                                                                                                       //
UI.registerHelper('adminUserEmail', function(user) {                                                                   // 1
  if (user && user.emails && user.emails[0] && user.emails[0].address) {                                               // 108
    return user.emails[0].address;                                                                                     //
  } else if (user && user.services && user.services.facebook && user.services.facebook.email) {                        //
    return user.services.facebook.email;                                                                               //
  } else if (user && user.services && user.services.google && user.services.google.email) {                            //
    return user.services.google.email;                                                                                 //
  }                                                                                                                    //
});                                                                                                                    // 107
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/js/templates.coffee.js                                                            //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.AdminDashboardViewWrapper.rendered = function() {                                                             // 1
  var node;                                                                                                            // 2
  node = this.firstNode;                                                                                               // 2
  return this.autorun(function() {                                                                                     //
    var data;                                                                                                          // 5
    data = Template.currentData();                                                                                     // 5
    if (data.view) {                                                                                                   // 7
      Blaze.remove(data.view);                                                                                         // 7
    }                                                                                                                  //
    while (node.firstChild) {                                                                                          // 8
      node.removeChild(node.firstChild);                                                                               // 9
    }                                                                                                                  //
    return data.view = Blaze.renderWithData(Template.AdminDashboardView, data, node);                                  //
  });                                                                                                                  //
};                                                                                                                     // 1
                                                                                                                       //
Template.AdminDashboardViewWrapper.destroyed = function() {                                                            // 1
  return Blaze.remove(this.data.view);                                                                                 //
};                                                                                                                     // 13
                                                                                                                       //
Template.AdminDashboardView.rendered = function() {                                                                    // 1
  var filter, length, table;                                                                                           // 17
  table = this.$('.dataTable').DataTable();                                                                            // 17
  filter = this.$('.dataTables_filter');                                                                               // 17
  length = this.$('.dataTables_length');                                                                               // 17
  filter.html('<div class="input-group"> <input type="search" class="form-control input-sm" placeholder="Search"></input> <div class="input-group-btn"> <button class="btn btn-sm btn-default"> <i class="fa fa-search"></i> </button> </div> </div>');
  length.html('<select class="form-control input-sm"> <option value="10">10</option> <option value="25">25</option> <option value="50">50</option> <option value="100">100</option> </select>');
  filter.find('input').on('keyup', function() {                                                                        // 17
    return table.search(this.value).draw();                                                                            //
  });                                                                                                                  //
  return length.find('select').on('change', function() {                                                               //
    return table.page.len(parseInt(this.value)).draw();                                                                //
  });                                                                                                                  //
};                                                                                                                     // 16
                                                                                                                       //
Template.AdminDashboardView.helpers({                                                                                  // 1
  hasDocuments: function() {                                                                                           // 48
    var ref;                                                                                                           // 49
    return ((ref = AdminCollectionsCount.findOne({                                                                     //
      collection: Session.get('admin_collection_name')                                                                 //
    })) != null ? ref.count : void 0) > 0;                                                                             //
  },                                                                                                                   //
  newPath: function() {                                                                                                // 48
    return Router.path('adminDashboard' + Session.get('admin_collection_name') + 'New');                               //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Template.adminEditBtn.helpers({                                                                                        // 1
  path: function() {                                                                                                   // 54
    return Router.path("adminDashboard" + Session.get('admin_collection_name') + "Edit", {                             //
      _id: this._id                                                                                                    // 55
    });                                                                                                                //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/js/events.coffee.js                                                               //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.AdminLayout.events({                                                                                          // 1
  'click .btn-delete': function(e, t) {                                                                                // 2
    var _id;                                                                                                           // 3
    _id = $(e.target).attr('doc');                                                                                     // 3
    if (Session.equals('admin_collection_name', 'Users')) {                                                            // 4
      Session.set('admin_id', _id);                                                                                    // 5
      return Session.set('admin_doc', Meteor.users.findOne(_id));                                                      //
    } else {                                                                                                           //
      Session.set('admin_id', parseID(_id));                                                                           // 8
      return Session.set('admin_doc', adminCollectionObject(Session.get('admin_collection_name')).findOne(parseID(_id)));
    }                                                                                                                  //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Template.AdminDeleteModal.events({                                                                                     // 1
  'click #confirm-delete': function() {                                                                                // 12
    var _id, collection;                                                                                               // 13
    collection = Session.get('admin_collection_name');                                                                 // 13
    _id = Session.get('admin_id');                                                                                     // 13
    return Meteor.call('adminRemoveDoc', collection, _id, function(e, r) {                                             //
      return $('#admin-delete-modal').modal('hide');                                                                   //
    });                                                                                                                //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Template.AdminDashboardUsersEdit.events({                                                                              // 1
  'click .btn-add-role': function(e, t) {                                                                              // 19
    console.log('adding user');                                                                                        // 20
    return Meteor.call('adminAddUserToRole', $(e.target).attr('user'), $(e.target).attr('role'));                      //
  },                                                                                                                   //
  'click .btn-remove-role': function(e, t) {                                                                           // 19
    console.log('removing user');                                                                                      // 23
    return Meteor.call('adminRemoveUserToRole', $(e.target).attr('user'), $(e.target).attr('role'));                   //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
Template.AdminHeader.events({                                                                                          // 1
  'click .btn-sign-out': function() {                                                                                  // 27
    return Meteor.logout(function() {                                                                                  //
      return Router.go((typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.logoutRedirect : void 0) || '/');
    });                                                                                                                //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/js/slim_scroll.js                                                                 //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
/*! Copyright (c) 2011 Piotr Rochala (http://rocha.la)                                                                 // 1
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)                                    // 2
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.                                              // 3
 *                                                                                                                     // 4
 * Version: 1.3.0                                                                                                      // 5
 *                                                                                                                     // 6
 */                                                                                                                    // 7
(function(f){jQuery.fn.extend({slimScroll:function(h){var a=f.extend({width:"auto",height:"250px",size:"7px",color:"#000",position:"right",distance:"1px",start:"top",opacity:0.4,alwaysVisible:!1,disableFadeOut:!1,railVisible:!1,railColor:"#333",railOpacity:0.2,railDraggable:!0,railClass:"slimScrollRail",barClass:"slimScrollBar",wrapperClass:"slimScrollDiv",allowPageScroll:!1,wheelStep:20,touchScrollStep:200,borderRadius:"7px",railBorderRadius:"7px"},h);this.each(function(){function r(d){if(s){d=d||
window.event;var c=0;d.wheelDelta&&(c=-d.wheelDelta/120);d.detail&&(c=d.detail/3);f(d.target||d.srcTarget||d.srcElement).closest("."+a.wrapperClass).is(b.parent())&&m(c,!0);d.preventDefault&&!k&&d.preventDefault();k||(d.returnValue=!1)}}function m(d,f,h){k=!1;var e=d,g=b.outerHeight()-c.outerHeight();f&&(e=parseInt(c.css("top"))+d*parseInt(a.wheelStep)/100*c.outerHeight(),e=Math.min(Math.max(e,0),g),e=0<d?Math.ceil(e):Math.floor(e),c.css({top:e+"px"}));l=parseInt(c.css("top"))/(b.outerHeight()-c.outerHeight());
e=l*(b[0].scrollHeight-b.outerHeight());h&&(e=d,d=e/b[0].scrollHeight*b.outerHeight(),d=Math.min(Math.max(d,0),g),c.css({top:d+"px"}));b.scrollTop(e);b.trigger("slimscrolling",~~e);v();p()}function C(){window.addEventListener?(this.addEventListener("DOMMouseScroll",r,!1),this.addEventListener("mousewheel",r,!1),this.addEventListener("MozMousePixelScroll",r,!1)):document.attachEvent("onmousewheel",r)}function w(){u=Math.max(b.outerHeight()/b[0].scrollHeight*b.outerHeight(),D);c.css({height:u+"px"});
var a=u==b.outerHeight()?"none":"block";c.css({display:a})}function v(){w();clearTimeout(A);l==~~l?(k=a.allowPageScroll,B!=l&&b.trigger("slimscroll",0==~~l?"top":"bottom")):k=!1;B=l;u>=b.outerHeight()?k=!0:(c.stop(!0,!0).fadeIn("fast"),a.railVisible&&g.stop(!0,!0).fadeIn("fast"))}function p(){a.alwaysVisible||(A=setTimeout(function(){a.disableFadeOut&&s||(x||y)||(c.fadeOut("slow"),g.fadeOut("slow"))},1E3))}var s,x,y,A,z,u,l,B,D=30,k=!1,b=f(this);if(b.parent().hasClass(a.wrapperClass)){var n=b.scrollTop(),
c=b.parent().find("."+a.barClass),g=b.parent().find("."+a.railClass);w();if(f.isPlainObject(h)){if("height"in h&&"auto"==h.height){b.parent().css("height","auto");b.css("height","auto");var q=b.parent().parent().height();b.parent().css("height",q);b.css("height",q)}if("scrollTo"in h)n=parseInt(a.scrollTo);else if("scrollBy"in h)n+=parseInt(a.scrollBy);else if("destroy"in h){c.remove();g.remove();b.unwrap();return}m(n,!1,!0)}}else{a.height="auto"==a.height?b.parent().height():a.height;n=f("<div></div>").addClass(a.wrapperClass).css({position:"relative",
overflow:"hidden",width:a.width,height:a.height});b.css({overflow:"hidden",width:a.width,height:a.height});var g=f("<div></div>").addClass(a.railClass).css({width:a.size,height:"100%",position:"absolute",top:0,display:a.alwaysVisible&&a.railVisible?"block":"none","border-radius":a.railBorderRadius,background:a.railColor,opacity:a.railOpacity,zIndex:90}),c=f("<div></div>").addClass(a.barClass).css({background:a.color,width:a.size,position:"absolute",top:0,opacity:a.opacity,display:a.alwaysVisible?
"block":"none","border-radius":a.borderRadius,BorderRadius:a.borderRadius,MozBorderRadius:a.borderRadius,WebkitBorderRadius:a.borderRadius,zIndex:99}),q="right"==a.position?{right:a.distance}:{left:a.distance};g.css(q);c.css(q);b.wrap(n);b.parent().append(c);b.parent().append(g);a.railDraggable&&c.bind("mousedown",function(a){var b=f(document);y=!0;t=parseFloat(c.css("top"));pageY=a.pageY;b.bind("mousemove.slimscroll",function(a){currTop=t+a.pageY-pageY;c.css("top",currTop);m(0,c.position().top,!1)});
b.bind("mouseup.slimscroll",function(a){y=!1;p();b.unbind(".slimscroll")});return!1}).bind("selectstart.slimscroll",function(a){a.stopPropagation();a.preventDefault();return!1});g.hover(function(){v()},function(){p()});c.hover(function(){x=!0},function(){x=!1});b.hover(function(){s=!0;v();p()},function(){s=!1;p()});b.bind("touchstart",function(a,b){a.originalEvent.touches.length&&(z=a.originalEvent.touches[0].pageY)});b.bind("touchmove",function(b){k||b.originalEvent.preventDefault();b.originalEvent.touches.length&&
(m((z-b.originalEvent.touches[0].pageY)/a.touchScrollStep,!0),z=b.originalEvent.touches[0].pageY)});w();"bottom"===a.start?(c.css({top:b.outerHeight()-c.outerHeight()}),m(0,!0)):"top"!==a.start&&(m(f(a.start).position().top,null,!0),a.alwaysVisible||c.hide());C()}});return this}});jQuery.fn.extend({slimscroll:jQuery.fn.slimScroll})})(jQuery);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/yogiben_admin/lib/client/js/autoForm.coffee.js                                                             //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
AutoForm.addHooks(['admin_insert', 'admin_update', 'adminNewUser', 'adminUpdateUser', 'adminSendResetPasswordEmail', 'adminChangePassword'], {
  beginSubmit: function() {                                                                                            // 9
    return $('.btn-primary').addClass('disabled');                                                                     //
  },                                                                                                                   //
  endSubmit: function() {                                                                                              // 9
    return $('.btn-primary').removeClass('disabled');                                                                  //
  },                                                                                                                   //
  onError: function(formType, error) {                                                                                 // 9
    return AdminDashboard.alertFailure(error.message);                                                                 //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
AutoForm.hooks({                                                                                                       // 2
  admin_insert: {                                                                                                      // 17
    onSubmit: function(insertDoc, updateDoc, currentDoc) {                                                             // 18
      var hook;                                                                                                        // 19
      hook = this;                                                                                                     // 19
      Meteor.call('adminInsertDoc', insertDoc, Session.get('admin_collection_name'), function(e, r) {                  // 19
        if (e) {                                                                                                       // 21
          return hook.done(e);                                                                                         //
        } else {                                                                                                       //
          return adminCallback('onInsert', [Session.get('admin_collection_name', insertDoc, updateDoc, currentDoc)], function(collection) {
            return hook.done(null, collection);                                                                        //
          });                                                                                                          //
        }                                                                                                              //
      });                                                                                                              //
      return false;                                                                                                    // 26
    },                                                                                                                 //
    onSuccess: function(formType, collection) {                                                                        // 18
      AdminDashboard.alertSuccess('Successfully created');                                                             // 28
      return Router.go("/admin/" + collection);                                                                        //
    }                                                                                                                  //
  },                                                                                                                   //
  admin_update: {                                                                                                      // 17
    onSubmit: function(insertDoc, updateDoc, currentDoc) {                                                             // 32
      var hook;                                                                                                        // 33
      hook = this;                                                                                                     // 33
      Meteor.call('adminUpdateDoc', updateDoc, Session.get('admin_collection_name'), Session.get('admin_id'), function(e, r) {
        if (e) {                                                                                                       // 35
          return hook.done(e);                                                                                         //
        } else {                                                                                                       //
          return adminCallback('onUpdate', [Session.get('admin_collection_name', insertDoc, updateDoc, currentDoc)], function(collection) {
            return hook.done(null, collection);                                                                        //
          });                                                                                                          //
        }                                                                                                              //
      });                                                                                                              //
      return false;                                                                                                    // 40
    },                                                                                                                 //
    onSuccess: function(formType, collection) {                                                                        // 32
      AdminDashboard.alertSuccess('Successfully updated');                                                             // 42
      return Router.go("/admin/" + collection);                                                                        //
    }                                                                                                                  //
  },                                                                                                                   //
  adminNewUser: {                                                                                                      // 17
    onSuccess: function(formType, result) {                                                                            // 46
      AdminDashboard.alertSuccess('Created user');                                                                     // 47
      return Router.go('/admin/Users');                                                                                //
    }                                                                                                                  //
  },                                                                                                                   //
  adminUpdateUser: {                                                                                                   // 17
    onSubmit: function(insertDoc, updateDoc, currentDoc) {                                                             // 51
      Meteor.call('adminUpdateUser', updateDoc, Session.get('admin_id'), this.done);                                   // 52
      return false;                                                                                                    // 53
    },                                                                                                                 //
    onSuccess: function(formType, result) {                                                                            // 51
      AdminDashboard.alertSuccess('Updated user');                                                                     // 55
      return Router.go('/admin/Users');                                                                                //
    }                                                                                                                  //
  },                                                                                                                   //
  adminSendResetPasswordEmail: {                                                                                       // 17
    onSuccess: function(formType, result) {                                                                            // 59
      return AdminDashboard.alertSuccess('Email sent');                                                                //
    }                                                                                                                  //
  },                                                                                                                   //
  adminChangePassword: {                                                                                               // 17
    onSuccess: function(operation, result, template) {                                                                 // 63
      return AdminDashboard.alertSuccess('Password reset');                                                            //
    }                                                                                                                  //
  }                                                                                                                    //
});                                                                                                                    //
                                                                                                                       //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
(function (pkg, symbols) {
  for (var s in symbols)
    (s in pkg) || (pkg[s] = symbols[s]);
})(Package['yogiben:admin'] = {}, {
  AdminDashboard: AdminDashboard
});

})();
