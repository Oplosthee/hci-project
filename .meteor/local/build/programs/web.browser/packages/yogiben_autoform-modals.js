//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var $ = Package.jquery.$;
var jQuery = Package.jquery.jQuery;
var Template = Package.templating.Template;
var Session = Package.session.Session;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var AutoForm = Package['aldeed:autoform'].AutoForm;
var Helpers = Package['raix:handlebar-helpers'].Helpers;
var StringTemplate = Package['mpowaga:string-template'].StringTemplate;
var Spacebars = Package.spacebars.Spacebars;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                               //
// packages/yogiben_autoform-modals/lib/client/template.modals.js                                                //
//                                                                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                 //
                                                                                                                 // 1
Template.__checkName("autoformModals");                                                                          // 2
Template["autoformModals"] = new Template("Template.autoformModals", (function() {                               // 3
  var view = this;                                                                                               // 4
  return HTML.DIV({                                                                                              // 5
    "class": "modal fade",                                                                                       // 6
    id: "afModal"                                                                                                // 7
  }, "\n		", HTML.DIV({                                                                                          // 8
    "class": function() {                                                                                        // 9
      return [ "modal-dialog ", Spacebars.mustache(view.lookup("cmModalDialogClass")) ];                         // 10
    }                                                                                                            // 11
  }, "\n			", HTML.DIV({                                                                                         // 12
    "class": "modal-content"                                                                                     // 13
  }, "\n				", HTML.DIV({                                                                                        // 14
    "class": "modal-header"                                                                                      // 15
  }, "\n					", HTML.Raw('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'), "\n					", HTML.H4({
    "class": "modal-title"                                                                                       // 17
  }, Blaze.View("lookup:title", function() {                                                                     // 18
    return Spacebars.makeRaw(Spacebars.mustache(view.lookup("title")));                                          // 19
  })), "\n				"), "\n				", HTML.DIV({                                                                           // 20
    "class": "modal-body"                                                                                        // 21
  }, "\n					", Blaze.If(function() {                                                                            // 22
    return Spacebars.dataMustache(view.lookup("$and"), view.lookup("cmCollection"), view.lookup("cmOperation"));
  }, function() {                                                                                                // 24
    return [ "\n					", HTML.P(Blaze.View("lookup:prompt", function() {                                          // 25
      return Spacebars.makeRaw(Spacebars.mustache(view.lookup("prompt")));                                       // 26
    })), "\n					", Blaze.If(function() {                                                                        // 27
      return Spacebars.dataMustache(view.lookup("$neq"), view.lookup("cmOperation"), "remove");                  // 28
    }, function() {                                                                                              // 29
      return [ "\n					", Blaze._TemplateWith(function() {                                                       // 30
        return {                                                                                                 // 31
          title: Spacebars.call(view.lookup("cmTitle")),                                                         // 32
          id: Spacebars.call(view.lookup("cmFormId")),                                                           // 33
          collection: Spacebars.call(view.lookup("cmCollection")),                                               // 34
          doc: Spacebars.call(view.lookup("cmDoc")),                                                             // 35
          type: Spacebars.call(view.lookup("cmAutoformType")),                                                   // 36
          buttonContent: Spacebars.call(view.lookup("cmButtonContent")),                                         // 37
          fields: Spacebars.call(view.lookup("cmFields")),                                                       // 38
          omitFields: Spacebars.call(view.lookup("cmOmitFields")),                                               // 39
          template: Spacebars.call(view.lookup("cmTemplate")),                                                   // 40
          "label-class": Spacebars.call(view.lookup("cmLabelClass")),                                            // 41
          "input-col-class": Spacebars.call(view.lookup("cmInputColClass")),                                     // 42
          buttonClasses: Spacebars.call(view.lookup("cmButtonClasses")),                                         // 43
          "afFieldInput-placeholder": Spacebars.call(view.lookup("cmPlaceholder")),                              // 44
          meteormethod: Spacebars.call(view.lookup("cmMeteorMethod"))                                            // 45
        };                                                                                                       // 46
      }, function() {                                                                                            // 47
        return Spacebars.include(view.lookupTemplate("quickForm"));                                              // 48
      }), "\n					" ];                                                                                           // 49
    }), "\n					" ];                                                                                             // 50
  }), "\n				"), "\n				", Blaze.If(function() {                                                                 // 51
    return Spacebars.dataMustache(view.lookup("$eq"), view.lookup("cmOperation"), "remove");                     // 52
  }, function() {                                                                                                // 53
    return [ "\n				", HTML.DIV({                                                                                // 54
      "class": "modal-footer"                                                                                    // 55
    }, "\n					", Blaze.If(function() {                                                                          // 56
      return Spacebars.call(view.lookup("closeButtonContent"));                                                  // 57
    }, function() {                                                                                              // 58
      return [ "\n					", HTML.A({                                                                               // 59
        role: "button",                                                                                          // 60
        href: "#",                                                                                               // 61
        "data-dismiss": "modal",                                                                                 // 62
        "class": function() {                                                                                    // 63
          return Spacebars.mustache(view.lookup("cmCloseButtonClasses"));                                        // 64
        }                                                                                                        // 65
      }, Blaze.View("lookup:closeButtonContent", function() {                                                    // 66
        return Spacebars.makeRaw(Spacebars.mustache(view.lookup("closeButtonContent")));                         // 67
      })), "\n					" ];                                                                                          // 68
    }), "\n					", HTML.BUTTON({                                                                                 // 69
      "class": function() {                                                                                      // 70
        return Spacebars.mustache(view.lookup("cmButtonClasses"));                                               // 71
      }                                                                                                          // 72
    }, Blaze.View("lookup:buttonContent", function() {                                                           // 73
      return Spacebars.makeRaw(Spacebars.mustache(view.lookup("buttonContent")));                                // 74
    })), "\n				"), "\n				" ];                                                                                  // 75
  }), "\n			"), "\n		"), "\n	");                                                                                 // 76
}));                                                                                                             // 77
                                                                                                                 // 78
Template.__checkName("afModal");                                                                                 // 79
Template["afModal"] = new Template("Template.afModal", (function() {                                             // 80
  var view = this;                                                                                               // 81
  return HTML.A({                                                                                                // 82
    href: "#afModal",                                                                                            // 83
    "class": function() {                                                                                        // 84
      return Spacebars.mustache(view.lookup("class"));                                                           // 85
    },                                                                                                           // 86
    collection: function() {                                                                                     // 87
      return Spacebars.mustache(view.lookup("collection"));                                                      // 88
    },                                                                                                           // 89
    operation: function() {                                                                                      // 90
      return Spacebars.mustache(view.lookup("operation"));                                                       // 91
    }                                                                                                            // 92
  }, "\n		", Blaze._InOuterTemplateScope(view, function() {                                                      // 93
    return Spacebars.include(function() {                                                                        // 94
      return Spacebars.call(view.templateContentBlock);                                                          // 95
    });                                                                                                          // 96
  }), "\n	");                                                                                                    // 97
}));                                                                                                             // 98
                                                                                                                 // 99
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                               //
// packages/yogiben_autoform-modals/lib/client/modals.coffee.js                                                  //
//                                                                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                 //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var cmOnSuccessCallback, collectionObj, defaultFormId, helpers, registeredAutoFormHooks;                         // 1
                                                                                                                 //
registeredAutoFormHooks = ['cmForm'];                                                                            // 1
                                                                                                                 //
defaultFormId = 'cmForm';                                                                                        // 1
                                                                                                                 //
cmOnSuccessCallback = null;                                                                                      // 1
                                                                                                                 //
AutoForm.addHooks('cmForm', {                                                                                    // 1
  onSuccess: function() {                                                                                        // 7
    return $('#afModal').modal('hide');                                                                          //
  }                                                                                                              //
});                                                                                                              //
                                                                                                                 //
collectionObj = function(name) {                                                                                 // 1
  return name.split('.').reduce(function(o, x) {                                                                 //
    return o[x];                                                                                                 //
  }, window);                                                                                                    //
};                                                                                                               // 10
                                                                                                                 //
Template.autoformModals.rendered = function() {                                                                  // 1
  var onEscKey;                                                                                                  // 16
  $('#afModal').modal({                                                                                          // 16
    show: false                                                                                                  // 16
  });                                                                                                            //
  onEscKey = function(e) {                                                                                       // 16
    if (e.keyCode === 27) {                                                                                      // 19
      return $('#afModal').modal('hide');                                                                        //
    }                                                                                                            //
  };                                                                                                             //
  $('#afModal').on('shown.bs.modal', function() {                                                                // 16
    return $(window).bind('keyup', onEscKey);                                                                    //
  });                                                                                                            //
  return $('#afModal').on('hidden.bs.modal', function() {                                                        //
    var i, key, len, results, sessionKeys;                                                                       // 26
    $(window).unbind('keyup', onEscKey);                                                                         // 26
    AutoForm.resetForm(Session.get('cmFormId') || defaultFormId);                                                // 26
    sessionKeys = ['cmCollection', 'cmOperation', 'cmDoc', 'cmButtonHtml', 'cmFields', 'cmOmitFields', 'cmButtonContent', 'cmTitle', 'cmButtonClasses', 'cmPrompt', 'cmTemplate', 'cmLabelClass', 'cmInputColClass', 'cmPlaceholder', 'cmFormId', 'cmAutoformType', 'cmMeteorMethod', 'cmCloseButtonContent', 'cmCloseButtonClasses'];
    results = [];                                                                                                // 51
    for (i = 0, len = sessionKeys.length; i < len; i++) {                                                        //
      key = sessionKeys[i];                                                                                      //
      results.push(delete Session.keys[key]);                                                                    // 51
    }                                                                                                            // 51
    return results;                                                                                              //
  });                                                                                                            //
};                                                                                                               // 15
                                                                                                                 //
Template.autoformModals.events({                                                                                 // 1
  'click button:not(.close)': function() {                                                                       // 54
    var _id, collection, operation;                                                                              // 55
    collection = Session.get('cmCollection');                                                                    // 55
    operation = Session.get('cmOperation');                                                                      // 55
    if (operation !== 'insert') {                                                                                // 58
      _id = Session.get('cmDoc')._id;                                                                            // 59
    }                                                                                                            //
    if (operation === 'remove') {                                                                                // 61
      return collectionObj(collection).remove(_id, function(e) {                                                 //
        if (e) {                                                                                                 // 63
          return alert('Sorry, this could not be deleted.');                                                     //
        } else {                                                                                                 //
          $('#afModal').modal('hide');                                                                           // 66
          return typeof cmOnSuccessCallback === "function" ? cmOnSuccessCallback() : void 0;                     //
        }                                                                                                        //
      });                                                                                                        //
    }                                                                                                            //
  }                                                                                                              //
});                                                                                                              //
                                                                                                                 //
helpers = {                                                                                                      // 1
  cmCollection: function() {                                                                                     // 70
    return Session.get('cmCollection');                                                                          //
  },                                                                                                             //
  cmOperation: function() {                                                                                      // 70
    return Session.get('cmOperation');                                                                           //
  },                                                                                                             //
  cmDoc: function() {                                                                                            // 70
    return Session.get('cmDoc');                                                                                 //
  },                                                                                                             //
  cmButtonHtml: function() {                                                                                     // 70
    return Session.get('cmButtonHtml');                                                                          //
  },                                                                                                             //
  cmFields: function() {                                                                                         // 70
    return Session.get('cmFields');                                                                              //
  },                                                                                                             //
  cmOmitFields: function() {                                                                                     // 70
    return Session.get('cmOmitFields');                                                                          //
  },                                                                                                             //
  cmButtonContent: function() {                                                                                  // 70
    return Session.get('cmButtonContent');                                                                       //
  },                                                                                                             //
  cmCloseButtonContent: function() {                                                                             // 70
    return Session.get('cmCloseButtonContent');                                                                  //
  },                                                                                                             //
  cmTitle: function() {                                                                                          // 70
    return Session.get('cmTitle');                                                                               //
  },                                                                                                             //
  cmButtonClasses: function() {                                                                                  // 70
    return Session.get('cmButtonClasses');                                                                       //
  },                                                                                                             //
  cmCloseButtonClasses: function() {                                                                             // 70
    return Session.get('cmCloseButtonClasses');                                                                  //
  },                                                                                                             //
  cmPrompt: function() {                                                                                         // 70
    return Session.get('cmPrompt');                                                                              //
  },                                                                                                             //
  cmTemplate: function() {                                                                                       // 70
    return Session.get('cmTemplate');                                                                            //
  },                                                                                                             //
  cmLabelClass: function() {                                                                                     // 70
    return Session.get('cmLabelClass');                                                                          //
  },                                                                                                             //
  cmInputColClass: function() {                                                                                  // 70
    return Session.get('cmInputColClass');                                                                       //
  },                                                                                                             //
  cmPlaceholder: function() {                                                                                    // 70
    return Session.get('cmPlaceholder');                                                                         //
  },                                                                                                             //
  cmFormId: function() {                                                                                         // 70
    return Session.get('cmFormId') || defaultFormId;                                                             //
  },                                                                                                             //
  cmAutoformType: function() {                                                                                   // 70
    if (Session.get('cmMeteorMethod')) {                                                                         // 105
      return 'method';                                                                                           //
    } else {                                                                                                     //
      return Session.get('cmOperation');                                                                         //
    }                                                                                                            //
  },                                                                                                             //
  cmModalDialogClass: function() {                                                                               // 70
    return Session.get('cmModalDialogClass');                                                                    //
  },                                                                                                             //
  cmMeteorMethod: function() {                                                                                   // 70
    return Session.get('cmMeteorMethod');                                                                        //
  },                                                                                                             //
  title: function() {                                                                                            // 70
    return StringTemplate.compile('{{{cmTitle}}}', helpers);                                                     //
  },                                                                                                             //
  prompt: function() {                                                                                           // 70
    return StringTemplate.compile('{{{cmPrompt}}}', helpers);                                                    //
  },                                                                                                             //
  buttonContent: function() {                                                                                    // 70
    return StringTemplate.compile('{{{cmButtonContent}}}', helpers);                                             //
  },                                                                                                             //
  closeButtonContent: function() {                                                                               // 70
    return StringTemplate.compile('{{{cmCloseButtonContent}}}', helpers);                                        //
  }                                                                                                              //
};                                                                                                               //
                                                                                                                 //
Template.autoformModals.helpers(helpers);                                                                        // 1
                                                                                                                 //
Template.afModal.events({                                                                                        // 1
  'click *': function(e, t) {                                                                                    // 125
    var html;                                                                                                    // 126
    e.preventDefault();                                                                                          // 126
    html = t.$('*').html();                                                                                      // 126
    Session.set('cmCollection', t.data.collection);                                                              // 126
    Session.set('cmOperation', t.data.operation);                                                                // 126
    Session.set('cmFields', t.data.fields);                                                                      // 126
    Session.set('cmOmitFields', t.data.omitFields);                                                              // 126
    Session.set('cmButtonHtml', html);                                                                           // 126
    Session.set('cmTitle', t.data.title || html);                                                                // 126
    Session.set('cmTemplate', t.data.template);                                                                  // 126
    Session.set('cmLabelClass', t.data.labelClass || t.data['label-class']);                                     // 126
    Session.set('cmInputColClass', t.data.inputColClass || t.data['input-col-class']);                           // 126
    Session.set('cmPlaceholder', t.data.placeholder === true ? 'schemaLabel' : '');                              // 126
    Session.set('cmFormId', t.data.formId);                                                                      // 126
    Session.set('cmMeteorMethod', t.data.meteormethod);                                                          // 126
    Session.set('cmModalDialogClass', t.data.dialogClass);                                                       // 126
    cmOnSuccessCallback = t.data.onSuccess;                                                                      // 126
    if (!_.contains(registeredAutoFormHooks, t.data.formId)) {                                                   // 146
      AutoForm.addHooks(t.data.formId, {                                                                         // 147
        onSuccess: function() {                                                                                  // 148
          return $('#afModal').modal('hide');                                                                    //
        }                                                                                                        //
      });                                                                                                        //
      registeredAutoFormHooks.push(t.data.formId);                                                               // 147
    }                                                                                                            //
    if (t.data.doc) {                                                                                            // 152
      Session.set('cmDoc', collectionObj(t.data.collection).findOne({                                            // 153
        _id: t.data.doc                                                                                          // 153
      }));                                                                                                       //
    }                                                                                                            //
    if (t.data.buttonContent) {                                                                                  // 155
      Session.set('cmButtonContent', t.data.buttonContent);                                                      // 156
    } else if (t.data.operation === 'insert') {                                                                  //
      Session.set('cmButtonContent', 'Create');                                                                  // 158
    } else if (t.data.operation === 'update') {                                                                  //
      Session.set('cmButtonContent', 'Update');                                                                  // 160
    } else if (t.data.operation === 'remove') {                                                                  //
      Session.set('cmButtonContent', 'Delete');                                                                  // 162
    }                                                                                                            //
    if (t.data.buttonClasses) {                                                                                  // 164
      Session.set('cmButtonClasses', t.data.buttonClasses);                                                      // 165
    } else if (t.data.operation === 'remove') {                                                                  //
      Session.set('cmButtonClasses', 'btn btn-danger');                                                          // 167
    } else {                                                                                                     //
      Session.set('cmButtonClasses', 'btn btn-primary');                                                         // 169
    }                                                                                                            //
    Session.set('cmCloseButtonContent', t.data.closeButtonContent || '');                                        // 126
    Session.set('cmCloseButtonClasses', t.data.closeButtonClasses || 'btn btn-default');                         // 126
    if (t.data.prompt) {                                                                                         // 174
      Session.set('cmPrompt', t.data.prompt);                                                                    // 175
    } else if (t.data.operation === 'remove') {                                                                  //
      Session.set('cmPrompt', 'Are you sure?');                                                                  // 177
    } else {                                                                                                     //
      Session.set('cmPrompt', '');                                                                               // 179
    }                                                                                                            //
    $('#afModal').data('bs.modal').options.backdrop = t.data.backdrop || true;                                   // 126
    return $('#afModal').modal('show');                                                                          //
  }                                                                                                              //
});                                                                                                              //
                                                                                                                 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['yogiben:autoform-modals'] = {};

})();
