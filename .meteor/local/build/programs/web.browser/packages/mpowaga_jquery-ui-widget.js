(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;

(function(){

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                           //
// packages/mpowaga_jquery-ui-widget/packages/mpowaga_jquery-ui-widget.js                                    //
//                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                             //
(function () {                                                                                               // 1
                                                                                                             // 2
////////////////////////////////////////////////////////////////////////////////////////////////////////     // 3
//                                                                                                    //     // 4
// packages/mpowaga:jquery-ui-widget/jquery.ui.widget.js                                              //     // 5
//                                                                                                    //     // 6
////////////////////////////////////////////////////////////////////////////////////////////////////////     // 7
                                                                                                      //     // 8
/*! jQuery UI - v1.11.4+CommonJS - 2015-08-28                                                         // 1   // 9
* http://jqueryui.com                                                                                 // 2   // 10
* Includes: widget.js                                                                                 // 3   // 11
* Copyright 2015 jQuery Foundation and other contributors; Licensed MIT */                            // 4   // 12
                                                                                                      // 5   // 13
(function( factory ) {                                                                                // 6   // 14
	if ( typeof define === "function" && define.amd ) {                                                  // 7   // 15
                                                                                                      // 8   // 16
		// AMD. Register as an anonymous module.                                                            // 9   // 17
		define([ "jquery" ], factory );                                                                     // 10  // 18
                                                                                                      // 11  // 19
	} else if ( typeof exports === "object" ) {                                                          // 12  // 20
                                                                                                      // 13  // 21
		// Node/CommonJS                                                                                    // 14  // 22
		factory( require( "jquery" ) );                                                                     // 15  // 23
                                                                                                      // 16  // 24
	} else {                                                                                             // 17  // 25
                                                                                                      // 18  // 26
		// Browser globals                                                                                  // 19  // 27
		factory( jQuery );                                                                                  // 20  // 28
	}                                                                                                    // 21  // 29
}(function( $ ) {                                                                                     // 22  // 30
/*!                                                                                                   // 23  // 31
 * jQuery UI Widget 1.11.4                                                                            // 24  // 32
 * http://jqueryui.com                                                                                // 25  // 33
 *                                                                                                    // 26  // 34
 * Copyright jQuery Foundation and other contributors                                                 // 27  // 35
 * Released under the MIT license.                                                                    // 28  // 36
 * http://jquery.org/license                                                                          // 29  // 37
 *                                                                                                    // 30  // 38
 * http://api.jqueryui.com/jQuery.widget/                                                             // 31  // 39
 */                                                                                                   // 32  // 40
                                                                                                      // 33  // 41
                                                                                                      // 34  // 42
var widget_uuid = 0,                                                                                  // 35  // 43
	widget_slice = Array.prototype.slice;                                                                // 36  // 44
                                                                                                      // 37  // 45
$.cleanData = (function( orig ) {                                                                     // 38  // 46
	return function( elems ) {                                                                           // 39  // 47
		var events, elem, i;                                                                                // 40  // 48
		for ( i = 0; (elem = elems[i]) != null; i++ ) {                                                     // 41  // 49
			try {                                                                                              // 42  // 50
                                                                                                      // 43  // 51
				// Only trigger remove when necessary to save time                                                // 44  // 52
				events = $._data( elem, "events" );                                                               // 45  // 53
				if ( events && events.remove ) {                                                                  // 46  // 54
					$( elem ).triggerHandler( "remove" );                                                            // 47  // 55
				}                                                                                                 // 48  // 56
                                                                                                      // 49  // 57
			// http://bugs.jquery.com/ticket/8235                                                              // 50  // 58
			} catch ( e ) {}                                                                                   // 51  // 59
		}                                                                                                   // 52  // 60
		orig( elems );                                                                                      // 53  // 61
	};                                                                                                   // 54  // 62
})( $.cleanData );                                                                                    // 55  // 63
                                                                                                      // 56  // 64
$.widget = function( name, base, prototype ) {                                                        // 57  // 65
	var fullName, existingConstructor, constructor, basePrototype,                                       // 58  // 66
		// proxiedPrototype allows the provided prototype to remain unmodified                              // 59  // 67
		// so that it can be used as a mixin for multiple widgets (#8876)                                   // 60  // 68
		proxiedPrototype = {},                                                                              // 61  // 69
		namespace = name.split( "." )[ 0 ];                                                                 // 62  // 70
                                                                                                      // 63  // 71
	name = name.split( "." )[ 1 ];                                                                       // 64  // 72
	fullName = namespace + "-" + name;                                                                   // 65  // 73
                                                                                                      // 66  // 74
	if ( !prototype ) {                                                                                  // 67  // 75
		prototype = base;                                                                                   // 68  // 76
		base = $.Widget;                                                                                    // 69  // 77
	}                                                                                                    // 70  // 78
                                                                                                      // 71  // 79
	// create selector for plugin                                                                        // 72  // 80
	$.expr[ ":" ][ fullName.toLowerCase() ] = function( elem ) {                                         // 73  // 81
		return !!$.data( elem, fullName );                                                                  // 74  // 82
	};                                                                                                   // 75  // 83
                                                                                                      // 76  // 84
	$[ namespace ] = $[ namespace ] || {};                                                               // 77  // 85
	existingConstructor = $[ namespace ][ name ];                                                        // 78  // 86
	constructor = $[ namespace ][ name ] = function( options, element ) {                                // 79  // 87
		// allow instantiation without "new" keyword                                                        // 80  // 88
		if ( !this._createWidget ) {                                                                        // 81  // 89
			return new constructor( options, element );                                                        // 82  // 90
		}                                                                                                   // 83  // 91
                                                                                                      // 84  // 92
		// allow instantiation without initializing for simple inheritance                                  // 85  // 93
		// must use "new" keyword (the code above always passes args)                                       // 86  // 94
		if ( arguments.length ) {                                                                           // 87  // 95
			this._createWidget( options, element );                                                            // 88  // 96
		}                                                                                                   // 89  // 97
	};                                                                                                   // 90  // 98
	// extend with the existing constructor to carry over any static properties                          // 91  // 99
	$.extend( constructor, existingConstructor, {                                                        // 92  // 100
		version: prototype.version,                                                                         // 93  // 101
		// copy the object used to create the prototype in case we need to                                  // 94  // 102
		// redefine the widget later                                                                        // 95  // 103
		_proto: $.extend( {}, prototype ),                                                                  // 96  // 104
		// track widgets that inherit from this widget in case this widget is                               // 97  // 105
		// redefined after a widget inherits from it                                                        // 98  // 106
		_childConstructors: []                                                                              // 99  // 107
	});                                                                                                  // 100
                                                                                                      // 101
	basePrototype = new base();                                                                          // 102
	// we need to make the options hash a property directly on the new instance                          // 103
	// otherwise we'll modify the options hash on the prototype that we're                               // 104
	// inheriting from                                                                                   // 105
	basePrototype.options = $.widget.extend( {}, basePrototype.options );                                // 106
	$.each( prototype, function( prop, value ) {                                                         // 107
		if ( !$.isFunction( value ) ) {                                                                     // 108
			proxiedPrototype[ prop ] = value;                                                                  // 109
			return;                                                                                            // 110
		}                                                                                                   // 111
		proxiedPrototype[ prop ] = (function() {                                                            // 112
			var _super = function() {                                                                          // 113
					return base.prototype[ prop ].apply( this, arguments );                                          // 114
				},                                                                                                // 115
				_superApply = function( args ) {                                                                  // 116
					return base.prototype[ prop ].apply( this, args );                                               // 117
				};                                                                                                // 118
			return function() {                                                                                // 119
				var __super = this._super,                                                                        // 120
					__superApply = this._superApply,                                                                 // 121
					returnValue;                                                                                     // 122
                                                                                                      // 123
				this._super = _super;                                                                             // 124
				this._superApply = _superApply;                                                                   // 125
                                                                                                      // 126
				returnValue = value.apply( this, arguments );                                                     // 127
                                                                                                      // 128
				this._super = __super;                                                                            // 129
				this._superApply = __superApply;                                                                  // 130
                                                                                                      // 131
				return returnValue;                                                                               // 132
			};                                                                                                 // 133
		})();                                                                                               // 134
	});                                                                                                  // 135
	constructor.prototype = $.widget.extend( basePrototype, {                                            // 136
		// TODO: remove support for widgetEventPrefix                                                       // 137
		// always use the name + a colon as the prefix, e.g., draggable:start                               // 138
		// don't prefix for widgets that aren't DOM-based                                                   // 139
		widgetEventPrefix: existingConstructor ? (basePrototype.widgetEventPrefix || name) : name           // 140
	}, proxiedPrototype, {                                                                               // 141
		constructor: constructor,                                                                           // 142
		namespace: namespace,                                                                               // 143
		widgetName: name,                                                                                   // 144
		widgetFullName: fullName                                                                            // 145
	});                                                                                                  // 146
                                                                                                      // 147
	// If this widget is being redefined then we need to find all widgets that                           // 148
	// are inheriting from it and redefine all of them so that they inherit from                         // 149
	// the new version of this widget. We're essentially trying to replace one                           // 150
	// level in the prototype chain.                                                                     // 151
	if ( existingConstructor ) {                                                                         // 152
		$.each( existingConstructor._childConstructors, function( i, child ) {                              // 153
			var childPrototype = child.prototype;                                                              // 154
                                                                                                      // 155
			// redefine the child widget using the same prototype that was                                     // 156
			// originally used, but inherit from the new version of the base                                   // 157
			$.widget( childPrototype.namespace + "." + childPrototype.widgetName, constructor, child._proto ); // 158
		});                                                                                                 // 159
		// remove the list of existing child constructors from the old constructor                          // 160
		// so the old child constructors can be garbage collected                                           // 161
		delete existingConstructor._childConstructors;                                                      // 162
	} else {                                                                                             // 163
		base._childConstructors.push( constructor );                                                        // 164
	}                                                                                                    // 165
                                                                                                      // 166
	$.widget.bridge( name, constructor );                                                                // 167
                                                                                                      // 168
	return constructor;                                                                                  // 169
};                                                                                                    // 170
                                                                                                      // 171
$.widget.extend = function( target ) {                                                                // 172
	var input = widget_slice.call( arguments, 1 ),                                                       // 173
		inputIndex = 0,                                                                                     // 174
		inputLength = input.length,                                                                         // 175
		key,                                                                                                // 176
		value;                                                                                              // 177
	for ( ; inputIndex < inputLength; inputIndex++ ) {                                                   // 178
		for ( key in input[ inputIndex ] ) {                                                                // 179
			value = input[ inputIndex ][ key ];                                                                // 180
			if ( input[ inputIndex ].hasOwnProperty( key ) && value !== undefined ) {                          // 181
				// Clone objects                                                                                  // 182
				if ( $.isPlainObject( value ) ) {                                                                 // 183
					target[ key ] = $.isPlainObject( target[ key ] ) ?                                               // 184
						$.widget.extend( {}, target[ key ], value ) :                                                   // 185
						// Don't extend strings, arrays, etc. with objects                                              // 186
						$.widget.extend( {}, value );                                                                   // 187
				// Copy everything else by reference                                                              // 188
				} else {                                                                                          // 189
					target[ key ] = value;                                                                           // 190
				}                                                                                                 // 191
			}                                                                                                  // 192
		}                                                                                                   // 193
	}                                                                                                    // 194
	return target;                                                                                       // 195
};                                                                                                    // 196
                                                                                                      // 197
$.widget.bridge = function( name, object ) {                                                          // 198
	var fullName = object.prototype.widgetFullName || name;                                              // 199
	$.fn[ name ] = function( options ) {                                                                 // 200
		var isMethodCall = typeof options === "string",                                                     // 201
			args = widget_slice.call( arguments, 1 ),                                                          // 202
			returnValue = this;                                                                                // 203
                                                                                                      // 204
		if ( isMethodCall ) {                                                                               // 205
			this.each(function() {                                                                             // 206
				var methodValue,                                                                                  // 207
					instance = $.data( this, fullName );                                                             // 208
				if ( options === "instance" ) {                                                                   // 209
					returnValue = instance;                                                                          // 210
					return false;                                                                                    // 211
				}                                                                                                 // 212
				if ( !instance ) {                                                                                // 213
					return $.error( "cannot call methods on " + name + " prior to initialization; " +                // 214
						"attempted to call method '" + options + "'" );                                                 // 215
				}                                                                                                 // 216
				if ( !$.isFunction( instance[options] ) || options.charAt( 0 ) === "_" ) {                        // 217
					return $.error( "no such method '" + options + "' for " + name + " widget instance" );           // 218
				}                                                                                                 // 219
				methodValue = instance[ options ].apply( instance, args );                                        // 220
				if ( methodValue !== instance && methodValue !== undefined ) {                                    // 221
					returnValue = methodValue && methodValue.jquery ?                                                // 222
						returnValue.pushStack( methodValue.get() ) :                                                    // 223
						methodValue;                                                                                    // 224
					return false;                                                                                    // 225
				}                                                                                                 // 226
			});                                                                                                // 227
		} else {                                                                                            // 228
                                                                                                      // 229
			// Allow multiple hashes to be passed on init                                                      // 230
			if ( args.length ) {                                                                               // 231
				options = $.widget.extend.apply( null, [ options ].concat(args) );                                // 232
			}                                                                                                  // 233
                                                                                                      // 234
			this.each(function() {                                                                             // 235
				var instance = $.data( this, fullName );                                                          // 236
				if ( instance ) {                                                                                 // 237
					instance.option( options || {} );                                                                // 238
					if ( instance._init ) {                                                                          // 239
						instance._init();                                                                               // 240
					}                                                                                                // 241
				} else {                                                                                          // 242
					$.data( this, fullName, new object( options, this ) );                                           // 243
				}                                                                                                 // 244
			});                                                                                                // 245
		}                                                                                                   // 246
                                                                                                      // 247
		return returnValue;                                                                                 // 248
	};                                                                                                   // 249
};                                                                                                    // 250
                                                                                                      // 251
$.Widget = function( /* options, element */ ) {};                                                     // 252
$.Widget._childConstructors = [];                                                                     // 253
                                                                                                      // 254
$.Widget.prototype = {                                                                                // 255
	widgetName: "widget",                                                                                // 256
	widgetEventPrefix: "",                                                                               // 257
	defaultElement: "<div>",                                                                             // 258
	options: {                                                                                           // 259
		disabled: false,                                                                                    // 260
                                                                                                      // 261
		// callbacks                                                                                        // 262
		create: null                                                                                        // 263
	},                                                                                                   // 264
	_createWidget: function( options, element ) {                                                        // 265
		element = $( element || this.defaultElement || this )[ 0 ];                                         // 266
		this.element = $( element );                                                                        // 267
		this.uuid = widget_uuid++;                                                                          // 268
		this.eventNamespace = "." + this.widgetName + this.uuid;                                            // 269
                                                                                                      // 270
		this.bindings = $();                                                                                // 271
		this.hoverable = $();                                                                               // 272
		this.focusable = $();                                                                               // 273
                                                                                                      // 274
		if ( element !== this ) {                                                                           // 275
			$.data( element, this.widgetFullName, this );                                                      // 276
			this._on( true, this.element, {                                                                    // 277
				remove: function( event ) {                                                                       // 278
					if ( event.target === element ) {                                                                // 279
						this.destroy();                                                                                 // 280
					}                                                                                                // 281
				}                                                                                                 // 282
			});                                                                                                // 283
			this.document = $( element.style ?                                                                 // 284
				// element within the document                                                                    // 285
				element.ownerDocument :                                                                           // 286
				// element is window or document                                                                  // 287
				element.document || element );                                                                    // 288
			this.window = $( this.document[0].defaultView || this.document[0].parentWindow );                  // 289
		}                                                                                                   // 290
                                                                                                      // 291
		this.options = $.widget.extend( {},                                                                 // 292
			this.options,                                                                                      // 293
			this._getCreateOptions(),                                                                          // 294
			options );                                                                                         // 295
                                                                                                      // 296
		this._create();                                                                                     // 297
		this._trigger( "create", null, this._getCreateEventData() );                                        // 298
		this._init();                                                                                       // 299
	},                                                                                                   // 300
	_getCreateOptions: $.noop,                                                                           // 301
	_getCreateEventData: $.noop,                                                                         // 302
	_create: $.noop,                                                                                     // 303
	_init: $.noop,                                                                                       // 304
                                                                                                      // 305
	destroy: function() {                                                                                // 306
		this._destroy();                                                                                    // 307
		// we can probably remove the unbind calls in 2.0                                                   // 308
		// all event bindings should go through this._on()                                                  // 309
		this.element                                                                                        // 310
			.unbind( this.eventNamespace )                                                                     // 311
			.removeData( this.widgetFullName )                                                                 // 312
			// support: jquery <1.6.3                                                                          // 313
			// http://bugs.jquery.com/ticket/9413                                                              // 314
			.removeData( $.camelCase( this.widgetFullName ) );                                                 // 315
		this.widget()                                                                                       // 316
			.unbind( this.eventNamespace )                                                                     // 317
			.removeAttr( "aria-disabled" )                                                                     // 318
			.removeClass(                                                                                      // 319
				this.widgetFullName + "-disabled " +                                                              // 320
				"ui-state-disabled" );                                                                            // 321
                                                                                                      // 322
		// clean up events and states                                                                       // 323
		this.bindings.unbind( this.eventNamespace );                                                        // 324
		this.hoverable.removeClass( "ui-state-hover" );                                                     // 325
		this.focusable.removeClass( "ui-state-focus" );                                                     // 326
	},                                                                                                   // 327
	_destroy: $.noop,                                                                                    // 328
                                                                                                      // 329
	widget: function() {                                                                                 // 330
		return this.element;                                                                                // 331
	},                                                                                                   // 332
                                                                                                      // 333
	option: function( key, value ) {                                                                     // 334
		var options = key,                                                                                  // 335
			parts,                                                                                             // 336
			curOption,                                                                                         // 337
			i;                                                                                                 // 338
                                                                                                      // 339
		if ( arguments.length === 0 ) {                                                                     // 340
			// don't return a reference to the internal hash                                                   // 341
			return $.widget.extend( {}, this.options );                                                        // 342
		}                                                                                                   // 343
                                                                                                      // 344
		if ( typeof key === "string" ) {                                                                    // 345
			// handle nested keys, e.g., "foo.bar" => { foo: { bar: ___ } }                                    // 346
			options = {};                                                                                      // 347
			parts = key.split( "." );                                                                          // 348
			key = parts.shift();                                                                               // 349
			if ( parts.length ) {                                                                              // 350
				curOption = options[ key ] = $.widget.extend( {}, this.options[ key ] );                          // 351
				for ( i = 0; i < parts.length - 1; i++ ) {                                                        // 352
					curOption[ parts[ i ] ] = curOption[ parts[ i ] ] || {};                                         // 353
					curOption = curOption[ parts[ i ] ];                                                             // 354
				}                                                                                                 // 355
				key = parts.pop();                                                                                // 356
				if ( arguments.length === 1 ) {                                                                   // 357
					return curOption[ key ] === undefined ? null : curOption[ key ];                                 // 358
				}                                                                                                 // 359
				curOption[ key ] = value;                                                                         // 360
			} else {                                                                                           // 361
				if ( arguments.length === 1 ) {                                                                   // 362
					return this.options[ key ] === undefined ? null : this.options[ key ];                           // 363
				}                                                                                                 // 364
				options[ key ] = value;                                                                           // 365
			}                                                                                                  // 366
		}                                                                                                   // 367
                                                                                                      // 368
		this._setOptions( options );                                                                        // 369
                                                                                                      // 370
		return this;                                                                                        // 371
	},                                                                                                   // 372
	_setOptions: function( options ) {                                                                   // 373
		var key;                                                                                            // 374
                                                                                                      // 375
		for ( key in options ) {                                                                            // 376
			this._setOption( key, options[ key ] );                                                            // 377
		}                                                                                                   // 378
                                                                                                      // 379
		return this;                                                                                        // 380
	},                                                                                                   // 381
	_setOption: function( key, value ) {                                                                 // 382
		this.options[ key ] = value;                                                                        // 383
                                                                                                      // 384
		if ( key === "disabled" ) {                                                                         // 385
			this.widget()                                                                                      // 386
				.toggleClass( this.widgetFullName + "-disabled", !!value );                                       // 387
                                                                                                      // 388
			// If the widget is becoming disabled, then nothing is interactive                                 // 389
			if ( value ) {                                                                                     // 390
				this.hoverable.removeClass( "ui-state-hover" );                                                   // 391
				this.focusable.removeClass( "ui-state-focus" );                                                   // 392
			}                                                                                                  // 393
		}                                                                                                   // 394
                                                                                                      // 395
		return this;                                                                                        // 396
	},                                                                                                   // 397
                                                                                                      // 398
	enable: function() {                                                                                 // 399
		return this._setOptions({ disabled: false });                                                       // 400
	},                                                                                                   // 401
	disable: function() {                                                                                // 402
		return this._setOptions({ disabled: true });                                                        // 403
	},                                                                                                   // 404
                                                                                                      // 405
	_on: function( suppressDisabledCheck, element, handlers ) {                                          // 406
		var delegateElement,                                                                                // 407
			instance = this;                                                                                   // 408
                                                                                                      // 409
		// no suppressDisabledCheck flag, shuffle arguments                                                 // 410
		if ( typeof suppressDisabledCheck !== "boolean" ) {                                                 // 411
			handlers = element;                                                                                // 412
			element = suppressDisabledCheck;                                                                   // 413
			suppressDisabledCheck = false;                                                                     // 414
		}                                                                                                   // 415
                                                                                                      // 416
		// no element argument, shuffle and use this.element                                                // 417
		if ( !handlers ) {                                                                                  // 418
			handlers = element;                                                                                // 419
			element = this.element;                                                                            // 420
			delegateElement = this.widget();                                                                   // 421
		} else {                                                                                            // 422
			element = delegateElement = $( element );                                                          // 423
			this.bindings = this.bindings.add( element );                                                      // 424
		}                                                                                                   // 425
                                                                                                      // 426
		$.each( handlers, function( event, handler ) {                                                      // 427
			function handlerProxy() {                                                                          // 428
				// allow widgets to customize the disabled handling                                               // 429
				// - disabled as an array instead of boolean                                                      // 430
				// - disabled class as method for disabling individual parts                                      // 431
				if ( !suppressDisabledCheck &&                                                                    // 432
						( instance.options.disabled === true ||                                                         // 433
							$( this ).hasClass( "ui-state-disabled" ) ) ) {                                                // 434
					return;                                                                                          // 435
				}                                                                                                 // 436
				return ( typeof handler === "string" ? instance[ handler ] : handler )                            // 437
					.apply( instance, arguments );                                                                   // 438
			}                                                                                                  // 439
                                                                                                      // 440
			// copy the guid so direct unbinding works                                                         // 441
			if ( typeof handler !== "string" ) {                                                               // 442
				handlerProxy.guid = handler.guid =                                                                // 443
					handler.guid || handlerProxy.guid || $.guid++;                                                   // 444
			}                                                                                                  // 445
                                                                                                      // 446
			var match = event.match( /^([\w:-]*)\s*(.*)$/ ),                                                   // 447
				eventName = match[1] + instance.eventNamespace,                                                   // 448
				selector = match[2];                                                                              // 449
			if ( selector ) {                                                                                  // 450
				delegateElement.delegate( selector, eventName, handlerProxy );                                    // 451
			} else {                                                                                           // 452
				element.bind( eventName, handlerProxy );                                                          // 453
			}                                                                                                  // 454
		});                                                                                                 // 455
	},                                                                                                   // 456
                                                                                                      // 457
	_off: function( element, eventName ) {                                                               // 458
		eventName = (eventName || "").split( " " ).join( this.eventNamespace + " " ) +                      // 459
			this.eventNamespace;                                                                               // 460
		element.unbind( eventName ).undelegate( eventName );                                                // 461
                                                                                                      // 462
		// Clear the stack to avoid memory leaks (#10056)                                                   // 463
		this.bindings = $( this.bindings.not( element ).get() );                                            // 464
		this.focusable = $( this.focusable.not( element ).get() );                                          // 465
		this.hoverable = $( this.hoverable.not( element ).get() );                                          // 466
	},                                                                                                   // 467
                                                                                                      // 468
	_delay: function( handler, delay ) {                                                                 // 469
		function handlerProxy() {                                                                           // 470
			return ( typeof handler === "string" ? instance[ handler ] : handler )                             // 471
				.apply( instance, arguments );                                                                    // 472
		}                                                                                                   // 473
		var instance = this;                                                                                // 474
		return setTimeout( handlerProxy, delay || 0 );                                                      // 475
	},                                                                                                   // 476
                                                                                                      // 477
	_hoverable: function( element ) {                                                                    // 478
		this.hoverable = this.hoverable.add( element );                                                     // 479
		this._on( element, {                                                                                // 480
			mouseenter: function( event ) {                                                                    // 481
				$( event.currentTarget ).addClass( "ui-state-hover" );                                            // 482
			},                                                                                                 // 483
			mouseleave: function( event ) {                                                                    // 484
				$( event.currentTarget ).removeClass( "ui-state-hover" );                                         // 485
			}                                                                                                  // 486
		});                                                                                                 // 487
	},                                                                                                   // 488
                                                                                                      // 489
	_focusable: function( element ) {                                                                    // 490
		this.focusable = this.focusable.add( element );                                                     // 491
		this._on( element, {                                                                                // 492
			focusin: function( event ) {                                                                       // 493
				$( event.currentTarget ).addClass( "ui-state-focus" );                                            // 494
			},                                                                                                 // 495
			focusout: function( event ) {                                                                      // 496
				$( event.currentTarget ).removeClass( "ui-state-focus" );                                         // 497
			}                                                                                                  // 498
		});                                                                                                 // 499
	},                                                                                                   // 500
                                                                                                      // 501
	_trigger: function( type, event, data ) {                                                            // 502
		var prop, orig,                                                                                     // 503
			callback = this.options[ type ];                                                                   // 504
                                                                                                      // 505
		data = data || {};                                                                                  // 506
		event = $.Event( event );                                                                           // 507
		event.type = ( type === this.widgetEventPrefix ?                                                    // 508
			type :                                                                                             // 509
			this.widgetEventPrefix + type ).toLowerCase();                                                     // 510
		// the original event may come from any element                                                     // 511
		// so we need to reset the target on the new event                                                  // 512
		event.target = this.element[ 0 ];                                                                   // 513
                                                                                                      // 514
		// copy original event properties over to the new event                                             // 515
		orig = event.originalEvent;                                                                         // 516
		if ( orig ) {                                                                                       // 517
			for ( prop in orig ) {                                                                             // 518
				if ( !( prop in event ) ) {                                                                       // 519
					event[ prop ] = orig[ prop ];                                                                    // 520
				}                                                                                                 // 521
			}                                                                                                  // 522
		}                                                                                                   // 523
                                                                                                      // 524
		this.element.trigger( event, data );                                                                // 525
		return !( $.isFunction( callback ) &&                                                               // 526
			callback.apply( this.element[0], [ event ].concat( data ) ) === false ||                           // 527
			event.isDefaultPrevented() );                                                                      // 528
	}                                                                                                    // 529
};                                                                                                    // 530
                                                                                                      // 531
$.each( { show: "fadeIn", hide: "fadeOut" }, function( method, defaultEffect ) {                      // 532
	$.Widget.prototype[ "_" + method ] = function( element, options, callback ) {                        // 533
		if ( typeof options === "string" ) {                                                                // 534
			options = { effect: options };                                                                     // 535
		}                                                                                                   // 536
		var hasOptions,                                                                                     // 537
			effectName = !options ?                                                                            // 538
				method :                                                                                          // 539
				options === true || typeof options === "number" ?                                                 // 540
					defaultEffect :                                                                                  // 541
					options.effect || defaultEffect;                                                                 // 542
		options = options || {};                                                                            // 543
		if ( typeof options === "number" ) {                                                                // 544
			options = { duration: options };                                                                   // 545
		}                                                                                                   // 546
		hasOptions = !$.isEmptyObject( options );                                                           // 547
		options.complete = callback;                                                                        // 548
		if ( options.delay ) {                                                                              // 549
			element.delay( options.delay );                                                                    // 550
		}                                                                                                   // 551
		if ( hasOptions && $.effects && $.effects.effect[ effectName ] ) {                                  // 552
			element[ method ]( options );                                                                      // 553
		} else if ( effectName !== method && element[ effectName ] ) {                                      // 554
			element[ effectName ]( options.duration, options.easing, callback );                               // 555
		} else {                                                                                            // 556
			element.queue(function( next ) {                                                                   // 557
				$( this )[ method ]();                                                                            // 558
				if ( callback ) {                                                                                 // 559
					callback.call( element[ 0 ] );                                                                   // 560
				}                                                                                                 // 561
				next();                                                                                           // 562
			});                                                                                                // 563
		}                                                                                                   // 564
	};                                                                                                   // 565
});                                                                                                   // 566
                                                                                                      // 567
var widget = $.widget;                                                                                // 568
                                                                                                      // 569
                                                                                                      // 570
                                                                                                      // 571
}));                                                                                                  // 572
                                                                                                      // 573
////////////////////////////////////////////////////////////////////////////////////////////////////////     // 582
                                                                                                             // 583
}).call(this);                                                                                               // 584
                                                                                                             // 585
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['mpowaga:jquery-ui-widget'] = {};

})();
