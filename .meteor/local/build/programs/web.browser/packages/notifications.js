//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Template = Package.templating.Template;
var Tracker = Package.tracker.Tracker;
var Deps = Package.tracker.Deps;
var Router = Package['iron:router'].Router;
var RouteController = Package['iron:router'].RouteController;
var moment = Package['mrt:moment'].moment;
var AutoForm = Package['aldeed:autoform'].AutoForm;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var Spacebars = Package.spacebars.Spacebars;
var Iron = Package['iron:core'].Iron;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                        //
// packages/notifications/lib/both/collections.coffee.js                                                  //
//                                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                          //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var NotificationsSchema;                                                                                  // 1
                                                                                                          //
this.Notifications = new Meteor.Collection('notifications');                                              // 1
                                                                                                          //
Notifications["new"] = function(doc) {                                                                    // 1
  if (typeof doc.owner === 'undefined') {                                                                 // 4
    doc.owner = Meteor.userId();                                                                          // 5
  }                                                                                                       //
  return Notifications.insert(doc);                                                                       //
};                                                                                                        // 3
                                                                                                          //
Notifications.readAll = function() {                                                                      // 1
  return Meteor.call('readAllNotifications');                                                             //
};                                                                                                        // 9
                                                                                                          //
Notifications.read = function(_id) {                                                                      // 1
  return Notifications.update(_id, {                                                                      //
    $set: {                                                                                               // 13
      read: true                                                                                          // 13
    }                                                                                                     //
  });                                                                                                     //
};                                                                                                        // 12
                                                                                                          //
NotificationsSchema = new SimpleSchema({                                                                  // 1
  owner: {                                                                                                // 16
    type: String,                                                                                         // 17
    regEx: SimpleSchema.RegEx.Id                                                                          // 17
  },                                                                                                      //
  link: {                                                                                                 // 16
    type: String,                                                                                         // 21
    optional: true                                                                                        // 21
  },                                                                                                      //
  title: {                                                                                                // 16
    type: String                                                                                          // 25
  },                                                                                                      //
  read: {                                                                                                 // 16
    type: Boolean,                                                                                        // 28
    defaultValue: false                                                                                   // 28
  },                                                                                                      //
  date: {                                                                                                 // 16
    type: Date,                                                                                           // 32
    autoValue: function() {                                                                               // 32
      if (this.isInsert) {                                                                                // 34
        return new Date();                                                                                //
      }                                                                                                   //
    }                                                                                                     //
  },                                                                                                      //
  icon: {                                                                                                 // 16
    type: String,                                                                                         // 38
    defaultValue: 'circle-o'                                                                              // 38
  },                                                                                                      //
  "class": {                                                                                              // 16
    type: String,                                                                                         // 42
    defaultValue: 'default'                                                                               // 42
  }                                                                                                       //
});                                                                                                       //
                                                                                                          //
Notifications.attachSchema(NotificationsSchema);                                                          // 1
                                                                                                          //
////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                        //
// packages/notifications/lib/both/router.coffee.js                                                       //
//                                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                          //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Router.map(function() {                                                                                   // 1
  this.route('notifications', {                                                                           // 2
    path: '/notifications',                                                                               // 3
    waitOn: function() {                                                                                  // 3
      return [Meteor.subscribe('notifications')];                                                         //
    }                                                                                                     //
  });                                                                                                     //
  return this.route('messages', {                                                                         //
    path: '/messages/:_id',                                                                               // 9
    layout: 'notifications'                                                                               // 9
  });                                                                                                     //
});                                                                                                       // 1
                                                                                                          //
////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                        //
// packages/notifications/lib/client/template.templates.js                                                //
//                                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                          //
                                                                                                          // 1
Template.__checkName("notificationsDropdown");                                                            // 2
Template["notificationsDropdown"] = new Template("Template.notificationsDropdown", (function() {          // 3
  var view = this;                                                                                        // 4
  return HTML.LI({                                                                                        // 5
    "class": "dropdown notificationsDropdown"                                                             // 6
  }, "\n    ", HTML.A({                                                                                   // 7
    href: "#",                                                                                            // 8
    "class": "notification"                                                                               // 9
  }, "\n      ", Blaze.If(function() {                                                                    // 10
    return Spacebars.call(view.lookup("notificationCount"));                                              // 11
  }, function() {                                                                                         // 12
    return [ "\n        ", HTML.I({                                                                       // 13
      "class": function() {                                                                               // 14
        return [ "fa fa-", Spacebars.mustache(view.lookup("dropdownIcon")) ];                             // 15
      }                                                                                                   // 16
    }), "\n        ", HTML.SPAN({                                                                         // 17
      "class": "badge bg-success"                                                                         // 18
    }, Blaze.View("lookup:notificationCount", function() {                                                // 19
      return Spacebars.mustache(view.lookup("notificationCount"));                                        // 20
    })), "\n      " ];                                                                                    // 21
  }, function() {                                                                                         // 22
    return [ "\n        ", HTML.I({                                                                       // 23
      "class": function() {                                                                               // 24
        return [ "fa fa-", Spacebars.mustache(view.lookup("dropdownIconEmpty")) ];                        // 25
      }                                                                                                   // 26
    }), "\n      " ];                                                                                     // 27
  }), "\n    "), "\n\n    ", HTML.UL({                                                                    // 28
    "class": "dropdown-menu",                                                                             // 29
    role: "menu"                                                                                          // 30
  }, "\n      ", Blaze.If(function() {                                                                    // 31
    return Spacebars.call(view.lookup("hasNotifications"));                                               // 32
  }, function() {                                                                                         // 33
    return [ "\n        ", Blaze.Each(function() {                                                        // 34
      return Spacebars.dataMustache(view.lookup("Notifications"), Spacebars.kw({                          // 35
        limit: 5,                                                                                         // 36
        unreadFirst: true                                                                                 // 37
      }));                                                                                                // 38
    }, function() {                                                                                       // 39
      return [ "\n          ", HTML.LI({                                                                  // 40
        "class": function() {                                                                             // 41
          return [ "notification ", Spacebars.mustache(view.lookup("notificationClass")) ];               // 42
        }                                                                                                 // 43
      }, "\n            ", HTML.A({                                                                       // 44
        href: function() {                                                                                // 45
          return Spacebars.mustache(view.lookup("link"));                                                 // 46
        }                                                                                                 // 47
      }, HTML.I({                                                                                         // 48
        "class": function() {                                                                             // 49
          return [ "fa fa-", Spacebars.mustache(view.lookup("icon")), " fa-fw text-", Spacebars.mustache(view.lookup("class")) ];
        }                                                                                                 // 51
      }), " ", Blaze.View("lookup:title", function() {                                                    // 52
        return Spacebars.mustache(view.lookup("title"));                                                  // 53
      })), "\n          "), "\n        " ];                                                               // 54
    }), "\n        ", HTML.LI({                                                                           // 55
      "class": "text-center"                                                                              // 56
    }, HTML.A({                                                                                           // 57
      href: "/notifications"                                                                              // 58
    }, "See all")), "\n      " ];                                                                         // 59
  }, function() {                                                                                         // 60
    return [ "\n        ", HTML.LI(HTML.DIV({                                                             // 61
      "class": "well-sm"                                                                                  // 62
    }, "0 Notifications")), "\n      " ];                                                                 // 63
  }), "\n    "), "\n  ");                                                                                 // 64
}));                                                                                                      // 65
                                                                                                          // 66
Template.__checkName("notifications");                                                                    // 67
Template["notifications"] = new Template("Template.notifications", (function() {                          // 68
  var view = this;                                                                                        // 69
  return HTML.DIV({                                                                                       // 70
    "class": "panel panel-default"                                                                        // 71
  }, HTML.Raw('\n    <div class="panel-heading">\n      <h3 class="panel-title">Notifications</h3>\n    </div>\n    '), HTML.DIV({
    "class": "panel-body"                                                                                 // 73
  }, "\n      ", HTML.UL({                                                                                // 74
    "class": "list-group"                                                                                 // 75
  }, "\n        ", Blaze.Each(function() {                                                                // 76
    return Spacebars.call(view.lookup("Notifications"));                                                  // 77
  }, function() {                                                                                         // 78
    return [ "\n          ", HTML.LI({                                                                    // 79
      "class": function() {                                                                               // 80
        return [ "list-group-item notification ", Spacebars.mustache(view.lookup("notificationClass")) ];
      }                                                                                                   // 82
    }, "\n            ", HTML.I({                                                                         // 83
      "class": function() {                                                                               // 84
        return [ "fa fa-", Spacebars.mustache(view.lookup("icon")), " text-", Spacebars.mustache(view.lookup("class")) ];
      }                                                                                                   // 86
    }), " \n            ", HTML.A({                                                                       // 87
      href: function() {                                                                                  // 88
        return Spacebars.mustache(view.lookup("link"));                                                   // 89
      }                                                                                                   // 90
    }, Blaze.View("lookup:title", function() {                                                            // 91
      return Spacebars.mustache(view.lookup("title"));                                                    // 92
    })), " \n            ", HTML.SPAN({                                                                   // 93
      "class": "pull-right"                                                                               // 94
    }, Blaze.View("lookup:ago", function() {                                                              // 95
      return Spacebars.mustache(view.lookup("ago"));                                                      // 96
    })), "\n          "), "\n        " ];                                                                 // 97
  }), "\n      "), "\n    "), "\n\n  ");                                                                  // 98
}));                                                                                                      // 99
                                                                                                          // 100
////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                        //
// packages/notifications/lib/client/templates.coffee.js                                                  //
//                                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                          //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var notificationClass, readNotification;                                                                  // 1
                                                                                                          //
notificationClass = function() {                                                                          // 1
  if (!this.read) {                                                                                       // 2
    return 'unread-notification';                                                                         //
  } else {                                                                                                //
    return '';                                                                                            //
  }                                                                                                       //
};                                                                                                        // 1
                                                                                                          //
readNotification = function() {                                                                           // 1
  return Notifications.read(this._id);                                                                    //
};                                                                                                        // 4
                                                                                                          //
Template.notificationsDropdown.helpers({                                                                  // 1
  notificationClass: notificationClass,                                                                   // 8
  dropdownIcon: function() {                                                                              // 8
    if (this.icon) {                                                                                      // 10
      return this.icon;                                                                                   //
    } else {                                                                                              //
      return 'bell';                                                                                      //
    }                                                                                                     //
  },                                                                                                      //
  dropdownIconEmpty: function() {                                                                         // 8
    if (this.iconEmpty) {                                                                                 // 12
      return this.iconEmpty;                                                                              //
    } else {                                                                                              //
      return 'bell-o';                                                                                    //
    }                                                                                                     //
  },                                                                                                      //
  hasNotifications: function() {                                                                          // 8
    return Notifications.find().count() > 0;                                                              //
  }                                                                                                       //
});                                                                                                       //
                                                                                                          //
Template.notificationsDropdown.events({                                                                   // 1
  'click .notification': readNotification                                                                 // 17
});                                                                                                       //
                                                                                                          //
Template.notifications.helpers({                                                                          // 1
  notificationClass: notificationClass,                                                                   // 20
  ago: function() {                                                                                       // 20
    return moment(this.date).fromNow();                                                                   //
  }                                                                                                       //
});                                                                                                       //
                                                                                                          //
Template.notifications.events({                                                                           // 1
  'click .notification': readNotification                                                                 // 25
});                                                                                                       //
                                                                                                          //
////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                        //
// packages/notifications/lib/client/helpers.coffee.js                                                    //
//                                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                          //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.registerHelper('Notifications', function(options) {                                              // 1
  var limit, order;                                                                                       // 2
  if (typeof window['Notifications'] !== 'undefined') {                                                   // 2
    if (options instanceof Spacebars.kw && options.hash) {                                                // 3
      if (options.hash.limit != null) {                                                                   // 4
        limit = options.hash.limit;                                                                       // 4
      }                                                                                                   //
      if (options.hash.unreadFirst != null) {                                                             // 5
        order = {                                                                                         // 5
          read: 1,                                                                                        // 5
          date: -1                                                                                        // 5
        };                                                                                                //
      }                                                                                                   //
    } else {                                                                                              //
      limit = 0;                                                                                          // 7
      order = {                                                                                           // 7
        date: -1                                                                                          // 8
      };                                                                                                  //
    }                                                                                                     //
    return Notifications.find({}, {                                                                       //
      limit: limit,                                                                                       // 10
      sort: order                                                                                         // 10
    }).fetch();                                                                                           //
  }                                                                                                       //
});                                                                                                       // 1
                                                                                                          //
Template.registerHelper('notificationCount', function() {                                                 // 1
  if (typeof window['Notifications'] !== 'undefined') {                                                   // 13
    return Notifications.find({                                                                           //
      read: false                                                                                         // 14
    }).count();                                                                                           //
  }                                                                                                       //
});                                                                                                       // 12
                                                                                                          //
////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                        //
// packages/notifications/lib/client/tracker.coffee.js                                                    //
//                                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                          //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Deps.autorun(function() {                                                                                 // 1
  return Meteor.subscribe('notifications');                                                               //
});                                                                                                       // 1
                                                                                                          //
////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.notifications = {};

})();
