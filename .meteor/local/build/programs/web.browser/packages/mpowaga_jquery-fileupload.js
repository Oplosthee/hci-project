(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;

(function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                  //
// packages/mpowaga_jquery-fileupload/packages/mpowaga_jquery-fileupload.js                         //
//                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                    //
(function () {                                                                                      // 1
                                                                                                    // 2
//////////////////////////////////////////////////////////////////////////////////////////////      // 3
//                                                                                          //      // 4
// packages/mpowaga:jquery-fileupload/jquery.fileupload.js                                  //      // 5
//                                                                                          //      // 6
//////////////////////////////////////////////////////////////////////////////////////////////      // 7
                                                                                            //      // 8
/*                                                                                          // 1    // 9
 * jQuery File Upload Plugin                                                                // 2    // 10
 * https://github.com/blueimp/jQuery-File-Upload                                            // 3    // 11
 *                                                                                          // 4    // 12
 * Copyright 2010, Sebastian Tschan                                                         // 5    // 13
 * https://blueimp.net                                                                      // 6    // 14
 *                                                                                          // 7    // 15
 * Licensed under the MIT license:                                                          // 8    // 16
 * http://www.opensource.org/licenses/MIT                                                   // 9    // 17
 */                                                                                         // 10   // 18
                                                                                            // 11   // 19
/* jshint nomen:false */                                                                    // 12   // 20
/* global define, require, window, document, location, Blob, FormData */                    // 13   // 21
                                                                                            // 14   // 22
(function (factory) {                                                                       // 15   // 23
    'use strict';                                                                           // 16   // 24
    if (typeof define === 'function' && define.amd) {                                       // 17   // 25
        // Register as an anonymous AMD module:                                             // 18   // 26
        define([                                                                            // 19   // 27
            'jquery',                                                                       // 20   // 28
            'jquery.ui.widget'                                                              // 21   // 29
        ], factory);                                                                        // 22   // 30
    } else if (typeof exports === 'object') {                                               // 23   // 31
        // Node/CommonJS:                                                                   // 24   // 32
        factory(                                                                            // 25   // 33
            require('jquery'),                                                              // 26   // 34
            require('./vendor/jquery.ui.widget')                                            // 27   // 35
        );                                                                                  // 28   // 36
    } else {                                                                                // 29   // 37
        // Browser globals:                                                                 // 30   // 38
        factory(window.jQuery);                                                             // 31   // 39
    }                                                                                       // 32   // 40
}(function ($) {                                                                            // 33   // 41
    'use strict';                                                                           // 34   // 42
                                                                                            // 35   // 43
    // Detect file input support, based on                                                  // 36   // 44
    // http://viljamis.com/blog/2012/file-upload-support-on-mobile/                         // 37   // 45
    $.support.fileInput = !(new RegExp(                                                     // 38   // 46
        // Handle devices which give false positives for the feature detection:             // 39   // 47
        '(Android (1\\.[0156]|2\\.[01]))' +                                                 // 40   // 48
            '|(Windows Phone (OS 7|8\\.0))|(XBLWP)|(ZuneWP)|(WPDesktop)' +                  // 41   // 49
            '|(w(eb)?OSBrowser)|(webOS)' +                                                  // 42   // 50
            '|(Kindle/(1\\.0|2\\.[05]|3\\.0))'                                              // 43   // 51
    ).test(window.navigator.userAgent) ||                                                   // 44   // 52
        // Feature detection for all other devices:                                         // 45   // 53
        $('<input type="file">').prop('disabled'));                                         // 46   // 54
                                                                                            // 47   // 55
    // The FileReader API is not actually used, but works as feature detection,             // 48   // 56
    // as some Safari versions (5?) support XHR file uploads via the FormData API,          // 49   // 57
    // but not non-multipart XHR file uploads.                                              // 50   // 58
    // window.XMLHttpRequestUpload is not available on IE10, so we check for                // 51   // 59
    // window.ProgressEvent instead to detect XHR2 file upload capability:                  // 52   // 60
    $.support.xhrFileUpload = !!(window.ProgressEvent && window.FileReader);                // 53   // 61
    $.support.xhrFormDataFileUpload = !!window.FormData;                                    // 54   // 62
                                                                                            // 55   // 63
    // Detect support for Blob slicing (required for chunked uploads):                      // 56   // 64
    $.support.blobSlice = window.Blob && (Blob.prototype.slice ||                           // 57   // 65
        Blob.prototype.webkitSlice || Blob.prototype.mozSlice);                             // 58   // 66
                                                                                            // 59   // 67
    // Helper function to create drag handlers for dragover/dragenter/dragleave:            // 60   // 68
    function getDragHandler(type) {                                                         // 61   // 69
        var isDragOver = type === 'dragover';                                               // 62   // 70
        return function (e) {                                                               // 63   // 71
            e.dataTransfer = e.originalEvent && e.originalEvent.dataTransfer;               // 64   // 72
            var dataTransfer = e.dataTransfer;                                              // 65   // 73
            if (dataTransfer && $.inArray('Files', dataTransfer.types) !== -1 &&            // 66   // 74
                    this._trigger(                                                          // 67   // 75
                        type,                                                               // 68   // 76
                        $.Event(type, {delegatedEvent: e})                                  // 69   // 77
                    ) !== false) {                                                          // 70   // 78
                e.preventDefault();                                                         // 71   // 79
                if (isDragOver) {                                                           // 72   // 80
                    dataTransfer.dropEffect = 'copy';                                       // 73   // 81
                }                                                                           // 74   // 82
            }                                                                               // 75   // 83
        };                                                                                  // 76   // 84
    }                                                                                       // 77   // 85
                                                                                            // 78   // 86
    // The fileupload widget listens for change events on file input fields defined         // 79   // 87
    // via fileInput setting and paste or drop events of the given dropZone.                // 80   // 88
    // In addition to the default jQuery Widget methods, the fileupload widget              // 81   // 89
    // exposes the "add" and "send" methods, to add or directly send files using            // 82   // 90
    // the fileupload API.                                                                  // 83   // 91
    // By default, files added via file input selection, paste, drag & drop or              // 84   // 92
    // "add" method are uploaded immediately, but it is possible to override                // 85   // 93
    // the "add" callback option to queue file uploads.                                     // 86   // 94
    $.widget('blueimp.fileupload', {                                                        // 87   // 95
                                                                                            // 88   // 96
        options: {                                                                          // 89   // 97
            // The drop target element(s), by the default the complete document.            // 90   // 98
            // Set to null to disable drag & drop support:                                  // 91   // 99
            dropZone: $(document),                                                          // 92   // 100
            // The paste target element(s), by the default undefined.                       // 93   // 101
            // Set to a DOM node or jQuery object to enable file pasting:                   // 94   // 102
            pasteZone: undefined,                                                           // 95   // 103
            // The file input field(s), that are listened to for change events.             // 96   // 104
            // If undefined, it is set to the file input fields inside                      // 97   // 105
            // of the widget element on plugin initialization.                              // 98   // 106
            // Set to null to disable the change listener.                                  // 99   // 107
            fileInput: undefined,                                                           // 100  // 108
            // By default, the file input field is replaced with a clone after              // 101  // 109
            // each input field change event. This is required for iframe transport         // 102  // 110
            // queues and allows change events to be fired for the same file                // 103  // 111
            // selection, but can be disabled by setting the following option to false:     // 104  // 112
            replaceFileInput: true,                                                         // 105  // 113
            // The parameter name for the file form data (the request argument name).       // 106  // 114
            // If undefined or empty, the name property of the file input field is          // 107  // 115
            // used, or "files[]" if the file input name property is also empty,            // 108  // 116
            // can be a string or an array of strings:                                      // 109  // 117
            paramName: undefined,                                                           // 110  // 118
            // By default, each file of a selection is uploaded using an individual         // 111  // 119
            // request for XHR type uploads. Set to false to upload file                    // 112  // 120
            // selections in one request each:                                              // 113  // 121
            singleFileUploads: true,                                                        // 114  // 122
            // To limit the number of files uploaded with one XHR request,                  // 115  // 123
            // set the following option to an integer greater than 0:                       // 116  // 124
            limitMultiFileUploads: undefined,                                               // 117  // 125
            // The following option limits the number of files uploaded with one            // 118  // 126
            // XHR request to keep the request size under or equal to the defined           // 119  // 127
            // limit in bytes:                                                              // 120  // 128
            limitMultiFileUploadSize: undefined,                                            // 121  // 129
            // Multipart file uploads add a number of bytes to each uploaded file,          // 122  // 130
            // therefore the following option adds an overhead for each file used           // 123  // 131
            // in the limitMultiFileUploadSize configuration:                               // 124  // 132
            limitMultiFileUploadSizeOverhead: 512,                                          // 125  // 133
            // Set the following option to true to issue all file upload requests           // 126  // 134
            // in a sequential order:                                                       // 127  // 135
            sequentialUploads: false,                                                       // 128  // 136
            // To limit the number of concurrent uploads,                                   // 129  // 137
            // set the following option to an integer greater than 0:                       // 130  // 138
            limitConcurrentUploads: undefined,                                              // 131  // 139
            // Set the following option to true to force iframe transport uploads:          // 132  // 140
            forceIframeTransport: false,                                                    // 133  // 141
            // Set the following option to the location of a redirect url on the            // 134  // 142
            // origin server, for cross-domain iframe transport uploads:                    // 135  // 143
            redirect: undefined,                                                            // 136  // 144
            // The parameter name for the redirect url, sent as part of the form            // 137  // 145
            // data and set to 'redirect' if this option is empty:                          // 138  // 146
            redirectParamName: undefined,                                                   // 139  // 147
            // Set the following option to the location of a postMessage window,            // 140  // 148
            // to enable postMessage transport uploads:                                     // 141  // 149
            postMessage: undefined,                                                         // 142  // 150
            // By default, XHR file uploads are sent as multipart/form-data.                // 143  // 151
            // The iframe transport is always using multipart/form-data.                    // 144  // 152
            // Set to false to enable non-multipart XHR uploads:                            // 145  // 153
            multipart: true,                                                                // 146  // 154
            // To upload large files in smaller chunks, set the following option            // 147  // 155
            // to a preferred maximum chunk size. If set to 0, null or undefined,           // 148  // 156
            // or the browser does not support the required Blob API, files will            // 149  // 157
            // be uploaded as a whole.                                                      // 150  // 158
            maxChunkSize: undefined,                                                        // 151  // 159
            // When a non-multipart upload or a chunked multipart upload has been           // 152  // 160
            // aborted, this option can be used to resume the upload by setting             // 153  // 161
            // it to the size of the already uploaded bytes. This option is most            // 154  // 162
            // useful when modifying the options object inside of the "add" or              // 155  // 163
            // "send" callbacks, as the options are cloned for each file upload.            // 156  // 164
            uploadedBytes: undefined,                                                       // 157  // 165
            // By default, failed (abort or error) file uploads are removed from the        // 158  // 166
            // global progress calculation. Set the following option to false to            // 159  // 167
            // prevent recalculating the global progress data:                              // 160  // 168
            recalculateProgress: true,                                                      // 161  // 169
            // Interval in milliseconds to calculate and trigger progress events:           // 162  // 170
            progressInterval: 100,                                                          // 163  // 171
            // Interval in milliseconds to calculate progress bitrate:                      // 164  // 172
            bitrateInterval: 500,                                                           // 165  // 173
            // By default, uploads are started automatically when adding files:             // 166  // 174
            autoUpload: true,                                                               // 167  // 175
                                                                                            // 168  // 176
            // Error and info messages:                                                     // 169  // 177
            messages: {                                                                     // 170  // 178
                uploadedBytes: 'Uploaded bytes exceed file size'                            // 171  // 179
            },                                                                              // 172  // 180
                                                                                            // 173  // 181
            // Translation function, gets the message key to be translated                  // 174  // 182
            // and an object with context specific data as arguments:                       // 175  // 183
            i18n: function (message, context) {                                             // 176  // 184
                message = this.messages[message] || message.toString();                     // 177  // 185
                if (context) {                                                              // 178  // 186
                    $.each(context, function (key, value) {                                 // 179  // 187
                        message = message.replace('{' + key + '}', value);                  // 180  // 188
                    });                                                                     // 181  // 189
                }                                                                           // 182  // 190
                return message;                                                             // 183  // 191
            },                                                                              // 184  // 192
                                                                                            // 185  // 193
            // Additional form data to be sent along with the file uploads can be set       // 186  // 194
            // using this option, which accepts an array of objects with name and           // 187  // 195
            // value properties, a function returning such an array, a FormData             // 188  // 196
            // object (for XHR file uploads), or a simple object.                           // 189  // 197
            // The form of the first fileInput is given as parameter to the function:       // 190  // 198
            formData: function (form) {                                                     // 191  // 199
                return form.serializeArray();                                               // 192  // 200
            },                                                                              // 193  // 201
                                                                                            // 194  // 202
            // The add callback is invoked as soon as files are added to the fileupload     // 195  // 203
            // widget (via file input selection, drag & drop, paste or add API call).       // 196  // 204
            // If the singleFileUploads option is enabled, this callback will be            // 197  // 205
            // called once for each file in the selection for XHR file uploads, else        // 198  // 206
            // once for each file selection.                                                // 199  // 207
            //                                                                              // 200  // 208
            // The upload starts when the submit method is invoked on the data parameter.   // 201  // 209
            // The data object contains a files property holding the added files            // 202  // 210
            // and allows you to override plugin options as well as define ajax settings.   // 203  // 211
            //                                                                              // 204  // 212
            // Listeners for this callback can also be bound the following way:             // 205  // 213
            // .bind('fileuploadadd', func);                                                // 206  // 214
            //                                                                              // 207  // 215
            // data.submit() returns a Promise object and allows to attach additional       // 208  // 216
            // handlers using jQuery's Deferred callbacks:                                  // 209  // 217
            // data.submit().done(func).fail(func).always(func);                            // 210  // 218
            add: function (e, data) {                                                       // 211  // 219
                if (e.isDefaultPrevented()) {                                               // 212  // 220
                    return false;                                                           // 213  // 221
                }                                                                           // 214  // 222
                if (data.autoUpload || (data.autoUpload !== false &&                        // 215  // 223
                        $(this).fileupload('option', 'autoUpload'))) {                      // 216  // 224
                    data.process().done(function () {                                       // 217  // 225
                        data.submit();                                                      // 218  // 226
                    });                                                                     // 219  // 227
                }                                                                           // 220  // 228
            },                                                                              // 221  // 229
                                                                                            // 222  // 230
            // Other callbacks:                                                             // 223  // 231
                                                                                            // 224  // 232
            // Callback for the submit event of each file upload:                           // 225  // 233
            // submit: function (e, data) {}, // .bind('fileuploadsubmit', func);           // 226  // 234
                                                                                            // 227  // 235
            // Callback for the start of each file upload request:                          // 228  // 236
            // send: function (e, data) {}, // .bind('fileuploadsend', func);               // 229  // 237
                                                                                            // 230  // 238
            // Callback for successful uploads:                                             // 231  // 239
            // done: function (e, data) {}, // .bind('fileuploaddone', func);               // 232  // 240
                                                                                            // 233  // 241
            // Callback for failed (abort or error) uploads:                                // 234  // 242
            // fail: function (e, data) {}, // .bind('fileuploadfail', func);               // 235  // 243
                                                                                            // 236  // 244
            // Callback for completed (success, abort or error) requests:                   // 237  // 245
            // always: function (e, data) {}, // .bind('fileuploadalways', func);           // 238  // 246
                                                                                            // 239  // 247
            // Callback for upload progress events:                                         // 240  // 248
            // progress: function (e, data) {}, // .bind('fileuploadprogress', func);       // 241  // 249
                                                                                            // 242  // 250
            // Callback for global upload progress events:                                  // 243  // 251
            // progressall: function (e, data) {}, // .bind('fileuploadprogressall', func); // 244  // 252
                                                                                            // 245  // 253
            // Callback for uploads start, equivalent to the global ajaxStart event:        // 246  // 254
            // start: function (e) {}, // .bind('fileuploadstart', func);                   // 247  // 255
                                                                                            // 248  // 256
            // Callback for uploads stop, equivalent to the global ajaxStop event:          // 249  // 257
            // stop: function (e) {}, // .bind('fileuploadstop', func);                     // 250  // 258
                                                                                            // 251  // 259
            // Callback for change events of the fileInput(s):                              // 252  // 260
            // change: function (e, data) {}, // .bind('fileuploadchange', func);           // 253  // 261
                                                                                            // 254  // 262
            // Callback for paste events to the pasteZone(s):                               // 255  // 263
            // paste: function (e, data) {}, // .bind('fileuploadpaste', func);             // 256  // 264
                                                                                            // 257  // 265
            // Callback for drop events of the dropZone(s):                                 // 258  // 266
            // drop: function (e, data) {}, // .bind('fileuploaddrop', func);               // 259  // 267
                                                                                            // 260  // 268
            // Callback for dragover events of the dropZone(s):                             // 261  // 269
            // dragover: function (e) {}, // .bind('fileuploaddragover', func);             // 262  // 270
                                                                                            // 263  // 271
            // Callback for the start of each chunk upload request:                         // 264  // 272
            // chunksend: function (e, data) {}, // .bind('fileuploadchunksend', func);     // 265  // 273
                                                                                            // 266  // 274
            // Callback for successful chunk uploads:                                       // 267  // 275
            // chunkdone: function (e, data) {}, // .bind('fileuploadchunkdone', func);     // 268  // 276
                                                                                            // 269  // 277
            // Callback for failed (abort or error) chunk uploads:                          // 270  // 278
            // chunkfail: function (e, data) {}, // .bind('fileuploadchunkfail', func);     // 271  // 279
                                                                                            // 272  // 280
            // Callback for completed (success, abort or error) chunk upload requests:      // 273  // 281
            // chunkalways: function (e, data) {}, // .bind('fileuploadchunkalways', func); // 274  // 282
                                                                                            // 275  // 283
            // The plugin options are used as settings object for the ajax calls.           // 276  // 284
            // The following are jQuery ajax settings required for the file uploads:        // 277  // 285
            processData: false,                                                             // 278  // 286
            contentType: false,                                                             // 279  // 287
            cache: false,                                                                   // 280  // 288
            timeout: 0                                                                      // 281  // 289
        },                                                                                  // 282  // 290
                                                                                            // 283  // 291
        // A list of options that require reinitializing event listeners and/or             // 284  // 292
        // special initialization code:                                                     // 285  // 293
        _specialOptions: [                                                                  // 286  // 294
            'fileInput',                                                                    // 287  // 295
            'dropZone',                                                                     // 288  // 296
            'pasteZone',                                                                    // 289  // 297
            'multipart',                                                                    // 290  // 298
            'forceIframeTransport'                                                          // 291  // 299
        ],                                                                                  // 292  // 300
                                                                                            // 293  // 301
        _blobSlice: $.support.blobSlice && function () {                                    // 294  // 302
            var slice = this.slice || this.webkitSlice || this.mozSlice;                    // 295  // 303
            return slice.apply(this, arguments);                                            // 296  // 304
        },                                                                                  // 297  // 305
                                                                                            // 298  // 306
        _BitrateTimer: function () {                                                        // 299  // 307
            this.timestamp = ((Date.now) ? Date.now() : (new Date()).getTime());            // 300  // 308
            this.loaded = 0;                                                                // 301  // 309
            this.bitrate = 0;                                                               // 302  // 310
            this.getBitrate = function (now, loaded, interval) {                            // 303  // 311
                var timeDiff = now - this.timestamp;                                        // 304  // 312
                if (!this.bitrate || !interval || timeDiff > interval) {                    // 305  // 313
                    this.bitrate = (loaded - this.loaded) * (1000 / timeDiff) * 8;          // 306  // 314
                    this.loaded = loaded;                                                   // 307  // 315
                    this.timestamp = now;                                                   // 308  // 316
                }                                                                           // 309  // 317
                return this.bitrate;                                                        // 310  // 318
            };                                                                              // 311  // 319
        },                                                                                  // 312  // 320
                                                                                            // 313  // 321
        _isXHRUpload: function (options) {                                                  // 314  // 322
            return !options.forceIframeTransport &&                                         // 315  // 323
                ((!options.multipart && $.support.xhrFileUpload) ||                         // 316  // 324
                $.support.xhrFormDataFileUpload);                                           // 317  // 325
        },                                                                                  // 318  // 326
                                                                                            // 319  // 327
        _getFormData: function (options) {                                                  // 320  // 328
            var formData;                                                                   // 321  // 329
            if ($.type(options.formData) === 'function') {                                  // 322  // 330
                return options.formData(options.form);                                      // 323  // 331
            }                                                                               // 324  // 332
            if ($.isArray(options.formData)) {                                              // 325  // 333
                return options.formData;                                                    // 326  // 334
            }                                                                               // 327  // 335
            if ($.type(options.formData) === 'object') {                                    // 328  // 336
                formData = [];                                                              // 329  // 337
                $.each(options.formData, function (name, value) {                           // 330  // 338
                    formData.push({name: name, value: value});                              // 331  // 339
                });                                                                         // 332  // 340
                return formData;                                                            // 333  // 341
            }                                                                               // 334  // 342
            return [];                                                                      // 335  // 343
        },                                                                                  // 336  // 344
                                                                                            // 337  // 345
        _getTotal: function (files) {                                                       // 338  // 346
            var total = 0;                                                                  // 339  // 347
            $.each(files, function (index, file) {                                          // 340  // 348
                total += file.size || 1;                                                    // 341  // 349
            });                                                                             // 342  // 350
            return total;                                                                   // 343  // 351
        },                                                                                  // 344  // 352
                                                                                            // 345  // 353
        _initProgressObject: function (obj) {                                               // 346  // 354
            var progress = {                                                                // 347  // 355
                loaded: 0,                                                                  // 348  // 356
                total: 0,                                                                   // 349  // 357
                bitrate: 0                                                                  // 350  // 358
            };                                                                              // 351  // 359
            if (obj._progress) {                                                            // 352  // 360
                $.extend(obj._progress, progress);                                          // 353  // 361
            } else {                                                                        // 354  // 362
                obj._progress = progress;                                                   // 355  // 363
            }                                                                               // 356  // 364
        },                                                                                  // 357  // 365
                                                                                            // 358  // 366
        _initResponseObject: function (obj) {                                               // 359  // 367
            var prop;                                                                       // 360  // 368
            if (obj._response) {                                                            // 361  // 369
                for (prop in obj._response) {                                               // 362  // 370
                    if (obj._response.hasOwnProperty(prop)) {                               // 363  // 371
                        delete obj._response[prop];                                         // 364  // 372
                    }                                                                       // 365  // 373
                }                                                                           // 366  // 374
            } else {                                                                        // 367  // 375
                obj._response = {};                                                         // 368  // 376
            }                                                                               // 369  // 377
        },                                                                                  // 370  // 378
                                                                                            // 371  // 379
        _onProgress: function (e, data) {                                                   // 372  // 380
            if (e.lengthComputable) {                                                       // 373  // 381
                var now = ((Date.now) ? Date.now() : (new Date()).getTime()),               // 374  // 382
                    loaded;                                                                 // 375  // 383
                if (data._time && data.progressInterval &&                                  // 376  // 384
                        (now - data._time < data.progressInterval) &&                       // 377  // 385
                        e.loaded !== e.total) {                                             // 378  // 386
                    return;                                                                 // 379  // 387
                }                                                                           // 380  // 388
                data._time = now;                                                           // 381  // 389
                loaded = Math.floor(                                                        // 382  // 390
                    e.loaded / e.total * (data.chunkSize || data._progress.total)           // 383  // 391
                ) + (data.uploadedBytes || 0);                                              // 384  // 392
                // Add the difference from the previously loaded state                      // 385  // 393
                // to the global loaded counter:                                            // 386  // 394
                this._progress.loaded += (loaded - data._progress.loaded);                  // 387  // 395
                this._progress.bitrate = this._bitrateTimer.getBitrate(                     // 388  // 396
                    now,                                                                    // 389  // 397
                    this._progress.loaded,                                                  // 390  // 398
                    data.bitrateInterval                                                    // 391  // 399
                );                                                                          // 392  // 400
                data._progress.loaded = data.loaded = loaded;                               // 393  // 401
                data._progress.bitrate = data.bitrate = data._bitrateTimer.getBitrate(      // 394  // 402
                    now,                                                                    // 395  // 403
                    loaded,                                                                 // 396  // 404
                    data.bitrateInterval                                                    // 397  // 405
                );                                                                          // 398  // 406
                // Trigger a custom progress event with a total data property set           // 399  // 407
                // to the file size(s) of the current upload and a loaded data              // 400  // 408
                // property calculated accordingly:                                         // 401  // 409
                this._trigger(                                                              // 402  // 410
                    'progress',                                                             // 403  // 411
                    $.Event('progress', {delegatedEvent: e}),                               // 404  // 412
                    data                                                                    // 405  // 413
                );                                                                          // 406  // 414
                // Trigger a global progress event for all current file uploads,            // 407  // 415
                // including ajax calls queued for sequential file uploads:                 // 408  // 416
                this._trigger(                                                              // 409  // 417
                    'progressall',                                                          // 410  // 418
                    $.Event('progressall', {delegatedEvent: e}),                            // 411  // 419
                    this._progress                                                          // 412  // 420
                );                                                                          // 413  // 421
            }                                                                               // 414  // 422
        },                                                                                  // 415  // 423
                                                                                            // 416  // 424
        _initProgressListener: function (options) {                                         // 417  // 425
            var that = this,                                                                // 418  // 426
                xhr = options.xhr ? options.xhr() : $.ajaxSettings.xhr();                   // 419  // 427
            // Accesss to the native XHR object is required to add event listeners          // 420  // 428
            // for the upload progress event:                                               // 421  // 429
            if (xhr.upload) {                                                               // 422  // 430
                $(xhr.upload).bind('progress', function (e) {                               // 423  // 431
                    var oe = e.originalEvent;                                               // 424  // 432
                    // Make sure the progress event properties get copied over:             // 425  // 433
                    e.lengthComputable = oe.lengthComputable;                               // 426  // 434
                    e.loaded = oe.loaded;                                                   // 427  // 435
                    e.total = oe.total;                                                     // 428  // 436
                    that._onProgress(e, options);                                           // 429  // 437
                });                                                                         // 430  // 438
                options.xhr = function () {                                                 // 431  // 439
                    return xhr;                                                             // 432  // 440
                };                                                                          // 433  // 441
            }                                                                               // 434  // 442
        },                                                                                  // 435  // 443
                                                                                            // 436  // 444
        _isInstanceOf: function (type, obj) {                                               // 437  // 445
            // Cross-frame instanceof check                                                 // 438  // 446
            return Object.prototype.toString.call(obj) === '[object ' + type + ']';         // 439  // 447
        },                                                                                  // 440  // 448
                                                                                            // 441  // 449
        _initXHRData: function (options) {                                                  // 442  // 450
            var that = this,                                                                // 443  // 451
                formData,                                                                   // 444  // 452
                file = options.files[0],                                                    // 445  // 453
                // Ignore non-multipart setting if not supported:                           // 446  // 454
                multipart = options.multipart || !$.support.xhrFileUpload,                  // 447  // 455
                paramName = $.type(options.paramName) === 'array' ?                         // 448  // 456
                    options.paramName[0] : options.paramName;                               // 449  // 457
            options.headers = $.extend({}, options.headers);                                // 450  // 458
            if (options.contentRange) {                                                     // 451  // 459
                options.headers['Content-Range'] = options.contentRange;                    // 452  // 460
            }                                                                               // 453  // 461
            if (!multipart || options.blob || !this._isInstanceOf('File', file)) {          // 454  // 462
                options.headers['Content-Disposition'] = 'attachment; filename="' +         // 455  // 463
                    encodeURI(file.name) + '"';                                             // 456  // 464
            }                                                                               // 457  // 465
            if (!multipart) {                                                               // 458  // 466
                options.contentType = file.type || 'application/octet-stream';              // 459  // 467
                options.data = options.blob || file;                                        // 460  // 468
            } else if ($.support.xhrFormDataFileUpload) {                                   // 461  // 469
                if (options.postMessage) {                                                  // 462  // 470
                    // window.postMessage does not allow sending FormData                   // 463  // 471
                    // objects, so we just add the File/Blob objects to                     // 464  // 472
                    // the formData array and let the postMessage window                    // 465  // 473
                    // create the FormData object out of this array:                        // 466  // 474
                    formData = this._getFormData(options);                                  // 467  // 475
                    if (options.blob) {                                                     // 468  // 476
                        formData.push({                                                     // 469  // 477
                            name: paramName,                                                // 470  // 478
                            value: options.blob                                             // 471  // 479
                        });                                                                 // 472  // 480
                    } else {                                                                // 473  // 481
                        $.each(options.files, function (index, file) {                      // 474  // 482
                            formData.push({                                                 // 475  // 483
                                name: ($.type(options.paramName) === 'array' &&             // 476  // 484
                                    options.paramName[index]) || paramName,                 // 477  // 485
                                value: file                                                 // 478  // 486
                            });                                                             // 479  // 487
                        });                                                                 // 480  // 488
                    }                                                                       // 481  // 489
                } else {                                                                    // 482  // 490
                    if (that._isInstanceOf('FormData', options.formData)) {                 // 483  // 491
                        formData = options.formData;                                        // 484  // 492
                    } else {                                                                // 485  // 493
                        formData = new FormData();                                          // 486  // 494
                        $.each(this._getFormData(options), function (index, field) {        // 487  // 495
                            formData.append(field.name, field.value);                       // 488  // 496
                        });                                                                 // 489  // 497
                    }                                                                       // 490  // 498
                    if (options.blob) {                                                     // 491  // 499
                        formData.append(paramName, options.blob, file.name);                // 492  // 500
                    } else {                                                                // 493  // 501
                        $.each(options.files, function (index, file) {                      // 494  // 502
                            // This check allows the tests to run with                      // 495  // 503
                            // dummy objects:                                               // 496  // 504
                            if (that._isInstanceOf('File', file) ||                         // 497  // 505
                                    that._isInstanceOf('Blob', file)) {                     // 498  // 506
                                formData.append(                                            // 499  // 507
                                    ($.type(options.paramName) === 'array' &&               // 500  // 508
                                        options.paramName[index]) || paramName,             // 501  // 509
                                    file,                                                   // 502  // 510
                                    file.uploadName || file.name                            // 503  // 511
                                );                                                          // 504  // 512
                            }                                                               // 505  // 513
                        });                                                                 // 506  // 514
                    }                                                                       // 507  // 515
                }                                                                           // 508  // 516
                options.data = formData;                                                    // 509  // 517
            }                                                                               // 510  // 518
            // Blob reference is not needed anymore, free memory:                           // 511  // 519
            options.blob = null;                                                            // 512  // 520
        },                                                                                  // 513  // 521
                                                                                            // 514  // 522
        _initIframeSettings: function (options) {                                           // 515  // 523
            var targetHost = $('<a></a>').prop('href', options.url).prop('host');           // 516  // 524
            // Setting the dataType to iframe enables the iframe transport:                 // 517  // 525
            options.dataType = 'iframe ' + (options.dataType || '');                        // 518  // 526
            // The iframe transport accepts a serialized array as form data:                // 519  // 527
            options.formData = this._getFormData(options);                                  // 520  // 528
            // Add redirect url to form data on cross-domain uploads:                       // 521  // 529
            if (options.redirect && targetHost && targetHost !== location.host) {           // 522  // 530
                options.formData.push({                                                     // 523  // 531
                    name: options.redirectParamName || 'redirect',                          // 524  // 532
                    value: options.redirect                                                 // 525  // 533
                });                                                                         // 526  // 534
            }                                                                               // 527  // 535
        },                                                                                  // 528  // 536
                                                                                            // 529  // 537
        _initDataSettings: function (options) {                                             // 530  // 538
            if (this._isXHRUpload(options)) {                                               // 531  // 539
                if (!this._chunkedUpload(options, true)) {                                  // 532  // 540
                    if (!options.data) {                                                    // 533  // 541
                        this._initXHRData(options);                                         // 534  // 542
                    }                                                                       // 535  // 543
                    this._initProgressListener(options);                                    // 536  // 544
                }                                                                           // 537  // 545
                if (options.postMessage) {                                                  // 538  // 546
                    // Setting the dataType to postmessage enables the                      // 539  // 547
                    // postMessage transport:                                               // 540  // 548
                    options.dataType = 'postmessage ' + (options.dataType || '');           // 541  // 549
                }                                                                           // 542  // 550
            } else {                                                                        // 543  // 551
                this._initIframeSettings(options);                                          // 544  // 552
            }                                                                               // 545  // 553
        },                                                                                  // 546  // 554
                                                                                            // 547  // 555
        _getParamName: function (options) {                                                 // 548  // 556
            var fileInput = $(options.fileInput),                                           // 549  // 557
                paramName = options.paramName;                                              // 550  // 558
            if (!paramName) {                                                               // 551  // 559
                paramName = [];                                                             // 552  // 560
                fileInput.each(function () {                                                // 553  // 561
                    var input = $(this),                                                    // 554  // 562
                        name = input.prop('name') || 'files[]',                             // 555  // 563
                        i = (input.prop('files') || [1]).length;                            // 556  // 564
                    while (i) {                                                             // 557  // 565
                        paramName.push(name);                                               // 558  // 566
                        i -= 1;                                                             // 559  // 567
                    }                                                                       // 560  // 568
                });                                                                         // 561  // 569
                if (!paramName.length) {                                                    // 562  // 570
                    paramName = [fileInput.prop('name') || 'files[]'];                      // 563  // 571
                }                                                                           // 564  // 572
            } else if (!$.isArray(paramName)) {                                             // 565  // 573
                paramName = [paramName];                                                    // 566  // 574
            }                                                                               // 567  // 575
            return paramName;                                                               // 568  // 576
        },                                                                                  // 569  // 577
                                                                                            // 570  // 578
        _initFormSettings: function (options) {                                             // 571  // 579
            // Retrieve missing options from the input field and the                        // 572  // 580
            // associated form, if available:                                               // 573  // 581
            if (!options.form || !options.form.length) {                                    // 574  // 582
                options.form = $(options.fileInput.prop('form'));                           // 575  // 583
                // If the given file input doesn't have an associated form,                 // 576  // 584
                // use the default widget file input's form:                                // 577  // 585
                if (!options.form.length) {                                                 // 578  // 586
                    options.form = $(this.options.fileInput.prop('form'));                  // 579  // 587
                }                                                                           // 580  // 588
            }                                                                               // 581  // 589
            options.paramName = this._getParamName(options);                                // 582  // 590
            if (!options.url) {                                                             // 583  // 591
                options.url = options.form.prop('action') || location.href;                 // 584  // 592
            }                                                                               // 585  // 593
            // The HTTP request method must be "POST" or "PUT":                             // 586  // 594
            options.type = (options.type ||                                                 // 587  // 595
                ($.type(options.form.prop('method')) === 'string' &&                        // 588  // 596
                    options.form.prop('method')) || ''                                      // 589  // 597
                ).toUpperCase();                                                            // 590  // 598
            if (options.type !== 'POST' && options.type !== 'PUT' &&                        // 591  // 599
                    options.type !== 'PATCH') {                                             // 592  // 600
                options.type = 'POST';                                                      // 593  // 601
            }                                                                               // 594  // 602
            if (!options.formAcceptCharset) {                                               // 595  // 603
                options.formAcceptCharset = options.form.attr('accept-charset');            // 596  // 604
            }                                                                               // 597  // 605
        },                                                                                  // 598  // 606
                                                                                            // 599  // 607
        _getAJAXSettings: function (data) {                                                 // 600  // 608
            var options = $.extend({}, this.options, data);                                 // 601  // 609
            this._initFormSettings(options);                                                // 602  // 610
            this._initDataSettings(options);                                                // 603  // 611
            return options;                                                                 // 604  // 612
        },                                                                                  // 605  // 613
                                                                                            // 606  // 614
        // jQuery 1.6 doesn't provide .state(),                                             // 607  // 615
        // while jQuery 1.8+ removed .isRejected() and .isResolved():                       // 608  // 616
        _getDeferredState: function (deferred) {                                            // 609  // 617
            if (deferred.state) {                                                           // 610  // 618
                return deferred.state();                                                    // 611  // 619
            }                                                                               // 612  // 620
            if (deferred.isResolved()) {                                                    // 613  // 621
                return 'resolved';                                                          // 614  // 622
            }                                                                               // 615  // 623
            if (deferred.isRejected()) {                                                    // 616  // 624
                return 'rejected';                                                          // 617  // 625
            }                                                                               // 618  // 626
            return 'pending';                                                               // 619  // 627
        },                                                                                  // 620  // 628
                                                                                            // 621  // 629
        // Maps jqXHR callbacks to the equivalent                                           // 622  // 630
        // methods of the given Promise object:                                             // 623  // 631
        _enhancePromise: function (promise) {                                               // 624  // 632
            promise.success = promise.done;                                                 // 625  // 633
            promise.error = promise.fail;                                                   // 626  // 634
            promise.complete = promise.always;                                              // 627  // 635
            return promise;                                                                 // 628  // 636
        },                                                                                  // 629  // 637
                                                                                            // 630  // 638
        // Creates and returns a Promise object enhanced with                               // 631  // 639
        // the jqXHR methods abort, success, error and complete:                            // 632  // 640
        _getXHRPromise: function (resolveOrReject, context, args) {                         // 633  // 641
            var dfd = $.Deferred(),                                                         // 634  // 642
                promise = dfd.promise();                                                    // 635  // 643
            context = context || this.options.context || promise;                           // 636  // 644
            if (resolveOrReject === true) {                                                 // 637  // 645
                dfd.resolveWith(context, args);                                             // 638  // 646
            } else if (resolveOrReject === false) {                                         // 639  // 647
                dfd.rejectWith(context, args);                                              // 640  // 648
            }                                                                               // 641  // 649
            promise.abort = dfd.promise;                                                    // 642  // 650
            return this._enhancePromise(promise);                                           // 643  // 651
        },                                                                                  // 644  // 652
                                                                                            // 645  // 653
        // Adds convenience methods to the data callback argument:                          // 646  // 654
        _addConvenienceMethods: function (e, data) {                                        // 647  // 655
            var that = this,                                                                // 648  // 656
                getPromise = function (args) {                                              // 649  // 657
                    return $.Deferred().resolveWith(that, args).promise();                  // 650  // 658
                };                                                                          // 651  // 659
            data.process = function (resolveFunc, rejectFunc) {                             // 652  // 660
                if (resolveFunc || rejectFunc) {                                            // 653  // 661
                    data._processQueue = this._processQueue =                               // 654  // 662
                        (this._processQueue || getPromise([this])).pipe(                    // 655  // 663
                            function () {                                                   // 656  // 664
                                if (data.errorThrown) {                                     // 657  // 665
                                    return $.Deferred()                                     // 658  // 666
                                        .rejectWith(that, [data]).promise();                // 659  // 667
                                }                                                           // 660  // 668
                                return getPromise(arguments);                               // 661  // 669
                            }                                                               // 662  // 670
                        ).pipe(resolveFunc, rejectFunc);                                    // 663  // 671
                }                                                                           // 664  // 672
                return this._processQueue || getPromise([this]);                            // 665  // 673
            };                                                                              // 666  // 674
            data.submit = function () {                                                     // 667  // 675
                if (this.state() !== 'pending') {                                           // 668  // 676
                    data.jqXHR = this.jqXHR =                                               // 669  // 677
                        (that._trigger(                                                     // 670  // 678
                            'submit',                                                       // 671  // 679
                            $.Event('submit', {delegatedEvent: e}),                         // 672  // 680
                            this                                                            // 673  // 681
                        ) !== false) && that._onSend(e, this);                              // 674  // 682
                }                                                                           // 675  // 683
                return this.jqXHR || that._getXHRPromise();                                 // 676  // 684
            };                                                                              // 677  // 685
            data.abort = function () {                                                      // 678  // 686
                if (this.jqXHR) {                                                           // 679  // 687
                    return this.jqXHR.abort();                                              // 680  // 688
                }                                                                           // 681  // 689
                this.errorThrown = 'abort';                                                 // 682  // 690
                that._trigger('fail', null, this);                                          // 683  // 691
                return that._getXHRPromise(false);                                          // 684  // 692
            };                                                                              // 685  // 693
            data.state = function () {                                                      // 686  // 694
                if (this.jqXHR) {                                                           // 687  // 695
                    return that._getDeferredState(this.jqXHR);                              // 688  // 696
                }                                                                           // 689  // 697
                if (this._processQueue) {                                                   // 690  // 698
                    return that._getDeferredState(this._processQueue);                      // 691  // 699
                }                                                                           // 692  // 700
            };                                                                              // 693  // 701
            data.processing = function () {                                                 // 694  // 702
                return !this.jqXHR && this._processQueue && that                            // 695  // 703
                    ._getDeferredState(this._processQueue) === 'pending';                   // 696  // 704
            };                                                                              // 697  // 705
            data.progress = function () {                                                   // 698  // 706
                return this._progress;                                                      // 699  // 707
            };                                                                              // 700  // 708
            data.response = function () {                                                   // 701  // 709
                return this._response;                                                      // 702  // 710
            };                                                                              // 703  // 711
        },                                                                                  // 704  // 712
                                                                                            // 705  // 713
        // Parses the Range header from the server response                                 // 706  // 714
        // and returns the uploaded bytes:                                                  // 707  // 715
        _getUploadedBytes: function (jqXHR) {                                               // 708  // 716
            var range = jqXHR.getResponseHeader('Range'),                                   // 709  // 717
                parts = range && range.split('-'),                                          // 710  // 718
                upperBytesPos = parts && parts.length > 1 &&                                // 711  // 719
                    parseInt(parts[1], 10);                                                 // 712  // 720
            return upperBytesPos && upperBytesPos + 1;                                      // 713  // 721
        },                                                                                  // 714  // 722
                                                                                            // 715  // 723
        // Uploads a file in multiple, sequential requests                                  // 716  // 724
        // by splitting the file up in multiple blob chunks.                                // 717  // 725
        // If the second parameter is true, only tests if the file                          // 718  // 726
        // should be uploaded in chunks, but does not invoke any                            // 719  // 727
        // upload requests:                                                                 // 720  // 728
        _chunkedUpload: function (options, testOnly) {                                      // 721  // 729
            options.uploadedBytes = options.uploadedBytes || 0;                             // 722  // 730
            var that = this,                                                                // 723  // 731
                file = options.files[0],                                                    // 724  // 732
                fs = file.size,                                                             // 725  // 733
                ub = options.uploadedBytes,                                                 // 726  // 734
                mcs = options.maxChunkSize || fs,                                           // 727  // 735
                slice = this._blobSlice,                                                    // 728  // 736
                dfd = $.Deferred(),                                                         // 729  // 737
                promise = dfd.promise(),                                                    // 730  // 738
                jqXHR,                                                                      // 731  // 739
                upload;                                                                     // 732  // 740
            if (!(this._isXHRUpload(options) && slice && (ub || mcs < fs)) ||               // 733  // 741
                    options.data) {                                                         // 734  // 742
                return false;                                                               // 735  // 743
            }                                                                               // 736  // 744
            if (testOnly) {                                                                 // 737  // 745
                return true;                                                                // 738  // 746
            }                                                                               // 739  // 747
            if (ub >= fs) {                                                                 // 740  // 748
                file.error = options.i18n('uploadedBytes');                                 // 741  // 749
                return this._getXHRPromise(                                                 // 742  // 750
                    false,                                                                  // 743  // 751
                    options.context,                                                        // 744  // 752
                    [null, 'error', file.error]                                             // 745  // 753
                );                                                                          // 746  // 754
            }                                                                               // 747  // 755
            // The chunk upload method:                                                     // 748  // 756
            upload = function () {                                                          // 749  // 757
                // Clone the options object for each chunk upload:                          // 750  // 758
                var o = $.extend({}, options),                                              // 751  // 759
                    currentLoaded = o._progress.loaded;                                     // 752  // 760
                o.blob = slice.call(                                                        // 753  // 761
                    file,                                                                   // 754  // 762
                    ub,                                                                     // 755  // 763
                    ub + mcs,                                                               // 756  // 764
                    file.type                                                               // 757  // 765
                );                                                                          // 758  // 766
                // Store the current chunk size, as the blob itself                         // 759  // 767
                // will be dereferenced after data processing:                              // 760  // 768
                o.chunkSize = o.blob.size;                                                  // 761  // 769
                // Expose the chunk bytes position range:                                   // 762  // 770
                o.contentRange = 'bytes ' + ub + '-' +                                      // 763  // 771
                    (ub + o.chunkSize - 1) + '/' + fs;                                      // 764  // 772
                // Process the upload data (the blob and potential form data):              // 765  // 773
                that._initXHRData(o);                                                       // 766  // 774
                // Add progress listeners for this chunk upload:                            // 767  // 775
                that._initProgressListener(o);                                              // 768  // 776
                jqXHR = ((that._trigger('chunksend', null, o) !== false && $.ajax(o)) ||    // 769  // 777
                        that._getXHRPromise(false, o.context))                              // 770  // 778
                    .done(function (result, textStatus, jqXHR) {                            // 771  // 779
                        ub = that._getUploadedBytes(jqXHR) ||                               // 772  // 780
                            (ub + o.chunkSize);                                             // 773  // 781
                        // Create a progress event if no final progress event               // 774  // 782
                        // with loaded equaling total has been triggered                    // 775  // 783
                        // for this chunk:                                                  // 776  // 784
                        if (currentLoaded + o.chunkSize - o._progress.loaded) {             // 777  // 785
                            that._onProgress($.Event('progress', {                          // 778  // 786
                                lengthComputable: true,                                     // 779  // 787
                                loaded: ub - o.uploadedBytes,                               // 780  // 788
                                total: ub - o.uploadedBytes                                 // 781  // 789
                            }), o);                                                         // 782  // 790
                        }                                                                   // 783  // 791
                        options.uploadedBytes = o.uploadedBytes = ub;                       // 784  // 792
                        o.result = result;                                                  // 785  // 793
                        o.textStatus = textStatus;                                          // 786  // 794
                        o.jqXHR = jqXHR;                                                    // 787  // 795
                        that._trigger('chunkdone', null, o);                                // 788  // 796
                        that._trigger('chunkalways', null, o);                              // 789  // 797
                        if (ub < fs) {                                                      // 790  // 798
                            // File upload not yet complete,                                // 791  // 799
                            // continue with the next chunk:                                // 792  // 800
                            upload();                                                       // 793  // 801
                        } else {                                                            // 794  // 802
                            dfd.resolveWith(                                                // 795  // 803
                                o.context,                                                  // 796  // 804
                                [result, textStatus, jqXHR]                                 // 797  // 805
                            );                                                              // 798  // 806
                        }                                                                   // 799  // 807
                    })                                                                      // 800  // 808
                    .fail(function (jqXHR, textStatus, errorThrown) {                       // 801  // 809
                        o.jqXHR = jqXHR;                                                    // 802  // 810
                        o.textStatus = textStatus;                                          // 803  // 811
                        o.errorThrown = errorThrown;                                        // 804  // 812
                        that._trigger('chunkfail', null, o);                                // 805  // 813
                        that._trigger('chunkalways', null, o);                              // 806  // 814
                        dfd.rejectWith(                                                     // 807  // 815
                            o.context,                                                      // 808  // 816
                            [jqXHR, textStatus, errorThrown]                                // 809  // 817
                        );                                                                  // 810  // 818
                    });                                                                     // 811  // 819
            };                                                                              // 812  // 820
            this._enhancePromise(promise);                                                  // 813  // 821
            promise.abort = function () {                                                   // 814  // 822
                return jqXHR.abort();                                                       // 815  // 823
            };                                                                              // 816  // 824
            upload();                                                                       // 817  // 825
            return promise;                                                                 // 818  // 826
        },                                                                                  // 819  // 827
                                                                                            // 820  // 828
        _beforeSend: function (e, data) {                                                   // 821  // 829
            if (this._active === 0) {                                                       // 822  // 830
                // the start callback is triggered when an upload starts                    // 823  // 831
                // and no other uploads are currently running,                              // 824  // 832
                // equivalent to the global ajaxStart event:                                // 825  // 833
                this._trigger('start');                                                     // 826  // 834
                // Set timer for global bitrate progress calculation:                       // 827  // 835
                this._bitrateTimer = new this._BitrateTimer();                              // 828  // 836
                // Reset the global progress values:                                        // 829  // 837
                this._progress.loaded = this._progress.total = 0;                           // 830  // 838
                this._progress.bitrate = 0;                                                 // 831  // 839
            }                                                                               // 832  // 840
            // Make sure the container objects for the .response() and                      // 833  // 841
            // .progress() methods on the data object are available                         // 834  // 842
            // and reset to their initial state:                                            // 835  // 843
            this._initResponseObject(data);                                                 // 836  // 844
            this._initProgressObject(data);                                                 // 837  // 845
            data._progress.loaded = data.loaded = data.uploadedBytes || 0;                  // 838  // 846
            data._progress.total = data.total = this._getTotal(data.files) || 1;            // 839  // 847
            data._progress.bitrate = data.bitrate = 0;                                      // 840  // 848
            this._active += 1;                                                              // 841  // 849
            // Initialize the global progress values:                                       // 842  // 850
            this._progress.loaded += data.loaded;                                           // 843  // 851
            this._progress.total += data.total;                                             // 844  // 852
        },                                                                                  // 845  // 853
                                                                                            // 846  // 854
        _onDone: function (result, textStatus, jqXHR, options) {                            // 847  // 855
            var total = options._progress.total,                                            // 848  // 856
                response = options._response;                                               // 849  // 857
            if (options._progress.loaded < total) {                                         // 850  // 858
                // Create a progress event if no final progress event                       // 851  // 859
                // with loaded equaling total has been triggered:                           // 852  // 860
                this._onProgress($.Event('progress', {                                      // 853  // 861
                    lengthComputable: true,                                                 // 854  // 862
                    loaded: total,                                                          // 855  // 863
                    total: total                                                            // 856  // 864
                }), options);                                                               // 857  // 865
            }                                                                               // 858  // 866
            response.result = options.result = result;                                      // 859  // 867
            response.textStatus = options.textStatus = textStatus;                          // 860  // 868
            response.jqXHR = options.jqXHR = jqXHR;                                         // 861  // 869
            this._trigger('done', null, options);                                           // 862  // 870
        },                                                                                  // 863  // 871
                                                                                            // 864  // 872
        _onFail: function (jqXHR, textStatus, errorThrown, options) {                       // 865  // 873
            var response = options._response;                                               // 866  // 874
            if (options.recalculateProgress) {                                              // 867  // 875
                // Remove the failed (error or abort) file upload from                      // 868  // 876
                // the global progress calculation:                                         // 869  // 877
                this._progress.loaded -= options._progress.loaded;                          // 870  // 878
                this._progress.total -= options._progress.total;                            // 871  // 879
            }                                                                               // 872  // 880
            response.jqXHR = options.jqXHR = jqXHR;                                         // 873  // 881
            response.textStatus = options.textStatus = textStatus;                          // 874  // 882
            response.errorThrown = options.errorThrown = errorThrown;                       // 875  // 883
            this._trigger('fail', null, options);                                           // 876  // 884
        },                                                                                  // 877  // 885
                                                                                            // 878  // 886
        _onAlways: function (jqXHRorResult, textStatus, jqXHRorError, options) {            // 879  // 887
            // jqXHRorResult, textStatus and jqXHRorError are added to the                  // 880  // 888
            // options object via done and fail callbacks                                   // 881  // 889
            this._trigger('always', null, options);                                         // 882  // 890
        },                                                                                  // 883  // 891
                                                                                            // 884  // 892
        _onSend: function (e, data) {                                                       // 885  // 893
            if (!data.submit) {                                                             // 886  // 894
                this._addConvenienceMethods(e, data);                                       // 887  // 895
            }                                                                               // 888  // 896
            var that = this,                                                                // 889  // 897
                jqXHR,                                                                      // 890  // 898
                aborted,                                                                    // 891  // 899
                slot,                                                                       // 892  // 900
                pipe,                                                                       // 893  // 901
                options = that._getAJAXSettings(data),                                      // 894  // 902
                send = function () {                                                        // 895  // 903
                    that._sending += 1;                                                     // 896  // 904
                    // Set timer for bitrate progress calculation:                          // 897  // 905
                    options._bitrateTimer = new that._BitrateTimer();                       // 898  // 906
                    jqXHR = jqXHR || (                                                      // 899  // 907
                        ((aborted || that._trigger(                                         // 900  // 908
                            'send',                                                         // 901  // 909
                            $.Event('send', {delegatedEvent: e}),                           // 902  // 910
                            options                                                         // 903  // 911
                        ) === false) &&                                                     // 904  // 912
                        that._getXHRPromise(false, options.context, aborted)) ||            // 905  // 913
                        that._chunkedUpload(options) || $.ajax(options)                     // 906  // 914
                    ).done(function (result, textStatus, jqXHR) {                           // 907  // 915
                        that._onDone(result, textStatus, jqXHR, options);                   // 908  // 916
                    }).fail(function (jqXHR, textStatus, errorThrown) {                     // 909  // 917
                        that._onFail(jqXHR, textStatus, errorThrown, options);              // 910  // 918
                    }).always(function (jqXHRorResult, textStatus, jqXHRorError) {          // 911  // 919
                        that._onAlways(                                                     // 912  // 920
                            jqXHRorResult,                                                  // 913  // 921
                            textStatus,                                                     // 914  // 922
                            jqXHRorError,                                                   // 915  // 923
                            options                                                         // 916  // 924
                        );                                                                  // 917  // 925
                        that._sending -= 1;                                                 // 918  // 926
                        that._active -= 1;                                                  // 919  // 927
                        if (options.limitConcurrentUploads &&                               // 920  // 928
                                options.limitConcurrentUploads > that._sending) {           // 921  // 929
                            // Start the next queued upload,                                // 922  // 930
                            // that has not been aborted:                                   // 923  // 931
                            var nextSlot = that._slots.shift();                             // 924  // 932
                            while (nextSlot) {                                              // 925  // 933
                                if (that._getDeferredState(nextSlot) === 'pending') {       // 926  // 934
                                    nextSlot.resolve();                                     // 927  // 935
                                    break;                                                  // 928  // 936
                                }                                                           // 929  // 937
                                nextSlot = that._slots.shift();                             // 930  // 938
                            }                                                               // 931  // 939
                        }                                                                   // 932  // 940
                        if (that._active === 0) {                                           // 933  // 941
                            // The stop callback is triggered when all uploads have         // 934  // 942
                            // been completed, equivalent to the global ajaxStop event:     // 935  // 943
                            that._trigger('stop');                                          // 936  // 944
                        }                                                                   // 937  // 945
                    });                                                                     // 938  // 946
                    return jqXHR;                                                           // 939  // 947
                };                                                                          // 940  // 948
            this._beforeSend(e, options);                                                   // 941  // 949
            if (this.options.sequentialUploads ||                                           // 942  // 950
                    (this.options.limitConcurrentUploads &&                                 // 943  // 951
                    this.options.limitConcurrentUploads <= this._sending)) {                // 944  // 952
                if (this.options.limitConcurrentUploads > 1) {                              // 945  // 953
                    slot = $.Deferred();                                                    // 946  // 954
                    this._slots.push(slot);                                                 // 947  // 955
                    pipe = slot.pipe(send);                                                 // 948  // 956
                } else {                                                                    // 949  // 957
                    this._sequence = this._sequence.pipe(send, send);                       // 950  // 958
                    pipe = this._sequence;                                                  // 951  // 959
                }                                                                           // 952  // 960
                // Return the piped Promise object, enhanced with an abort method,          // 953  // 961
                // which is delegated to the jqXHR object of the current upload,            // 954  // 962
                // and jqXHR callbacks mapped to the equivalent Promise methods:            // 955  // 963
                pipe.abort = function () {                                                  // 956  // 964
                    aborted = [undefined, 'abort', 'abort'];                                // 957  // 965
                    if (!jqXHR) {                                                           // 958  // 966
                        if (slot) {                                                         // 959  // 967
                            slot.rejectWith(options.context, aborted);                      // 960  // 968
                        }                                                                   // 961  // 969
                        return send();                                                      // 962  // 970
                    }                                                                       // 963  // 971
                    return jqXHR.abort();                                                   // 964  // 972
                };                                                                          // 965  // 973
                return this._enhancePromise(pipe);                                          // 966  // 974
            }                                                                               // 967  // 975
            return send();                                                                  // 968  // 976
        },                                                                                  // 969  // 977
                                                                                            // 970  // 978
        _onAdd: function (e, data) {                                                        // 971  // 979
            var that = this,                                                                // 972  // 980
                result = true,                                                              // 973  // 981
                options = $.extend({}, this.options, data),                                 // 974  // 982
                files = data.files,                                                         // 975  // 983
                filesLength = files.length,                                                 // 976  // 984
                limit = options.limitMultiFileUploads,                                      // 977  // 985
                limitSize = options.limitMultiFileUploadSize,                               // 978  // 986
                overhead = options.limitMultiFileUploadSizeOverhead,                        // 979  // 987
                batchSize = 0,                                                              // 980  // 988
                paramName = this._getParamName(options),                                    // 981  // 989
                paramNameSet,                                                               // 982  // 990
                paramNameSlice,                                                             // 983  // 991
                fileSet,                                                                    // 984  // 992
                i,                                                                          // 985  // 993
                j = 0;                                                                      // 986  // 994
            if (!filesLength) {                                                             // 987  // 995
                return false;                                                               // 988  // 996
            }                                                                               // 989  // 997
            if (limitSize && files[0].size === undefined) {                                 // 990  // 998
                limitSize = undefined;                                                      // 991  // 999
            }                                                                               // 992  // 1000
            if (!(options.singleFileUploads || limit || limitSize) ||                       // 993  // 1001
                    !this._isXHRUpload(options)) {                                          // 994  // 1002
                fileSet = [files];                                                          // 995  // 1003
                paramNameSet = [paramName];                                                 // 996  // 1004
            } else if (!(options.singleFileUploads || limitSize) && limit) {                // 997  // 1005
                fileSet = [];                                                               // 998  // 1006
                paramNameSet = [];                                                          // 999  // 1007
                for (i = 0; i < filesLength; i += limit) {                                  // 1000
                    fileSet.push(files.slice(i, i + limit));                                // 1001
                    paramNameSlice = paramName.slice(i, i + limit);                         // 1002
                    if (!paramNameSlice.length) {                                           // 1003
                        paramNameSlice = paramName;                                         // 1004
                    }                                                                       // 1005
                    paramNameSet.push(paramNameSlice);                                      // 1006
                }                                                                           // 1007
            } else if (!options.singleFileUploads && limitSize) {                           // 1008
                fileSet = [];                                                               // 1009
                paramNameSet = [];                                                          // 1010
                for (i = 0; i < filesLength; i = i + 1) {                                   // 1011
                    batchSize += files[i].size + overhead;                                  // 1012
                    if (i + 1 === filesLength ||                                            // 1013
                            ((batchSize + files[i + 1].size + overhead) > limitSize) ||     // 1014
                            (limit && i + 1 - j >= limit)) {                                // 1015
                        fileSet.push(files.slice(j, i + 1));                                // 1016
                        paramNameSlice = paramName.slice(j, i + 1);                         // 1017
                        if (!paramNameSlice.length) {                                       // 1018
                            paramNameSlice = paramName;                                     // 1019
                        }                                                                   // 1020
                        paramNameSet.push(paramNameSlice);                                  // 1021
                        j = i + 1;                                                          // 1022
                        batchSize = 0;                                                      // 1023
                    }                                                                       // 1024
                }                                                                           // 1025
            } else {                                                                        // 1026
                paramNameSet = paramName;                                                   // 1027
            }                                                                               // 1028
            data.originalFiles = files;                                                     // 1029
            $.each(fileSet || files, function (index, element) {                            // 1030
                var newData = $.extend({}, data);                                           // 1031
                newData.files = fileSet ? element : [element];                              // 1032
                newData.paramName = paramNameSet[index];                                    // 1033
                that._initResponseObject(newData);                                          // 1034
                that._initProgressObject(newData);                                          // 1035
                that._addConvenienceMethods(e, newData);                                    // 1036
                result = that._trigger(                                                     // 1037
                    'add',                                                                  // 1038
                    $.Event('add', {delegatedEvent: e}),                                    // 1039
                    newData                                                                 // 1040
                );                                                                          // 1041
                return result;                                                              // 1042
            });                                                                             // 1043
            return result;                                                                  // 1044
        },                                                                                  // 1045
                                                                                            // 1046
        _replaceFileInput: function (data) {                                                // 1047
            var input = data.fileInput,                                                     // 1048
                inputClone = input.clone(true),                                             // 1049
                restoreFocus = input.is(document.activeElement);                            // 1050
            // Add a reference for the new cloned file input to the data argument:          // 1051
            data.fileInputClone = inputClone;                                               // 1052
            $('<form></form>').append(inputClone)[0].reset();                               // 1053
            // Detaching allows to insert the fileInput on another form                     // 1054
            // without loosing the file input value:                                        // 1055
            input.after(inputClone).detach();                                               // 1056
            // If the fileInput had focus before it was detached,                           // 1057
            // restore focus to the inputClone.                                             // 1058
            if (restoreFocus) {                                                             // 1059
                inputClone.focus();                                                         // 1060
            }                                                                               // 1061
            // Avoid memory leaks with the detached file input:                             // 1062
            $.cleanData(input.unbind('remove'));                                            // 1063
            // Replace the original file input element in the fileInput                     // 1064
            // elements set with the clone, which has been copied including                 // 1065
            // event handlers:                                                              // 1066
            this.options.fileInput = this.options.fileInput.map(function (i, el) {          // 1067
                if (el === input[0]) {                                                      // 1068
                    return inputClone[0];                                                   // 1069
                }                                                                           // 1070
                return el;                                                                  // 1071
            });                                                                             // 1072
            // If the widget has been initialized on the file input itself,                 // 1073
            // override this.element with the file input clone:                             // 1074
            if (input[0] === this.element[0]) {                                             // 1075
                this.element = inputClone;                                                  // 1076
            }                                                                               // 1077
        },                                                                                  // 1078
                                                                                            // 1079
        _handleFileTreeEntry: function (entry, path) {                                      // 1080
            var that = this,                                                                // 1081
                dfd = $.Deferred(),                                                         // 1082
                errorHandler = function (e) {                                               // 1083
                    if (e && !e.entry) {                                                    // 1084
                        e.entry = entry;                                                    // 1085
                    }                                                                       // 1086
                    // Since $.when returns immediately if one                              // 1087
                    // Deferred is rejected, we use resolve instead.                        // 1088
                    // This allows valid files and invalid items                            // 1089
                    // to be returned together in one set:                                  // 1090
                    dfd.resolve([e]);                                                       // 1091
                },                                                                          // 1092
                successHandler = function (entries) {                                       // 1093
                    that._handleFileTreeEntries(                                            // 1094
                        entries,                                                            // 1095
                        path + entry.name + '/'                                             // 1096
                    ).done(function (files) {                                               // 1097
                        dfd.resolve(files);                                                 // 1098
                    }).fail(errorHandler);                                                  // 1099
                },                                                                          // 1100
                readEntries = function () {                                                 // 1101
                    dirReader.readEntries(function (results) {                              // 1102
                        if (!results.length) {                                              // 1103
                            successHandler(entries);                                        // 1104
                        } else {                                                            // 1105
                            entries = entries.concat(results);                              // 1106
                            readEntries();                                                  // 1107
                        }                                                                   // 1108
                    }, errorHandler);                                                       // 1109
                },                                                                          // 1110
                dirReader, entries = [];                                                    // 1111
            path = path || '';                                                              // 1112
            if (entry.isFile) {                                                             // 1113
                if (entry._file) {                                                          // 1114
                    // Workaround for Chrome bug #149735                                    // 1115
                    entry._file.relativePath = path;                                        // 1116
                    dfd.resolve(entry._file);                                               // 1117
                } else {                                                                    // 1118
                    entry.file(function (file) {                                            // 1119
                        file.relativePath = path;                                           // 1120
                        dfd.resolve(file);                                                  // 1121
                    }, errorHandler);                                                       // 1122
                }                                                                           // 1123
            } else if (entry.isDirectory) {                                                 // 1124
                dirReader = entry.createReader();                                           // 1125
                readEntries();                                                              // 1126
            } else {                                                                        // 1127
                // Return an empy list for file system items                                // 1128
                // other than files or directories:                                         // 1129
                dfd.resolve([]);                                                            // 1130
            }                                                                               // 1131
            return dfd.promise();                                                           // 1132
        },                                                                                  // 1133
                                                                                            // 1134
        _handleFileTreeEntries: function (entries, path) {                                  // 1135
            var that = this;                                                                // 1136
            return $.when.apply(                                                            // 1137
                $,                                                                          // 1138
                $.map(entries, function (entry) {                                           // 1139
                    return that._handleFileTreeEntry(entry, path);                          // 1140
                })                                                                          // 1141
            ).pipe(function () {                                                            // 1142
                return Array.prototype.concat.apply(                                        // 1143
                    [],                                                                     // 1144
                    arguments                                                               // 1145
                );                                                                          // 1146
            });                                                                             // 1147
        },                                                                                  // 1148
                                                                                            // 1149
        _getDroppedFiles: function (dataTransfer) {                                         // 1150
            dataTransfer = dataTransfer || {};                                              // 1151
            var items = dataTransfer.items;                                                 // 1152
            if (items && items.length && (items[0].webkitGetAsEntry ||                      // 1153
                    items[0].getAsEntry)) {                                                 // 1154
                return this._handleFileTreeEntries(                                         // 1155
                    $.map(items, function (item) {                                          // 1156
                        var entry;                                                          // 1157
                        if (item.webkitGetAsEntry) {                                        // 1158
                            entry = item.webkitGetAsEntry();                                // 1159
                            if (entry) {                                                    // 1160
                                // Workaround for Chrome bug #149735:                       // 1161
                                entry._file = item.getAsFile();                             // 1162
                            }                                                               // 1163
                            return entry;                                                   // 1164
                        }                                                                   // 1165
                        return item.getAsEntry();                                           // 1166
                    })                                                                      // 1167
                );                                                                          // 1168
            }                                                                               // 1169
            return $.Deferred().resolve(                                                    // 1170
                $.makeArray(dataTransfer.files)                                             // 1171
            ).promise();                                                                    // 1172
        },                                                                                  // 1173
                                                                                            // 1174
        _getSingleFileInputFiles: function (fileInput) {                                    // 1175
            fileInput = $(fileInput);                                                       // 1176
            var entries = fileInput.prop('webkitEntries') ||                                // 1177
                    fileInput.prop('entries'),                                              // 1178
                files,                                                                      // 1179
                value;                                                                      // 1180
            if (entries && entries.length) {                                                // 1181
                return this._handleFileTreeEntries(entries);                                // 1182
            }                                                                               // 1183
            files = $.makeArray(fileInput.prop('files'));                                   // 1184
            if (!files.length) {                                                            // 1185
                value = fileInput.prop('value');                                            // 1186
                if (!value) {                                                               // 1187
                    return $.Deferred().resolve([]).promise();                              // 1188
                }                                                                           // 1189
                // If the files property is not available, the browser does not             // 1190
                // support the File API and we add a pseudo File object with                // 1191
                // the input value as name with path information removed:                   // 1192
                files = [{name: value.replace(/^.*\\/, '')}];                               // 1193
            } else if (files[0].name === undefined && files[0].fileName) {                  // 1194
                // File normalization for Safari 4 and Firefox 3:                           // 1195
                $.each(files, function (index, file) {                                      // 1196
                    file.name = file.fileName;                                              // 1197
                    file.size = file.fileSize;                                              // 1198
                });                                                                         // 1199
            }                                                                               // 1200
            return $.Deferred().resolve(files).promise();                                   // 1201
        },                                                                                  // 1202
                                                                                            // 1203
        _getFileInputFiles: function (fileInput) {                                          // 1204
            if (!(fileInput instanceof $) || fileInput.length === 1) {                      // 1205
                return this._getSingleFileInputFiles(fileInput);                            // 1206
            }                                                                               // 1207
            return $.when.apply(                                                            // 1208
                $,                                                                          // 1209
                $.map(fileInput, this._getSingleFileInputFiles)                             // 1210
            ).pipe(function () {                                                            // 1211
                return Array.prototype.concat.apply(                                        // 1212
                    [],                                                                     // 1213
                    arguments                                                               // 1214
                );                                                                          // 1215
            });                                                                             // 1216
        },                                                                                  // 1217
                                                                                            // 1218
        _onChange: function (e) {                                                           // 1219
            var that = this,                                                                // 1220
                data = {                                                                    // 1221
                    fileInput: $(e.target),                                                 // 1222
                    form: $(e.target.form)                                                  // 1223
                };                                                                          // 1224
            this._getFileInputFiles(data.fileInput).always(function (files) {               // 1225
                data.files = files;                                                         // 1226
                if (that.options.replaceFileInput) {                                        // 1227
                    that._replaceFileInput(data);                                           // 1228
                }                                                                           // 1229
                if (that._trigger(                                                          // 1230
                        'change',                                                           // 1231
                        $.Event('change', {delegatedEvent: e}),                             // 1232
                        data                                                                // 1233
                    ) !== false) {                                                          // 1234
                    that._onAdd(e, data);                                                   // 1235
                }                                                                           // 1236
            });                                                                             // 1237
        },                                                                                  // 1238
                                                                                            // 1239
        _onPaste: function (e) {                                                            // 1240
            var items = e.originalEvent && e.originalEvent.clipboardData &&                 // 1241
                    e.originalEvent.clipboardData.items,                                    // 1242
                data = {files: []};                                                         // 1243
            if (items && items.length) {                                                    // 1244
                $.each(items, function (index, item) {                                      // 1245
                    var file = item.getAsFile && item.getAsFile();                          // 1246
                    if (file) {                                                             // 1247
                        data.files.push(file);                                              // 1248
                    }                                                                       // 1249
                });                                                                         // 1250
                if (this._trigger(                                                          // 1251
                        'paste',                                                            // 1252
                        $.Event('paste', {delegatedEvent: e}),                              // 1253
                        data                                                                // 1254
                    ) !== false) {                                                          // 1255
                    this._onAdd(e, data);                                                   // 1256
                }                                                                           // 1257
            }                                                                               // 1258
        },                                                                                  // 1259
                                                                                            // 1260
        _onDrop: function (e) {                                                             // 1261
            e.dataTransfer = e.originalEvent && e.originalEvent.dataTransfer;               // 1262
            var that = this,                                                                // 1263
                dataTransfer = e.dataTransfer,                                              // 1264
                data = {};                                                                  // 1265
            if (dataTransfer && dataTransfer.files && dataTransfer.files.length) {          // 1266
                e.preventDefault();                                                         // 1267
                this._getDroppedFiles(dataTransfer).always(function (files) {               // 1268
                    data.files = files;                                                     // 1269
                    if (that._trigger(                                                      // 1270
                            'drop',                                                         // 1271
                            $.Event('drop', {delegatedEvent: e}),                           // 1272
                            data                                                            // 1273
                        ) !== false) {                                                      // 1274
                        that._onAdd(e, data);                                               // 1275
                    }                                                                       // 1276
                });                                                                         // 1277
            }                                                                               // 1278
        },                                                                                  // 1279
                                                                                            // 1280
        _onDragOver: getDragHandler('dragover'),                                            // 1281
                                                                                            // 1282
        _onDragEnter: getDragHandler('dragenter'),                                          // 1283
                                                                                            // 1284
        _onDragLeave: getDragHandler('dragleave'),                                          // 1285
                                                                                            // 1286
        _initEventHandlers: function () {                                                   // 1287
            if (this._isXHRUpload(this.options)) {                                          // 1288
                this._on(this.options.dropZone, {                                           // 1289
                    dragover: this._onDragOver,                                             // 1290
                    drop: this._onDrop,                                                     // 1291
                    // event.preventDefault() on dragenter is required for IE10+:           // 1292
                    dragenter: this._onDragEnter,                                           // 1293
                    // dragleave is not required, but added for completeness:               // 1294
                    dragleave: this._onDragLeave                                            // 1295
                });                                                                         // 1296
                this._on(this.options.pasteZone, {                                          // 1297
                    paste: this._onPaste                                                    // 1298
                });                                                                         // 1299
            }                                                                               // 1300
            if ($.support.fileInput) {                                                      // 1301
                this._on(this.options.fileInput, {                                          // 1302
                    change: this._onChange                                                  // 1303
                });                                                                         // 1304
            }                                                                               // 1305
        },                                                                                  // 1306
                                                                                            // 1307
        _destroyEventHandlers: function () {                                                // 1308
            this._off(this.options.dropZone, 'dragenter dragleave dragover drop');          // 1309
            this._off(this.options.pasteZone, 'paste');                                     // 1310
            this._off(this.options.fileInput, 'change');                                    // 1311
        },                                                                                  // 1312
                                                                                            // 1313
        _setOption: function (key, value) {                                                 // 1314
            var reinit = $.inArray(key, this._specialOptions) !== -1;                       // 1315
            if (reinit) {                                                                   // 1316
                this._destroyEventHandlers();                                               // 1317
            }                                                                               // 1318
            this._super(key, value);                                                        // 1319
            if (reinit) {                                                                   // 1320
                this._initSpecialOptions();                                                 // 1321
                this._initEventHandlers();                                                  // 1322
            }                                                                               // 1323
        },                                                                                  // 1324
                                                                                            // 1325
        _initSpecialOptions: function () {                                                  // 1326
            var options = this.options;                                                     // 1327
            if (options.fileInput === undefined) {                                          // 1328
                options.fileInput = this.element.is('input[type="file"]') ?                 // 1329
                        this.element : this.element.find('input[type="file"]');             // 1330
            } else if (!(options.fileInput instanceof $)) {                                 // 1331
                options.fileInput = $(options.fileInput);                                   // 1332
            }                                                                               // 1333
            if (!(options.dropZone instanceof $)) {                                         // 1334
                options.dropZone = $(options.dropZone);                                     // 1335
            }                                                                               // 1336
            if (!(options.pasteZone instanceof $)) {                                        // 1337
                options.pasteZone = $(options.pasteZone);                                   // 1338
            }                                                                               // 1339
        },                                                                                  // 1340
                                                                                            // 1341
        _getRegExp: function (str) {                                                        // 1342
            var parts = str.split('/'),                                                     // 1343
                modifiers = parts.pop();                                                    // 1344
            parts.shift();                                                                  // 1345
            return new RegExp(parts.join('/'), modifiers);                                  // 1346
        },                                                                                  // 1347
                                                                                            // 1348
        _isRegExpOption: function (key, value) {                                            // 1349
            return key !== 'url' && $.type(value) === 'string' &&                           // 1350
                /^\/.*\/[igm]{0,3}$/.test(value);                                           // 1351
        },                                                                                  // 1352
                                                                                            // 1353
        _initDataAttributes: function () {                                                  // 1354
            var that = this,                                                                // 1355
                options = this.options,                                                     // 1356
                data = this.element.data();                                                 // 1357
            // Initialize options set via HTML5 data-attributes:                            // 1358
            $.each(                                                                         // 1359
                this.element[0].attributes,                                                 // 1360
                function (index, attr) {                                                    // 1361
                    var key = attr.name.toLowerCase(),                                      // 1362
                        value;                                                              // 1363
                    if (/^data-/.test(key)) {                                               // 1364
                        // Convert hyphen-ated key to camelCase:                            // 1365
                        key = key.slice(5).replace(/-[a-z]/g, function (str) {              // 1366
                            return str.charAt(1).toUpperCase();                             // 1367
                        });                                                                 // 1368
                        value = data[key];                                                  // 1369
                        if (that._isRegExpOption(key, value)) {                             // 1370
                            value = that._getRegExp(value);                                 // 1371
                        }                                                                   // 1372
                        options[key] = value;                                               // 1373
                    }                                                                       // 1374
                }                                                                           // 1375
            );                                                                              // 1376
        },                                                                                  // 1377
                                                                                            // 1378
        _create: function () {                                                              // 1379
            this._initDataAttributes();                                                     // 1380
            this._initSpecialOptions();                                                     // 1381
            this._slots = [];                                                               // 1382
            this._sequence = this._getXHRPromise(true);                                     // 1383
            this._sending = this._active = 0;                                               // 1384
            this._initProgressObject(this);                                                 // 1385
            this._initEventHandlers();                                                      // 1386
        },                                                                                  // 1387
                                                                                            // 1388
        // This method is exposed to the widget API and allows to query                     // 1389
        // the number of active uploads:                                                    // 1390
        active: function () {                                                               // 1391
            return this._active;                                                            // 1392
        },                                                                                  // 1393
                                                                                            // 1394
        // This method is exposed to the widget API and allows to query                     // 1395
        // the widget upload progress.                                                      // 1396
        // It returns an object with loaded, total and bitrate properties                   // 1397
        // for the running uploads:                                                         // 1398
        progress: function () {                                                             // 1399
            return this._progress;                                                          // 1400
        },                                                                                  // 1401
                                                                                            // 1402
        // This method is exposed to the widget API and allows adding files                 // 1403
        // using the fileupload API. The data parameter accepts an object which             // 1404
        // must have a files property and can contain additional options:                   // 1405
        // .fileupload('add', {files: filesList});                                          // 1406
        add: function (data) {                                                              // 1407
            var that = this;                                                                // 1408
            if (!data || this.options.disabled) {                                           // 1409
                return;                                                                     // 1410
            }                                                                               // 1411
            if (data.fileInput && !data.files) {                                            // 1412
                this._getFileInputFiles(data.fileInput).always(function (files) {           // 1413
                    data.files = files;                                                     // 1414
                    that._onAdd(null, data);                                                // 1415
                });                                                                         // 1416
            } else {                                                                        // 1417
                data.files = $.makeArray(data.files);                                       // 1418
                this._onAdd(null, data);                                                    // 1419
            }                                                                               // 1420
        },                                                                                  // 1421
                                                                                            // 1422
        // This method is exposed to the widget API and allows sending files                // 1423
        // using the fileupload API. The data parameter accepts an object which             // 1424
        // must have a files or fileInput property and can contain additional options:      // 1425
        // .fileupload('send', {files: filesList});                                         // 1426
        // The method returns a Promise object for the file upload call.                    // 1427
        send: function (data) {                                                             // 1428
            if (data && !this.options.disabled) {                                           // 1429
                if (data.fileInput && !data.files) {                                        // 1430
                    var that = this,                                                        // 1431
                        dfd = $.Deferred(),                                                 // 1432
                        promise = dfd.promise(),                                            // 1433
                        jqXHR,                                                              // 1434
                        aborted;                                                            // 1435
                    promise.abort = function () {                                           // 1436
                        aborted = true;                                                     // 1437
                        if (jqXHR) {                                                        // 1438
                            return jqXHR.abort();                                           // 1439
                        }                                                                   // 1440
                        dfd.reject(null, 'abort', 'abort');                                 // 1441
                        return promise;                                                     // 1442
                    };                                                                      // 1443
                    this._getFileInputFiles(data.fileInput).always(                         // 1444
                        function (files) {                                                  // 1445
                            if (aborted) {                                                  // 1446
                                return;                                                     // 1447
                            }                                                               // 1448
                            if (!files.length) {                                            // 1449
                                dfd.reject();                                               // 1450
                                return;                                                     // 1451
                            }                                                               // 1452
                            data.files = files;                                             // 1453
                            jqXHR = that._onSend(null, data);                               // 1454
                            jqXHR.then(                                                     // 1455
                                function (result, textStatus, jqXHR) {                      // 1456
                                    dfd.resolve(result, textStatus, jqXHR);                 // 1457
                                },                                                          // 1458
                                function (jqXHR, textStatus, errorThrown) {                 // 1459
                                    dfd.reject(jqXHR, textStatus, errorThrown);             // 1460
                                }                                                           // 1461
                            );                                                              // 1462
                        }                                                                   // 1463
                    );                                                                      // 1464
                    return this._enhancePromise(promise);                                   // 1465
                }                                                                           // 1466
                data.files = $.makeArray(data.files);                                       // 1467
                if (data.files.length) {                                                    // 1468
                    return this._onSend(null, data);                                        // 1469
                }                                                                           // 1470
            }                                                                               // 1471
            return this._getXHRPromise(false, data && data.context);                        // 1472
        }                                                                                   // 1473
                                                                                            // 1474
    });                                                                                     // 1475
                                                                                            // 1476
}));                                                                                        // 1477
                                                                                            // 1478
//////////////////////////////////////////////////////////////////////////////////////////////      // 1487
                                                                                                    // 1488
}).call(this);                                                                                      // 1489
                                                                                                    // 1490
//////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['mpowaga:jquery-fileupload'] = {};

})();
