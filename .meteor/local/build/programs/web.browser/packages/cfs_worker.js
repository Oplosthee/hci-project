(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var FS = Package['cfs:base-package'].FS;
var DDP = Package['ddp-client'].DDP;
var PowerQueue = Package['cfs:power-queue'].PowerQueue;
var Mongo = Package.mongo.Mongo;

(function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/cfs_worker/packages/cfs_worker.js                        //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
                                                                     // 1
///////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['cfs:worker'] = {};

})();
