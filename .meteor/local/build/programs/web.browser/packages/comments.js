//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var Template = Package.templating.Template;
var AutoForm = Package['aldeed:autoform'].AutoForm;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var Spacebars = Package.spacebars.Spacebars;
var SimpleSchema = Package['aldeed:simple-schema'].SimpleSchema;
var MongoObject = Package['aldeed:simple-schema'].MongoObject;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var __coffeescriptShare;

(function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/comments/lib/both/collections.coffee.js                                                             //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var CommentsSchema;                                                                                             // 1
                                                                                                                //
this.Comments = new Meteor.Collection('comments');                                                              // 1
                                                                                                                //
CommentsSchema = new SimpleSchema({                                                                             // 1
  doc: {                                                                                                        // 4
    type: String,                                                                                               // 5
    regEx: SimpleSchema.RegEx.Id                                                                                // 5
  },                                                                                                            //
  owner: {                                                                                                      // 4
    type: String,                                                                                               // 9
    regEx: SimpleSchema.RegEx.Id,                                                                               // 9
    autoform: {                                                                                                 // 9
      options: function() {                                                                                     // 12
        return _.map(Meteor.users.find().fetch(), function(user) {                                              //
          return {                                                                                              //
            label: user.emails[0].address,                                                                      // 14
            value: user._id                                                                                     // 14
          };                                                                                                    //
        });                                                                                                     //
      }                                                                                                         //
    }                                                                                                           //
  },                                                                                                            //
  createdAt: {                                                                                                  // 4
    type: Date,                                                                                                 // 18
    autoValue: function() {                                                                                     // 18
      if (this.isInsert) {                                                                                      // 20
        return new Date();                                                                                      //
      }                                                                                                         //
    }                                                                                                           //
  },                                                                                                            //
  content: {                                                                                                    // 4
    type: String,                                                                                               // 24
    label: 'Comment'                                                                                            // 24
  }                                                                                                             //
});                                                                                                             //
                                                                                                                //
Comments.attachSchema(CommentsSchema);                                                                          // 1
                                                                                                                //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/comments/lib/client/template.templates.js                                                           //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
                                                                                                                // 1
Template.__checkName("commentForm");                                                                            // 2
Template["commentForm"] = new Template("Template.commentForm", (function() {                                    // 3
  var view = this;                                                                                              // 4
  return Blaze._TemplateWith(function() {                                                                       // 5
    return {                                                                                                    // 6
      collection: Spacebars.call("Comments"),                                                                   // 7
      id: Spacebars.call("commentForm"),                                                                        // 8
      type: Spacebars.call("insert"),                                                                           // 9
      commentDocId: Spacebars.call(view.lookup("_id"))                                                          // 10
    };                                                                                                          // 11
  }, function() {                                                                                               // 12
    return Spacebars.include(view.lookupTemplate("autoForm"), function() {                                      // 13
      return [ "\n      ", Blaze._TemplateWith(function() {                                                     // 14
        return {                                                                                                // 15
          name: Spacebars.call("content"),                                                                      // 16
          id: Spacebars.call("content"),                                                                        // 17
          rows: Spacebars.call(2)                                                                               // 18
        };                                                                                                      // 19
      }, function() {                                                                                           // 20
        return Spacebars.include(view.lookupTemplate("afQuickField"));                                          // 21
      }), "\n    ", HTML.BUTTON({                                                                               // 22
        type: "submit",                                                                                         // 23
        doc: function() {                                                                                       // 24
          return Spacebars.mustache(view.lookup("_id"));                                                        // 25
        },                                                                                                      // 26
        "class": "btn btn-primary btn-sm"                                                                       // 27
      }, "Comment"), "\n    ", Blaze.If(function() {                                                            // 28
        return Spacebars.call(view.lookup("cancelButton"));                                                     // 29
      }, function() {                                                                                           // 30
        return [ "\n    ", HTML.A({                                                                             // 31
          "class": "btn-cancel-comment"                                                                         // 32
        }, "Cancel"), "\n    " ];                                                                               // 33
      }), "\n  " ];                                                                                             // 34
    });                                                                                                         // 35
  });                                                                                                           // 36
}));                                                                                                            // 37
                                                                                                                // 38
Template.__checkName("commentFormToggle");                                                                      // 39
Template["commentFormToggle"] = new Template("Template.commentFormToggle", (function() {                        // 40
  var view = this;                                                                                              // 41
  return Blaze.If(function() {                                                                                  // 42
    return Spacebars.dataMustache(view.lookup("commentingOn"), view.lookup("_id"));                             // 43
  }, function() {                                                                                               // 44
    return [ "\n        ", HTML.HR(), "\n        ", Blaze._TemplateWith(function() {                            // 45
      return {                                                                                                  // 46
        _id: Spacebars.call(view.lookup("_id")),                                                                // 47
        cancelButton: Spacebars.call(true)                                                                      // 48
      };                                                                                                        // 49
    }, function() {                                                                                             // 50
      return Spacebars.include(view.lookupTemplate("commentForm"));                                             // 51
    }), "\n    " ];                                                                                             // 52
  }, function() {                                                                                               // 53
    return [ "\n        ", HTML.BUTTON({                                                                        // 54
      "class": "btn btn-primary btn-sm btn-comment",                                                            // 55
      doc: function() {                                                                                         // 56
        return Spacebars.mustache(view.lookup("_id"));                                                          // 57
      }                                                                                                         // 58
    }, "Comment"), "\n    " ];                                                                                  // 59
  });                                                                                                           // 60
}));                                                                                                            // 61
                                                                                                                // 62
Template.__checkName("comments");                                                                               // 63
Template["comments"] = new Template("Template.comments", (function() {                                          // 64
  var view = this;                                                                                              // 65
  return HTML.DIV({                                                                                             // 66
    "class": "comments"                                                                                         // 67
  }, "\n        ", Blaze.If(function() {                                                                        // 68
    return Spacebars.dataMustache(view.lookup("CommentsByDoc"), view.lookup("_id"));                            // 69
  }, function() {                                                                                               // 70
    return [ "\n            ", HTML.HR(), "\n            ", Blaze.Each(function() {                             // 71
      return Spacebars.dataMustache(view.lookup("CommentsByDoc"), view.lookup("_id"));                          // 72
    }, function() {                                                                                             // 73
      return [ "\n                    ", Spacebars.include(view.lookupTemplate("comment")), "\n            " ];
    }), "\n        " ];                                                                                         // 75
  }, function() {                                                                                               // 76
    return [ "\n            ", Spacebars.include(view.lookupTemplate("noComments")), "\n        " ];            // 77
  }), "\n    ");                                                                                                // 78
}));                                                                                                            // 79
                                                                                                                // 80
Template.__checkName("comment");                                                                                // 81
Template["comment"] = new Template("Template.comment", (function() {                                            // 82
  var view = this;                                                                                              // 83
  return HTML.DIV({                                                                                             // 84
    "class": "media comment"                                                                                    // 85
  }, "\n		", HTML.A({                                                                                           // 86
    "class": "pull-left",                                                                                       // 87
    href: "#"                                                                                                   // 88
  }, "\n			", Blaze._TemplateWith(function() {                                                                  // 89
    return {                                                                                                    // 90
      _id: Spacebars.call(view.lookup("owner"))                                                                 // 91
    };                                                                                                          // 92
  }, function() {                                                                                               // 93
    return Spacebars.include(view.lookupTemplate("profileThumb"));                                              // 94
  }), "\n		"), "\n		", HTML.DIV({                                                                               // 95
    "class": "media-body"                                                                                       // 96
  }, "\n            ", HTML.P(Blaze.View("lookup:content", function() {                                         // 97
    return Spacebars.mustache(view.lookup("content"));                                                          // 98
  })), "\n			", HTML.SMALL({                                                                                    // 99
    "class": "media-heading"                                                                                    // 100
  }, Blaze.View("lookup:niceName", function() {                                                                 // 101
    return Spacebars.mustache(view.lookup("niceName"), view.lookup("owner"));                                   // 102
  })), "\n		"), "\n	");                                                                                         // 103
}));                                                                                                            // 104
                                                                                                                // 105
Template.__checkName("hasComments");                                                                            // 106
Template["hasComments"] = new Template("Template.hasComments", (function() {                                    // 107
  var view = this;                                                                                              // 108
  return Blaze.Each(function() {                                                                                // 109
    return Spacebars.call(view.lookup("comments"));                                                             // 110
  }, function() {                                                                                               // 111
    return [ "\n        ", HTML.DIV({                                                                           // 112
      style: "padding:20px"                                                                                     // 113
    }, "\n	        ", HTML.DIV({                                                                                // 114
      "class": "tip left"                                                                                       // 115
    }, "\n                ", Blaze.View("lookup:content", function() {                                          // 116
      return Spacebars.mustache(view.lookup("content"));                                                        // 117
    }), "\n            "), "\n        "), "\n    " ];                                                           // 118
  });                                                                                                           // 119
}));                                                                                                            // 120
                                                                                                                // 121
Template.__checkName("noComments");                                                                             // 122
Template["noComments"] = new Template("Template.noComments", (function() {                                      // 123
  var view = this;                                                                                              // 124
  return "";                                                                                                    // 125
}));                                                                                                            // 126
                                                                                                                // 127
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/comments/lib/client/templates.coffee.js                                                             //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.commentFormToggle.events({                                                                             // 1
  'click .btn-comment': function(e, t) {                                                                        // 2
    return Session.set('commentingOn', $(e.currentTarget).attr('doc'));                                         //
  },                                                                                                            //
  'click .btn-cancel-comment': function(e, t) {                                                                 // 2
    return Session.set('commentingOn', null);                                                                   //
  }                                                                                                             //
});                                                                                                             //
                                                                                                                //
Template.commentFormToggle.destroyed = function() {                                                             // 1
  return Session.set('commentingOn', null);                                                                     //
};                                                                                                              // 7
                                                                                                                //
AutoForm.hooks({                                                                                                // 1
  commentForm: {                                                                                                // 10
    onSuccess: function(operation, result, template) {                                                          // 12
      return Session.set('commentingOn', null);                                                                 //
    }                                                                                                           //
  }                                                                                                             //
});                                                                                                             //
                                                                                                                //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/comments/lib/client/helpers.coffee.js                                                               //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.registerHelper('CommentsByDoc', function(_id) {                                                        // 1
  if (typeof window['Comments'] !== 'undefined') {                                                              // 2
    return Comments.find({                                                                                      //
      doc: _id                                                                                                  // 3
    }, {                                                                                                        //
      sort: {                                                                                                   // 3
        createdAt: -1                                                                                           // 3
      }                                                                                                         //
    }).fetch();                                                                                                 //
  }                                                                                                             //
});                                                                                                             // 1
                                                                                                                //
Template.registerHelper('CommentsByUser', function(_id) {                                                       // 1
  if (typeof window['Comments'] !== 'undefined') {                                                              // 6
    return Comments.find({                                                                                      //
      owner: _id                                                                                                // 7
    }, {                                                                                                        //
      sort: {                                                                                                   // 7
        createdAt: -1                                                                                           // 7
      }                                                                                                         //
    }).fetch();                                                                                                 //
  }                                                                                                             //
});                                                                                                             // 5
                                                                                                                //
Template.registerHelper('CommentsByCollection', function(collection) {                                          // 1
  var Comments, comments;                                                                                       // 10
  if (typeof window['Comments'] !== 'undefined') {                                                              // 10
    Comments = [];                                                                                              // 11
    comments = window['Comments'].find({                                                                        // 11
      owner: Meteor.userId()                                                                                    // 12
    }, {                                                                                                        //
      sort: {                                                                                                   // 12
        createdAt: -1                                                                                           // 12
      }                                                                                                         //
    }).fetch();                                                                                                 //
    collection = window[collection];                                                                            // 11
    _.each(comments, function(favorite) {                                                                       // 11
      if (collection.findOne({                                                                                  // 16
        _id: favorite.doc                                                                                       // 16
      })) {                                                                                                     //
        return Comments.push(collection.findOne({                                                               //
          _id: favorite.doc                                                                                     // 16
        }));                                                                                                    //
      }                                                                                                         //
    });                                                                                                         //
    return Comments;                                                                                            //
  }                                                                                                             //
});                                                                                                             // 9
                                                                                                                //
Template.registerHelper('commentCount', function(_id) {                                                         // 1
  if (typeof window['Comments'] !== 'undefined') {                                                              // 21
    return Comments.find({                                                                                      //
      doc: _id                                                                                                  // 22
    }).fetch().length;                                                                                          //
  }                                                                                                             //
});                                                                                                             // 20
                                                                                                                //
Template.registerHelper('commentingOn', function(_id) {                                                         // 1
  return Session.equals('commentingOn', _id);                                                                   //
});                                                                                                             // 24
                                                                                                                //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/comments/lib/client/autoform.coffee.js                                                              //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
AutoForm.hooks({                                                                                                // 1
  commentForm: {                                                                                                // 1
    onError: function(operation, error, template) {                                                             // 2
      return sAlert.error(error);                                                                               //
    },                                                                                                          //
    formToDoc: function(doc, ss, formId) {                                                                      // 2
      doc.doc = Template.instance().data.commentDocId;                                                          // 6
      doc.owner = Meteor.userId();                                                                              // 6
      return doc;                                                                                               //
    }                                                                                                           //
  }                                                                                                             //
});                                                                                                             //
                                                                                                                //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.comments = {};

})();
