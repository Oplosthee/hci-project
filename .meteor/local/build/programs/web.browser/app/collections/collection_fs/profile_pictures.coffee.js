(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// collections/collection_fs/profile_pictures.coffee.js                //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.ProfilePictures = new FS.Collection("profilePictures", {          // 1
  stores: [                                                            // 2
    new FS.Store.GridFS("images", {                                    //
      transformWrite: function(fileObj, readStream, writeStream) {     // 3
        if (gm.isAvailable) {                                          // 6
          return gm(readStream, fileObj.name()).autoOrient().stream().pipe(writeStream);
        } else {                                                       //
          return readStream.pipe(writeStream);                         //
        }                                                              //
      }                                                                //
    }), new FS.Store.GridFS("thumbs", {                                //
      transformWrite: function(fileObj, readStream, writeStream) {     // 11
        var size;                                                      // 14
        if (gm.isAvailable) {                                          // 14
          size = {                                                     // 15
            width: 100,                                                // 15
            height: 100                                                // 15
          };                                                           //
          return gm(readStream, fileObj.name()).autoOrient().resize(size.width + "^>", size.height + "^>").gravity("Center").extent(size.width, size.height).stream().pipe(writeStream);
        } else {                                                       //
          return readStream.pipe(writeStream);                         //
        }                                                              //
      }                                                                //
    })                                                                 //
  ],                                                                   //
  filter: {                                                            // 2
    allow: {                                                           // 21
      contentTypes: ['image/*']                                        // 22
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
