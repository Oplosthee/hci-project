(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// collections/collection_fs/attachments.coffee.js                     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.Attachments = new FS.Collection("Attachments", {                  // 1
  stores: [                                                            // 2
    new FS.Store.GridFS("attachments", {                               //
      transformWrite: function(fileObj, readStream, writeStream) {     // 3
        if (gm.isAvailable) {                                          // 6
          if (fileObj.original.type.substr(0, 5) === 'image') {        // 7
            return gm(readStream, fileObj.name()).autoOrient().stream().pipe(writeStream);
          } else {                                                     //
            return readStream.pipe(writeStream);                       //
          }                                                            //
        } else {                                                       //
          return readStream.pipe(writeStream);                         //
        }                                                              //
      }                                                                //
    })                                                                 //
  ]                                                                    //
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
