(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// collections/posts.coffee.js                                         //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.Posts = new Meteor.Collection('posts');                           // 1
                                                                       //
Schemas.Posts = new SimpleSchema({                                     // 1
  title: {                                                             // 4
    type: String,                                                      // 5
    max: 60                                                            // 5
  },                                                                   //
  content: {                                                           // 4
    type: String,                                                      // 9
    autoform: {                                                        // 9
      rows: 5                                                          // 11
    }                                                                  //
  },                                                                   //
  createdAt: {                                                         // 4
    type: Date,                                                        // 14
    autoValue: function() {                                            // 14
      if (this.isInsert) {                                             // 16
        return new Date();                                             //
      }                                                                //
    }                                                                  //
  },                                                                   //
  updatedAt: {                                                         // 4
    type: Date,                                                        // 20
    optional: true,                                                    // 20
    autoValue: function() {                                            // 20
      if (this.isUpdate) {                                             // 23
        return new Date();                                             //
      }                                                                //
    }                                                                  //
  },                                                                   //
  picture: {                                                           // 4
    type: String,                                                      // 27
    autoform: {                                                        // 27
      afFieldInput: {                                                  // 29
        type: 'fileUpload',                                            // 30
        collection: 'Attachments'                                      // 30
      }                                                                //
    }                                                                  //
  },                                                                   //
  owner: {                                                             // 4
    type: String,                                                      // 34
    regEx: SimpleSchema.RegEx.Id,                                      // 34
    autoValue: function() {                                            // 34
      if (this.isInsert) {                                             // 37
        return Meteor.userId();                                        //
      }                                                                //
    },                                                                 //
    autoform: {                                                        // 34
      options: function() {                                            // 40
        return _.map(Meteor.users.find().fetch(), function(user) {     //
          return {                                                     //
            label: user.emails[0].address,                             // 42
            value: user._id                                            // 42
          };                                                           //
        });                                                            //
      }                                                                //
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
Posts.attachSchema(Schemas.Posts);                                     // 1
                                                                       //
Posts.helpers({                                                        // 1
  author: function() {                                                 // 48
    var ref, ref1, ref2, user;                                         // 49
    user = Meteor.users.findOne(this.owner);                           // 49
    if (((user != null ? (ref = user.profile) != null ? ref.firstName : void 0 : void 0) != null) && (user != null ? (ref1 = user.profile) != null ? ref1.lastName : void 0 : void 0)) {
      return user.profile.firstName + ' ' + user.profile.lastName;     //
    } else {                                                           //
      return user != null ? (ref2 = user.emails) != null ? ref2[0].address : void 0 : void 0;
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
