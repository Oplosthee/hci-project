(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// collections/questions.coffee.js                                     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.Questions = new Meteor.Collection('questions');                   // 1
                                                                       //
Schemas.Questions = new SimpleSchema({                                 // 1
  question: {                                                          // 4
    type: String,                                                      // 5
    max: 60                                                            // 5
  },                                                                   //
  answer: {                                                            // 4
    type: String,                                                      // 9
    max: 60                                                            // 9
  },                                                                   //
  createdAt: {                                                         // 4
    type: Date,                                                        // 13
    autoValue: function() {                                            // 13
      if (this.isInsert) {                                             // 15
        return new Date();                                             //
      }                                                                //
    }                                                                  //
  },                                                                   //
  updatedAt: {                                                         // 4
    type: Date,                                                        // 19
    optional: true,                                                    // 19
    autoValue: function() {                                            // 19
      if (this.isUpdate) {                                             // 22
        return new Date();                                             //
      }                                                                //
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
Questions.attachSchema(Schemas.Questions);                             // 1
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
