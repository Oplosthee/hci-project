(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// collections/users.coffee.js                                         //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Schemas.UserProfile = new SimpleSchema({                               // 1
  picture: {                                                           // 3
    type: String,                                                      // 4
    optional: true,                                                    // 4
    label: 'Profile picture',                                          // 4
    autoform: {                                                        // 4
      afFieldInput: {                                                  // 8
        type: 'fileUpload',                                            // 9
        collection: 'ProfilePictures'                                  // 9
      }                                                                //
    }                                                                  //
  },                                                                   //
  firstName: {                                                         // 3
    type: String,                                                      // 13
    optional: true                                                     // 13
  },                                                                   //
  lastName: {                                                          // 3
    type: String,                                                      // 17
    optional: true                                                     // 17
  },                                                                   //
  birthday: {                                                          // 3
    type: Date,                                                        // 21
    optional: true                                                     // 21
  },                                                                   //
  bio: {                                                               // 3
    type: String,                                                      // 25
    optional: true,                                                    // 25
    autoform: {                                                        // 25
      rows: 4                                                          // 28
    }                                                                  //
  },                                                                   //
  location: {                                                          // 3
    type: String,                                                      // 31
    optional: true,                                                    // 31
    autoform: {                                                        // 31
      type: 'map',                                                     // 34
      geolocation: true,                                               // 34
      searchBox: true,                                                 // 34
      autolocate: true                                                 // 34
    }                                                                  //
  },                                                                   //
  country: {                                                           // 3
    type: String,                                                      // 40
    label: 'Nationality',                                              // 40
    allowedValues: Utils.countryList,                                  // 40
    optional: true                                                     // 40
  }                                                                    //
});                                                                    //
                                                                       //
Schemas.User = new SimpleSchema({                                      // 1
  username: {                                                          // 48
    type: String,                                                      // 49
    regEx: /^[a-z0-9A-Z_]{3,15}$/,                                     // 49
    optional: true                                                     // 49
  },                                                                   //
  emails: {                                                            // 48
    type: [Object],                                                    // 55
    optional: true                                                     // 55
  },                                                                   //
  "emails.$.address": {                                                // 48
    type: String,                                                      // 59
    regEx: SimpleSchema.RegEx.Email                                    // 59
  },                                                                   //
  "emails.$.verified": {                                               // 48
    type: Boolean                                                      // 63
  },                                                                   //
  createdAt: {                                                         // 48
    type: Date                                                         // 66
  },                                                                   //
  profile: {                                                           // 48
    type: Schemas.UserProfile,                                         // 69
    optional: true                                                     // 69
  },                                                                   //
  services: {                                                          // 48
    type: Object,                                                      // 73
    optional: true,                                                    // 73
    blackbox: true                                                     // 73
  },                                                                   //
  roles: {                                                             // 48
    type: [String],                                                    // 78
    blackbox: true,                                                    // 78
    optional: true                                                     // 78
  }                                                                    //
});                                                                    //
                                                                       //
Meteor.users.attachSchema(Schemas.User);                               // 1
                                                                       //
this.StarterSchemas = Schemas;                                         // 1
                                                                       //
Meteor.users.helpers({                                                 // 1
  hasRole: function(role) {                                            // 89
    var ref;                                                           // 90
    return ((ref = this.roles) != null ? ref.indexOf(role) : void 0) > -1;
  }                                                                    //
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
