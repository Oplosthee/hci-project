(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/sAlert.coffee.js                                        //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.startup(function() {                                            // 1
  if (Meteor.isClient) {                                               // 2
    return sAlert.config({                                             //
      effect: "stackslide",                                            // 4
      position: "bottom-right",                                        // 4
      timeout: 3000,                                                   // 4
      html: false                                                      // 4
    });                                                                //
  }                                                                    //
});                                                                    // 1
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
