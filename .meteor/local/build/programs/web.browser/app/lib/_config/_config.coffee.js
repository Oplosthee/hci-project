(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/_config.coffee.js                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.Config = {                                                        // 4
  name: 'Ludus',                                                       // 7
  title: function() {                                                  // 7
    return TAPi18n.__('configTitle');                                  //
  },                                                                   //
  subtitle: function() {                                               // 7
    return TAPi18n.__('configSubtitle');                               //
  },                                                                   //
  logo: function() {                                                   // 7
    return '<b>' + this.name + '</b>';                                 //
  },                                                                   //
  footer: function() {                                                 // 7
    return 'Ludus Prototype - 2017';                                   //
  },                                                                   //
  emails: {                                                            // 7
    from: 'no-reply@' + Meteor.absoluteUrl(),                          // 19
    contact: 'hello' + Meteor.absoluteUrl()                            // 19
  },                                                                   //
  username: false,                                                     // 7
  defaultLanguage: 'nl',                                               // 7
  dateFormat: 'D/M/YYYY',                                              // 7
  privacyUrl: 'http://meteorfactory.io',                               // 7
  termsUrl: 'http://meteorfactory.io',                                 // 7
  legal: {                                                             // 7
    address: 'Jessnerstrasse 18, 12047 Berlin',                        // 35
    name: 'Meteor Factory',                                            // 35
    url: 'http://benjaminpeterjones.com'                               // 35
  },                                                                   //
  about: 'http://meteorfactory.io',                                    // 7
  blog: 'http://learn.meteorfactory.io',                               // 7
  socialMedia: {                                                       // 7
    facebook: {                                                        // 43
      url: 'http://facebook.com/benjaminpeterjones',                   // 44
      icon: 'facebook'                                                 // 44
    },                                                                 //
    twitter: {                                                         // 43
      url: 'http://twitter.com/BenPeterJones',                         // 47
      icon: 'twitter'                                                  // 47
    },                                                                 //
    github: {                                                          // 43
      url: 'http://github.com/yogiben',                                // 50
      icon: 'github'                                                   // 50
    },                                                                 //
    info: {                                                            // 43
      url: 'http://meteorfactory.io',                                  // 53
      icon: 'link'                                                     // 53
    }                                                                  //
  },                                                                   //
  homeRoute: '/',                                                      // 7
  publicRoutes: ['home'],                                              // 7
  dashboardRoute: '/'                                                  // 7
};                                                                     //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
