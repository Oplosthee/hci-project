(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/router.coffee.js                                        //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
var onAfterAction, publicRoutes;                                       // 1
                                                                       //
this.subs = new SubsManager();                                         // 1
                                                                       //
Router.configure({                                                     // 1
  layoutTemplate: "masterLayout",                                      // 4
  loadingTemplate: "loading",                                          // 4
  notFoundTemplate: "notFound",                                        // 4
  routeControllerNameConverter: "camelCase",                           // 4
  onBeforeAction: function() {                                         // 4
    if (Config.username && Meteor.userId() && !Meteor.user().username) {
      this.redirect('/setUserName');                                   // 12
    }                                                                  //
    return this.next();                                                //
  }                                                                    //
});                                                                    //
                                                                       //
Router.waitOn(function() {                                             // 1
  return subs.subscribe('user');                                       //
});                                                                    // 15
                                                                       //
onAfterAction = function() {                                           // 1
  var $bd;                                                             // 19
  if (Meteor.isClient) {                                               // 19
    window.scrollTo(0, 0);                                             // 20
    $bd = $('.modal-backdrop');                                        // 20
    $bd.removeClass('in');                                             // 20
    return setTimeout(function() {                                     //
      return $bd.remove();                                             //
    }, 300);                                                           //
  }                                                                    //
};                                                                     // 18
                                                                       //
Router.onAfterAction(onAfterAction);                                   // 1
                                                                       //
publicRoutes = _.union(Config.publicRoutes || [], ['home', 'atSignIn', 'atSignUp', 'atForgotPassword', 'atSignOut']);
                                                                       //
Router.plugin('ensureSignedIn', {                                      // 1
  except: publicRoutes                                                 // 40
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
