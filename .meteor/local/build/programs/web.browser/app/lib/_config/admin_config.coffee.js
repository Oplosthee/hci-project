(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/admin_config.coffee.js                                  //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
this.AdminConfig = {                                                   // 1
  name: Config.name,                                                   // 2
  collections: {                                                       // 2
    Groups: {                                                          // 5
      color: 'green',                                                  // 6
      icon: 'users',                                                   // 6
      tableColumns: [                                                  // 6
        {                                                              //
          label: 'Naam',                                               // 9
          name: 'name'                                                 // 9
        }                                                              //
      ]                                                                //
    },                                                                 //
    Questions: {                                                       // 5
      color: 'red',                                                    // 12
      icon: 'pencil',                                                  // 12
      tableColumns: [                                                  // 12
        {                                                              //
          label: 'Vraag',                                              // 15
          name: 'question'                                             // 15
        }, {                                                           //
          label: 'Antwoord',                                           // 16
          name: 'answer'                                               // 16
        }                                                              //
      ]                                                                //
    }                                                                  //
                                                                       // 18
    /*                                                                 // 18
    		Posts:                                                           //
    			color: 'red'                                                    //
    			icon: 'pencil'                                                  //
    			extraFields: ['owner']                                          //
    			tableColumns: [                                                 //
    				{ label: 'Title', name: 'title' }                              //
    				{ label: 'User', name: 'author()', template: 'adminUserCell' }
    			]                                                               //
    		Comments:                                                        //
    			color: 'green'                                                  //
    			icon: 'comments'                                                //
    			extraFields: ['doc', 'owner']                                   //
    			tableColumns: [                                                 //
    				{ label: 'Content', name: 'content' }                          //
    				{ label: 'Post', name: 'docTitle()', template: 'adminPostCell' }
    				{ label: 'User', name: 'author()', template: 'adminUserCell' }
    			]                                                               //
    			children: [                                                     //
    				{                                                              //
    					find: (comment) ->                                            //
    						Posts.find comment.doc, limit: 1                             //
    				}                                                              //
    				{                                                              //
    					find: (comment) ->                                            //
    						Meteor.users.find comment.owner, limit: 1                    //
    				}                                                              //
    			]                                                               //
     */                                                                //
  },                                                                   //
  dashboard: {                                                         // 2
    homeUrl: '/'                                                       // 48
  },                                                                   //
  autoForm: {                                                          // 2
    omitFields: ['createdAt', 'updatedAt']                             // 51
  }                                                                    //
};                                                                     //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
