(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/seo.coffee.js                                           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Meteor.startup(function() {                                            // 1
  if (Meteor.isClient) {                                               // 2
    return SEO.config({                                                //
      title: Config.name,                                              // 4
      meta: {                                                          // 4
        title: Config.name,                                            // 6
        description: Config.subtitle                                   // 6
      }                                                                //
    });                                                                //
  }                                                                    //
});                                                                    // 1
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
