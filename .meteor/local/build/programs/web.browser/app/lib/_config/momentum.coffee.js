(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/_config/momentum.coffee.js                                      //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
if (Meteor.isClient) {                                                 // 1
  Template.registerHelper('momentumIRTransition', function() {         // 2
    return function(from, to, element) {                               //
      return "fade";                                                   //
    };                                                                 //
  });                                                                  //
}                                                                      //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
