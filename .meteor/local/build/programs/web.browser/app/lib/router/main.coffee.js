(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// lib/router/main.coffee.js                                           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Router.map(function() {                                                // 1
  return this.route("dashboard", {                                     //
    path: "/",                                                         // 3
    waitOn: function() {                                               // 3
      return [subs.subscribe('posts'), subs.subscribe('questions'), subs.subscribe('comments'), subs.subscribe('attachments')];
    },                                                                 //
    data: function() {                                                 // 3
      return {                                                         //
        posts: Posts.find({}, {                                        // 12
          sort: {                                                      // 12
            createdAt: -1                                              // 12
          }                                                            //
        }).fetch(),                                                    //
        questions: Questions.find({}, {                                // 12
          sort: {                                                      // 13
            createdAt: -1                                              // 13
          }                                                            //
        }).fetch()                                                     //
      };                                                               //
    }                                                                  //
  });                                                                  //
});                                                                    // 1
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
