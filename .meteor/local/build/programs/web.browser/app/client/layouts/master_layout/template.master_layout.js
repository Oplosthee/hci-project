(function(){
Template.__checkName("masterLayout");
Template["masterLayout"] = new Template("Template.masterLayout", (function() {
  var view = this;
  return [ HTML.DIV({
    "class": function() {
      return [ "layout master-layout ", Spacebars.mustache(Spacebars.dot(view.lookup("$"), "Session", "get"), "sidebar") ];
    }
  }, "\n		", Spacebars.include(view.lookupTemplate("navbar")), "\n		", Blaze._TemplateWith(function() {
    return {
      plugin: Spacebars.call("iron-router"),
      options: Spacebars.call(view.lookup("momentumIRTransition"))
    };
  }, function() {
    return Spacebars.include(view.lookupTemplate("momentum"), function() {
      return [ "\n			", Spacebars.include(view.lookupTemplate("yield")), "\n		" ];
    });
  }), "\n	"), HTML.Raw("\n	\n	<!-- Prevent footer from popping up when route loading -->\n	"), Blaze.If(function() {
    return Spacebars.call(view.lookup("isRouteReady"));
  }, function() {
    return [ "\n	", Spacebars.include(view.lookupTemplate("footer")), "\n	" ];
  }), "\n\n	", Spacebars.include(view.lookupTemplate("autoformModals")) ];
}));

}).call(this);
