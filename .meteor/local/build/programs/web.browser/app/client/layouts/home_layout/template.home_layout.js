(function(){
Template.__checkName("homeLayout");
Template["homeLayout"] = new Template("Template.homeLayout", (function() {
  var view = this;
  return [ HTML.DIV({
    "class": "layout home-layout"
  }, "\n    ", HTML.DIV({
    "class": "layout home-layout"
  }, "\n      ", Spacebars.include(view.lookupTemplate("yield")), "\n    "), "\n  "), "\n  ", Spacebars.include(view.lookupTemplate("footer")) ];
}));

}).call(this);
