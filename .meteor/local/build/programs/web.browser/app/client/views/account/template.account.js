(function(){
Template.__checkName("account");
Template["account"] = new Template("Template.account", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-account"
  }, "\n		", HTML.DIV({
    "class": "container"
  }, "\n			", HTML.DIV({
    "class": "row"
  }, "\n				", HTML.DIV({
    "class": "col-md-6 col-md-offset-3"
  }, "\n					", HTML.DIV({
    "class": "panel panel-default"
  }, "\n						  ", HTML.DIV({
    "class": "panel-heading"
  }, "\n								", HTML.H3({
    "class": "panel-title"
  }, Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "settings");
  })), "\n						  "), "\n						  ", HTML.DIV({
    "class": "panel-body"
  }, "\n								", Spacebars.include(view.lookupTemplate("updatePassword")), "\n						  "), "\n					"), "\n					", HTML.Raw('<!--\n					<div class="panel panel-danger">\n						  <div class="panel-heading">\n								<h3 class="panel-title">{{_ "delete_account"}}</h3>\n						  </div>\n						  <div class="panel-body">\n								<p>{{_ "delete_account_warn"}}</p>\n								<p>\n									<a class="btn btn-danger" data-toggle="modal" href=\'#delete-account\'>{{_ "delete_account"}}</a>\n									<div class="modal fade" id="delete-account">\n										<div class="modal-dialog">\n											<div class="modal-content">\n												<div class="modal-header">\n													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n													<h4 class="modal-title">{{_ "delete_account"}}</h4>\n												</div>\n												<div class="modal-body">\n													{{_ "delete_account_warn2"}}\n												</div>\n												<div class="modal-footer">\n													<button type="button" class="btn btn-default" data-dismiss="modal">{{_ "close"}}</button>\n													<button type="button" class="btn btn-danger js-delete-account">{{_ "delete"}}</button>\n												</div>\n											</div>\n										</div>\n									</div>\n								</p>\n						  </div>\n					</div>\n					 -->'), "\n				"), "\n			"), "\n		"), "\n	");
}));

Template.__checkName("updatePassword");
Template["updatePassword"] = new Template("Template.updatePassword", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-update-password"
  }, "\n  	", Blaze._TemplateWith(function() {
    return {
      schema: Spacebars.call(Spacebars.dot(view.lookup("Schemas"), "updatePassword")),
      id: Spacebars.call("updatePassword")
    };
  }, function() {
    return Spacebars.include(view.lookupTemplate("autoForm"), function() {
      return [ "\n  	", HTML.FIELDSET("\n  	  ", HTML.LEGEND(Blaze.View("lookup:_", function() {
        return Spacebars.mustache(view.lookup("_"), "change_pswd");
      })), "\n  	  ", Blaze._TemplateWith(function() {
        return {
          name: Spacebars.call("old"),
          type: Spacebars.call("password")
        };
      }, function() {
        return Spacebars.include(view.lookupTemplate("afQuickField"));
      }), "\n  	  ", Blaze._TemplateWith(function() {
        return {
          name: Spacebars.call("new"),
          type: Spacebars.call("password")
        };
      }, function() {
        return Spacebars.include(view.lookupTemplate("afQuickField"));
      }), "\n  	  ", Blaze._TemplateWith(function() {
        return {
          name: Spacebars.call("confirm"),
          type: Spacebars.call("password")
        };
      }, function() {
        return Spacebars.include(view.lookupTemplate("afQuickField"));
      }), "\n  	  ", HTML.DIV("\n  	    ", HTML.BUTTON({
        type: "submit",
        "class": "btn btn-primary"
      }, Blaze.View("lookup:_", function() {
        return Spacebars.mustache(view.lookup("_"), "change_pswd");
      })), "\n  	  "), "\n  	"), "\n  	" ];
    });
  }), "\n  ");
}));

Template.__checkName("setUserName");
Template["setUserName"] = new Template("Template.setUserName", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-set-username"
  }, "\n		", HTML.DIV({
    "class": "container"
  }, "\n			", HTML.DIV({
    "class": "col-md-6 col-md-offset-3"
  }, "\n				", HTML.DIV({
    "class": "panel panel-default"
  }, "\n					", HTML.DIV({
    "class": "panel-heading"
  }, "\n						", HTML.H3({
    "class": "panel-title"
  }, Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "account_details");
  })), "\n					"), "\n					", HTML.DIV({
    "class": "panel-body"
  }, "\n						", Blaze._TemplateWith(function() {
    return {
      collection: Spacebars.call("Meteor.users"),
      doc: Spacebars.call(view.lookup("user")),
      type: Spacebars.call("update"),
      id: Spacebars.call("setUserName")
    };
  }, function() {
    return Spacebars.include(view.lookupTemplate("autoForm"), function() {
      return [ "\n						", HTML.FIELDSET("\n							", HTML.LEGEND("Choose your name"), "\n					    	", Blaze._TemplateWith(function() {
        return {
          name: Spacebars.call("username")
        };
      }, function() {
        return Spacebars.include(view.lookupTemplate("afQuickField"));
      }), "\n					    	", HTML.DIV("\n					      		", HTML.BUTTON({
        type: "submit",
        "class": "btn btn-primary"
      }, "Save"), "\n							"), "\n						"), "\n						" ];
    });
  }), "\n					"), "\n				"), "\n			"), "\n		"), "\n	");
}));

}).call(this);
