(function(){
Template.__checkName("dashboard");
Template["dashboard"] = new Template("Template.dashboard", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-dashboard"
  }, "\n		", HTML.DIV({
    "class": "container"
  }, "\n			", HTML.DIV({
    "class": "row"
  }, "\n				", Spacebars.include(view.lookupTemplate("questions")), "\n				", HTML.Raw('<!--<div class="col-md-3">\n					{{> addPost}}\n					{{> favoritesSidebar}}\n				</div>\n				<div class="col-md-9">\n					{{> posts}}\n				</div>-->'), "\n			"), "\n		"), "\n	");
}));

Template.__checkName("addPost");
Template["addPost"] = new Template("Template.addPost", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-add-post"
  }, "\n		", HTML.DIV({
    "class": "panel panel-default"
  }, "\n				", HTML.DIV({
    "class": "panel-heading"
  }, "\n					", HTML.H3({
    "class": "panel-title"
  }, Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "add_post");
  })), "\n				"), "\n				", HTML.DIV({
    "class": "panel-body"
  }, "\n					", Blaze._TemplateWith(function() {
    return {
      id: Spacebars.call("add"),
      collection: Spacebars.call("Posts"),
      type: Spacebars.call("insert")
    };
  }, function() {
    return Spacebars.include(view.lookupTemplate("autoForm"), function() {
      return [ "\n					", Blaze._TemplateWith(function() {
        return {
          name: Spacebars.call("title")
        };
      }, function() {
        return Spacebars.include(view.lookupTemplate("afQuickField"));
      }), "\n					", Blaze._TemplateWith(function() {
        return {
          name: Spacebars.call("content")
        };
      }, function() {
        return Spacebars.include(view.lookupTemplate("afQuickField"));
      }), "\n					", Blaze._TemplateWith(function() {
        return {
          name: Spacebars.call("picture")
        };
      }, function() {
        return Spacebars.include(view.lookupTemplate("afQuickField"));
      }), "\n					", HTML.BUTTON({
        type: "submit",
        "class": "btn btn-primary"
      }, Blaze.View("lookup:_", function() {
        return Spacebars.mustache(view.lookup("_"), "add_post_btn");
      })), "\n					" ];
    });
  }), "\n				"), "\n		"), "\n	");
}));

Template.__checkName("questions");
Template["questions"] = new Template("Template.questions", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-questions"
  }, "\n		", Blaze.Each(function() {
    return Spacebars.call(view.lookup("questions"));
  }, function() {
    return [ "\n		", Spacebars.include(view.lookupTemplate("question")), "\n		" ];
  }, function() {
    return [ "\n		", Spacebars.include(view.lookupTemplate("noData")), "\n		" ];
  }), "\n	");
}));

Template.__checkName("question");
Template["question"] = new Template("Template.question", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-question"
  }, "\n		", HTML.DIV({
    "class": "panel panel-default"
  }, "\n			", HTML.DIV({
    "class": "panel-heading"
  }, "\n				", HTML.H3({
    "class": "panel-title"
  }, "\n					", HTML.B(Blaze.View("lookup:question", function() {
    return Spacebars.mustache(view.lookup("question"));
  })), "\n				"), "\n			"), "\n			", HTML.DIV({
    "class": "panel-body"
  }, "\n				", HTML.FORM({
    "class": "question-answer"
  }, "\n					", HTML.DIV({
    "class": "form-group"
  }, "\n						", HTML.Raw('<input type="text" class="form-control" name="answer">'), "\n						", HTML.INPUT({
    type: "hidden",
    name: "solution",
    value: function() {
      return Spacebars.mustache(view.lookup("answer"));
    }
  }), "\n					"), "\n					", HTML.Raw('<button type="submit" class="btn btn-primary">Antwoord Controleren</button>'), "\n				"), "\n			"), "\n		"), "\n	");
}));

Template.__checkName("posts");
Template["posts"] = new Template("Template.posts", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-posts"
  }, "\n		", Blaze.Each(function() {
    return Spacebars.call(view.lookup("posts"));
  }, function() {
    return [ "\n		", Spacebars.include(view.lookupTemplate("post")), "\n		" ];
  }, function() {
    return [ "\n		", Spacebars.include(view.lookupTemplate("noData")), "\n		" ];
  }), "\n	");
}));

Template.__checkName("post");
Template["post"] = new Template("Template.post", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-post"
  }, "\n		", HTML.DIV({
    "class": "panel panel-default"
  }, "\n				", HTML.DIV({
    "class": "panel-heading"
  }, "\n					", HTML.H3({
    "class": "panel-title"
  }, "\n						", Blaze.View("lookup:title", function() {
    return Spacebars.mustache(view.lookup("title"));
  }), "\n						", HTML.SPAN({
    "class": "pull-right"
  }, "\n							", HTML.Raw('<i class="fa fa-thumbs-up"></i>'), " ", Spacebars.With(function() {
    return Spacebars.dataMustache(view.lookup("favoritesByDoc"), view.lookup("_id"));
  }, function() {
    return Blaze.View("lookup:count", function() {
      return Spacebars.mustache(view.lookup("count"));
    });
  }), "\n							", HTML.Raw('<i class="fa fa-comments"></i>'), " ", Blaze.View("lookup:commentCount", function() {
    return Spacebars.mustache(view.lookup("commentCount"), view.lookup("_id"));
  }), "\n						"), "\n					"), "\n				"), "\n				", HTML.DIV({
    "class": "panel-body"
  }, "\n					", HTML.P({
    "class": "text-center"
  }, "\n						", Spacebars.With(function() {
    return Spacebars.dataMustache(view.lookup("getDoc"), view.lookup("picture"), "Attachments");
  }, function() {
    return [ "\n							", HTML.IMG({
      src: function() {
        return Spacebars.mustache(view.lookup("url"));
      },
      "class": "img-thumbnail img-rounded"
    }), "\n						" ];
  }), "\n					"), "\n					", HTML.DIV({
    "class": "media"
  }, "\n						", HTML.A({
    "class": "pull-left",
    href: "#"
  }, "\n							", Blaze._TemplateWith(function() {
    return {
      userId: Spacebars.call(view.lookup("owner")),
      shape: Spacebars.call("circle")
    };
  }, function() {
    return Spacebars.include(view.lookupTemplate("avatar"));
  }), "\n						"), "\n						", HTML.DIV({
    "class": "media-body"
  }, "\n							", HTML.P(Blaze.View("lookup:content", function() {
    return Spacebars.mustache(view.lookup("content"));
  })), "\n							", Blaze.If(function() {
    return Spacebars.dataMustache(view.lookup("$eq"), view.lookup("owner"), Spacebars.dot(view.lookup("User"), "_id"));
  }, function() {
    return [ "\n							", HTML.SMALL("\n								", Blaze._TemplateWith(function() {
      return {
        title: Spacebars.call("Update post"),
        omitFields: Spacebars.call("createdAt,updatedAt,owner"),
        "class": Spacebars.call("text-primary"),
        collection: Spacebars.call("Posts"),
        operation: Spacebars.call("update"),
        doc: Spacebars.call(view.lookup("_id"))
      };
    }, function() {
      return Spacebars.include(view.lookupTemplate("afModal"), function() {
        return [ "\n									", HTML.I({
          "class": "fa fa-pencil",
          buttoncontent: "Edit post"
        }), " ", Blaze.View("lookup:_", function() {
          return Spacebars.mustache(view.lookup("_"), "edit_post");
        }), "\n								" ];
      });
    }), "\n								", Blaze._TemplateWith(function() {
      return {
        title: Spacebars.call("Delete post"),
        "class": Spacebars.call("text-danger"),
        collection: Spacebars.call("Posts"),
        operation: Spacebars.call("remove"),
        doc: Spacebars.call(view.lookup("_id"))
      };
    }, function() {
      return Spacebars.include(view.lookupTemplate("afModal"), function() {
        return [ "\n									", HTML.I({
          "class": "fa fa-times",
          buttoncontent: "Edit post"
        }), " ", Blaze.View("lookup:_", function() {
          return Spacebars.mustache(view.lookup("_"), "delete");
        }), "\n								" ];
      });
    }), "\n							"), "\n							" ];
  }), "\n						"), "\n					"), "\n				"), "\n				", HTML.DIV({
    "class": "panel-footer"
  }, "\n					", Blaze._TemplateWith(function() {
    return {
      _id: Spacebars.call(view.lookup("_id"))
    };
  }, function() {
    return Spacebars.include(view.lookupTemplate("favoriteButton"));
  }), "\n					", Spacebars.include(view.lookupTemplate("commentFormToggle")), "\n					", Spacebars.include(view.lookupTemplate("comments")), "\n				"), "\n		"), "\n	");
}));

Template.__checkName("favoritesSidebar");
Template["favoritesSidebar"] = new Template("Template.favoritesSidebar", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-favorites-sidebar"
  }, "\n		", Blaze.If(function() {
    return Spacebars.call(Spacebars.dot(view.lookup("myFavorites"), "count"));
  }, function() {
    return [ "\n		", HTML.DIV({
      "class": "panel panel-default"
    }, "\n			", HTML.DIV({
      "class": "panel-heading"
    }, "\n					", HTML.H3({
      "class": "panel-title"
    }, Blaze.View("lookup:_", function() {
      return Spacebars.mustache(view.lookup("_"), "favorites");
    })), "\n			"), "\n			", HTML.DIV({
      "class": "panel-body"
    }, "\n				", HTML.UL("\n				", Blaze.Each(function() {
      return Spacebars.call(view.lookup("myFavorites"));
    }, function() {
      return [ "\n					", HTML.LI("\n						", HTML.A(Blaze.View("lookup:_id", function() {
        return Spacebars.mustache(view.lookup("_id"));
      })), "\n					"), "\n				" ];
    }), "\n				"), "\n			"), "\n		"), "\n		" ];
  }), "\n	");
}));

}).call(this);
