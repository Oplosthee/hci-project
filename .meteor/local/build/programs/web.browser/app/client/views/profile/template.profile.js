(function(){
Template.__checkName("profile");
Template["profile"] = new Template("Template.profile", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-profile"
  }, "\n		", HTML.DIV({
    "class": "container"
  }, "\n			", HTML.DIV({
    "class": "row"
  }, "\n				", Blaze._TemplateWith(function() {
    return {
      id: Spacebars.call("updatePicture"),
      collection: Spacebars.call(view.lookup("Users")),
      doc: Spacebars.call(view.lookup("User")),
      type: Spacebars.call("update")
    };
  }, function() {
    return Spacebars.include(view.lookupTemplate("autoForm"), function() {
      return [ "\n				", HTML.DIV({
        "class": "col-md-4"
      }, "\n						", Blaze._TemplateWith(function() {
        return {
          name: Spacebars.call("profile.picture")
        };
      }, function() {
        return Spacebars.include(view.lookupTemplate("afQuickField"));
      }), "\n				"), "\n				" ];
    });
  }), "\n				", Blaze._TemplateWith(function() {
    return {
      id: Spacebars.call("updateProfile"),
      collection: Spacebars.call(view.lookup("Users")),
      doc: Spacebars.call(view.lookup("User")),
      type: Spacebars.call("update")
    };
  }, function() {
    return Spacebars.include(view.lookupTemplate("autoForm"), function() {
      return [ "\n				", HTML.DIV({
        "class": "col-md-8"
      }, "\n					", Blaze._TemplateWith(function() {
        return {
          name: Spacebars.call("profile"),
          omitFields: Spacebars.call("profile.picture")
        };
      }, function() {
        return Spacebars.include(view.lookupTemplate("afQuickField"));
      }), "\n					", HTML.BUTTON({
        type: "submit",
        "class": "btn btn-primary"
      }, Blaze.View("lookup:_", function() {
        return Spacebars.mustache(view.lookup("_"), "update");
      })), "\n				"), "\n				" ];
    });
  }), "\n			"), "\n		"), "\n	");
}));

}).call(this);
