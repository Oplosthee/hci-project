(function(){
Template.__checkName("home");
Template["home"] = new Template("Template.home", (function() {
  var view = this;
  return [ HTML.NAV({
    "class": "navbar navbar-default navbar-fixed-top",
    role: "navigation"
  }, "\n        ", HTML.DIV({
    "class": "container"
  }, "\n            ", HTML.DIV({
    "class": "row"
  }, "\n                ", HTML.A({
    "class": "navbar-brand link",
    href: function() {
      return Spacebars.mustache(view.lookup("pathFor"), "home");
    }
  }, Blaze.View("lookup:Config.name", function() {
    return Spacebars.mustache(Spacebars.dot(view.lookup("Config"), "name"));
  })), "\n                ", HTML.DIV({
    id: "main-nav",
    "class": "navbar-collapse collapse"
  }, "\n                    ", HTML.UL({
    "class": "nav navbar-nav navbar-right"
  }, "\n                        ", Blaze.If(function() {
    return Spacebars.call(view.lookup("User"));
  }, function() {
    return [ "\n                        ", Spacebars.include(view.lookupTemplate("userDropdown")), "\n                        " ];
  }, function() {
    return [ "\n                        ", HTML.LI("\n                            ", HTML.A({
      href: function() {
        return Spacebars.mustache(view.lookup("pathFor"), "atSignIn");
      },
      "class": "link"
    }, Blaze.View("lookup:_", function() {
      return Spacebars.mustache(view.lookup("_"), "login2");
    })), "\n                        "), "\n                        ", HTML.LI("\n                            ", HTML.A({
      href: function() {
        return Spacebars.mustache(view.lookup("pathFor"), "atSignUp");
      },
      "class": "link btn btn-hollow"
    }, Blaze.View("lookup:_", function() {
      return Spacebars.mustache(view.lookup("_"), "signup");
    })), "\n                        "), "\n                        " ];
  }), "\n                    "), "\n                "), "\n            "), "\n        "), "\n    "), "\n    ", HTML.SECTION({
    id: "intro",
    "class": "colored text-center"
  }, "\n        ", HTML.DIV({
    "class": "container"
  }, "\n            ", HTML.DIV({
    "class": "intro-well wow animated bounceIn"
  }, "\n                ", HTML.DIV({
    "class": "row"
  }, "\n                    ", HTML.DIV({
    "class": "col-lg-12 col-sm-12"
  }, "\n                        ", HTML.H1(Blaze.View("lookup:Config.title", function() {
    return Spacebars.mustache(Spacebars.dot(view.lookup("Config"), "title"));
  })), "\n                        ", HTML.H2({
    "class": "hidden-xs"
  }, Blaze.View("lookup:Config.subtitle", function() {
    return Spacebars.mustache(Spacebars.dot(view.lookup("Config"), "subtitle"));
  })), "\n                        ", Blaze.If(function() {
    return Spacebars.call(view.lookup("User"));
  }, function() {
    return [ "\n                        ", HTML.A({
      href: function() {
        return Spacebars.mustache(view.lookup("pathFor"), "dashboard");
      },
      "class": "btn btn-success"
    }, HTML.I({
      "class": "fa fa-dashboard"
    }), " ", Blaze.View("lookup:_", function() {
      return Spacebars.mustache(view.lookup("_"), "dashboard");
    })), "\n                        " ];
  }, function() {
    return [ "\n                        ", HTML.A({
      "class": "btn btn-hollow",
      href: function() {
        return Spacebars.mustache(view.lookup("pathFor"), "atSignUp");
      }
    }, Blaze.View("lookup:_", function() {
      return Spacebars.mustache(view.lookup("_"), "getstarted");
    })), "\n                        " ];
  }), "\n                    "), "\n                "), "\n            "), "\n        "), "\n    "), "\n    ", HTML.SECTION({
    id: "how-it-works",
    "class": "text-center"
  }, "\n        ", HTML.DIV({
    "class": "container wow fadeIn animated"
  }, "\n            ", HTML.DIV({
    "class": "row col-md-12"
  }, "\n                ", HTML.H3(Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "how");
  })), "\n                ", HTML.H4(Blaze.View("lookup:Config.name", function() {
    return Spacebars.mustache(Spacebars.dot(view.lookup("Config"), "name"));
  }), " - ", Blaze.View("lookup:Config.subtitle", function() {
    return Spacebars.mustache(Spacebars.dot(view.lookup("Config"), "subtitle"));
  })), "\n            "), "\n        "), "\n        ", HTML.DIV({
    "class": "container"
  }, "\n            ", HTML.DIV({
    "class": "row",
    id: "icon-row"
  }, "\n                ", HTML.DIV({
    "class": "col-md-4 wow fadeInUp animated",
    "data-wow-delay": "0.4s"
  }, "\n                    ", HTML.Raw('<div class="iconbox">\n                        <i class="fa fa-plane fa-3x"></i>\n                    </div>'), "\n                    ", HTML.H2({
    "class": "icntitle"
  }, "1. ", Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "step_1");
  })), "\n                    ", HTML.P({
    "class": "icnp"
  }, Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "step_1_text");
  })), "\n                "), "\n                ", HTML.DIV({
    "class": "col-md-4 wow fadeInUp animated",
    "data-wow-delay": "0.8s"
  }, "\n                    ", HTML.Raw('<div class="iconbox">\n                        <i class="fa fa-bolt fa-3x"></i>\n                    </div>'), "\n                    ", HTML.H2({
    "class": "icntitle"
  }, "2. ", Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "step_2");
  })), "\n                    ", HTML.P({
    "class": "icnp"
  }, Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "step_2_text");
  })), "\n                "), "\n                ", HTML.DIV({
    "class": "col-md-4 wow fadeInUp animated",
    "data-wow-delay": "1.2s"
  }, "\n                    ", HTML.Raw('<div class="iconbox">\n                        <i class="fa fa-check fa-3x"></i>\n                    </div>'), "\n                    ", HTML.H2({
    "class": "icntitle"
  }, "3. ", Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "step_3");
  })), "\n                    ", HTML.P({
    "class": "icnp"
  }, Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "step_3_text");
  })), "\n                "), "\n            "), "\n        "), "\n    "), "\n    ", HTML.SECTION({
    id: "ipad",
    "class": "colored text-center"
  }, "\n        ", HTML.DIV({
    "class": "container"
  }, "\n            ", HTML.DIV({
    "class": "row"
  }, "\n                ", HTML.DIV({
    "class": "col-sm-6"
  }, "\n                    ", HTML.H3(Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "section_ipad_header");
  })), "\n                    ", HTML.P(Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "section_ipad_text");
  })), "\n                    ", HTML.P({
    "class": "text-center"
  }, "\n                    ", HTML.A({
    href: function() {
      return Spacebars.mustache(Spacebars.dot(view.lookup("Config"), "about"));
    },
    target: "_blank",
    "class": "btn btn-success"
  }, "\n                    ", Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "section_ipad_btn");
  }), "\n                    "), "\n                    "), "\n                "), "\n                ", HTML.Raw('<div class="col-sm-6">\n                    <img id="ipad" src="/img/ipad.png" alt="" class="wow animated fadeInRight" data-wow-delay="1s">\n                </div>'), "\n            "), "\n        "), "\n    "), "\n    ", HTML.SECTION({
    id: "final"
  }, "\n        ", HTML.DIV({
    "class": "container"
  }, "\n            ", HTML.DIV({
    "class": "row"
  }, "\n                ", HTML.DIV({
    "class": "col-md-4 text-center wow fadeInDown animated",
    "data-wow-delay": "0.8s"
  }, "\n                    ", HTML.H3("\n                    ", Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "final_text");
  }), "\n                    "), "\n                    ", HTML.DIV({
    "class": "text-center"
  }, "\n                        ", HTML.A({
    "class": "btn btn-primary",
    href: function() {
      return Spacebars.mustache(view.lookup("pathFor"), "atSignUp");
    }
  }, Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "final_cta");
  })), "\n                    "), "\n                "), "\n                ", HTML.Raw('<div class="col-md-8  wow fadeInUp animated">\n                    <img src="img/screenshot.png" class="img-responsive wow" data-wow-delay="0.6s" alt="picture">\n                </div>'), "\n            "), "\n        "), "\n    ") ];
}));

}).call(this);
