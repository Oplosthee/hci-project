(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// client/views/account/account.coffee.js                              //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
AutoForm.hooks({                                                       // 1
  updatePassword: {                                                    // 1
    onSubmit: function(insertDoc, updateDoc, currentDoc) {             // 2
      if (insertDoc["new"] !== insertDoc.confirm) {                    // 3
        sAlert.error('Wachtwoorden zijn niet hetzelfde!');             // 4
        return false;                                                  // 5
      }                                                                //
      Accounts.changePassword(insertDoc.old, insertDoc["new"], function(e) {
        $('.btn-primary').attr('disabled', null);                      // 7
        if (e) {                                                       // 8
          return sAlert.error(e.message);                              //
        } else {                                                       //
          return sAlert.success('Veranderingen opgeslagen!');          //
        }                                                              //
      });                                                              //
      return false;                                                    //
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
Template.account.events({                                              // 1
  'click .js-delete-account': function() {                             // 16
    return Meteor.call('deleteAccount', Meteor.userId());              //
  }                                                                    //
});                                                                    //
                                                                       //
Template.setUserName.helpers({                                         // 1
  user: function() {                                                   // 20
    return Meteor.user();                                              //
  }                                                                    //
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
