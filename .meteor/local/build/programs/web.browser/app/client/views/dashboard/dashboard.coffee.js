(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// client/views/dashboard/dashboard.coffee.js                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.question.events({                                             // 1
  'submit form': function(event) {                                     // 1
    var answer, solution;                                              // 2
    event.preventDefault();                                            // 2
    answer = event.target.answer.value;                                // 2
    solution = event.target.solution.value;                            // 2
    if (answer === solution) {                                         // 7
      return sAlert.success('Dit antwoord is juist!');                 //
    } else {                                                           //
      return sAlert.error('Dit antwoord is niet juist.');              //
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
Template.dashboard.rendered = function() {                             // 1
  return document.body.style.backgroundColor = Meteor.user().profile.lastName;
};                                                                     // 12
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
