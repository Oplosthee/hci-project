(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// client/views/home/home.coffee.js                                    //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.home.rendered = function() {                                  // 1
  var options, w, winHeight, winWidth;                                 // 2
  w = new WOW().init();                                                // 2
  winWidth = $(window).width();                                        // 2
  winHeight = $(window).height();                                      // 2
  $("#intro").css({                                                    // 2
    width: winWidth,                                                   // 13
    height: winHeight                                                  // 13
  });                                                                  //
  $(window).resize(function() {                                        // 2
    return $("#intro").css({                                           //
      width: $(window).width(),                                        // 18
      height: $(window).height()                                       // 18
    });                                                                //
  });                                                                  //
  if (!Utils.isMobile) {                                               // 22
    options = {                                                        // 23
      forceHeight: false,                                              // 24
      smoothScrolling: false                                           // 24
    };                                                                 //
    return skrollr.init(options).refresh();                            //
  }                                                                    //
};                                                                     // 1
                                                                       //
Template.home.destroyed = function() {                                 // 1
  return $('body').attr('style', '');                                  //
};                                                                     // 29
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
