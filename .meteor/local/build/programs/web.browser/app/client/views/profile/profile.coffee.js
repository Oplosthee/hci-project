(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// client/views/profile/profile.coffee.js                              //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
AutoForm.hooks({                                                       // 1
  updateProfile: {                                                     // 2
    onSuccess: function(operation, result, template) {                 // 3
      return sAlert.success('Profile updated');                        //
    },                                                                 //
    onError: function(operation, error, template) {                    // 3
      return sAlert.error(error);                                      //
    }                                                                  //
  },                                                                   //
  updatePicture: {                                                     // 2
    onSuccess: function(operation, result, template) {                 // 9
      return sAlert.success('Picture Updated');                        //
    },                                                                 //
    onError: function(operation, error, template) {                    // 9
      return sAlert.error(error);                                      //
    }                                                                  //
  }                                                                    //
});                                                                    //
                                                                       //
Template.profile.events({                                              // 1
  'change form#updatePicture input': function(e, t) {                  // 17
    return Meteor.setTimeout(function() {                              //
      return $('form#updatePicture').submit();                         //
    }, 10);                                                            //
  }                                                                    //
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
