(function(){
Template.__checkName("navbar");
Template["navbar"] = new Template("Template.navbar", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-navbar"
  }, "\n    ", HTML.DIV({
    "class": "navbar navbar-inverse navbar-static-top"
  }, "\n      ", HTML.DIV({
    "class": "container close-navbar"
  }, "\n        ", HTML.DIV({
    "class": "navbar-header"
  }, "\n          ", HTML.Raw('<input type="checkbox" id="sliding-menu-controller" class="hide">'), "\n          ", HTML.Raw('<label type="button" class="navbar-toggle" for="sliding-menu-controller">\n            <span class="icon"></span>\n          </label>'), "\n          ", HTML.DIV({
    "class": "sliding-menu visible-xs"
  }, "\n            ", HTML.DIV({
    "class": "panel panel-default"
  }, "\n              ", Blaze.If(function() {
    return Spacebars.call(view.lookup("User"));
  }, function() {
    return [ "\n                ", HTML.DIV({
      "class": "panel-heading"
    }, Blaze.View("lookup:niceName", function() {
      return Spacebars.mustache(view.lookup("niceName"), Spacebars.dot(view.lookup("User"), "_id"));
    })), "\n                ", HTML.DIV({
      "class": "list-group"
    }, "\n                  ", HTML.Comment('<a class="list-group-item" href="{{pathFor \'profile\'}}">{{_ "profile"}}</a>'), "\n                  ", Blaze.If(function() {
      return Spacebars.dataMustache(view.lookup("isInRole"), "admin");
    }, function() {
      return [ "\n                    ", HTML.A({
        "class": "list-group-item",
        href: function() {
          return Spacebars.mustache(view.lookup("pathFor"), "adminDashboard");
        }
      }, Blaze.View("lookup:_", function() {
        return Spacebars.mustache(view.lookup("_"), "admin");
      })), "\n                  " ];
    }), "\n                  ", HTML.A({
      "class": "list-group-item",
      href: function() {
        return Spacebars.mustache(view.lookup("pathFor"), "account");
      }
    }, Blaze.View("lookup:_", function() {
      return Spacebars.mustache(view.lookup("_"), "settings");
    })), "\n                  ", HTML.Comment('<a class="list-group-item" href="{{pathFor \'notifications\'}}">\n                    {{#if notificationCount}}\n                      <span class="badge bg-success">{{notificationCount}}</span>\n                    {{/if}}\n                    {{_ "notifications"}}\n                  </a>'), "\n                  ", HTML.A({
      "class": "list-group-item",
      href: "/sign-out"
    }, Blaze.View("lookup:_", function() {
      return Spacebars.mustache(view.lookup("_"), "signout");
    })), "\n                "), "\n              " ];
  }), "\n            "), "\n          "), "\n          ", HTML.A({
    "class": "navbar-brand",
    href: function() {
      return Blaze.If(function() {
        return Spacebars.call(view.lookup("User"));
      }, function() {
        return Blaze.View("lookup:Config.dashboardRoute", function() {
          return Spacebars.mustache(Spacebars.dot(view.lookup("Config"), "dashboardRoute"));
        });
      }, function() {
        return Blaze.View("lookup:Config.homeRoute", function() {
          return Spacebars.mustache(Spacebars.dot(view.lookup("Config"), "homeRoute"));
        });
      });
    }
  }, Blaze.View("lookup:Config.logo", function() {
    return Spacebars.makeRaw(Spacebars.mustache(Spacebars.dot(view.lookup("Config"), "logo")));
  })), "\n        "), "\n        ", HTML.DIV({
    "class": "navbar-collapse collapse",
    id: "bs-example-navbar-collapse-1"
  }, "\n          ", HTML.UL({
    "class": "nav navbar-nav navbar-right"
  }, "\n            ", Blaze.If(function() {
    return Spacebars.call(view.lookup("User"));
  }, function() {
    return [ "\n              ", HTML.Comment("{{> notificationsDropdown}}"), "\n              ", Blaze.If(function() {
      return Spacebars.dataMustache(view.lookup("isInRole"), "admin");
    }, function() {
      return [ "\n                ", HTML.LI(HTML.A({
        href: function() {
          return Spacebars.mustache(view.lookup("pathFor"), "adminDashboard");
        }
      }, Blaze.View("lookup:_", function() {
        return Spacebars.mustache(view.lookup("_"), "admin");
      }))), "\n              " ];
    }), "\n              ", Spacebars.include(view.lookupTemplate("userDropdown")), "\n              ", HTML.LI({
      "class": "visible-xs"
    }, "\n                ", HTML.A({
      href: function() {
        return Spacebars.mustache(view.lookup("pathFor"), "profile");
      },
      "class": "close-navbar"
    }, Blaze.View("lookup:_", function() {
      return Spacebars.mustache(view.lookup("_"), "edit_profile");
    })), "\n              "), "\n              ", HTML.LI({
      "class": "visible-xs"
    }, "\n                ", HTML.A({
      "class": "close-navbar",
      href: "/sign-out"
    }, Blaze.View("lookup:_", function() {
      return Spacebars.mustache(view.lookup("_"), "signout");
    })), "\n              "), "\n            " ];
  }, function() {
    return [ "\n              ", HTML.LI("\n                ", HTML.Comment('<a class="close-navbar" href="/sign-up">{{_ "signup"}}</a>'), "\n              "), "\n              ", HTML.LI("\n                ", HTML.A({
      "class": "close-navbar",
      href: "/sign-in"
    }, Blaze.View("lookup:_", function() {
      return Spacebars.mustache(view.lookup("_"), "login");
    })), "\n              "), "\n            " ];
  }), "\n          "), "\n        "), "\n      "), "\n    "), "\n  ");
}));

}).call(this);
