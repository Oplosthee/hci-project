(function(){
Template.__checkName("adminUserCell");
Template["adminUserCell"] = new Template("Template.adminUserCell", (function() {
  var view = this;
  return HTML.A({
    href: function() {
      return Spacebars.mustache(view.lookup("pathFor"), "adminDashboardUsersEdit", Spacebars.kw({
        _id: Spacebars.dot(view.lookup("doc"), "owner")
      }));
    },
    "class": "label label-default"
  }, "\n    ", Blaze.View("lookup:value", function() {
    return Spacebars.mustache(view.lookup("value"));
  }), "\n  ");
}));

Template.__checkName("adminPostCell");
Template["adminPostCell"] = new Template("Template.adminPostCell", (function() {
  var view = this;
  return HTML.A({
    href: function() {
      return Spacebars.mustache(view.lookup("pathFor"), "adminDashboardPostsEdit", Spacebars.kw({
        _id: Spacebars.dot(view.lookup("doc"), "doc")
      }));
    },
    "class": "label label-default"
  }, "\n    ", Blaze.View("lookup:value", function() {
    return Spacebars.mustache(view.lookup("value"));
  }), "\n  ");
}));

}).call(this);
