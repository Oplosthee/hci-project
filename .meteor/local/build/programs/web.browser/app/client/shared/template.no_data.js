(function(){
Template.__checkName("noData");
Template["noData"] = new Template("Template.noData", (function() {
  var view = this;
  return HTML.DIV({
    "class": "template-no-data"
  }, "\n    ", HTML.H3({
    "class": "text-center"
  }, "\n      ", Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "not_found");
  }), "\n    "), "\n  ");
}));

}).call(this);
