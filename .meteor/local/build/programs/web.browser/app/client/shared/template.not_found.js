(function(){
Template.__checkName("notFound");
Template["notFound"] = new Template("Template.notFound", (function() {
  var view = this;
  return HTML.H1(Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "not_found");
  }));
}));

}).call(this);
