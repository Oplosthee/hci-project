(function(){
Template.__checkName("footer");
Template["footer"] = new Template("Template.footer", (function() {
  var view = this;
  return HTML.FOOTER("\n		", HTML.DIV({
    "class": "container"
  }, "\n			", HTML.DIV({
    "class": "row"
  }, "\n				", HTML.DIV({
    "class": "col-md-12 text-center"
  }, "\n					", Blaze.View("lookup:Config.footer", function() {
    return Spacebars.makeRaw(Spacebars.mustache(Spacebars.dot(view.lookup("Config"), "footer")));
  }), "\n				"), "\n			"), "\n		"), "\n	");
}));

}).call(this);
