(function(){
Template.__checkName("userDropdown");
Template["userDropdown"] = new Template("Template.userDropdown", (function() {
  var view = this;
  return HTML.LI({
    "class": "dropdown hidden-xs"
  }, "\n    ", HTML.A({
    href: "#",
    "class": "dropdown-toggle link",
    "data-toggle": "dropdown"
  }, Blaze.View("lookup:niceName", function() {
    return Spacebars.mustache(view.lookup("niceName"), Spacebars.dot(view.lookup("User"), "_id"));
  }), " ", HTML.Raw('<i class="fa fa-chevron-down i-xs"></i>')), "\n    ", HTML.UL({
    "class": "dropdown-menu"
  }, "\n      ", HTML.Raw('<!--<li><a href="{{pathFor \'profile\'}}">{{_ "profile"}}</a></li>-->'), "\n      ", HTML.LI(HTML.A({
    href: function() {
      return Spacebars.mustache(view.lookup("pathFor"), "account");
    }
  }, Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "settings");
  }))), "\n      ", HTML.Raw('<li class="divider"></li>'), "\n      ", HTML.LI(HTML.A({
    href: "/sign-out"
  }, Blaze.View("lookup:_", function() {
    return Spacebars.mustache(view.lookup("_"), "signout");
  }))), "\n    "), "\n  ");
}));

}).call(this);
