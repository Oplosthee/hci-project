(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// client/shared/helpers.coffee.js                                     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.registerHelper('Config', function() {                         // 1
  return Config;                                                       //
});                                                                    // 1
                                                                       //
Template.registerHelper('Schemas', function() {                        // 1
  return Schemas;                                                      //
});                                                                    // 4
                                                                       //
Template.registerHelper('Utils', function() {                          // 1
  return Utils;                                                        //
});                                                                    // 7
                                                                       //
Template.registerHelper('socialMedia', function() {                    // 1
  return _.map(Config.socialMedia, function(obj) {                     //
    return obj;                                                        //
  });                                                                  //
});                                                                    // 10
                                                                       //
Template.registerHelper('currentRoute', function() {                   // 1
  if (Router && Router.current && Router.current()) {                  // 15
    return Router.current();                                           //
  }                                                                    //
});                                                                    // 14
                                                                       //
Template.registerHelper('isRouteReady', function() {                   // 1
  return Router && Router.current && Router.current() && Router.current()._waitlist._notReadyCount === 0;
});                                                                    // 18
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
