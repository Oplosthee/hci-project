(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// client/shared/navbar.coffee.js                                      //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
__coffeescriptShare = typeof __coffeescriptShare === 'object' ? __coffeescriptShare : {}; var share = __coffeescriptShare;
Template.navbar.events({                                               // 1
  'change #sliding-menu-controller': function(e) {                     // 2
    return $('body')[e.currentTarget.checked ? 'addClass' : 'removeClass']('no-overflow');
  },                                                                   //
  'click .sliding-menu a': function() {                                // 2
    return $('#sliding-menu-controller').prop('checked', false);       //
  }                                                                    //
});                                                                    //
                                                                       //
/////////////////////////////////////////////////////////////////////////

}).call(this);
