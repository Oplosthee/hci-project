@Questions = new Meteor.Collection('questions');

Schemas.Questions = new SimpleSchema
    question:
        type:String
        max: 60
        
    answer:
        type:String
        max: 60

    createdAt:
        type: Date
        autoValue: ->
            if this.isInsert
                new Date()

    updatedAt:
        type:Date
        optional:true
        autoValue: ->
            if this.isUpdate
                new Date()

Questions.attachSchema(Schemas.Questions)