@Schemas = {}

# Non collection Schemas
Schemas.updatePassword = new SimpleSchema
  old:
    type: String
    label: "Huidig wachtwoord"
    max: 50

  new:
    type: String
    min: 6
    max: 20
    label: "Nieuw wachtwoord"

  confirm:
    type: String
    min: 6
    max: 20
    label: "Bevestig nieuw wachtwoord"