@Groups = new Meteor.Collection('groups');

Schemas.Groups = new SimpleSchema
    name:
        type:String
        max: 60

Groups.attachSchema(Schemas.Groups)