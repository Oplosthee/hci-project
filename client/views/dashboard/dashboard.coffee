Template.question.events 'submit form': (event) ->
    event.preventDefault()

    answer = event.target.answer.value;
    solution = event.target.solution.value;

    if answer == solution
        sAlert.success 'Dit antwoord is juist!'
    else
        sAlert.error 'Dit antwoord is niet juist.'

Template.dashboard.rendered = () ->
    document.body.style.backgroundColor = Meteor.user().profile.lastName